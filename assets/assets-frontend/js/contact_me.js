$(function() {

    $("#contactForm [required]").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
			console.log('error submit');
        	console.log(mail_sendsave_url);
			//console.log(success_message_sendmail);
		},
        submitSuccess: function($form, event) {
			console.log('success submit mail_sendsave_url');
            // Prevent spam click and default submit behaviour
            $("#submit-contact-btn").attr("disabled", true);
            event.preventDefault();
   
            // get values from FORM
            var surname = $("input#surname").val();
            var name = $("input#name").val();
            var city = $("input#city").val();
            var email = $("input#email").val();
            var phone = $("input#phone").val();
            var message = $("textarea#message").val();
		//	var studio = $("select#studio").val();
			var provenienza = $("input#provenienza").val();
        //    var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
        //    if (firstName.indexOf(' ') >= 0) {
         //       firstName = name.split(' ').slice(0, -1).join(' ');
          //  }
		  
			console.log('invio dati cognome: ' + surname + ' citta: ' + city);
            $.ajax({
                url: mail_sendsave_url,
                type: "POST",
                data: {
                    name: name,
                    surname: surname,
                    city: city,
                    phone: phone,
                    email: email,
                    message: message,
					provenienza: provenienza
                },
                cache: false,
                success: function(messJson) {
					var json = $.parseJSON(messJson);
                    // Enable button & show success message
                    $("#btnSubmit").attr("disabled", false);
					
					$('#success').html("<div class='alert alert-"+json['type']+"'>");
                    $('#success > .alert-'+json['type']).html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-'+json['type'])
                        .append("<strong>"+json['message']+"</strong>");
                    $('#success > .alert-'+json['type'])
                        .append('</div>');
					
					
                   /* $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>"+success_message_sendmail+"</strong>");
                    $('#success > .alert-success')
                        .append('</div>');*/

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                    $('#success > .alert-danger').append("<strong>"+failure_message_sendmail);
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });
	
	$("#newsletterForm [required]").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
			console.log('error submit');
        	console.log(mail_sendsave_url);
			//console.log(success_message_sendmail);
		},
        submitSuccess: function($form, event) {
			console.log('success submit');
            // Prevent spam click and default submit behaviour
            $("#submit-news-btn").attr("disabled", true);
            event.preventDefault();
   
            // get values from FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
 	  
            $.ajax({
                url: mail_newsletter_url,
                type: "POST",
                data: {
                    name: name,
                    email: email
                },
                cache: false,
                success: function(messJson) {
					//console.log(messJson); 
                    var json = $.parseJSON(messJson);
					//console.log(json); 
					// Enable button & show success message
                    $("#submit-news-btn").attr("disabled", false);
                    $('#success').html("<div class='alert alert-"+json['type']+"'>");
                    $('#success > .alert-'+json['type']).html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-'+json['type'])
                        .append("<strong>"+json['message']+"</strong>");
                    $('#success > .alert-'+json['type'])
                        .append('</div>');
							
                    //clear all fields
                    $('#newsletterForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                    $('#success > .alert-danger').append("<strong>"+failure_message_sendmail);
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#newsletterForm').trigger("reset");
					$("#submit-news-btn").attr("disabled", false);
                },
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });
	
    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});

// When clicking on Full hide fail/success boxes
$('#name').focus(function() {
    $('#success').html('');
});
