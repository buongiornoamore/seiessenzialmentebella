/*VALIDATION*/
function validateCheckbox(id){
	if($("#" + id).is(':checked')){
		$("#"+id).closest("p").removeClass('has-error-div');
		return true;
	} else {
		$("#"+id).closest("p").addClass('has-error-div');
		return false;
	}
}
	

function validateEmail(id){
//	var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+@[a-zA-Z0-9]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if(filter.test($("#" + id).val())){
		//$("#" + id).removeClass('has-error');
		$("#" + id).removeClass('has-error');
		return true;
	} else {
		$("#" + id).addClass('has-error');
		return false;
	}
}			

function validateSelectText(id, defval){
	if($("#" + id).val() != defval){
		$("#"+id).removeClass('has-error');
		return true;
	} else {
		$("#"+id).addClass('has-error');
		return false;
	}
}
function validateText(id){	
	if($("#" + id).val() != ''){
		$("#"+id).removeClass('has-error');
		return true;
	}
	else{
		$("#"+id).addClass('has-error');
		return false;
	}
}
function validateNumeric(id, len, required){
	var a = $("#" + id).val();
	var filter = /^[0-9]+$/;
	if(required) {
		if(filter.test(a) && a.length >= len){
			$("#"+id).removeClass('has-error');
			return true;
		}else{
			$("#"+id).addClass('has-error');
			return false;
		}
	} else {
		if(a != '') {
			if(filter.test(a) && a.length >= len){
				$("#"+id).removeClass('has-error');
				return true;
			}else{
				$("#"+id).addClass('has-error');
				return false;
			}
		} else {
			$("#"+id).removeClass('has-error');
			return true;
		}
	}
}
function validateReCaptcha(id){	
	if($("#" + id).val() != ''){
		console.log('validateReCaptcha ' + $("#" + id).val());
		$("#span-captcha").html('');
		$("#span-captcha").hide();
		return true;
	}
	else{
		$("#span-captcha").html('Errore inserisci il captcha');
		$("#span-captcha").show();
		return false;
	}
}