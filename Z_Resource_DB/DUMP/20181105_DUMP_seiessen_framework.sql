-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Nov 05, 2018 alle 10:29
-- Versione del server: 10.1.33-MariaDB
-- Versione PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seiessen_framework`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `benefici`
--

CREATE TABLE `benefici` (
  `id_benefici` int(11) NOT NULL,
  `titolo_benefici` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `benefici`
--

INSERT INTO `benefici` (`id_benefici`, `titolo_benefici`) VALUES
(1, 'Una valutazione mirata sull’aspetto fisico che senti in disarmonia'),
(3, 'Raggiungere la migliore versione di te dal punto di vista fisico'),
(4, 'Un volto più fresco e riposato'),
(5, 'Un risultato naturale, senza la percezione di aver fatto un trattamento di medicina estetica'),
(6, 'Equilibrio della tua immagine corporea'),
(7, 'Un piano d’azione personalizzato'),
(8, 'Migliora l’aspetto cutaneo'),
(10, 'Idrata'),
(11, 'Contrasta i processi d’invecchiamento'),
(12, 'Pelle più fresca'),
(13, 'Creas consciencia de lo que viniste a hacer, de tu misión de vida'),
(15, 'Reconoces tus sombras y te haces consciente de ellas'),
(16, 'reconoces tu luz '),
(17, 'brillas con luz propia'),
(18, 'te haces consciente de cuales son tus dones y de como utilizarlos'),
(19, 'te reencuentras con tus aprendizajes y los pones en práctica'),
(28, 'Dona freschezza e luminosità.\n'),
(29, 'Effetto tensore.'),
(30, 'Rimodella il viso.\n\n'),
(31, 'Effetto bio-rigenerante creando sostegno in profondità.'),
(32, 'Raggiungere la migliore versione di te esternamente ed internamente.'),
(33, 'Conseguire un risultato naturale, senza la percezione di aver fatto un trattamento di medicina estetica'),
(34, 'Rivalutare (se c’è l’hai) o conoscere (se non c’è l’hai) la cosmesi appropriata per te'),
(35, 'Dare equilibrio alla tua vita.'),
(37, 'Integrare i risultati di armonizzazione del tuo volto con il benessere della tua vita'),
(38, 'Armonizzare il tuo volto con un approccio olistico'),
(39, 'Rinnovare la tua immagine corporea.'),
(40, 'Sentirti a tuo agio dentro la tua pelle.'),
(41, 'Sentirti più leggera'),
(42, 'Migliorare la tua digestione.'),
(43, 'Disintossicare il tuo sistema digestivo.'),
(44, 'Stabilire una relazione di benessere con il cibo.'),
(45, 'Scegliere cosa comprare e non comprare tra i vari prodotti alimentari.'),
(46, 'Identificare gli alimenti che nutrono la tua anima.'),
(48, 'Capire come avere più energia.'),
(49, 'Volto riposato'),
(50, 'Rughe meno evidenti'),
(51, 'Attenua i solchi profondi'),
(52, 'Pelle più liscia'),
(53, 'Ascelle più fresche e asciutte'),
(54, 'Più comodità nel muovere le braccia'),
(55, 'Riduzione dei batteri che creano il cattivo odore. '),
(56, 'Una vita più socievole e meno imbarazzante.'),
(57, 'Azione liftante.'),
(58, 'Migliora l’elasticità.'),
(60, 'Ringiovanimento cutaneo'),
(61, 'L’attività giornaliera si può riprendere da subito.'),
(62, 'Migliora le rughe peribuccali e perioculari. '),
(63, 'Migliora le cicatrici post-acneiche.'),
(64, 'Attenua le macchie superficiali della pelle.'),
(65, 'Migliora le smagliature.'),
(66, 'Migliora la texture della pelle.'),
(67, 'Migliora la tonicità del collo e palpebre superiori.'),
(68, 'Attenuare le rughe (guance, contorno labbra e occhi, collo, decolleté).'),
(69, 'Attenuare i solchi (naso-labiale, marionetta).'),
(70, 'Armonizzare le labbra.'),
(71, 'Ripristinare la zona malare e zigomatica.'),
(72, 'Armonizzare il contorno mandibolare.'),
(73, 'Migliora la texture e la luminosità della cute.'),
(74, 'Migliora le macchie.'),
(75, 'Azione antibatterica ottenendo risultati positivi sull’acne.'),
(76, 'Equilibra la produzione di sebo.'),
(77, 'Riduce l’adiposità localizzata, permettendo di armonizzare il corpo.'),
(78, 'Rassoda la pelle.'),
(79, 'Riduce la cellulite'),
(80, 'Migliora la ritenzione idrica.'),
(81, 'Rinforza il cuoio capelluto nei trattamenti di caduta di capelli. '),
(83, 'Un naso più omogeneo'),
(84, 'Uniformità della linea nasale, senza “gobba”.'),
(85, 'Miglioramento del profilo.'),
(86, 'Procedura senza intervento chirurgico.'),
(87, 'Ritorno alla vita normale, subito dopo il trattamento.'),
(88, 'Tonicità cutanea'),
(90, 'Riduzione della cellulite.'),
(91, 'Miglioramento delle smagliature.'),
(92, 'Miglioramento sulle rughe superficiali.'),
(93, 'Riduzione dell’adiposità localizzata sui fianchi, addome, cosce.'),
(94, 'Diminuisce la ritenzione dei liquidi.'),
(95, 'Previene la formazione di capillari.'),
(96, 'Migliora le gambe pesanti o gonfie, dando sensazione di leggerezza. '),
(97, 'Aiuta la cicatrizzazione delle ulcere.'),
(98, 'Accorciamento dell’eccesso di pelle sugli occhi.'),
(99, 'Occhi più aperti e armonici.'),
(100, 'Apparenza più fresca.'),
(101, 'Pelle liscia e pulita libera di fibromi o verruche.'),
(102, 'Non è un intervento chirurgico, per cui non invasivo.'),
(103, 'Raggiungere la migliore versione di te internamente ed anche esternamente.'),
(104, ' Lasciar andare il passato.'),
(105, 'Rimuovere le convinzioni limitanti che stanno bloccando la tua energia e felicità.'),
(106, 'Dare equilibrio alla tua vita.'),
(107, 'Conoscere come puoi vivere una vita che ti dia soddisfazione.'),
(108, 'Essere consapevole della tua luce ma anche delle tue ombre e come esse possono aiutarti a vibrare alto.'),
(109, 'Integrare questi risultati nella tua vita.'),
(110, 'Valorizzare la tua bellezza'),
(111, 'L’armonizzazione del tuo volto.'),
(112, 'Il piacere di sentirti bene dentro della tua pelle.'),
(114, 'L\'armonizzazione del tuo volto'),
(115, 'Sarà rallentato il processo d’invecchiamento.\n\n'),
(116, 'Un intestino più efficace nelle sue funzioni.'),
(117, 'Migliorerà la digestione. '),
(118, 'Si ridurrà l’adiposità localizzata più facilmente.'),
(119, 'Renderà più efficaci i trattamenti di medicina estetica.'),
(120, 'Ti sentirai leggera dentro il tuo corpo.'),
(121, 'Avrai un organismo che funziona in modo equilibrato.'),
(122, 'Renderà più efficace i risultati di una alimentazione consapevole.'),
(123, 'Migliorerà il tuo umore.'),
(124, 'Pelle più luminosa e sana.'),
(125, 'Capelli e unghie più forti e sani.'),
(126, 'Sarai libera di scegliere il cosmetico cha fa bene alla tua pelle.'),
(127, 'Non sarai più ingannata dalle pubblicità.'),
(128, 'Potrai combattere in modo efficace l’invecchiamento della pelle.'),
(129, 'Potrai valutare i prodotti che ti applicano nelle spa o che ti vengono consigliati.'),
(130, 'Sarai in grado di scegliere i cosmetici da usare sui tuoi figli.'),
(131, 'Capirai come avere una pelle sana.'),
(132, 'Se hai macchie, potrai trattarle in un modo più idoneo.'),
(133, 'Avrai più energia.'),
(134, 'Migliorerà la tua digestione.'),
(135, 'Potrai identificare gli alimenti che nutrono la tua anima e il tuo corpo.'),
(136, 'Avrai una pelle sana e luminosa.'),
(137, 'Ti sentirai leggera e attiva.'),
(138, 'Ti aiuterà a rallentare e/o prevenire la formazione delle rughe.'),
(139, 'Stabilirai una relazione di benessere con il cibo.'),
(140, 'Sentirai la tua vita in equilibrio.'),
(141, 'Migliorerà la tua memoria.'),
(142, 'Troverai il tuo peso ideale.'),
(143, 'Podrás desintoxicar y desinflamar tu piel y no seguir agrediéndola con sustancias que dañan su equilibro y el bienestar de tu cuerpo.');

-- --------------------------------------------------------

--
-- Struttura della tabella `benefici_traduzioni`
--

CREATE TABLE `benefici_traduzioni` (
  `id_benefici_traduzioni` int(11) NOT NULL,
  `id_benefici` int(11) NOT NULL,
  `testo_benefici` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `benefici_traduzioni`
--

INSERT INTO `benefici_traduzioni` (`id_benefici_traduzioni`, `id_benefici`, `testo_benefici`, `lingua_traduzione_id`) VALUES
(1, 1, 'Una valutazione mirata sull’aspetto fisico che senti in disarmonia.', 1),
(3, 3, 'Raggiungere la migliore versione di te dal punto di vista fisico.', 1),
(4, 4, 'Un volto più fresco e riposato.', 1),
(5, 5, 'Un risultato naturale, senza la percezione di aver fatto un trattamento di medicina estetica.', 1),
(6, 6, 'Equilibrio della tua immagine corporea.', 1),
(7, 7, 'Un piano d’azione personalizzato.', 1),
(8, 8, 'improves the skin\'s appearance', 2),
(10, 10, 'Idrata.', 1),
(11, 11, 'Contrasta i processi d’invecchiamento.', 1),
(12, 12, 'Pelle più fresca.', 1),
(13, 1, 'a targeted assessment of the physical appearance that you feel disagreeable', 2),
(15, 3, 'Obtener la mejor versión de tí esternamente e internamente', 3),
(16, 5, 'Conseguir un resultato natural, sin la percepción de haber hecho un tratamiento de medicina estética.', 3),
(17, 1, 'Una evaluación sobre ese aspecto físico que sientes en desarmonía.', 3),
(18, 4, 'Un rostro fresco y reposado.', 3),
(19, 6, 'Equilibrio de tu imagen corporal.', 3),
(20, 7, 'Un plan de acción personalizado.', 3),
(21, 8, 'Migliora il tuo aspetto cutaneo.', 1),
(22, 8, 'Mejora el aspecto cutáneo.', 3),
(24, 10, 'Hidrata.', 3),
(25, 11, 'Contrarresta el proceso de envejecimiento.', 3),
(27, 12, 'Piel más fresca.', 3),
(28, 13, 'Creas consciencia de lo que viniste a hacer, de tu misión de vida.', 3),
(30, 15, 'Reconoces tus sombras y te haces consciente de ellas.', 3),
(31, 16, 'Reconoces tu luz.', 3),
(32, 17, 'Brillas con luz propia.', 3),
(33, 18, 'Te haces consciente de cuales son tus dones y de como utilizarlos.', 3),
(34, 19, 'Te reencuentras con tus aprendizajes y los pones en práctica.', 3),
(35, 32, 'Raggiungere la migliore versione di te esternamente ed internamente.', 1),
(36, 32, 'Obtener la mejor versión de tí esternamente e internamente.', 3),
(37, 33, 'Conseguire un risultato naturale, senza la percezione di aver fatto un trattamento di medicina estetica.', 1),
(38, 33, 'Conseguir un resultato natural, sin la percepción de haber hecho un tratamiento de medicina estética.', 3),
(39, 34, 'Rivalutare (se c’è l’hai) o conoscere (se non c’è l’hai) la cosmesi appropriata per te.', 1),
(40, 34, 'Reevaluar (si lo tienes) o conocer (si no lo tienes) los cométicos apropiados para tí.', 3),
(41, 35, 'Dare equilibrio alla tua vita.', 1),
(42, 35, 'Conceder equilibrio a tu vida.', 3),
(45, 37, 'Integrare i risultati di armonizzazione del tuo volto con il benessere della tua vita.', 1),
(46, 37, 'Integrar los resultados de armominía de tu rostro con el bienestar de tu vida. ', 3),
(47, 38, 'Armonizzare il tuo volto con un approccio olistico.', 1),
(48, 38, 'Armonizar tu rostro con un enfoque holístico.', 3),
(51, 39, 'Rinnovare la tua immagine corporea.', 1),
(52, 39, 'Renovar tu imagen corporal.', 3),
(53, 40, 'Sentirti a tuo agio dentro la tua pelle.', 1),
(54, 40, 'Sentirte esplendida dentro de tu propia piel.', 3),
(55, 41, 'Sentirti più leggera.', 1),
(56, 41, 'Sentire más ligera.', 3),
(57, 42, 'Migliorare la tua digestione.', 1),
(58, 42, 'Mejorar tu digestión.', 3),
(59, 43, 'Disintossicare il tuo sistema digestivo.', 1),
(60, 43, 'Desintoxicar tu sistema digestivo.', 3),
(61, 44, 'Stabilire una relazione di benessere con il cibo.', 1),
(62, 44, 'Establecer una relación de bienestar con los alimentos.', 3),
(63, 45, 'Scegliere cosa comprare e non comprare tra i vari prodotti alimentari.', 1),
(64, 45, 'Saber escoger que comprar y que no comprar entre los productos alimentares.', 3),
(65, 46, 'Identificare gli alimenti che nutrono la tua anima.', 3),
(66, 46, 'Identificar los alimentos que nutren tu alma.', 3),
(68, 48, 'Capire come avere più energia.', 1),
(69, 48, 'Descubrir como obtener mayor energía.', 3),
(70, 52, 'Pelle più liscia', 1),
(71, 52, 'Piel más lisa.', 3),
(72, 51, 'Attenua i solchi profondi.', 1),
(73, 51, 'Atenúa los sulcos profundos.', 3),
(74, 50, 'Rughe meno evidenti.', 1),
(75, 50, 'Arrugas menos evidentes.', 3),
(76, 49, 'Volto riposato.', 1),
(77, 49, 'Rostro reposado.', 3),
(78, 28, 'Dona freschezza e luminosità.', 1),
(79, 28, 'Dona frescura y luminosidad.', 3),
(80, 29, 'Effetto tensore.', 1),
(81, 29, 'Efecto tensor.', 3),
(82, 30, 'Rimodella il viso.', 1),
(83, 30, 'Remodela el rostro.', 3),
(84, 31, 'Effetto bio-rigenerante creando sostegno in profondità.', 1),
(85, 31, 'Efecto bio-regenerante creando un soporte en profondidad.', 3),
(86, 53, 'Ascelle più fresche e asciutte.', 1),
(87, 53, 'Axilas más frescas y secas.', 3),
(88, 54, 'Più comodità nel muovere le braccia.', 1),
(89, 54, 'Mayor libertad en el movimiento de los brazos.', 3),
(90, 55, 'Riduzione dei batteri che creano il cattivo odore. ', 1),
(91, 55, 'Reducción de bacterias que crean mal olor. ', 3),
(92, 56, 'Una vita più socievole e meno imbarazzante.', 1),
(93, 56, 'Una vida más social y menos embarazosa.', 3),
(94, 57, 'Azione liftante.', 1),
(95, 57, 'Acción liftante.', 3),
(96, 58, 'Migliora l’elasticità.', 1),
(97, 58, 'Mejora la elasticidad.', 3),
(100, 60, 'Ringiovanimento cutaneo.', 1),
(101, 60, 'Rejuvenecimiento  cutáneo.', 3),
(102, 61, 'L’attività giornaliera si può riprendere da subito.', 1),
(103, 61, 'Las actividades del día se pueden continuar inmendiatamente despuès del tratamiento.', 3),
(104, 62, 'Migliora le rughe peribuccali e perioculari. ', 1),
(105, 62, 'Mejora las arrugas alrededor de la boca y ojos. ', 3),
(106, 63, 'Migliora le cicatrici post-acneiche.', 1),
(107, 63, 'Mejora las cicatrices del acnè.', 3),
(108, 64, 'Attenua le macchie superficiali della pelle.', 1),
(109, 64, 'Atenùa las discromias superficiales de la piel.', 3),
(110, 65, 'Migliora le smagliature.', 1),
(111, 65, 'Mejora las estrìas.', 3),
(112, 66, 'Migliora la texture della pelle.', 1),
(113, 66, 'Mejora la textura de la piel.', 3),
(114, 67, 'Migliora la tonicità del collo e palpebre superiori.', 1),
(115, 67, 'Ayuda en la tonificaciòn de cuello y párpados superiores.', 3),
(116, 68, 'Attenuare le rughe (guance, contorno labbra e occhi, collo, decolleté).', 1),
(117, 68, 'Atenúa las arrugas (mejillas, contorno de los labios y ojos, cuello, escote).', 3),
(118, 69, 'Attenuare i solchi (naso-labiale, marionetta).', 1),
(119, 69, 'Atenúa los sulcos (naso-labiales, marioneta).', 3),
(120, 70, 'Armonizzare le labbra.', 1),
(121, 70, 'Armoniza los labios.', 3),
(122, 71, 'Ripristinare la zona malare e zigomatica.', 1),
(123, 71, 'Restablece el área malar y pómulos.', 3),
(124, 72, 'Armonizzare il contorno mandibolare.', 1),
(125, 72, 'Armoniza el contorno mandibular.', 3),
(126, 73, 'Migliora la texture e la luminosità della cute.', 1),
(127, 73, 'Mejora la textura y la luminosidad de la piel.', 3),
(128, 74, 'Migliora le macchie.', 1),
(129, 74, 'Mejora las discromías (manchas).', 3),
(130, 75, 'Azione antibatterica ottenendo risultati positivi sull’acne.', 1),
(131, 75, 'Acción antibacterica proporcionando resultados positivos en el acné.', 3),
(132, 76, 'Equilibra la produzione di sebo.', 1),
(133, 76, 'Equilibra la producción de sebo.', 3),
(134, 77, 'Riduce l’adiposità localizzata, permettendo di armonizzare il corpo.', 1),
(135, 77, 'Reduce la adiposidad localizda, permitiendo la armonización del cuerpo.', 3),
(136, 78, 'Rassoda la pelle.', 1),
(137, 78, 'Tonifica la piel.', 3),
(138, 79, 'Riduce la cellulite.', 1),
(139, 79, 'Reduce la celulitis.', 3),
(140, 80, 'Migliora la ritenzione idrica.', 1),
(141, 80, 'Mejora la retención hídrica.', 3),
(142, 81, 'Rinforza il cuoio capelluto nei trattamenti di caduta di capelli. ', 1),
(143, 81, 'Refuerza el cuero cabelludo en los tratamientos de caída del cabello. ', 3),
(145, 83, 'Un naso più omogeneo.', 1),
(146, 83, 'Una narìz homogénea.', 3),
(147, 84, 'Uniformità della linea nasale, senza “gobba”.', 1),
(148, 84, 'Uniformidad de la linea nasal, sin “protuberancia”.', 3),
(149, 85, 'Miglioramento del profilo.', 1),
(150, 85, 'Mejora el perfil.', 3),
(151, 86, 'Procedura senza intervento chirurgico.', 1),
(152, 86, 'Procedimiento sin intervención quirúrgica.', 3),
(153, 87, 'Ritorno alla vita normale, subito dopo il trattamento.', 1),
(154, 87, 'Vida socialmente activa inmediatamente después del tratamiento.', 3),
(155, 88, 'Tonicità cutanea.', 1),
(156, 88, 'Tonicidad cutánea', 3),
(159, 90, 'Riduzione della cellulite.', 1),
(160, 90, 'Reducción de la celulitis.', 3),
(161, 91, 'Miglioramento delle smagliature.', 1),
(162, 91, 'Mejora las estrias.', 3),
(163, 92, 'Miglioramento sulle rughe superficiali.', 1),
(164, 92, 'Mejora las arrugas superficiales.', 3),
(165, 93, 'Riduzione dell’adiposità localizzata sui fianchi, addome, cosce.', 1),
(166, 93, 'Reducción de la adiposidad localizada.', 3),
(167, 94, 'Diminuisce la ritenzione dei liquidi.', 1),
(168, 94, 'Diminuye la retención de líquidos.', 3),
(169, 95, 'Previene la formazione di capillari.', 1),
(170, 95, 'Previene la formación de capilares.', 3),
(171, 96, 'Migliora le gambe pesanti o gonfie, dando sensazione di leggerezza. ', 1),
(172, 96, 'Mejora las piernas pesadas e inflamadas, regalando bienestar en el movimiento. ', 3),
(173, 97, 'Aiuta la cicatrizzazione delle ulcere.', 1),
(174, 97, 'Ayuda a la cicatrización de úlceras.', 3),
(175, 98, 'Accorciamento dell’eccesso di pelle sugli occhi.', 1),
(176, 98, 'Acorta el ecceso de piel de los parpados.', 3),
(177, 99, 'Occhi più aperti e armonici.', 1),
(178, 99, 'Ojos más abiertos y armoniosos.', 3),
(179, 100, 'Apparenza più fresca.', 1),
(180, 100, 'Apariencia más fresca.', 3),
(181, 101, 'Pelle liscia e pulita libera di fibromi o verruche.', 1),
(182, 101, 'Piel más lisa y limpia libre de verrugas.', 3),
(183, 102, 'Non è un intervento chirurgico, per cui non invasivo.', 1),
(184, 102, 'No es un procedimiento quirúrgico, por ende no es invasivo.', 3),
(185, 103, 'Raggiungere la migliore versione di te internamente ed anche esternamente.', 1),
(186, 103, 'Lograr la mejor versión de tí internamente y también externamente.', 3),
(187, 104, ' Lasciar andare il passato.', 1),
(188, 104, 'Dejar ir el pasado.', 3),
(189, 105, 'Rimuovere le convinzioni limitanti che stanno bloccando la tua energia e felicità.', 1),
(190, 105, 'Remover las creencias limitantes que estan bloqueando tu energía y felicidad.', 3),
(191, 106, 'Dare equilibrio alla tua vita.', 1),
(192, 106, 'Dar equilibrio a tu vida.', 3),
(193, 107, 'Conoscere come puoi vivere una vita che ti dia soddisfazione.', 1),
(194, 107, 'Conocer cómo puedes vivir una vida que te de satisfacción.', 3),
(195, 108, 'Essere consapevole della tua luce ma anche delle tue ombre e come esse possono aiutarti a vibrare alto.', 1),
(196, 108, 'Ser consciente de tu luz y también de tus sombras y como pueden ayudarte a vibrar alto.', 3),
(197, 109, 'Integrare questi risultati nella tua vita.', 1),
(198, 109, 'Integrar los resultados en tu vida.', 3),
(199, 110, 'Valorizzare la tua bellezza.', 1),
(202, 110, 'Valorizar tu belleza.', 3),
(203, 111, 'L’armonizzazione del tuo volto.', 1),
(204, 111, 'La armonización de tu rostro.', 3),
(205, 112, 'Il piacere di sentirti bene dentro della tua pelle.', 1),
(206, 112, 'El placer de sentirse bien dentro de tu propia piel.', 3),
(207, 114, 'L\'armonizzazione del tuo volto.', 1),
(208, 114, 'La armonización de tu rostro.', 3),
(209, 115, 'Sarà rallentato il processo d’invecchiamento.\n\n', 1),
(210, 115, 'Enlentecerá el proceso de envejecimiento.', 3),
(211, 116, 'Un intestino più efficace nelle sue funzioni.', 1),
(212, 116, 'Un intestino más efectivo en sus funciones.', 3),
(213, 117, 'Migliorerà la digestione. ', 1),
(214, 117, 'Mejorará la digestión', 3),
(215, 118, 'Si ridurrà l’adiposità localizzata più facilmente.', 1),
(216, 118, 'Reducirá la adiposidad localizada más fácilmente.', 3),
(217, 119, 'Renderà più efficaci i trattamenti di medicina estetica.', 1),
(218, 119, 'Hará que los tratamientos de medicina estética sean más efectivos.', 3),
(219, 120, 'Ti sentirai leggera dentro il tuo corpo.', 1),
(220, 120, 'Te sentirás más ligera dentro de tu cuerpo.', 3),
(221, 121, 'Avrai un organismo che funziona in modo equilibrato.', 1),
(222, 121, 'Tendrás un organismo funcionando equilibradamente.', 3),
(223, 122, 'Renderà più efficace i risultati di una alimentazione consapevole.', 1),
(224, 122, 'Los resultados de una alimentación consciente serán más efectivos.', 3),
(225, 123, 'Migliorerà il tuo umore.', 1),
(226, 123, 'Mejorará tu estado de humor.', 3),
(227, 124, 'Pelle più luminosa e sana.', 1),
(228, 124, 'Piel más luminosa y saludable.', 1),
(229, 125, 'Capelli e unghie più forti e sani.', 1),
(230, 125, 'Cabello y uñas más fuertes y saludables.', 3),
(231, 126, 'Sarai libera di scegliere il cosmetico cha fa bene alla tua pelle.', 1),
(232, 126, 'Serás libre de elegir el cosmético que sea bueno para tu piel.', 3),
(233, 127, 'Non sarai più ingannata dalle pubblicità.', 1),
(234, 127, 'Ya no te engañarán los astutos anuncios publicitarios.', 3),
(235, 128, 'Potrai combattere in modo efficace l’invecchiamento della pelle.', 1),
(236, 128, 'Podrás combatir en modo eficaz el envejecimiento de la piel.', 3),
(237, 129, 'Potrai valutare i prodotti che ti applicano nelle spa o che ti vengono consigliati.', 1),
(238, 129, 'Podrás evaluar los productos que te aplican en el spa o aquellos que te sean aconsejados.', 3),
(239, 130, 'Sarai in grado di scegliere i cosmetici da usare sui tuoi figli.', 1),
(240, 130, 'Podrás elegir los cosméticos compatibles con la piel en tus hijos.', 3),
(241, 131, 'Capirai come avere una pelle sana.', 1),
(242, 131, 'Comprenderás cómo tener una piel saludable.', 3),
(243, 132, 'Se hai macchie, potrai trattarle in un modo più idoneo.', 1),
(244, 132, 'Si tienes manchas, podrás tratarlas de una manera más apropiada.', 3),
(245, 133, 'Avrai più energia.', 1),
(246, 133, 'Te sentirás con más energía.', 3),
(247, 134, 'Migliorerà la tua digestione.', 1),
(248, 134, 'Mejorará la digestión', 3),
(249, 135, 'Potrai identificare gli alimenti che nutrono la tua anima e il tuo corpo.', 1),
(250, 135, 'Podrás identificar los alimentos que nutren tu alma y tu cuerpo.', 3),
(251, 136, 'Avrai una pelle sana e luminosa.', 1),
(252, 136, 'Tendrás una piel sana y luminosa.', 3),
(253, 137, 'Ti sentirai leggera e attiva.', 1),
(254, 137, 'Te sentirás ligera y activa.', 3),
(255, 138, 'Ti aiuterà a rallentare e/o prevenire la formazione delle rughe.', 1),
(256, 138, 'Será de ayuda para disminuir y/o prevenir la formación de arrugas.', 3),
(257, 139, 'Stabilirai una relazione di benessere con il cibo.', 1),
(258, 139, 'Establecerás una relación de bienestar con la comida.', 3),
(259, 140, 'Sentirai la tua vita in equilibrio.', 1),
(260, 140, 'Sentirás tu vida más equilibrada.', 3),
(261, 141, 'Migliorerà la tua memoria.', 1),
(262, 141, 'Mejorará tu memoria.', 3),
(263, 142, 'Troverai il tuo peso ideale.', 1),
(264, 142, 'Obtendrás tu peso ideal.', 3),
(265, 143, 'Podrás desintoxicar y desinflamar tu piel y no seguir agrediéndola con sustancias que dañan su equilibro y el bienestar de tu cuerpo.', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `commenti`
--

CREATE TABLE `commenti` (
  `id_commento` int(11) NOT NULL,
  `nome_commento` varchar(250) NOT NULL,
  `testo_commento` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `commenti`
--

INSERT INTO `commenti` (`id_commento`, `nome_commento`, `testo_commento`, `lingua_traduzione_id`) VALUES
(1, ' Privacy della paziente', 'Ho fatto una consulenza di medicina estetica per curiosità. Il mio obiettivo era quello di togliere stanchezza al viso, per ottenere un aspetto più fresco ma naturale. La dottoressa mi ha proposto una seduta di biostimolazione con acido ialuronico e successivamente due sedute di filler. Sono pienamente soddisfatta del risultato. Competente e professionale la consiglio pienamente\n\n', 1),
(2, 'Antonina Caddeo', 'Grazie alla dottoressa Orlena che è un fantastico medico estetico ma è un altrettanto fantastico medico dell’anima, posso dire che lei mi ha dato armonia al mio fisico ma soprattutto ha cambiato profondamente il modo che avevo di vedere me stessa. Grazie al suo aiuto sono un\'altra persona più sicura, più decisa, più forte con un\'autostima che non ho mai avuto. ', 1),
(3, 'Anonimo', 'Ho iniziato a vedere, mio malgrado, il viso sciupato, questo purtroppo mi ha resa infelice. Ho saputo che c\'era una dottoressa che si occupa di medicina estetica e mi sono subito rivolta a lei. Ora posso realmente constatare un cambiamento quasi miracoloso sul viso. Mi sento rinata e consiglio a tutti di lasciar stare i pregiudizi e prendersi cura del proprio corpo .', 1),
(4, 'Erika Montero', 'Escuchar a Orlena describir la vibración que tienen los números de mi vida fue como una inyección de energía, que representó una guía con la que me sentí identificada, ampliando así mi punto de vista para mejorar ciertas características y para afianzar otras. Es un regalo de autoconocimiento que recomiendo.\n\n', 3),
(5, 'Geraldine Betancourt', 'Orlena Zotti, es una mujer que logra una alquimia única entre su saber científico y espiritual. Gracias a ella, ahora tengo una guía , un manual precioso, que incluye mis sombras, mis aspectos menos conocidos y trabajados, que me permiten seguir mi proyecto de vida. ', 3),
(6, 'Privacy della paziente', 'Faccio sempre tanto sport e mantengo una alimentazione sana, ma i risultati non sono mai stati buoni per quello che riguarda alla ritenzione e cosce piene di buchi. Dopo di aver fatto dei trattamenti con la Dott.ssa Orlena ho ottenuto risultati visibili da avermi cambiato una taglia di pantaloni. Sono molto contenta dei risultati di questo percorso che sto facendo.', 1),
(7, 'Claudia Fernandez', 'El aporte de Orlena fue maravilloso para mi vida. Algo así como poder ver en perspectiva mis lugares más luminosos y los más oscuros. Me ha sido muy útil y ha aumentado mi confianza. Recomiendo totalmente el programa “Brilla Intensamente”.', 3),
(8, 'Dulce Huerta', 'El programa “Brilla intensamente” es una herramienta que me ha ayudado a conocerme mejor desde mi luz y mi sombra y me ha recordado que la energía no se crea ni se destruye, solo se transforma, lo cual me hace estar consciente y me da la guía para ser cada día mi mejor versión.', 3),
(9, 'Cinzia Passera', 'La dottoressa Orlena è un vulcano di istinto, intuito ed energia ma anche professionalità, competenza e preparazione.Il suo concetto di \'\'avere cura\'\' abbraccia l\'individuo nella sua totalità senza trascurare l\'aspetto più vero, profondo e interiore. Perché è lì che c\'è la nostra essenza e lei sa come valorizzarla. Si serve della scienza, ma è alla nostra anima che parla. Grazie Orlena per la solarità che emani.', 1),
(10, 'Jessica Alzini', 'Ho conosciuto Orlena tramite un suo video postato in Facebook. Mi è piaciuto fin da subito e gli argomenti trattati erano molto interessanti. Ho intrapreso con lei un percorso personale che mi ha dato modo di scoprire cose nuove e nuovi mezzi per migliorarmi. Grazie per la tua empatia, professionalità, semplicità e spontaneità.', 1),
(11, 'Alessandra Massaccesi', 'La dottoressa Orlena è una professionista molto preparata, sa metterti subito a proprio agio, riesce a spiegare argomenti e concetti facendo degli esempi pratici è piena di iniziative, pronta a darti nuove soluzioni che rispecchiano le proprie esigenze personali. Una donna che emana vibrazioni alte e piena di luce. Grazie Orlena per il tuo sostegno e per i tuoi insegnamenti .', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `constants_framework`
--

CREATE TABLE `constants_framework` (
  `id_cf` int(11) NOT NULL,
  `cf_name` varchar(100) NOT NULL,
  `cf_value` varchar(250) NOT NULL,
  `cf_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `constants_framework`
--

INSERT INTO `constants_framework` (`id_cf`, `cf_name`, `cf_value`, `cf_description`) VALUES
(1, 'SITE_URL_PATH', 'https://www.seiessenzialmentebella.com', 'Indirizzo online del sito'),
(2, 'SITE_TITLE_NAME', 'Sei essenzialmente bella', 'Titolo del sito online'),
(3, 'SMTP_HOST_CUSTOM', 'secureit18.sgcpanel.com', 'Indirizzo del server SMTP'),
(4, 'SMTP_USER_CUSTOM', 'sei@seiessenzialmentebella.com', 'Utente del server SMTP'),
(5, 'SMTP_PASS_CUSTOM', 'oz17@Sei', 'Password del server SMTP'),
(6, 'DB_HOSTNAME', '77.104.188.118', 'Indirizzo del DB'),
(7, 'DB_USERNAME', 'seiessen', 'Utente del DB'),
(8, 'DB_PASSWORD', 'sei17@Cpanel', 'Password del DB'),
(9, 'DB_DATABASE', 'seiessen_framework', 'Nome del DB'),
(10, 'COMPANY_NAME', 'Seiessenzialmentebella', 'Nome della società/ditta'),
(11, 'COMPANY_EMAIL', 'sei@seiessenzialmentebella.com', 'Email della società/ditta'),
(12, 'COMPANY_COPYRIGHT', '&copy; 2017 Seiessenzialmentebella', 'Testo footer copyright'),
(13, 'COMPANY_PHONE', '+39 3923576272', 'Telefono società/ditta'),
(14, 'COMPANY_ADDRESS', 'Sardegna', 'Indirizzo società/ditta'),
(15, 'GPLUS_LINK', '', 'Link social G+'),
(16, 'YOUTUBE_LINK', 'https://www.youtube.com/channel/UCN1HINdAy4Ng-0Pdq-7bJdg', 'Link social YOUTUBE'),
(17, 'PINTEREST_LINK', '', 'Link social PINTEREST'),
(18, 'TWITTER_LINK', '', 'Link social TWITTER'),
(19, 'FACEBOOK_LINK', 'https://www.facebook.com/dottoressaorlenazotti/', 'Link social FACEBOOK'),
(20, 'INSTAGRAM_LINK', 'https://www.instagram.com/dottoressaorlenazotti/', 'Link social INSTAGRAM'),
(21, 'GOOGLE_ANALITYCS_ID', 'UA-108374307-1', 'UID Google Analitycs'),
(22, 'PAYPAL_EMAIL', '', 'Email di Paypal per i pagamenti'),
(23, 'PAYPAL_ENV', '', 'Environment di paypal sviluppo/produzione'),
(24, 'PAYPAL_SANDBOX_CLIENT_ID', '', 'Cliend ID sandbox di Paypal'),
(25, 'PAYPAL_LIVE_CLIENT_ID', '', 'Cliend ID sandbox di Paypal LIVE'),
(26, 'STRIPE_PK', '', 'Stipe Public Key'),
(27, 'STRIPE_SK', '', 'Stripe Secret Key');

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_moduli`
--

CREATE TABLE `contatti_moduli` (
  `id_contatto` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `cognome` varchar(255) NOT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `citta` varchar(255) NOT NULL,
  `messaggio` text NOT NULL,
  `provenienza` varchar(250) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_moduli`
--

INSERT INTO `contatti_moduli` (`id_contatto`, `nome`, `cognome`, `email`, `telefono`, `citta`, `messaggio`, `provenienza`, `data`, `stato_contatto`, `data_unsubscribe`, `id_lingua`) VALUES
(10, 'Claudia Patricia', '', 'cardonab10@yahoo.es', '3017765308', '0', 'Orlena buenas noches. Soy medica. Trabajo en Colombia. Soy como tu médica Estetica y Medica Biologica. Te conoci a traves de mujer holistica. He estado inquieta en hacer escuela de emprendedoras pero veo que el enfoque es mas que todo virtual y nosotras trabajamos mas desde el consultorio. Que yo apenas estoy pensando en abrir. Tu me podrias aconsejar por favor que tan util puede ser para mi. Mil gracias  ', 'contattami', '2018-03-04 20:02:56', 1, NULL, 3),
(14, 'Barbara', '', 'babigari28@gmail.com', '', '0', 'Riempito formulario- vautazione alimentazione', '', '0000-00-00 00:00:00', 1, NULL, 1),
(20, 'Radhairis Santana medina', '', 'santanamradhairis@gmail.com', '8493410306', '0', 'Como desintoxico mi cuerpo naturalmente y k desaparescan la celulitis ', 'Regenera tu cuerpo', '2018-07-12 14:36:04', 1, NULL, 3),
(22, 'Sandra', '', 'chillosingui@gmail.com', '8298381607', '0', 'Tengo unas manchas en las piernas desde pequeñas es posible borrarlas', 'Regenera tu cuerpo', '2018-07-15 07:20:19', 1, NULL, 3),
(24, 'Maria Carolina Mundarain', '', 'carolmundarain@hotmail.com', '3057478251', '1', 'Mi niña no se como ver la grabacion del 17/7/18, por favor ayudame...\nBesos\nCarolina', 'contattami', '2018-07-24 15:05:07', 1, NULL, 3),
(25, 'Salome jose', '', 'salomejosejose3911@gmail.com', '8096965790', '2', 'Hola me interesa mejorar el aspecto de mi piel, tengo manchas y es reseca , he tratado con muchos productos y tratamientos y nada , me gustaría me ayuden . Gracias ', 'Regenera tu piel', '2018-07-27 13:19:02', 1, NULL, 3),
(26, 'Hilda', '', 'mariahildacasilla@gmail.com', '8099124187', '0', 'Dominica Rep. Tengo la piel con Mancha blanca sólo en cuello ahora después feblis 50 años en mi juventud me exponía al sol porque no sabía que me haría daños', 'Evaluación online', '2018-07-27 14:04:07', 1, NULL, 3),
(27, 'Ailice', '', 'ailicecespinal@hotmail.com', '', '0', 'regenera tu cuerpo', '', '0000-00-00 00:00:00', 1, NULL, 3),
(29, 'Thainali ', '', 'thainalirodriguez@gmail.com', '', '0', 'Cómo se cual es compatible ', 'Regenera tu piel', '2018-07-31 04:22:45', 1, NULL, 3),
(30, 'marilu gonzalez', '', 'amorjavimariluta@gmail.com', '04242447200', '0', 'quisiera saber el costo', 'Regenera tu cuerpo', '2018-08-08 15:48:22', 1, NULL, 3),
(36, 'valeria', '', 'valeria.spagnuolo13@gmail.com', '3282960860', '0', 'Ciao sono Napoli e vorrei iniziare un percorso con te che comprenda corpo e viso. Ho molta strada da fare...perdere 20 kl e imparare ad alimentarmi correttamente...Ho grasso e cellulite sulle gambe, ma ho anche la pelle delicata con capillari evidenti sulle coscie e couperose sulle giancie. sopratutto il mio viso ultimamente grida aiuto! oltre alla couperose, ai pori dilatati sul naso e ai punti neri sul mento mi sono uscite negli ultimi anni molte macchie scure sia sulle guancie che sull\'area sopra il labbro superiore. Cerco sempre di proteggermi dal sole ma non basta! Vorrei fare un check up con te e capire cosa si puo fare. Grazie!', 'Rigenera il tuo corpo', '2018-08-29 07:43:06', 1, NULL, 1),
(37, 'Alice Miazzo', '', 'ice.miazzo@hotmail.it', '', '0', 'Interessata retreat marruecos. Contatto Roberta.', '', '1999-11-29 23:00:00', 1, NULL, 1),
(38, 'Sara Scalmaniate', '', 'sarascal84@hotmail.com', '', '0', 'Interessata retreat marruecos novembre 2018. Contatto Roberta.', '', '0000-00-00 00:00:00', 1, NULL, 1),
(39, 'orlena', '', 'orlenazp@hotmail.com', '', '0', 'Prueba', '', '0000-00-00 00:00:00', 1, NULL, 1),
(40, '0', '', '0', '0', '0', '0', '0', '2018-09-13 21:31:34', 1, NULL, 1),
(41, '0', '', '0', '0', '0', '0', '0', '2018-09-21 20:53:42', 1, NULL, 1),
(42, '0', '', '0', '0', '0', '0', '0', '2018-09-26 09:32:45', 1, NULL, 1),
(43, 'Roberto', '0', 'rerewffds@ti.ni', '324234324234', '0', 'dsfdsfsdfsdf', 'Viaje en mi mondo', '2018-10-30 10:28:59', 1, NULL, 3),
(44, 'Frisco', '0', 'cansnadhasd@gdfd.com', '32423423423', '0', 'dddd', 'Viaje en mi mondo', '2018-10-30 10:30:57', 1, NULL, 3),
(45, 'Marco', '0', 'ricciiasdasdas@gsfgsfs.ot', '234234234234234', '0', '2efsdfsfd sdf sfs df', 'contattami', '2018-10-31 16:13:55', 1, NULL, 1),
(46, 'Bella', '0', 'ksksdjfsdf@tiscali.it', '234234243', '0', 'sdfsdfsdf', 'contattami', '2018-10-31 16:15:59', 1, NULL, 1),
(47, 'Iena', 'Cisco', 'dfsfdsdfsdf@tin.out', '34224234234', 'Milanello', 'sdfsfsdfsdfsdf', 'contattami', '2018-10-31 16:17:29', 1, NULL, 1),
(48, 'dsfsfd', 'sdfsdf', 'roberto.rossi77@gmail.com', '243242', 'dsfsdf', 'sdfsdfdsf', 'contattami', '2018-11-03 18:02:55', 1, NULL, 3),
(49, 'Roberto', 'Rossi', 'roberto.rossi77@gmail.com', '42423423', 'Roma', 'sdfsdfsdfds', 'contattami', '2018-11-03 18:07:31', 1, NULL, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_newsletter`
--

CREATE TABLE `contatti_newsletter` (
  `id_contatto_newsletter` int(11) NOT NULL,
  `nome_contatto` varchar(250) NOT NULL,
  `email_contatto` varchar(250) NOT NULL,
  `data_contatto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_newsletter`
--

INSERT INTO `contatti_newsletter` (`id_contatto_newsletter`, `nome_contatto`, `email_contatto`, `data_contatto`, `stato_contatto`, `data_unsubscribe`, `lingua_traduzione_id`) VALUES
(11, 'STEFANIA', 'stefi.estrella@hotmail.it', '2018-02-02 02:47:00', 1, NULL, 1),
(12, 'Maria Grazia Catarinicchia', 'mrosolino@alice.it', '2018-02-02 02:51:33', 1, NULL, 1),
(13, 'Elena', 'elenabazzana@yahoo.it', '2018-02-02 05:07:35', 1, NULL, 1),
(17, 'Cinzia Passera', 'cinziaopassera@gmail.com', '2018-02-02 07:53:00', 1, NULL, 1),
(18, 'Misay silva', 'misay@hotmail.com', '2018-02-02 09:48:44', 1, NULL, 3),
(19, 'Silvia Adriana Mundarain Trujillo ', 'mundarainsilvia@hotmail.com', '2018-02-02 10:04:50', 1, NULL, 3),
(20, 'Carolina', 'carolmundarain@hotmail.com', '2018-02-02 10:54:21', 1, NULL, 3),
(21, 'Stefania', 'stefaniat@yahoo.it', '2018-02-02 11:15:36', 1, NULL, 1),
(22, 'Claudia', 'ianniclaudia77@gmail.com', '2018-02-02 11:54:18', 1, NULL, 1),
(23, 'Claudia Fernández', 'claudiafernandeze@gmail.com', '2018-02-02 12:08:33', 1, NULL, 3),
(24, 'Linda Cattaneo', 'linda.c.cattaneo@gmail.com', '2018-02-02 12:17:21', 1, NULL, 1),
(25, 'Erika', 'erikamontero1@gmail.com', '2018-02-02 12:24:46', 1, NULL, 3),
(26, 'Alessandra', 'alessandra@omodei.it', '2018-02-02 12:54:35', 1, NULL, 1),
(27, 'Paola', 'paolina77@gmail.com', '2018-02-02 12:59:33', 1, NULL, 1),
(29, 'Silvia', 'silviapi@tiscali.it', '2018-02-02 15:10:14', 1, NULL, 1),
(30, 'Erika', 'bertocci.erika@gmail.com', '2018-02-02 16:26:00', 1, NULL, 1),
(31, 'Barbara ', 'barbararjpg@yahoo.it', '2018-02-02 16:31:14', 1, NULL, 1),
(32, 'Chiara violino', 'violinochiara@gmail.com', '2018-02-02 17:16:28', 1, NULL, 1),
(33, 'chantal', 'chantal.pasquettaz@gmail.com', '2018-02-03 00:50:32', 1, NULL, 1),
(34, 'Laura ', 'laura.mobilee@gmail.com', '2018-02-03 02:06:33', 1, NULL, 1),
(35, 'Alice', 'alicemoro.ali@gmail.com', '2018-07-05 12:25:54', 3, '2018-07-05 07:25:54', 1),
(36, 'Jessica ', 'x.jessica@virgilio.it', '2018-02-03 09:57:28', 1, NULL, 1),
(37, 'Federica Cinus', 'fedecinus78@libero.it', '2018-02-03 13:07:39', 1, NULL, 1),
(38, 'Deborah', 'deborahforzanini@gmail.com', '2018-02-03 13:35:20', 1, NULL, 1),
(39, 'Daniela', 'luglio73@libero.it', '2018-02-04 08:12:09', 1, NULL, 1),
(40, 'Raffaella', 'raffy.medina@gmail.com', '2018-02-04 13:41:03', 1, NULL, 1),
(41, 'Agostina Sol ', 'agostina.cisella@gmail.com', '2018-02-04 16:59:26', 1, NULL, 1),
(42, 'roberta', 'zanetti.rz@gmail.com', '2018-02-04 22:57:01', 1, NULL, 1),
(43, 'Rosalba', 'rosalba_piar@yahoo.it', '2018-02-05 07:17:18', 1, NULL, 1),
(44, 'Fátima', 'fvasquezv79@gmail.com', '2018-02-08 22:14:34', 1, NULL, 3),
(45, 'Virginia ', 'virgy1266@gmail.com', '2018-02-10 09:29:41', 1, NULL, 1),
(46, 'Lara', 'laraferr69@gmail.com', '2018-02-10 13:50:02', 1, NULL, 1),
(47, 'Luisa Elena Perez', 'lelenapv@hotmail.com', '2018-02-09 23:00:00', 1, NULL, 3),
(48, 'Iolanda', 'ferraroiolanda@gmail.com', '2018-02-15 06:34:50', 1, NULL, 1),
(49, 'Anita angiolini', 'sales@angiolinianita.com', '2018-02-16 07:27:53', 1, NULL, 1),
(50, 'Tiziana', 'info@tizianarinaldi.it', '2018-02-17 01:49:07', 1, NULL, 1),
(51, 'Vera', 'verasganga@gmail.com', '2018-02-21 00:39:18', 1, NULL, 1),
(52, 'María Alejandra Monteverde', 'mariamonteverdembs@gmail.com', '2018-02-25 23:00:00', 1, NULL, 3),
(53, 'orle', 'orlenazp@hotmail.com', '2018-02-28 23:00:00', 1, NULL, 1),
(54, 'Paola Brivio', 'redpaoletta@gmail.com', '2018-06-07 15:33:47', 3, '2018-06-07 10:33:47', 1),
(55, 'Fabiana Lugo', 'fabiana.lugo@gmail.com', '2018-03-07 07:50:09', 1, NULL, 3),
(56, 'Barbara ', 'babigari28@gmail.com', '2018-03-21 14:02:09', 1, NULL, 1),
(57, 'Viviana saltos', 'scindy20@hotmail.com', '2018-04-04 10:21:57', 1, NULL, 3),
(58, 'Irene ', 'irene.rodriguez93@gmail.com', '2018-04-16 05:12:35', 1, NULL, 3),
(59, 'Haydée Rodríguez ', 'haydeerodriguez04@gmail..com', '2018-04-19 04:48:16', 1, NULL, 3),
(60, 'Paola', 'paola.deglinnocenti1981@gmail.com', '2018-05-02 04:03:23', 1, NULL, 1),
(61, 'Sabrina', 'sabri.damico@gmail.com', '2018-05-06 03:26:26', 1, NULL, 1),
(62, 'nicole', 'a.nicolezunino@icloud.com', '2018-05-30 06:48:07', 1, NULL, 1),
(63, 'Palmiris ', 'palmi_69@hotmail.com', '2018-06-01 13:51:21', 1, NULL, 1),
(64, 'Michell ', 'michell_118@hotmail.com', '2018-06-05 13:19:38', 1, NULL, 3),
(65, 'Maria Chiara Cassi', 'mariachiara.cassi1979@gmail.com', '2018-06-07 00:40:40', 1, NULL, 1),
(66, 'Carolina Puchet', 'carolinapuchet@esenciasagrada.com', '2018-06-19 09:29:32', 1, NULL, 3),
(67, 'Margaret', 'esbellezaintegral@gmail.com', '2018-07-09 13:41:45', 1, NULL, 3),
(68, 'María Belisario M.', 'mbelisario_07@hotmail.com', '2018-07-09 15:31:53', 1, NULL, 3),
(69, 'Rosy Danesi', 'rodanesi@hotmail.it', '2018-07-09 17:10:06', 1, NULL, 3),
(70, 'Victoria Jiménez ', 'mvjimenez106@gmail.com', '2018-07-09 18:33:59', 1, NULL, 3),
(71, 'Dabeyva Henriquez ', 'dabeiva16@gmail.com', '2018-07-09 19:18:06', 1, NULL, 3),
(72, 'Jessy', 'jessicaferalenca@gmail.com', '2018-07-09 20:41:39', 1, NULL, 3),
(73, 'Elosa Seoane', 'elisaseoane97@hotmail.com', '2018-07-09 21:46:40', 1, NULL, 3),
(74, 'Rosy', 'rosyledezma@yahoo.it', '2018-07-09 22:30:32', 1, NULL, 1),
(75, 'Mayra', 'Mayra-bh75@hotmail.com', '2018-07-10 01:20:17', 1, NULL, 3),
(76, 'Nancy', 'nvillaman@hotmail.com', '2018-07-10 03:41:17', 1, NULL, 3),
(77, 'Mavel Martins ', 'mavelmartinsc@gmail.com', '2018-07-10 04:19:20', 1, NULL, 3),
(78, 'Lina', 'linatela@hotmail.com', '2018-07-10 04:34:42', 1, NULL, 3),
(79, 'Maritza', 'malmonte6@hotmail.com', '2018-07-10 04:42:11', 1, NULL, 3),
(80, 'Maria', 'mccabrera@hotmail.com', '2018-07-10 04:58:16', 1, NULL, 3),
(81, 'Gloria  Reyes Vicente ', 'gloriarv_1705@hotmail.com', '2018-07-10 05:15:05', 1, NULL, 3),
(82, 'Dania peña ', 'dradavelisse@gmail.com', '2018-07-10 05:47:34', 1, NULL, 3),
(84, 'Fiordaliza espinal', 'crisfior0807@gmail.com', '2018-07-10 06:09:55', 1, NULL, 3),
(85, 'Belgica ', 'belgiyvic27@gmail.com', '2018-07-10 06:24:06', 1, NULL, 3),
(86, 'Yeimi', 'yeimilaveterana@hotmail.com', '2018-07-10 07:13:36', 1, NULL, 3),
(87, 'Erika Pacheco ', 'erikkapacheco2010@gmail.com', '2018-07-10 08:22:48', 1, NULL, 3),
(88, 'Imma', 'Immacolataborriello@gmail.com', '2018-07-10 08:32:48', 1, NULL, 1),
(89, 'Carmen Rosa Gutiérrez', 'rosabonita04@hotmail.com', '2018-07-10 08:41:04', 1, NULL, 3),
(90, 'Teresa Bisceglia', 'teresabisceglia24@alice.it', '2018-07-10 08:58:04', 1, NULL, 1),
(91, 'Anny', 'annyfm23@gmail.com', '2018-07-10 09:36:33', 1, NULL, 3),
(92, 'Yacquelyn Alberto ', 'yacquelinalberto@gmail.com', '2018-07-10 10:10:03', 1, NULL, 3),
(93, 'Glenis rodriguez', 'glenismr09_27@hotmail.com', '2018-07-10 11:56:13', 1, NULL, 3),
(94, 'María Salgado', 'msalgado778@gmail.com', '2018-07-10 12:56:02', 1, NULL, 3),
(95, 'Yvette', 'yvettegalarza@icloud.com', '2018-07-10 13:06:50', 1, NULL, 3),
(96, 'Olga', 'omartinez_de_maiel@hotmail.com', '2018-07-10 14:23:43', 1, NULL, 3),
(97, 'Mildred Palacios Salazar', 'mildred_pa@hotmail.com', '2018-07-10 14:56:38', 1, NULL, 3),
(98, 'Lourdes García', 'lulygv2@gmail.com', '2018-07-10 15:48:30', 1, NULL, 3),
(99, 'Emelin ', 'Emelinoviedo2509@gmail.com', '2018-07-11 00:31:38', 1, NULL, 1),
(100, 'Cinzia Salmi', 'salmicinzia333@gmail.com', '2018-07-11 01:08:07', 1, NULL, 1),
(101, 'Yojanni altagracia Quero', 'yohanni.2105@gmail.com', '2018-07-11 01:35:42', 1, NULL, 3),
(102, 'Laura cabrera', 'elegancestylerd@gmail.com', '2018-07-11 02:58:54', 1, NULL, 3),
(103, 'Altagracia Pichardo', 'pichardoaltagracia05@gmail.com', '2018-07-11 04:20:50', 1, NULL, 3),
(104, 'Gladys', 'gladysg1620@hotmail.com', '2018-07-11 05:08:51', 1, NULL, 3),
(105, 'Alessandra', 'Alessandramascia@yahoo.it', '2018-07-11 05:42:49', 1, NULL, 1),
(106, 'Sol', 'solmarj_jcc@hotmail.com', '2018-07-11 08:07:32', 1, NULL, 3),
(107, 'Virginia ', 'virgomioni@gmail.com', '2018-07-11 08:22:24', 1, NULL, 1),
(108, 'Wendy  c.Almonte g.', 'almontegarciawendycarolina@gmail.com', '2018-07-11 11:22:44', 1, NULL, 3),
(109, 'kirsy baez ', 'l.kirsybaez04@hotmail.com', '2018-07-11 12:37:23', 1, NULL, 1),
(110, 'Simona serra', 'simona.glicine@gmail.com', '2018-07-11 16:09:42', 1, NULL, 1),
(111, 'Chiara', 'lachiaraemme@gmail.com', '2018-07-20 05:55:39', 3, '2018-07-20 00:55:39', 1),
(112, 'Marleny marte', 'marlenymarte29@gmail.com', '2018-07-12 01:18:55', 1, NULL, 3),
(113, 'Ruth Torres ', 'Rugitomo76@gmail.com', '2018-07-12 01:42:58', 1, NULL, 3),
(114, 'Natascia ', 'mariani.natascia@gmail.com', '2018-07-12 02:34:23', 1, NULL, 1),
(115, 'Milagros ', 'mellys01@windowslive.com', '2018-07-12 03:14:53', 1, NULL, 3),
(116, 'Rosely ', 'rosely0929@hotmail.com', '2018-07-12 03:52:53', 1, NULL, 3),
(117, 'Bethania  Ortega', 'bethaniaortega@hotmail.com', '2018-07-12 05:56:02', 1, NULL, 3),
(118, 'Verónica Espinoza Espinoza ', 'polyday_26_2@hotmail.com', '2018-07-12 07:02:13', 1, NULL, 3),
(119, 'Anita Rojas', 'anitarojas67@hotmail.com', '2018-07-12 08:08:07', 1, NULL, 1),
(120, 'Giulia', 'giulyfer1987@yahoo.it', '2018-07-12 11:41:50', 1, NULL, 1),
(121, 'Karla sudario ', 'ferssu12@hotmail.com', '2018-07-12 11:54:33', 1, NULL, 3),
(122, 'Eliana', 'elisan446@gmail.com', '2018-07-12 12:01:09', 1, NULL, 1),
(123, 'Besayda martinez', 'besayda0117@hotmail.com', '2018-07-12 12:03:45', 1, NULL, 3),
(124, 'Radhairis Santana medina', 'santanamradhairis@gmail.com', '2018-07-12 14:28:34', 1, NULL, 3),
(125, 'Kenia', 'keveval@gmail.com', '2018-07-12 15:08:27', 1, NULL, 3),
(126, 'Filonila ', 'filonilagarcia12@hotmail.com', '2018-07-12 15:10:37', 1, NULL, 3),
(127, 'Anna Rita Perra', 'zifio63@gmail.com', '2018-07-12 15:32:04', 1, NULL, 1),
(129, 'Desiree Robles', 'deyaroca@gmail.com', '2018-07-12 18:17:11', 1, NULL, 3),
(130, 'Roxana Aguirre', 'roxeteaguirre8@hotmail.com', '2018-07-12 18:45:04', 1, NULL, 3),
(131, 'Karina Aristy', 'charylka@gmail.com', '2018-07-12 21:55:00', 1, NULL, 3),
(132, 'Johana ', 'johangel4@gmail.com', '2018-07-12 22:06:35', 1, NULL, 3),
(133, 'Simona ', 'simonacarbone35@gmail.com', '2018-07-19 20:52:16', 3, '2018-07-19 15:52:16', 1),
(134, 'Barbara', 'barbaragaiottino@gmail.com', '2018-07-12 22:27:37', 1, NULL, 1),
(135, 'Daniela', 'danielabrusadelli@hotmail.it', '2018-07-12 23:15:17', 1, NULL, 1),
(136, 'Manuela', 'diorema.931@gmail.com', '2018-07-13 00:17:23', 1, NULL, 1),
(137, 'Giorgia ', 'giosk8er@hotmail.it', '2018-07-13 00:46:19', 1, NULL, 1),
(138, 'Valentina Stinchetti', 'vstinchetti@yahoo.it', '2018-07-13 02:32:08', 1, NULL, 1),
(139, 'Chiara', 'coccolhius@gmail.com', '2018-07-13 03:46:21', 1, NULL, 1),
(140, 'Alexandra Morales Verdezoto', 'alexita_1911@hotmail.com', '2018-07-13 04:01:34', 1, NULL, 3),
(141, 'Elena Migliorati', '988882@stud.unive.it', '2018-07-12 22:00:00', 1, NULL, 1),
(142, 'Francisca ', 'flaquefrancisca@gmail.com', '2018-07-13 05:42:40', 1, NULL, 3),
(143, 'Roxana Guillén ', 'roxiiguillen@hotmail.es', '2018-07-13 12:25:40', 1, NULL, 3),
(144, 'Susy', 'rsusy27@hotmail.com', '2018-07-13 15:25:14', 1, NULL, 3),
(145, 'Rosanna', 'jguil4977@ail.com', '2018-07-13 15:54:54', 1, NULL, 3),
(146, 'Manuela', 'pirlomanuela@gmail.com', '2018-07-12 22:00:00', 1, NULL, 1),
(147, 'Denisse Peguero', 'denissepeguero@hotmail.com', '2018-07-14 01:54:11', 1, NULL, 3),
(148, 'Wilma', 'wilmanaranjo@yahoo.es', '2018-07-14 04:00:34', 1, NULL, 3),
(149, 'Jully', 'anajulieth125@gmail.com', '2018-07-14 05:11:33', 1, NULL, 3),
(150, 'Yimaila Olguin', 'olguinyimaila@gmail.com', '2018-07-14 06:13:30', 1, NULL, 3),
(152, 'Yadira', 'ydipuglia@gmail.com', '2018-07-14 10:16:27', 1, NULL, 3),
(153, 'Fernanda', 'm.Fernanda.1968@hotmail.com', '2018-07-14 11:26:05', 1, NULL, 3),
(154, 'Anita Granda', 'ani.granda75@outlook.es', '2018-07-14 11:39:48', 1, NULL, 3),
(155, 'Mayra pimentel', 'mayra.ptel@hotmail.com', '2018-07-14 11:51:39', 1, NULL, 3),
(156, 'Olga Fabelo', 'olguifv28@gmail.com', '2018-07-14 13:42:25', 1, NULL, 3),
(157, 'Yacilis', 'yacilis.marquez@gmail.com', '2018-07-14 13:42:40', 1, NULL, 3),
(158, 'Rosanna mejia', 'mejia.rosannaelizabeth@gmail.com', '2018-07-14 13:45:37', 1, NULL, 3),
(159, 'Leidis Mejia ', 'leidisgmejiamejia@gmail.com', '2018-07-14 15:26:45', 1, NULL, 3),
(160, 'Vicky', 'viajesvip@gmail.com', '2018-07-14 19:29:35', 1, NULL, 3),
(161, 'Francesca ', 'kelita.f@gmail.com', '2018-07-15 00:35:00', 1, NULL, 1),
(162, 'adda abreu', 'addaabbreu@gmail.com', '2018-07-15 03:56:25', 1, NULL, 3),
(163, 'Cristian Martínez ', 'emimar.nav@hotmail.com', '2018-07-15 05:59:44', 1, NULL, 3),
(164, 'Sandra', 'chillosingui@gmail.com', '2018-07-15 07:13:09', 1, NULL, 3),
(165, 'Lili Contreras', 'lilib2017cuenca@gmail.com', '2018-07-15 07:34:06', 1, NULL, 3),
(166, 'Elizabeth cruz', 'elizabethyinbel0530@gmail.com', '2018-07-15 08:06:15', 1, NULL, 3),
(167, 'Ledy liriano', 'ledybliriano@hotmail.com', '2018-07-15 08:59:44', 1, NULL, 3),
(168, 'Marjorie rivera ', 'marjorierivera1980@gmail.com', '2018-07-15 11:10:51', 1, NULL, 3),
(169, 'María Elena Espejo', 'mariaelenaespejo@hotmail.es', '2018-07-15 12:45:54', 1, NULL, 3),
(170, 'Prisca Luna ', 'prisca_luna@hotmail.com', '2018-07-15 13:16:15', 1, NULL, 3),
(171, 'Hada', 'hadacampanita2014@gmail.com', '2018-07-15 13:53:43', 1, NULL, 1),
(172, 'Alexandra ', '443045789az@gmail.com', '2018-07-15 17:48:28', 1, NULL, 3),
(173, 'Jazmin rojas ', 'jazminrojas2205@gmail.com', '2018-07-15 21:42:47', 1, NULL, 3),
(174, 'Sara', 'S69west@yahoo.it', '2018-07-16 07:38:59', 3, '2018-07-16 02:38:59', 1),
(175, 'Laulette', 'laulettemakeup@gmail.com', '2018-07-16 04:31:05', 1, NULL, 3),
(176, 'Kikey rodriguez', 'kikeyrodriguez17@gmail.com', '2018-07-16 06:40:48', 1, NULL, 3),
(177, 'Mirela', 'mirellajesu@hotmail.com', '2018-07-16 06:47:57', 1, NULL, 3),
(178, 'Milagros ', 'adelitaliz19@gmail.com', '2018-07-16 07:44:41', 1, NULL, 3),
(179, 'Antonella', 'antocade64@gmail.com', '2018-07-16 08:23:34', 1, NULL, 1),
(180, 'Elizabeth ', 'torres.elizabeth@gmail.com', '2018-07-16 14:16:55', 1, NULL, 3),
(181, 'aurora', 'carboniaurora@gmail.com', '2018-07-16 14:48:46', 1, NULL, 1),
(182, 'Cristina Rudi', 'cristina.rudi719@gmail.com', '2018-07-16 21:51:04', 1, NULL, 1),
(183, 'Lara', 'laracavallero@gmail.com', '2018-07-17 03:25:28', 1, NULL, 1),
(184, 'Maria Paola', 'paolacaria72@gmail.com', '2018-07-20 00:51:04', 1, NULL, 1),
(185, 'Rosmery Requena de Núñez ', 'rosmeryrequena@gmail.com', '2018-07-24 12:40:26', 1, NULL, 3),
(186, 'María Michell silvestre', '1381michell@gmail.com', '2018-07-23 22:00:00', 1, NULL, 3),
(187, 'carolina de los Santos', 'rcarolina30@hotmail.com', '2018-07-25 05:57:08', 1, NULL, 3),
(188, 'Flor Maria Gutierrez', 'flor.gutierrez@meduca.gob.pa', '2018-07-25 07:56:24', 1, NULL, 3),
(189, 'Awilda', 'mnezawilda@gmail.com', '2018-07-28 20:10:16', 1, NULL, 3),
(190, 'Aracelis Ramirez', 'aracelisramirez15@gmail.com', '2018-07-29 11:05:37', 1, NULL, 3),
(191, 'Desiree', 'mogollondesi12@gmail.com', '2018-07-29 13:18:11', 1, NULL, 3),
(192, 'Laura medina', 'lapausini66@hotmail.com', '2018-07-30 21:45:02', 1, NULL, 3),
(193, 'Yaira ', 'yairavaldezm@gmail.com', '2018-07-31 06:33:53', 1, NULL, 3),
(195, 'Yoselin', 'marcapasos2000@hotmail.com', '2018-08-03 07:09:11', 1, NULL, 3),
(196, 'Liliana', 'lilidsalazar@hotmail.com', '2018-08-03 07:11:04', 1, NULL, 3),
(197, 'Ingrid', 'icbg-77@hotmail.com', '2018-08-03 07:16:29', 1, NULL, 3),
(199, 'andrea', 'anmir_01@hotmail.com', '2018-08-03 10:33:33', 1, NULL, 3),
(200, 'Rosemary', 'rossymerry2@gmail.com', '2018-08-03 11:22:37', 1, NULL, 3),
(201, 'Tania', 'testanga260@gmail.com', '2018-08-21 10:27:33', 3, '2018-08-21 05:27:33', 3),
(202, 'Yexica gomez', 'yexicavaleria@gmail..com', '2018-08-03 21:53:38', 1, NULL, 3),
(203, 'gloria Chirinos ', 'gloriachirinos65@gmail.com', '2018-08-04 02:06:46', 1, NULL, 3),
(204, 'Leivys Rojas', 'leivysrojas@hotmail.com', '2018-08-04 03:07:00', 1, NULL, 3),
(205, 'Yohalis Brazon', 'yohalis2010@hotmail.com', '2018-08-04 04:06:19', 1, NULL, 3),
(206, 'Madeleine duran', 'duranzambrano@hotmail.com', '2018-08-04 04:13:10', 1, NULL, 3),
(207, 'Lorena Trueblood', 'lorenatruebloodv@gmail.com', '2018-08-04 05:34:20', 1, NULL, 3),
(208, 'Ugles Graterol', 'uglesgrat@hotmail.com', '2018-08-04 06:02:56', 1, NULL, 3),
(209, 'Orsola Pugliese', 'orsolapuglieseg@gmail.com', '2018-08-04 06:43:23', 1, NULL, 3),
(210, 'Yaneth gomez', 'yanethgomez2274@gmail.com', '2018-08-04 09:21:58', 1, NULL, 3),
(211, 'JENNY', 'jelidiaz@gmail.com', '2018-08-04 13:19:32', 1, NULL, 3),
(212, 'Odanni geronimo cid', 'laoda10@hotmail.com', '2018-08-04 14:03:22', 1, NULL, 3),
(213, 'Elizabeth Durán', 'eli.d66@hotmail.com', '2018-08-03 22:00:00', 1, NULL, 3),
(214, 'Stefanny', 'tifannynt@hotmail.com', '2018-08-04 16:11:46', 1, NULL, 3),
(215, 'Yohana', 'dyohana@gmail.com', '2018-08-05 00:28:17', 1, NULL, 3),
(216, 'Rosa Garcia', 'rosa_garcia64@hotmail.com', '2018-08-05 01:55:52', 1, NULL, 3),
(217, 'Blanca', 'blankrosapreciosa@hotmail.com', '2018-08-05 02:10:35', 1, NULL, 3),
(218, 'Magda Hernandez ', 'magdaesperanza41@hotmail.com', '2018-08-04 22:00:00', 1, NULL, 3),
(220, 'Paula Zorrilla', 'paulajosefinaz@gmail.com', '2018-08-05 02:29:12', 1, NULL, 3),
(221, 'Miriam Caraballo', 'miriamcaraballo66@gmail.com', '2018-08-05 02:58:33', 1, NULL, 3),
(222, 'Yidris García', 'elisaulvillafranca@hotmail.com', '2018-08-05 02:58:38', 1, NULL, 3),
(223, 'Gemma Rodrigo', 'pazgemma@hotmail.es', '2018-08-05 03:12:39', 1, NULL, 3),
(224, 'Alex de la cruz', 'miabonilla-09@hotmail.com', '2018-08-05 03:24:12', 1, NULL, 3),
(225, 'Maria susana montilva de guerrero ', 'mariasusanamontilva@gmail.com', '2018-08-05 03:31:46', 1, NULL, 3),
(226, 'Maria Quintero', 'marielosquint@hotmail.com', '2018-08-05 04:05:25', 1, NULL, 3),
(227, 'Rosa Briceño ', 'rosabriceno250@gmail.com', '2018-08-04 22:00:00', 1, NULL, 3),
(228, 'Yeneraida', 'jeneraira@gmail.com', '2018-08-05 04:32:45', 1, NULL, 3),
(229, 'yonika', 'yonikafigueroa1@gmail.com', '2018-08-05 04:52:31', 1, NULL, 3),
(230, 'Franci Goncalves', 'Francis5862@hitmail.com', '2018-08-05 06:13:19', 1, NULL, 3),
(231, 'Karen Olivete', 'olivettikarem@hotmail.com', '2018-08-05 06:29:20', 1, NULL, 3),
(232, 'Ana elizabeth zapata', 'anaelizabeth@hotmail.com', '2018-08-04 22:00:00', 1, NULL, 3),
(233, 'Evelin', 'Evelin30119@hotmail.com', '2018-08-05 07:36:45', 1, NULL, 3),
(234, 'Naty Sánchez ', 'natysdeluis@hotmail.com', '2018-08-05 08:34:50', 1, NULL, 3),
(235, 'Mari Carmen', 'tabarca18@hotmail.com', '2018-08-05 08:34:57', 1, NULL, 3),
(236, 'Morayma camacho', 'morayma12@hotmail.com', '2018-08-05 08:50:34', 1, NULL, 3),
(237, 'Prodileida', 'prodilegones1962@gmail..com', '2018-08-05 09:12:37', 1, NULL, 3),
(238, 'Isabel Ruza', 'isabelita.ruza@hotmail.com', '2018-08-05 09:13:01', 1, NULL, 3),
(239, 'Isabel paz', 'mgomez_12@hotmail.com', '2018-08-05 09:59:13', 1, NULL, 3),
(240, 'Juanita.b, GUEDEZ.Z', 'jbguedezz654@gmail.com', '2018-08-05 10:55:51', 1, NULL, 3),
(241, 'Marisol Cardenas', 'marisolcardenasjacome@yahoo.com', '2018-08-05 11:32:45', 1, NULL, 3),
(242, 'Rocio Eisinger', 'eisinger-04@hotmail.com', '2018-08-05 11:52:40', 1, NULL, 3),
(243, 'Alicia Arcia', 'alicia-arcia2011@hotmail.com', '2018-08-05 12:07:40', 1, NULL, 3),
(244, 'alexandra sirit alcala', 'alexandrasirit@hotmail.com', '2018-08-05 12:22:52', 1, NULL, 3),
(245, 'Rosalina de la Cruz', 'rosalinadelacruz33@gmail.com', '2018-08-05 13:33:10', 1, NULL, 3),
(246, 'Yahusyn ', 'nysuenahir@hotmail.com', '2018-08-05 13:45:08', 1, NULL, 3),
(247, 'Rosa', 'rossemery.p@gmail.com', '2018-08-05 14:13:47', 1, NULL, 3),
(248, 'Magnolia Montoya ', 'mag7131@hotmail.com', '2018-08-05 15:06:13', 1, NULL, 3),
(249, 'Roosmary', 'roosmarybarrios@gmail.com', '2018-08-05 15:07:57', 1, NULL, 3),
(250, 'Ligia rivera', 'ligiarivera59@hotmail.com', '2018-08-05 16:13:44', 1, NULL, 3),
(251, 'Mercedes', 'mercedesduque2748@gmail.com', '2018-08-05 17:20:30', 1, NULL, 3),
(252, 'Laura franco', 'laufranco@gmail.com', '2018-08-05 17:43:35', 1, NULL, 3),
(253, 'Miriampernia', 'Miriampernia62@hotmail.com', '2018-08-05 18:52:44', 1, NULL, 3),
(255, 'Elizabeth Hernandez', 'francyher66@gmail.com', '2018-08-05 20:07:43', 1, NULL, 3),
(256, 'Yelitza Bravo ', 'yebravo@hotmail.com', '2018-08-05 23:53:22', 1, NULL, 3),
(257, 'AMADA PRADO', 'amadadeljesus@gmail.com', '2018-08-06 02:16:45', 1, NULL, 3),
(258, 'Damarys Salazar', 'Damarysmimi@hotmail.com', '2018-08-06 02:22:59', 1, NULL, 3),
(259, 'Laura Nava', 'lacelnavi06@gmail.com', '2018-08-06 03:02:43', 1, NULL, 3),
(260, 'Maira ', 'maira_bolivar207@hotmail.com', '2018-08-05 22:00:00', 1, NULL, 3),
(261, 'Yumila', 'yumilachrinos15@gmail.com', '2018-08-06 03:45:05', 1, NULL, 3),
(262, 'Evelyn Barragan', 'evelynbarragan@gmail.com', '2018-08-06 03:53:43', 1, NULL, 3),
(264, 'Ysabel Fernandez Lopez', 'ysabelfernandez25@gmail.com', '2018-08-06 13:29:35', 3, '2018-08-06 08:29:35', 3),
(265, 'Sandra', 'sandrab_flores@hotmail.com', '2018-08-06 04:06:18', 1, NULL, 3),
(266, 'Orimar Meneses', 'orimarm@gmail.com', '2018-08-06 04:24:07', 1, NULL, 3),
(267, 'thays wilson ', 'thayswm@gmail.com', '2018-08-06 04:30:25', 1, NULL, 3),
(268, 'aida brito', 'aida_lamamy@hotmail.com', '2018-08-06 04:36:41', 1, NULL, 3),
(269, 'Natacha ', 'natacharoman@hotmail.com', '2018-08-06 04:53:12', 1, NULL, 3),
(270, 'Exilia Tillero', 'edilio.tillero1@gmail.com', '2018-08-06 05:03:10', 1, NULL, 3),
(271, 'Magdilys Ramirez ', 'magdilysramirez30@gmail.com', '2018-08-06 05:50:28', 1, NULL, 3),
(272, 'Alix marquez ', 'alixmarquez81@yahoo.com', '2018-08-06 06:04:19', 1, NULL, 3),
(273, 'Yanira', 'yani_dani_24@hotmail.com', '2018-08-06 06:27:45', 1, NULL, 3),
(274, 'Yailin charmel', 'yailincharmel@gmail.com', '2018-08-06 06:31:51', 1, NULL, 3),
(275, 'Marisol Davila', 'cameromarisol1968@gmail.com', '2018-08-06 06:41:12', 1, NULL, 3),
(276, 'Carolina', 'joanych26@gmail.com', '2018-08-06 06:43:34', 1, NULL, 3),
(277, 'Olga xiomara', 'xiocontr@gmai.com', '2018-08-06 06:46:52', 1, NULL, 3),
(278, 'MARISOL MALPICA', 'marisolmalpica32@gmail.com', '2018-08-06 07:19:51', 1, NULL, 3),
(279, 'Yecenia', 'yeceniarodriguez79@gmail.com', '2018-08-06 08:10:00', 1, NULL, 3),
(280, 'Omaira Leiba', 'omairaleiba@gmail.com', '2018-08-06 08:35:02', 1, NULL, 3),
(281, 'zuleima ruedas', 'zuleimaruedas29@hotmail.com', '2018-08-06 08:50:52', 1, NULL, 3),
(282, 'JOSMARYS', 'josmarysgabrielafermin79@gmail.com', '2018-08-06 08:52:43', 1, NULL, 3),
(283, 'María montilla', 'mariamon29@live.com', '2018-08-06 08:54:05', 1, NULL, 3),
(284, 'mariluz bello', 'maribello880@gmail.com', '2018-08-06 09:02:02', 1, NULL, 3),
(285, 'MAGALY APOSTOL', 'apostol.maga29@gmail.com', '2018-08-06 09:09:53', 1, NULL, 3),
(286, 'Marelis Bastidas ', 'Dennyjose40@hotmail.com', '2018-08-06 09:10:26', 1, NULL, 3),
(287, 'Sherlymar ', 'sherlymar19@hotmail.com', '2018-08-06 09:11:18', 1, NULL, 3),
(288, 'Yelitza Lopez ', 'Yelitza548@hotmail.com', '2018-08-06 09:14:20', 1, NULL, 3),
(289, 'Maryory zerpa', 'maryory1588@hotmail.com', '2018-08-06 09:41:06', 1, NULL, 3),
(290, 'Ana Sanchez', 'isfelia3@gmail.com', '2018-08-06 10:00:53', 1, NULL, 3),
(291, 'Mercedes Quintero ', 'mq_ontiveros@hotmail.com', '2018-08-06 10:20:51', 1, NULL, 3),
(292, 'Roserely', 'roserely@hotmail.com', '2018-08-06 10:37:18', 1, NULL, 3),
(293, 'Maria de Leon', 'Lic.mariabarrientos@holmain.com', '2018-08-06 11:01:58', 1, NULL, 3),
(294, 'mairobys Perez', 'distribuidorasarasina@gmail.com', '2018-08-06 11:09:41', 1, NULL, 3),
(295, 'Ileana Terán', 'camacholozada@hotmail.com', '2018-08-06 11:24:20', 1, NULL, 3),
(296, 'Marlene', 'rodriguezmarle1968@gmail.com', '2018-08-06 11:47:55', 1, NULL, 3),
(297, 'Deniss', 'den-mar@hotmail.com', '2018-08-06 12:34:39', 1, NULL, 3),
(298, 'Yajaira', 'dayana.ing.unefa@gmail.com', '2018-08-06 12:38:44', 1, NULL, 3),
(299, 'Carolina', 'gkarolinag@hotmail.com', '2018-08-06 13:03:19', 1, NULL, 3),
(300, 'Tomaglis Gascón', 'tomaglisgascon@gmail.com', '2018-08-06 13:15:27', 1, NULL, 3),
(301, 'Xiomara Espinoza', 'xioma.jose@hotmail.com', '2018-08-06 13:16:54', 1, NULL, 3),
(302, 'Xiomara Espinoza', 'xiomajose7@gmail.com', '2018-08-06 13:18:28', 1, NULL, 3),
(303, 'Milagros Luna', 'atitoluna@hotmail.com', '2018-08-06 14:08:06', 1, NULL, 3),
(304, 'Mary', 'margeca16@gmail.com', '2018-08-06 14:13:32', 1, NULL, 3),
(305, 'Ana', 'axeljaromero@hotmail.com', '2018-08-05 22:00:00', 1, NULL, 3),
(306, 'edelmira sepulveda ', 'sepulveda11358@hotmail.com', '2018-08-06 15:08:30', 1, NULL, 3),
(307, 'Yermelith ramirez ', 'yermelithcastillofuentes@hotmail.com', '2018-08-06 15:29:25', 1, NULL, 3),
(308, 'yapsy Ibarra ', 'yapsy_13@hotmail.com', '2018-08-06 16:18:20', 1, NULL, 3),
(309, 'Venencia feliz', 'venen.feliz@gmail.com', '2018-08-06 16:24:39', 1, NULL, 3),
(310, 'Gloria Gómez ', 'gloes29@hotmail.com', '2018-08-06 16:53:05', 1, NULL, 3),
(311, 'Romina Colina', 'rdca.937@gmail.com', '2018-08-06 16:59:53', 1, NULL, 1),
(312, 'Lucinda dos santos pestana', 'lucinda2santos@hotmail.com', '2018-08-06 17:00:28', 1, NULL, 3),
(313, 'Ana Daniela ', 'danielarm01@gmail.com', '2018-08-06 17:37:35', 1, NULL, 3),
(314, 'Viddelys nava', 'viddelys@gmail.com', '2018-08-06 18:12:30', 1, NULL, 3),
(315, 'Zurin carrero', 'zcarrero568204@hotmail.com', '2018-08-06 19:30:39', 1, NULL, 3),
(316, 'Neydis montilva', 'neydis6@hotmail.com', '2018-08-06 19:41:00', 1, NULL, 3),
(317, 'Roxana Sattori', 'roxanasattori@gmail.com', '2018-08-06 19:49:10', 1, NULL, 3),
(318, 'Liudys Mata', 'liudys.mata@gmail.com', '2018-08-06 20:00:31', 1, NULL, 3),
(319, 'Elsy Severino', 'olicris07@hotmail.com', '2018-08-07 00:55:57', 1, NULL, 3),
(320, 'Marilu Gonzalez', 'amorjavimariluta@gmail.com', '2018-08-09 00:41:32', 3, '2018-08-08 19:41:32', 3),
(321, 'Diña gonzalez', 'dinagonzalezb@hotmail.com', '2018-08-07 02:41:50', 1, NULL, 3),
(322, 'Mayra García Zamora', 'mmayrag@hotmail.com', '2018-08-07 02:59:13', 1, NULL, 3),
(323, 'Jerermy Rubín', 'jerru9@gmail.com', '2018-08-07 03:31:10', 1, NULL, 3),
(324, 'Andreina Mañez', 'andreina.dquintana@gmail.com', '2018-08-07 03:47:27', 1, NULL, 3),
(325, 'Blanca ', 'clari_11918357@hotmail.com', '2018-08-07 03:51:27', 1, NULL, 3),
(326, 'Alexandra ', 'alexandracr19@hotmail.com', '2018-08-07 05:49:08', 1, NULL, 3),
(327, 'francis', 'fran_perez14@hotmail.com', '2018-08-07 05:54:12', 1, NULL, 3),
(328, 'mariela coromoto arguinzones ', 'coroarg65@gmail.com', '2018-08-07 06:28:32', 1, NULL, 3),
(329, 'Aleima Velásquez ', 'aleimavelasquez@hotmail.com', '2018-08-07 06:31:00', 1, NULL, 3),
(330, 'Paola portilla', 'portillachavez2014@gmail.com', '2018-08-07 06:56:24', 1, NULL, 3),
(331, 'Bianca miosotis de la rosa ', '8295174161@gimeil.com', '2018-08-07 06:58:37', 1, NULL, 3),
(332, 'Landanzurylp@promedan.net', 'landanzurylp@promedan.net', '2018-08-07 07:02:41', 1, NULL, 3),
(333, 'Danielys parica', 'Danielysparica@hotmail.com', '2018-08-07 07:56:06', 1, NULL, 3),
(334, 'Fanny', 'fanny_m_arroyo@hotmail.com', '2018-08-07 08:03:46', 1, NULL, 3),
(335, 'Carolina Santos ', 'carolina_santos0510@hotmail.com', '2018-08-06 22:00:00', 1, NULL, 3),
(336, 'Luz ', 'luznaranjovi@gmail.com', '2018-08-07 10:21:13', 1, NULL, 3),
(337, 'corina', 'yubizaylira@gmail.com', '2018-08-07 10:49:09', 1, NULL, 3),
(338, 'Rafaelina', 'mendezmendezrafaelina@gmail.com', '2018-08-07 11:12:06', 1, NULL, 3),
(339, 'Rosa tierno', 'parabrisyaracuy@hotmail.com', '2018-08-07 12:01:25', 1, NULL, 3),
(340, 'Johana chacon', 'coro_1507@hotmail.com', '2018-08-07 12:13:45', 1, NULL, 3),
(341, 'Irene Canelon', 'irenecancol64@gmail.com', '2018-08-07 12:20:02', 1, NULL, 3),
(342, 'Luisa merlano', 'luisamerlano@yahoo.es', '2018-08-07 12:59:08', 1, NULL, 3),
(343, 'Mariliana Figueroa', 'marilianajfs@gmail.com', '2018-08-07 13:54:02', 1, NULL, 3),
(344, 'Carmen Maria Alvarez de Gonzalez', 'carmary7599@gmail.com', '2018-08-07 14:07:51', 1, NULL, 3),
(345, 'Pilar Tovar Lee', 'pilartovarlee@gmail.com', '2018-08-21 11:04:42', 3, '2018-08-21 06:04:42', 3),
(346, 'Odalixis', 'odalixismarin@hotmail.com', '2018-08-07 14:32:04', 1, NULL, 3),
(347, 'meraris', 'merarisaular@hotmail.com', '2018-08-07 14:55:48', 1, NULL, 3),
(348, 'maria parra', 'mariabeparra@gmail.com', '2018-08-07 15:04:26', 1, NULL, 3),
(349, 'Nieves', 'nievesmoccia@hotmail.com', '2018-08-07 18:07:06', 1, NULL, 3),
(350, 'Maria Alejandra', 'mariaale5120@gmail.com', '2018-08-07 18:08:54', 1, NULL, 3),
(351, 'Yraima', 'yraimacamejo@hotmail.com', '2018-08-07 18:11:32', 1, NULL, 3),
(352, 'Ruby', 'rubromu@gmail.com', '2018-08-07 19:17:10', 1, NULL, 3),
(353, 'Prodileida', 'prodilegones1962@g.mail..com', '2018-08-07 19:35:55', 1, NULL, 3),
(354, 'Yineth', 'yineth30@outlook.com', '2018-08-07 21:43:34', 1, NULL, 3),
(355, 'Carlina Villarroel', 'Carlina_vdm@hotmail.com', '2018-08-07 23:35:35', 1, NULL, 3),
(356, 'Carmen Boliivar', 'carmenbol2012@hotmail.com', '2018-08-08 02:14:35', 1, NULL, 3),
(357, 'oelys alcala', 'oelysalcala@hotmail.com', '2018-08-08 02:19:30', 1, NULL, 3),
(358, 'nayibe perez', 'nayibeperez33@hotmail.com', '2018-08-08 03:23:53', 1, NULL, 3),
(359, 'zaida', 'zaidatorrealba9@gmail.com', '2018-08-08 03:40:39', 1, NULL, 3),
(360, 'Maria Elena bracato ', 'bracatomary.479@gmail.com', '2018-08-08 03:52:04', 1, NULL, 3),
(361, 'Yudith ', 'yuke_24@hotmail.com', '2018-08-08 04:10:33', 1, NULL, 3),
(362, 'Maryoly rondon', 'mayovirgo@hotmail.com', '2018-08-08 04:21:58', 1, NULL, 3),
(363, 'Yhojaira Jorgelina Reyes ', 'yhojairareyesgeraldo@gmail.com', '2018-08-08 04:27:06', 1, NULL, 3),
(364, 'María graciela mora cedeño ', 'chelinaabril@hotmail.com', '2018-08-08 04:37:15', 1, NULL, 3),
(365, 'Maria spadaro', 'spadarocarmelita@hotmail.com', '2018-08-08 04:37:44', 1, NULL, 3),
(366, 'Ricci Borrego ', 'pilin2n@hotmail.com', '2018-08-08 04:43:18', 1, NULL, 3),
(367, 'Aytza perez', 'aytzaperez@hotmail.com', '2018-08-08 04:47:46', 1, NULL, 3),
(368, 'marbelis lopez', 'dramarlop@Gmail.com', '2018-08-08 04:58:35', 1, NULL, 3),
(369, 'Carola', 'fresitacaroles@hotmail.com', '2018-08-08 06:05:11', 1, NULL, 3),
(370, 'Taniabatista', 'taniabatista.5@hotmail.com', '2018-08-08 06:17:04', 1, NULL, 3),
(371, 'Antônia alves', 'abreuantonia@hotmail.com', '2018-08-08 07:10:19', 1, NULL, 3),
(372, 'Milagros', 'milagrosmartinez-22@hotmail.com', '2018-08-08 07:39:42', 1, NULL, 3),
(373, 'lili', 'lilibecita27@Hotmail.com', '2018-08-08 08:26:51', 1, NULL, 3),
(374, 'maria barraez', 'mariajbarfer83@gmail.com', '2018-08-08 08:29:08', 1, NULL, 3),
(375, 'Yurancy', 'yurancymartin@hotmail.com', '2018-08-08 08:31:37', 1, NULL, 3),
(376, 'Vicnan', 'vicnanr@hotmail.com', '2018-08-08 09:10:55', 1, NULL, 3),
(377, 'ANDREINA ', 'andreinamart2016@gmail.com', '2018-08-08 09:30:15', 1, NULL, 3),
(378, 'Juanita', 'sirenita_2372@hotmail.com', '2018-08-08 09:32:28', 1, NULL, 3),
(379, 'Maritza Graterol', 'marycoromoto@hotmail.es', '2018-08-08 09:51:54', 1, NULL, 3),
(380, 'nellys lugo', 'nellysdelcarmen13@hotmail.com', '2018-08-08 11:36:48', 1, NULL, 3),
(381, 'fanny becerra rodriguez', 'fannyjacque@hotmail.com', '2018-08-08 13:28:13', 1, NULL, 3),
(382, 'Claudia ortiz', 'claudianita08@gmail.com', '2018-08-08 13:40:32', 1, NULL, 3),
(383, 'Madeleine Villanueva', 'madeleinevillanueva@gmail.com', '2018-08-07 22:00:00', 1, NULL, 3),
(384, 'Ali escobar', 'ali_yusma@hotmail.es', '2018-08-08 15:02:53', 1, NULL, 3),
(385, 'Ani', 'anivel14@gmail.com', '2018-08-08 16:10:47', 1, NULL, 3),
(386, 'Cecilia Indalino', 'cecyndalino@hotmail.com', '2018-08-08 16:24:51', 1, NULL, 3),
(387, 'María José Rodríguez ', 'mjrdegomes@hotmail.com', '2018-08-08 17:29:30', 1, NULL, 3),
(388, 'Elizabet Lozano Sanchez ', 'elinanoam@hotmail.com', '2018-08-08 17:47:58', 1, NULL, 3),
(389, 'Juana ', 'juana.144@outlook.com', '2018-08-07 22:00:00', 1, NULL, 3),
(390, 'Elizabeth', 'elizabethcastanedaospitia@gmail.com', '2018-08-08 18:40:57', 1, NULL, 3),
(391, 'Fatima medina', 'fatimamedinac@hotmail.com', '2018-08-08 18:51:44', 1, NULL, 3),
(392, 'adelina ', 'ade19202009@hotmail.com', '2018-08-08 19:29:52', 1, NULL, 3),
(393, 'Yelismar Salazar ', 'salayeld@gmail.com', '2018-08-09 01:23:02', 1, NULL, 3),
(394, 'Suharmy Dorvil', 'dsuharmy@gmail.com', '2018-08-09 01:48:21', 1, NULL, 3),
(395, 'Maria Alejandra Gamboa ', 'mariagamboa23@hotmail.com', '2018-08-09 02:20:41', 1, NULL, 3),
(396, 'Daris Sánchez', 'daris.sanchez@hotmail.com', '2018-08-09 03:16:51', 1, NULL, 3),
(397, 'Yoselin reynoso', 'yoselinreynoso01@gmail.com', '2018-08-09 04:03:33', 1, NULL, 3),
(398, 'Roselin Batista zabala', 'Rosybatista19@gmail.com', '2018-08-09 05:03:14', 1, NULL, 3),
(399, 'Judith Arguello ', 'judisol2010@gmail.com', '2018-10-25 11:16:59', 3, '2018-10-25 06:16:59', 3),
(400, 'Laura Vitali', 'lauravitali57@gmail.com', '2018-08-09 05:31:44', 1, NULL, 3),
(401, 'Maryuri bencomo', 'maryupili@hotmail.com', '2018-08-09 05:34:15', 1, NULL, 3),
(402, 'Candida González', 'candy_barquisimeto@hotmail.com', '2018-08-09 06:09:01', 1, NULL, 3),
(403, 'Yaritza', 'hyaritza@gmail.com', '2018-08-09 07:25:51', 1, NULL, 3),
(404, 'Myurika', 'Myurikarolis@gmail.com', '2018-08-09 07:28:34', 1, NULL, 3),
(405, 'Belkis porras', 'Belkisporras0@gmail.com', '2018-08-09 07:41:24', 1, NULL, 3),
(406, 'Neida perez ', 'neidaluciaarrizaga@gmil.com', '2018-08-09 08:04:03', 1, NULL, 3),
(408, 'Mariela', 'marielayestaran@gmail.com', '2018-08-09 08:23:07', 1, NULL, 3),
(409, 'Milena Tovar cañon', 'milenitatovar@hotmail.com', '2018-08-09 08:33:55', 1, NULL, 3),
(410, 'Carminia maria villarreal', 'villarrealcarminia@yahoo.es', '2018-08-09 08:41:33', 1, NULL, 3),
(411, 'Kirsys ortega', 'kirsys.ortega@hotmail.com', '2018-08-09 08:42:50', 1, NULL, 3),
(412, 'Alirio Mundo', 'aliriomundo@gmail.com', '2018-08-09 09:08:12', 1, NULL, 3),
(413, 'Maydi Fernández', 'maydi.f@hotmail.com', '2018-08-08 22:00:00', 1, NULL, 3),
(414, 'Elennys vicuña', 'elennyszkarilu14@gmil.com', '2018-08-09 09:33:32', 1, NULL, 3),
(416, 'Virginia medina', 'virginiamedinapimentel@gmail.com', '2018-08-09 09:40:50', 1, NULL, 3),
(417, 'Maria de Lourdes ', 'Mariadelourdesc515@gmail.com', '2018-08-09 09:43:48', 1, NULL, 3),
(418, 'Milexis parra', 'milexis_83@hotmail.com', '2018-08-09 09:44:35', 1, NULL, 3),
(419, 'Milagro Rojas', 'milagrorojasmedina@hotmail.com', '2018-08-09 09:45:10', 1, NULL, 3),
(420, 'Tatiana', 'deicytatiana9@gmail.com', '2018-08-09 09:58:41', 1, NULL, 3),
(421, 'Yubisays fuentes', 'fuentes.yubisays@gmail.com', '2018-08-09 10:01:50', 1, NULL, 3),
(422, 'Carmen', 'carmenmarlyse@hotmail.com', '2018-08-09 10:11:08', 1, NULL, 3),
(423, 'Mercedes beatriz romero churio', 'mercedebrchurio456@hotmail.com', '2018-08-09 10:59:14', 1, NULL, 3),
(424, 'Ada Ruiz', 'aruizsalud@hotmail.com', '2018-08-09 11:02:08', 1, NULL, 3),
(425, 'JOLY RIZQUEZ', 'jolykas@hotmail.com', '2018-08-09 11:28:39', 1, NULL, 3),
(426, 'Julia Segura', 'juliasegura53728@gmail.com', '2018-08-09 11:37:54', 1, NULL, 3),
(428, 'francis vergara', 'fvergara05@gmail.com', '2018-08-09 11:55:27', 1, NULL, 3),
(429, 'Yenifer', 'yesuche6@gmail.com', '2018-08-09 12:25:31', 1, NULL, 3),
(430, 'Belkis ', 'belkisgomez1021@gmail.com', '2018-08-09 13:01:13', 1, NULL, 3),
(431, 'Luz Garcia', 'sarayluz_2012@hotmail.com', '2018-08-09 14:50:33', 1, NULL, 3),
(432, 'Mery ysnez', 'meyl58@hotmail.com', '2018-08-09 15:52:04', 1, NULL, 3),
(433, 'Vanessa', 'Consiguetuobjetivo.info@gmail.com', '2018-08-09 16:33:44', 1, NULL, 3),
(434, 'A elfa severino', 'anelfaseverino@hotmail.com', '2018-08-09 17:16:26', 1, NULL, 3),
(435, 'Rosanna galan', 'rosannagalan37@gmail.com', '2018-08-09 17:28:01', 1, NULL, 3),
(436, 'Soraima', 'soraimaa@gmail.com', '2018-08-09 17:34:18', 1, NULL, 3),
(437, 'Francy mora ', 'francy_coromoto_mora@hotmail.com', '2018-08-09 17:55:32', 1, NULL, 3),
(438, 'Adriana', 'admacoco@hotmail.com', '2018-08-09 18:20:16', 1, NULL, 3),
(439, 'Rosa ', 'rosataveras04@hotmail.com', '2018-08-09 18:46:55', 1, NULL, 3),
(440, 'Juana Raldiris Díaz Sierra', 'juana.diazs@minerd.gob.do', '2018-08-09 20:20:40', 1, NULL, 3),
(441, 'Belkis ', 'belkis0316@hotmail.com', '2018-08-10 02:30:50', 1, NULL, 1),
(442, 'Cristina Contreras', 'contreras2707@hotmail.com', '2018-08-10 03:07:27', 1, NULL, 3),
(444, 'Maritza Villavicencio ', 'marvillve@hotmail.com', '2018-08-10 04:02:51', 1, NULL, 3),
(445, 'neyda suarez', 'nsuarez1225@gmail.com', '2018-08-10 04:47:09', 1, NULL, 3),
(446, 'Yesika rocca ', 'yesikart-@hotmail.com', '2018-08-10 05:03:21', 1, NULL, 3),
(447, 'Doris Contreras', 'doriselcon@gmail.com', '2018-08-10 06:00:20', 1, NULL, 3),
(448, 'Dinorah ortega', 'chikdesiree84@gmail.com', '2018-08-10 06:49:20', 1, NULL, 3),
(449, 'Carmen', 'penalverc007@gmail.com', '2018-08-10 06:52:11', 1, NULL, 3),
(450, 'Nidia ', 'nivizar@gmail.com', '2018-08-10 08:00:23', 1, NULL, 3),
(452, 'Silvia garcia', 'silvi_lacortiglia@hotmail.com', '2018-08-10 08:40:41', 1, NULL, 3),
(453, 'kenya de vargas', 'kenya4581@gmail.com', '2018-08-10 12:06:29', 1, NULL, 3),
(454, 'María Antonia', 'mariantoniamut@hotmail.com', '2018-08-10 15:29:38', 1, NULL, 3),
(455, 'Yahaira rijo ', 'yahairarijo@hotmail.com', '2018-08-10 16:17:28', 1, NULL, 3),
(456, 'Virginia castro', 'castrovirginia@hotmail.com', '2018-08-10 16:24:31', 1, NULL, 3),
(457, 'Dalia colla ', 'franchesca0924@hotmail.com', '2018-08-10 17:17:04', 1, NULL, 3),
(458, 'Magaly ', 'mosiog31@gmail.com', '2018-08-10 18:48:01', 1, NULL, 3),
(459, 'Suhail', 'yorlinsuhaitovar@hotmail.com', '2018-08-10 20:52:40', 1, NULL, 3),
(460, 'Gissell', 'Gissellnavas12@gmail.com', '2018-08-10 21:26:58', 1, NULL, 3),
(461, 'Paola', 'giovideidda@tiscali.it', '2018-08-11 23:24:28', 1, NULL, 1),
(462, 'Ime Lizarraga', 'hc@imelizarraga.com', '2018-08-22 18:56:57', 1, NULL, 3),
(463, 'valeria', 'valeria.spagnuolo13@gmail.com', '2018-08-29 07:29:40', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates`
--

CREATE TABLE `email_templates` (
  `id_template` int(11) NOT NULL,
  `nome_template` varchar(250) NOT NULL,
  `testo_template` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL,
  `id_tipo_template_default` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_templates`
--

INSERT INTO `email_templates` (`id_template`, `nome_template`, `testo_template`, `lingua_traduzione_id`, `id_tipo_template_default`) VALUES
(1, 'Benvenuta', 'Ciao <b>VAR_NOME_CLIENTE</b>,\n<br><br>\n<br>grazie per esserti iscritto a <a href=\"https://www.seiessenzialmentebella.com/it/home target=\"_blank\"><span class=\"orange-text\">Sei Essenzialmente Bella.</a></span><br><br>\n<p align=\"left\">Volevo parlare un po’ con te dei miei 3 pilastri fondamentali nel lavoro:</p>\n<br>\n<p><ul align=\"left\">\n<li>La Medicina Estetica, la mia passione da 13 anni. Un mondo in cui attraverso l’armonizzazione della bellezza, ho integrato due concetti: bellezza esterna e bellezza interna e per me questo a un unico significato: “la consapevolezza dell’equilibrio”.</li> \n<li>L’alimentazione, una esperienza realmente vissuta che voglio condividere con te, guidandoti per capire come si sente il tuo corpo con quello che mangi, cosa ti dà energia e come integrare in maniera equilibrata i tuoi nutrienti, raggiungendo così il tuo benessere generale e il miglioramento della tua immagine corporea.</li>\n<li>La valorizzazione personale, una forma per renderti nella tua vita più serena, più presente, più amorevole e compassionevole con te stessa e soprattutto più felice. </li>\n</ul>\n<p align=\"justify\">\n<br></br>\nCome ti ho detto la medicina estetica per me è un concetto di equilibrio. Per questa ragione abbino essa all’uso di cosmetici compatibili con la pelle.\n<br></br>\nMa cosa vuol dire compatibile con la pelle? In parole semplici, che i cosmetici sono fatti con ingredienti che non interferiscono nella autoregolazione della pelle. Essa ha la capacità di adattarsi ai cambiamenti esterni (sole, freddo, umidità). Quindi quando utilizzi cosmetici che interferiscono con questa capacità, ciò che accade è che pensi di usare una crema per risolvere una situazione e invece la sta provocando. Anzi tante volte riesci a vedere cambiamenti “positivi” in poco tempo ma questo non vuole dire che è la soluzione giusta. Se vuoi una conferma di quello che ti dico, smetti di usare i cosmetici per una settimana e quello che succederà sicuramente ti farà riflettere.\n<br></br>\nTi invito a partecipare al programma “Rigenera la tua pelle” dove parlo di questo argomento, così potrai avere una nuova coscienza sui cosmetici che utilizzi per “nutrire” la tua pelle. \n<br></br>\nC’è una soluzione per poter scegliere meglio i cosmetici, da tutto quello che usi per il benessere del tuo volto a ciò che utilizzi per il tuo corpo (includendo deodoranti e shampoo).\n<br></br>\nIl programma si svolgerà esclusivamente ONLINE.\n<br></br>\nFai <a href=\"http://bit.ly/Rigeneralatuapelle\">clic QUA</a> per sapere in modo più approfondito di cosa si tratta.  Per ricevere il resto della informazione come data, ora e prezzo, contattami.\n<br></br>\n<b>Fa per te se vuoi incrementare la tua conoscienza sui cosmetici che applichi sulla tua pelle.</b>\n<br></br>\n<b><span class=\"orange-text\">Ricorda che il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.</b></span>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti.</big></span>\n<br></br>\n<b>P.S: Non tutto quello che è bio o vegan certifica che un cosmetico sia compatibile con la pelle.</b>\n\n\n', 1, 1),
(3, 'Bienvenida', 'Bienvenida VAR_NOME_CLIENTE,\n<br><br>\n<br>gracias por haberte inscrito en <a href=\"https://www.seiessenzialmentebella.com/it/home\"><span class=\"orange-text\">Sei Essenzialmente Bella.</a></span><br><br>\n<p aligh=\"left\">Quería hablar un poquito contigo acerca de mis 3 pilares de trabajo:</p>\n<br>\n<p><ul align=\"left\">\n<li>La Medicina Estética, mi pasión desde hace 13 años. Un mundo en el cual a través de la armonización de la belleza, he integrado dos conceptos: belleza externa y belleza interna y esto para mí significa una cosa única: “la conciencia del equilibrio”.</li>\n<li>La alimentación, una experiencia vivida que quiero compartir contigo, serè tu guia en la interpretación de cómo se siente en tu cuerpo lo que comes, que alimentos te dan energía y cómo integrar de forma equilibrada tus nutrientes. De igual modo podrás aprender conmigo como nutrir de una manera adecuada tu piel sin causar desequilibrios en tu organismo. Logrando así, tu bienestar general y mejorando tu imagen corporal.</li>\n<li>La valoración personal, una forma de hacerte dentro de tu vida, màs conciente, más serena, más presente, más amorosa y compasiva contigo misma y por supuesto más feliz.</li>\n</ul>\n<br>\n<p align=\"justify\">\n<br></br>\nEntonces, en pocas plabras: trabajando como médico estético durante 13 años poco a poco me di cuenta de que faltaba algo. Entendí que lo que quería hacer tiene su base en el equilibrio y la conciencia. Por esta razón, hoy ayudo a las mujeres a sentirse bien dentro de su piel a través del amor propio y el cuidado propio. Para esto uso como herramientas la medicina estética, la cosmetología consciente, la alimentación intuitiva partiendo siempre del amor propio unificado con el empoderamiento personal. Porque no se trata solo de brillar externamente, si no que tu luz se refleje externamente con armonía desde tu interior.\n<br></br>\nTe invito a conocer mis programas:\n<ul>\n<li><a href=\"https://www.eresesencialmentebella.com/es/programas/3/regenera-tu-cuerpo\"> Regenera tu cuerpo.</a></li>\n<li><a href=\"https://www.eresesencialmentebella.com/es/programas/7/regenera-tu-piel\">Regenera tu piel.</a></li>\n<li><a href=\"https://www.eresesencialmentebella.com/es/programas/4/nutre-cuerpo-y-alma\">Nutre cuerpo y alma.</a></li>\n<li><a href=\"https://www.eresesencialmentebella.com/es/programas/5/evaluacion-online\">Evaluación Online.</a></li>\n</ul>\n<br></br>\nBueno, poco a poco irás conociendo todo este mundo y los beneficios que puedes obtener.\n<br></br>\n<b><span class=\"orange-text\">Cuídate siempre, eres la única persona en el mundo sin la cual no puedes vivir.</b></span>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti</big></span>\n<br></br>\n<br></br>\n<br></br>\nSígueme por Instagram/Facebook @dottoressaorlenazotti\n', 3, 1),
(4, 'LINK', '<a href=\"url del link\" target=\"_blank\">TESTO VISIBILE</a>', 1, 0),
(5, 'Contatto ricevuto dal sito', 'Ciao VAR_NOME_CLIENTE,\nti ringraziamo per averci contattato.', 1, 2),
(6, 'Contacto recibido de la web', 'Ciao VAR_NOME_CLIENTE, gracias por contactarnos.', 3, 2),
(9, 'Alimenta corpo e anima', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nIl programma<i><strong> “Alimenta corpo e anima 3”</i></strong>, si svolge in sessioni individuali online. Ha una durata di 4 mesi ed è suddiviso in 5 moduli. \n<br></br>\nLe sessioni online  avranno una cadenza di 15 giorni ed una durata da 60 a 90 minuti ciascuna. \n<br><br>\nIl contatto avverrà tramite <strong>Skype:<big> sei essenzialmente bella.</strong></big> Ti consiglio di aggiungere il contatto alla tua lista così lo avrai a portata di mano quando cominceremmo.\n<br></br>\n<u>Contenuto: </u>\n<br></br>\n<strong>Modulo 1 (2 sessioni)</strong>\n<br></br>\nCarboidrati, lipidi e proteine:\n<br></br>\n<ul>\n<li>Come agiscono nell’organismo.</li>\n<li>Quali sono i benefici.</li>\n<li>Piatto consapevole e salutare </li>\n<li>Gruppi di alimenti </li>\n<li>Esperimenti e consigli</li>\n</ul>\n<br></br>\n<strong>Modulo 2 (2 sessioni)</strong>\n<br></br>\nFondamentalmente parleremo dei cibi dell’anima. Individueremo l’area della tua vita che ti genera insoddisfazione e definiremo come potresti iniziare ad agire per ottenere un risultato diverso. \n<br></br>\n“Non puoi risolvere un problema con lo stesso tipo di pensiero che hai usato per crearlo.” Albert Einstein.\n<br></br>\n<strong>Modulo 3 (2 sessioni)</strong>\n<br></br>\nImparerai a vedere il cibo che mangi in maniera differente, saranno i primi passi per iniziare a mangiare con consapevolezza.\nParleremmo di energia: Quali alimenti ti possono aiutare ad incrementare l’energia e quali invece la riducono.\n<br></br>\nVedremmo come puoi migliorare la tua digestione e come potresti ottenere benefici dai superfood.\n<br></br>\nConoscerai gli additivi alimentari.\n<br></br>\nDiventerai consapevole di come si sviluppa l’obesità ed il sovrappeso.\n<br></br>\n<strong>Modulo 4</strong>\n<br></br>\nLa relazione che intercorre tra consapevolezza piena e alimentazione e i suoi benefici.\n<br></br>\n<strong>Modulo 5</strong>\n<br></br>\nCome leggere le etichette degli alimenti. Conoscere come è fatto un prodotto non solo è importante nella cosmesi ma anche in quello che mangi, perché alla fine l’obbiettivo principale di questa azione è nutrire le nostre cellule.\n<br></br>\nPurificherai e disintossicherai il tuo organismo.\n<br></br>\n<big><b>In regalo con il programma avrai:</big></b>\n<br></br>\na)	4 Ebooks: Osa e crea / Crostata autunnale / Risveglio energetico / Nettalingua\n<br></br>\nb)	Ricette varie per implementare le novità\n<br></br>\nIl programma si compone di una parte teorica, per capire come funzionano gli alimenti nel nostro corpo, ed una parte pratica per imparare a riconoscere come il tuo corpo risponde agli alimenti.\nLe sessioni sono individuali e le date saranno programmate prima d’iniziare il percorso. Eventuali variazioni della data e dell’ora dovrai comunicarmele con un anticipo di almeno 48 ore, per poter riprogrammare la sessione.\n<br></br>\n<b>Il pagamento avverrà tramite PayPal</b>: orlena.zotti@gmail.com\n<br></br>\nIl costo di questo programma, pagando in un\'unica soluzione, è di 582 Euro.\n<br></br>\nHai l’opzione di poter frazionare il pagamento in 4 rate, il cui pagamento dovrà essere effettuato prima di iniziare ogni modulo. Se non viene saldata la rata non si potrà accedere al modulo. La quarta rata permette l’accesso al 4° e 5° modulo.\n<br></br>\n<ul>\n<li>1° = 220</li>\n<li>2° = 141</li>\n<li>3°= 141</li>\n<li>4°= 141</li>\n</ul>\n<br></br>\nSpero che le informazioni siano state complete e chiare.\n<br></br>\n<b><span class=\"orange-text\">Desidero per te una giornata piena d’ispirazione!</b></span>\n<br></br>\n<span class=\"blu-text\">Dottoressa Orlena Zotti.</span>\n\n\n\n\n', 1, 0),
(10, 'Nutre cuerpo y alma', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\n<br>El programa <i><b>“Nutre corpo e anima 3”</i></b> se lleva a cabo en sesiones individuales online. Tiene una duración de 4 meses y un contenido dividido en 5 módulos.</br>\n<br>Cada 15 días realizaremos sesiones que durarán entre 60 a 90 minutos.</br>\n<br>Las sesiones se realizarán a través de <strong>Skype:<big> sei essenzialmente bella.</strong></big> Te aconsejo de agregar el contacto en tu lista, así lo tendrás a la mano cuando iniciemos.</br>\n<br></br>\n<u>Contenido:</u>\n<br></br>\n<b>Módulo 1 (2 sesiones)</b>\n<br></br>\nCarbohidratos, lípidos y proteínas:\n<br></br>\n<ul>\n<li>¿Cómo funcionan en el cuerpo?</li>\n<li>¿Cómo te benefician?</li>\n<li>Un plato consciente y saludable</li>\n<li>Grupos de alimentos</li>\n<li>Experimentos y consejos</li>\n</ul>\n<br></br>\n<b>Módulo 2 (2 sesiones)</b>\n<br></br>\nBásicamente hablaremos sobre los alimentos del alma. El área de tu vida donde vives desde la insatisfacción y cómo podrías empezar a actuar para obtener un resultado diferente.\n<br></br>\n\"No se puede resolver un problema con el mismo tipo de pensamiento que se ha usado para crearlo\". Albert einstein.\n<br></br>\n<b>Módulo 3 (2 sesiones)</b>\n<br></br>\nAprenderás a ver de una manera diferente  los alimentos que comes, serán los primeros pasos para comenzar una alimentación consciente.\n<br></br>\nHablaremos de energía: ¿qué alimentos pueden ayudarte a aumentar la energía y cuáles pueden disminuirla?\n<br></br>\n¿Cómo puedes mejorar su digestión y cómo puedes obtener los beneficios de los superalimentos?\n<br></br>\nAditivos alimentarios.\n<br></br>\n¿Por qué se desarrollan la obesidad y el sobrepeso?\n<br></br>\n<b>Módulo 4</b>\n<br></br>\nEstar presente cuando te alimentas y los beneficios que te brinda.\n<br></br>\n<b>Módulo 5</b>\n<br></br>\n¿Cómo leer una etiqueta y para qué te sirve? Leer como es hecho un producto no sólo es importante en la cosmética, también debes crear consciencia sobre la forma en la que estás alimentando tu cuerpo, porque al final el objetivo principal de alimentación es nutrir nuestras células.\n<br></br>\nPrepárate para purificar y desintoxicar tu cuerpo.\n<br></br>\n<b><big>El programa tiene estos regalos para ti:</b></big>\n<br></br>\na) 4 Ebooks: Atrévete y crea / Despierta energía / Limpiador de lengua/ Alimentos que rejuvenecen\n<br></br>\nb) Varias recetas para implementar las noticias\n<br></br>\n\nEl programa consta de una parte teórica, para entender como funcionan los alimentos en nuestro cuerpo (global) y una parte práctica, de esta manera, aprenderá a reconocer cómo responde tu cuerpo a través de lo que comes (individual).\n<br></br>\nComo las sesiones son individuales, las fechas se programarán antes de comenzar el recorrido. En el momento en el cual se deban presentar modificaciones en los días y horarios preestablecidos se debe avisar con un tiempo de anticipación de 48 horas, para la reprogramación. \n<br></br>\n<strong>El pago se realiza por PayPal:</strong> orlena.zotti@gmail.com\n<br></br>\nEl costo de este programa en pago único es de 582 euros.\n<br></br>\nTambién tienes la opción de hacer el pago en cuotas, éstas serán pagadas antes de comenzar cada módulo, si no se cancela la cuota no será enviado el módulo. La 4° cuota permite el acceso al 4to y 5to modulo.\n<br></br>\n<ul>\n<li>1º = 220 </li>\n<li>2º = 141 </li>\n<li>3º = 141 </li>\n<li>4° = 141 </li>\n</ul>\n<br></br>\nSi queda algún tipo de dudas, puedes responder este email.\n<br></br>\n<strong><span class=\"orange-tect\">¡Te deseo un día lleno de inspiración!</strong></span>\n<br></br>\n<span class=\"blu-text\">Dottoressa Orlena Zotti.</span>\n\n\n\n\n\n', 3, 0),
(11, 'La tua valutazione online', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nti spiego come funziona il programma più indicato per te.\n<i><b>\"La tua valutazione online\"</i></b> è un programma che viene offerto alle persone che sentono il desiderio di conoscere cosa potrebbero fare per migliorare ciò che non soddisfa sia del proprio volto che del corpo.\n<br></br>\nIn più riceverai come bonus un video con diversi esercizi di yoga facciale che ti aiuteranno a tonificare la tua pelle.\nGli esercizi saranno divisi per zone e ti sarà spiegato come abbinarli, il tempo a dedicarli, come realizzarli e a cosa servono.\n<br></br>\nLa seduta avverrà tramite <strong><big> Zoom.</big></strong> Ti invierò il link di collegamento il giorno della sessione.\n<br></br>\nDue giorni prima di iniziare la visita dovrai inviare le foto del volto e/o del tuo corpo, a seconda dell’area di cui desideri parlare. \n<br></br>\n<b>Le foto del volto devono essere scattate:</b>\n<br></br>\n<ul>\n<li>con buona luce \n<li>1 foto di fronte\n<li>1 foto per ogni profilo\n<li>deve essere visibile tutto il volto\n</ul>\n<br></br>\n<b>Le foto del corpo, a seconda della parte interessata, devono essere scattate:</b>\n<br></br>\n<ul>\n<li>addome</li>\n<br>un’immagine frontale ed una per ogni profilo. Se si tratta di rilassamento della cute addominale, la foto deve essere scattata con il torace piegato un po’ in avanti, in modo che il rilassamento si possa apprezzare meglio.</br>\n<li>le gambe e glutei </li>\n<br>ogni profilo (sia frontale che laterale)</br>\n</ul>\n<br></br>\n<b>La foto di una altra zona specifica che vorresti trattare.</b>\n<br></br>\nLa visita durerà 40 - 60 minuti e dopo aver valutato la tua situazione ti indicherò quali sono i trattamenti che potresti effettuare, ti spiegherò in cosa consistono e ti consiglierò i prodotti che si possono utilizzare nel tuo caso.  In questo modo sarai più informata e ti sentirai più tranquillità nel momento in cui sceglierai il tuo medico estetico per il trattamento. Se posso indirizzarti da un collega che si trovi nella tua zona lo farò con vero piacere.\n<br></br>\n<big><a href=\"https://drive.google.com/drive/folders/1IasP80NDC2Sq_7ukbof_VmNdAh4Kr2CU?usp=sharing\">Cliccando QUA</a></big> troverai esempi di come dovresti scattare le foto.\n<br></br>\n<u>Il tuo investimento: 66 Euro.</u>\n<br></br>\nIl pagamento deve essere effettuato <b>almeno 24 ore prima della sessione</b> di valutazione tramite PayPal all’indirizzo: orlena.zotti@gmail.com  \n<br></br>\nSe non hai PayPal sicuramente troveremo una soluzione.\n<br></br>\nSe hai dubbi puoi rispondere a questa email.\n<br></br>\n<strong><span class=\"orange-text\">Ricorda che il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.</strong></span>\n<br></br>\n<span class=\"blu-text\">Dottoressa Orlena Zotti.</span>\n\n\n\n', 1, 0),
(12, 'Evaluación online', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\n<i><b>“Evaluación online”</i></b> es un programa que se ofrece a las personas que no pueden realizar una consulta presencial, pero que tienen el deseo de saber lo que podrían hacer para mejorar ese algo que no le gusta, ya sea la cara o el cuerpo.\n<br></br>\nAdemás, recibirás como bonus un video con diferentes ejercicios de yoga facial que te ayudarán a rejuvenecer tu piel.\nLos ejercicios se dividirán por zonas y se te explicará cómo combinarlos, el tiempo a dedicarles, cómo hacerlos y para que te sirven.\n<br></br>\nLa evaluación sera realizada por <b><big>Zoom.</big></b> El link te lo enviaré el día de la sesión. \n<br></br>\nDos días antes de la consulta, deberás enviar una foto de tu rostro y / o  cuerpo, según la zona de la cual desee hablar.\n<br></br>\n<b>La foto de la cara debe ser tomada:</b>\n<br></br>\n<ul>\n<li>Con luz</li>\n<li>1 foto frontal</li>\n<li>foto para cada perfil</li>\n<li>Se debe ver toda la cara</li>\n</ul>\n<br></br>\n<b>La foto del cuerpo debe ser tomada de manera que sea vea toda la zona:</b>\n<br></br>\n<ul>\n<li>abdomen</li> \n<br>frontal y cada lado. Si tu situación es falta de tonicidad de la piel abdominal, la foto debe tomarse con el tronco inclinado un poco hacia adelante para que se pueda apreciar mejor la poca tonicidad.</br>\n</ul>\n<br></br>\n<ul>\n<li>piernas y glúteos </li>\n<br>foto de cada perfil.</br>\n</ul>\n<br></br>\n<b>La foto de un área específica que quisieras tratar.</b>\n<br></br>\nLa consulta durará entre 40-60 minutos, y después de evaluar tu situación, te hare saber cuáles son los tratamientos que se pueden realizar, te voy a explicar en qué consisten y te diré los nombres de productos que se pueden encontrar a nivel mundial. De esta forma, estarás informada y te sentirás más tranquila al elegir tu médico estético tratante. Y si está en mis manos te pondré en contacto con un colega que está en tu zona geográfica.\n<br></br>\n<bi><a href=\"https://drive.google.com/open?id=1IasP80NDC2Sq_7ukbof_VmNdAh4Kr2CU\">Haciendo click  AQUI,</big></a> encontrarás ejemplos de cómo debes tomar las fotos.\n<br></br>\n<u>La inversión: 66 euros.</u>\n<br></br>\nEl pago se debe<b> 48 horas antes de la sesión</b> a través de PayPal a la dirección: orlena.zotti@gmail.com\n<br></br>\nSi no tiene PayPal, seguramente encontraremos una solución.\n<br></br>\nSi aún tienes dudas, puede responder a este correo electrónico.\n<br></br>\n<b><span class=\"orange-text\">Recuerda que la mejor inversión que puedes hacer es en ti misma porque no hay nada que tenga más valor que tú.</b></span>\n<br></br>\n<span class=\"blu-text\">Dottoressa Orlena Zotti.</span>\n\n\n\n', 3, 0),
(15, 'Newsletter febrero 2018', 'Ciao, <b>VAR_NOME_CLIENTE</b>\n<br></br>\nParliamo di armonia:\n<br></br>\nPossiamo accettare ciò che ci capita con una sorta di rassegnazione o fare in modo che quello che ci rende felici accada nella nostra vita. Possiamo anche vivere con entusiasmo o sconforto, con allegria o tristezza. Il punto è quello di essere consapevole che la scelta è sempre nostra e come vogliamo vivere la nostra vita dipende solo da noi.\n<br></br>\nInizia a vedere la vita in modo diverso da quello che sei abituata, inizia ad uscire da quello stato di zombi dove ti hanno insegnato che le cose non si mettono in discussione. Non esiste una verità assoluta, l’unica verità assoluta che esiste sta dentro di te perché solo tu sai che cosa ti aiuta a vibrare alto e in armonia.\n<br></br>\nCome già sai uno degli strumenti che uso per dare armonia, in questo caso alla parte fisica del nostro essere, è la medicina estetica. Il botulino è uno dei trattamenti che più mi piace per dare quella apparenza riposata al volto. \n<br></br>\nTi domanderai come funziona. Ha una durata di 90 giorni nell’organismo ma il risultato di questo trattamento dura da 4 a 6 mesi. Ti spiego, la funzione del botulino è quella di diminuire la forza di contrazione dei muscoli mimici, dopo che il botulino viene eliminato dall’organismo, il rilassamento muscolare si mantiene grazie in parte alla memoria cellulare. \n<br></br>\nIl botulino non gonfia e non si usa per riempire, il suo utilizzo si basa soltanto sulla diminuzione della contrazione dei muscoli mimici.\n<br></br>\nPer sapere di più sul botulino puoi premere <span class=\"orange-text\"><a href=\"https://www.seiessenzialmentebella.com/it/trattamenti/3/botox\">QUI.</a></span>\n<br></br>\nAttraverso quello che mangi puoi raggiungere la tua giusta proporzione di armonia del tuo corpo tramite la rigenerazione cellulare.\n<br></br>\nIl mangiare è uno degli atti attraverso il quale il nostro corpo si mantiene giovane, con energia e in salute, perché le cellule dell’organismo prendono quello che serve per la sua rigenerazione da ciò che mangiamo. Lo sapevi che le cellule si rigenerano tutti i giorni? Compromettiti con te stessa e fai il primo passo per avere una alimentazione consapevole.\n<br></br>\nSe ti interessa sapere di più, ti posso aiutare e ti invito a conoscere il programma <strong>“Nutre corpo e anima 3”</strong> facendo clic <span class=\"orange-text\"><a href=\"https://www.seiessenzialmentebella.com/it/programmi/4/nutre-corpo-e-anima-3\">QUI.</a></span>\n<br></br>\nVivi in armonia in tutti i tuoi corpi: fisico, mentale, emozionale, spirituale ed energetico.\n<br></br>\n<span class=\"orange-text\"><b>Desidero per te una giornata piena d’ispirazione e armonia.</a></b>\n<br></br>\n<span class=\"blu-text\">Dottoressa Orlena Zotti.</a>\n \n\n', 1, 0),
(16, 'Boletín Marzo 2018', 'Hola, <b>VAR_NOME_CLIENTE</b>\n<br></br>\nHablemos de armonía:\n<br></br>\nPodemos aceptar lo que nos sucede con una especie de resignación o asegurarnos de que aquello que nos hace felices suceda en nuestras vidas. También podemos vivir con entusiasmo o desaliento, con alegría o tristeza. El punto es ser consciente de que la elección siempre es nuestra y que la forma en que queremos vivir depende solo de nosotras.\n<br></br>\nComienza a ver la vida de forma diferente a lo que estás acostumbrada, comienza a salir de ese estado zombi donde te enseñaron que las cosas no se cuestionaban. No hay una verdad absoluta, la única verdad absoluta que existe está dentro de ti porque solo tú sabes lo que te ayuda a vibrar alto y en armonía.\n<br></br>\nComo ya sabes, una de las herramientas que utilizo para dar armonía, en este caso, a la parte física de nuestro ser, es la medicina estética. El Botox es uno de los tratamientos que más me gusta para darle esa apariencia relajada y reposada al rostro.\n<br></br>\nTe preguntarás cómo funciona. El producto dura 90 días en el cuerpo pero el resultado de este tratamiento dura de 4 a 6 meses. Te explico, la función del Botox es disminuir la fuerza de contracción de los músculos mímicos, después de que el Botox es eliminado del cuerpo, la relajación muscular se mantiene gracias en parte a la memoria celular.\n<br></br>\nEl Botox no te hincha y no se usa para rellenar, su uso se basa únicamente en la disminución de la contracción de los músculos mímicos.\n<br></br>\nPara saber más sobre este tratamiento, puede presionar <a href=\"https://www.seiessenzialmentebella.com/es/tratamientos/3/botox\">AQUÍ.</a>\n<br></br>\nGracias a lo que comes puedes alcanzar la proporción correcta de armonía en tu cuerpo a través de la regeneración celular.\n<br></br>\nComer es uno de los actos por los cuales nuestro cuerpo se mantiene joven, enérgico y saludable, porque las células del organismo toman de lo que comemos lo que se necesita para su regeneración. ¿Sabías que las células se regeneran todos los días? Comprométete y da el primer paso para tener una alimentación consciente.\n<br></br>\nSi quieres saber más, te puedo ayudar invitándote a que conozcas el programa <strong>\"Nutre corpo e anima 3\"</strong> haciendo clic <a href=\"https://www.seiessenzialmentebella.com/es/programas/4/nutre-corpo-e-anima-3\">AQUÍ.</a>\n<br></br>\nVive en armonía con todos tus cuerpos: físico, mental, emocional, espiritual y  energético.\n<br></br>\n<b><span class=\"orange-text\">Te deseo un día lleno de inspiración y armonía.</b></span>\n<br></br>\n<span class=\"blu-text\">Dra. Orlena Zotti.</span>\n', 3, 0),
(17, 'Formulario', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nQuesto messaggio è rivolto alle donne che si vogliono sentire bene con sé stesse. Sto creando un nuovo metodo in medicina estetica per accompagnarle a raggiungere questo obiettivo.\n<br></br>\nIo sono Orlena Zotti, medico estetico, mi piace aiutare le donne a sentirsi bene dentro la loro propria pelle attraverso il rispetto di sé stesse e la consapevolezza del loro valore.\n<br></br>\nCompilare questo formulario ti richiederà 5 minuti del tuo tempo, in cambio riceverai una consulenza gratuita online di 30 minuti, scegliendo il tema che preferisci che troverai dentro il formulario.\n<br></br>\nUna volta compilato mi metterò in contatto con te per organizzare il tuo appuntamento.\n<br></br>\nGrazie per il tuo supporto.  \n<br></br>\nFai clic <a href=\"https://docs.google.com/forms/d/1SgH2cNgT8V2kT_hpm7uKVNopjRIvcPVMcLnYR5G5H9U/edit?usp=sharing\"><strong>QUI</a></strong>\n<br></br>\n<span class=\"orange-text\"><big><b>Desidero per te una giornata piena d\'ispirazione!</span></big></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti</span></big>\n\n', 1, 0),
(18, 'Aprile 2018', 'Ciao, <b>VAR_NOME_CLIENTE</b>\n<br></br>\n<br>Un esercizio che faccio molto frequentemente è quello di respirare profondamente, con ispirazioni lunghe e profonde ed espirazioni leggere, buttando completamente l’aria che si trova dentro di me. In questo modo e particolarmente quando sono in uno stato di ansia, stress o rabbia mi do la opportunità di ascoltare cosa sento e come lo sento.</br>\n<br>T’invito a respirare veramente, piano piano potrai notare dei cambiamenti nella tua vita, cambiamenti che girano intorno alla serenità, a dare una risposta più tranquilla e amorosa perché sarai consapevole delle tue emozioni e potrai gestirle in modo più accurato senza la necessità di reagire.</br>\n<br>Ci sono tante ragioni per cui ti potresti sentire già stanca alle 11:00 del mattino, e si, ancora ti mancano tante ore per arrivare a fine giornata.</br>\n<br>L’alimentazione è molto importante per aiutarti ad avere l’energia di cui hai bisogno è anche importante per aiutare a rigenerare le tue cellule, sopra tutto in questa stagione, meravigliosa primavera, ma passare dal freddo al caldo non è poi così bello per il corpo perché se non sei equilibrata emotivamente e hai un sistema immunologico debole, sicuramente sarai vittima di più di un raffreddore.</br>\n<br>Perciò ti lascio 3 consigli:</br>\n<ul>\n<li>Per la energia: fai colazione con carboidrati complessi, questi si assorbono piano piano, dandoti l’energia di cui hai bisogno durante la giornata. Pranza leggero, niente che appesantisca la tua digestione, certamente dovresti essere a conoscenza di quali alimenti digerisci bene e quali no. Elimina/riduci gli zuccheri.</li>\n<li>Consuma alimenti ad alto contenuto di vitamina C, in ordine decrescente: peperoni rossi, kiwi, prezzemolo, fragola, papaya, arance, limone, melone, pompelmo, mandarini, mango.</li>\n<li>Consuma alimenti che contengo zinco, è un minerale che fortifica il sistema immunologico aumentando la produzione di globuli bianchi per combattere le infezioni in modo più efficace. Frutti secchi, legumi, cereali integrali, ostriche, carne rossa. </li>\n</ul>\n<br></br>\nSe vuoi avere una alimentazione più consapevole e soprattutto come aiutare il tuo corpo a sentirsi bene inizia il mio programma <a href=\"https://www.seiessenzialmentebella.com/it/programmi/4/nutre-corpo-e-anima-3\">“Nutre corpo e anima 3”</a> attraverso il quale ti guiderò e aiuterò ad avere un’alimentazione intelligente e soprattutto consapevole. \n<br></br>\nNella prossima newsletter ho deciso di scrivere su un argomento che mi sembra molto importante e di cui non si ha tanta conoscenza. \n<br></br>\nCome stai nutrendo la tua pelle? Quali cosmetici stai usando? Sei consapevole di come sono fatti quasi tutti i cosmetici? Sei attenta a quello che compri anche in erboristeria?\n<br></br>\n<span class=\"orange-text\"><big><b>Desidero per te una giornata piena d’ispirazione!</span></big></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti.</span></big>\n', 1, 0),
(19, 'Abril 2018', 'Ciao, <b>VAR_NOME_CLIENTE</b>\n<br></br>\nUn ejercicio que hago muy frecuentemente es respirar profundamente, con inspiraciones largas y profundas y exhalaciones ligeras, expulsando completamente el aire que está dentro de mí. De esta manera, y especialmente cuando estoy en un estado de ansiedad, estrés o enojo, me doy la oportunidad de escuchar lo que siento y cómo lo siento.\n<br></br>\nTe invito a respirar realmente, lentamente notarás cambios en tu vida, cambios que giran en torno a la serenidad y en dar una respuesta más pacífica y amorosa porque estarás al tanto de tus emociones y podrás manejarlas con mayor facilidad sin la necesidad de reaccionar.\n<br></br>\nHay tantas razones por las que te puedes sentir cansada ya a las 11:00 am. Y sí, todavía tienes tantas horas para llegar al final del día.\n<br></br>\nLa nutrición es muy importante para ayudarte a tener la energía que necesitas, también es importante para ayudar a regenerar tus células, sobre todo en esta temporada, maravillosa primavera, donde pasar del frío al calor no es tan agradable para el cuerpo, porque si no estás emocionalmente equilibrada y tienes un sistema inmune débil, seguramente serás víctima de más de un resfriado.\n<br></br>\nAsí que te dejo 3 consejos:\n<br></br>\n<ul>\n<li>Para obtener energía: has el desayuno con carbohidratos complejos, se absorben lentamente, dándote la energía que necesitas durante el día. Almuerza ligero, no comas nada que dificulte tu digestión, sin duda debes saber qué alimentos digieres bien y cuáles te cuesta un poco más. Eliminar / reducir azúcares.</li>\n<li>Consuma alimentos ricos en vitamina C, en orden descendente: pimientos rojos, kiwi, perejil, fresa, papaya, naranjas, limón, melón, pomelo, mandarinas, mango.</li>\n<li>Consume alimentos que contengan zinc, es un mineral que fortalece el sistema inmunitario al aumentar la producción de glóbulos blancos para combatir las infecciones de manera más efectiva. Se encuentra en las frutas secas, legumbres, cereales integrales, ostras, carne roja.</li>\n</ul>\n<br></br>\nSi quieres tener una dieta más consciente y sobre todo cómo ayudar a tu cuerpo a sentirse bien, comienza mi programa <a href=\"https://www.seiessenzialmentebella.com/es/programas/4/nutre-corpo-e-anima-3\">\"Nutre corpo e anima 3\"</a> a través del cual te guiaré y te ayudaré a tener una dieta inteligente y sobre todo conciente.\n<br></br>\nEn el próximo boletín decidí escribir sobre un tema que me parece muy importante y sobre el cual no hay mucho conocimiento.\n<br></br>\n¿Cómo estás alimentando tu piel? ¿Qué cosméticos estás usando? ¿Eres conciente de cómo están hechos los cosméticos que utilizas? ¿Estás atenta a los cosméticos que compras en negocios biológicos?\n<br></br>\n<span class=\"orange-text\"><big><b>¡Te deseo un día lleno de inspiración!</span></big></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti.</span></big>\n', 3, 0),
(20, 'Newsletter Maggio 2018', 'Ciao, <b>VAR_NOME_CLIENTE</b>\n<br></br>\nIntroducendo un argomento che mi sembra molto importante, ti scrivo oggi della consapevolezza che dovresti avere su quali cosmetici applichi per aiutare la tua pelle ad equilibrarsi e così difendersi. Lei ha bisogno solo di buoni prodotti che l’aiutino a mantenere il suo equilibrio e così autodifendersi dalla aggressività dell’ambiente e dal processo d’ossidazione che accade all’interno dell’organismo e che forma i radicali liberi. \n<br></br>\nPer questo oggi ti aiuto ad acquisire conoscenza sui prodotti di sintesi che puoi trovare in un cosmetico; questi sono: paraffine, siliconi, acrilici, derivati del petrolio. Esatto! Hai letto benissimo: derivati del petrolio. Ma c’è di più, perché non li trovi scritti così sulla lista d’ingredienti. Ti faccio vedere solo una parte di alcuni nomi estranei che potresti trovare sulla etichetta:\n<font color=\"red\">Paraffinum Liquidum, Mineral Oil, Polysobutene Hydrogenated, Ceresin, Isododecane, Isohexadodecane, Hydrogenated Polydecene, Propylene Glycol, Dicaprylate/Dicaprate Neopentyl Glycol, Carbomer, Gliceracrilati, Acrylates Copolymer, Ammonium Acrylates Copolymer,Dibutyl Lauroyl Glutamide, Butylene, Ethylene, Styrene. </font></br>\n<br></br>\nLoro interferiscono con la capacità che ha la pelle di adattarsi all’ambiente esterno e difendersi dai radicali liberi e logicamente dai raggi solari.\n<br></br>\nLa pelle deve traspirare assecondando l’ambiente in cui si trova, quando applichi cosmetici contenenti prodotti di sintesi, stai creando una pellicola sulla pelle facendo intrappolare l’acqua in questo prodotto di sintesi, in modo tale che la percezione della pelle è: “sono idratata” trascurando il funzionamento della barriera cutanea; questo origina: pori dilatati, macchie, invecchiamento precoce, infiammazione, ecc.\n<br></br>\nAttualmente esistono sul mercato prodotti bio-compatibili con la nostra pelle. Inizia ad essere più consapevole di come stai nutrendo l’organo più esteso del corpo, il tuo involucro, la tua protezione.\nProteggi la tua pelle come la tua pelle protegge te!\n<br></br>\nSe vuoi approfondire questo argomento e avere la capacità di:\n<ul>\n<li>scegliere un buon cosmetico;</li>\n<li>sapere che prodotti puoi trovare sul mercato, almeno quelli che con me hanno funzionato;</li>\n<li>sapere come trattare le macchie attraverso la alimentazione e l’utilizzo di un trattamento cosmetico adeguato, inviami una email.</li>\n</ul>\n<br></br>\nQuesto programma viene offerto solo alle mie pazienti e alle donne iscritte nella newsletter, non lo troverai sul sito web. Per avere un\'idea del programma, ti invito a cercare nella mia bio di Instagran o su Facebook il regalo che ho preparato per te.\n<br></br>\n<span class=\"orange-text\"><big><b>Desidero per te una giornata piena d’ispirazione!</span></big></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti.</span></big>\n', 1, 0),
(21, 'Boletin Mayo 2018', 'Hola, <b>VAR_NOME_CLIENTE</b>\n<br></br>\nIntroduciendo un tema que me parece muy importante, te escribo hoy sobre el conocimiento que deberías tener de los cosméticos que aplicas en tu piel. Ella necesita solo buenos productos que lo ayuden a mantenerse en equilibrio y, por lo tanto, auto defenderse de la agresividad del entorno y el proceso de oxidación que se produce dentro del cuerpo y del cual se originan los radicales libres.\n<br></br>\nEs por esto que hoy te ayudo a adquirir conciencia sobre productos sintéticos que puedes encontrar en un cosmético; estos son: parafinas, siliconas, acrílicos, derivados del petróleo. Exactamente! Leíste muy bien: derivados del petróleo. Pero hay más, porque no los encuentras escritos así en la lista de ingredientes. Te mostraré solo una parte de algunos nombres muy extraños que puedes encontrar en la etiqueta:\n<font color=\"red\">Paraffinum Liquidum, Mineral Oil, Polysobutene Hydrogenated, Ceresin, Isododecane, Isohexadodecane, Hydrogenated Polydecene, Propylene Glycol, Dicaprylate/Dicaprate Neopentyl Glycol, Carbomer, Gliceracrilati, Acrylates Copolymer, Ammonium Acrylates Copolymer,Dibutyl Lauroyl Glutamide, Butylene, Ethylene, Styrene.</font></br>\n<br></br>\nEllos interfieren con la capacidad de la piel para adaptarse al medio externo y para defenderse de los radicales libres y lógicamente de los rayos del sol.\n<br></br>\nLa piel debe respirar de acuerdo con el entorno donde se encuentra, cuando aplicas cosméticos que contienen productos sintéticos, éstos crean una capa muy delgada en la piel haciendo que el agua quede atrapada en este producto sintético, de modo que la percepción de la piel es: \" estoy hidratada” descuidando así el funcionamiento de la barrera cutánea; esto origina: poros dilatados, manchas, envejecimiento prematuro, inflamación, etc.\n<br></br>\nActualmente hay productos en el mercado biocompatibles con tu piel. Comienza a ser más conciente de cómo alimentas el órgano más grande del cuerpo, tu revestimiento, tu protección.\n<br></br>\n¡Protege tu piel como tu piel te protege a ti!\n<br></br>\nSi desea profundizar en este tema y tener la capacidad de:\n<ul>\n<li>elegir un buen cosmético;</li>\n<li>saber qué productos puedes encontrar en el mercado, al menos aquellos que me han funcionado a mi;</li>\n<li>Saber cómo puedes tratar las manchas a través de la alimentación y de la aplicación de un tratamiento cosmético adecuado, envíame un email.</li>\n</ul>\n<br></br>\nEste programa lo estoy ofreciendo solo a mis pacientes y a las chicas registradas en el boletín, no lo encontrarás en el sitio web. Para que tengas una idea de lo que trato en el programa te invito a buscar  en mi bio de Instagran y en facebook  el regalo que preparé para ti.\n<br></br>\n<span class=\"orange-text\"><big><b>¡Te deseo un día lleno de inspiración!</span></big></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti.</span></big>\n', 3, 0),
(22, 'Boletin Junio', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\nHemos llegado a la estación del sol, del mar y el bronceado de tu piel. Me gustaría decirte solo dos cosas sobre este discurso:\n<br></br>\n<span class=\"blu-text\"><b>1. Si tienes manchas,</span></b> no te vuelvas loca poniéndote bloqueador solar, atención con esto, no estoy diciendo que no tengas que ponértelo, pero si crees que el protector solar evitará que las manchas empeoren, esto está mal.\n<br></br>\nMe gustaría iluminarte en una cosa. Las manchas generalmente tienen dos componentes, dérmica y epidérmica, es decir, profunda y superficial. En la dermis hay vasos sanguíneos, cuando la vasodilatación ocurre debido al calor producido por la exposición al sol, esta vasodilatación estimula la melanina acumulada de forma irregular en la dermis haciendo que las manchas cutáneas se vuelvan más evidentes.\n<br></br>\nDigamos que no es tan correcto decir: me aplico la protección, pero las manchas se acentúan más. Ya sabes, después de leer esto, que así no es cómo funciona.\n<br></br>\n<b><big>¿Consejos?</b></big> Sombra, sombra, sombra, y aun así te broncearás. Por lo cual, protege tu cara con un sombrero grande de esos chic que te hacen ver elegante, con grades lentes de sol e, inolvidable, el protector solar.\n<br></br>\nToma en cuenta que si no estás realizando un protocolo con cosméticos dermocompatibles para equilibrar la fisiología de tu piel, siempre serás esclava de los tratamientos anti-machas que funcionan durante el invierno, pero las manchas reaparecen durante el verano.\n<br></br>\n<span class=\"blu-text\"><b>2. Recuerda que demasiado sol</span></b> hace que tu cuerpo produzca radicales libres en exceso y más allá de las arrugas que se formarán más adelante en el tiempo debido a la falta de precauciones, también se produce el envejecimiento de tus células que será muy acelerado exactamente porque no tienes precauciones.\n<br></br>\n<b>¿Cuáles pueden ser las precauciones a tener en cuenta para nutrir tu piel y tus células?</b>\n<br></br>\n<ul>\n<li> Quizás es repetitivo, pero bebe agua, sí, sí, al menos un litro y medio al día.</li>\n<li>Hidrata tu piel, tanto la cara como el cuerpo, tienes dos formas para hidratarla, ambas se potencian:</li>\n<br></br>\n<span class=\"blu-text\"><b>a) Usar cremas humectantes con ingredientes activos eficaces,</span></b> que se encuentren en los primeros 5 lugares de la lista de ingredientes (INCI) y sin derivados del petróleo, acrílicos, etc.\n<br></br>\n<span class=\"blu-text\"><b>b) Realizar tratamientos de medicina estética como la biorestructuración</span></b> que proporciona una hidratación profunda y, al mismo tiempo, dona luz, elasticidad y tono a la piel.\n<br></br>\n<li> Batidos verdes y ricos en antioxidantes. No cuesta nada desayunar con un gran vaso de antioxidantes o al menos agregarlo al desayuno si comes mucho. </li>\n</ul>\n<br></br>\nHaz un buen uso del PDF \"Alimentos para rejuvenecer\" que te envié cuando te inscribiste al boletín.\n<br></br>\n<span class=\"orange-text\">Comienza a proteger tu piel de forma adecuada.</span>\n<br></br>\nTambién te recuerdo que en la biografía de mi Instagram: @dottoressaorlenazotti encontrarás el PDF regalo <i> “Protege tu piel como tu piel te protege a ti”,</i> donde explico de manera introductoria y simple la importancia del uso de cosméticos dermocompatibles.\n<br></br>\nPor último, practica la acción de dar gracias. Esta vez, agradece de otra manera, da gracias por esas cosas que das por sentado pero que te ayudan mucho en tus días.\n<br></br>\nPor ejemplo:\n<br></br>\n<ul>\n<li>Gracias por mis rodillas que me permiten caminar.</li>\n<li>Gracias por cada libro que leo y porque sé leer.</li>\n<li>Agradezco porque todos los días respiro un aire fresco y limpio.</li>\n</ul>\n<br></br>\n<b><i>Ya sabes que el equilibrio consiste en nutrir cuerpo, mente y espíritu.</b></i>\n<br></br>\nEspero que este boletín te inspire a comenzar a hacer algo diferente en tu vida. \n<br></br>\n<span class=\"orange-text\"><big><b>Te deseo un día lleno de inspiración.</span></big></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti.</span></big>\n', 3, 0),
(23, 'Newsletter Junio', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nSiamo arrivati alla stagione del sole, del mare e di prendere un po’ di colore sulla pelle.\nVorrei dirti solo due cose riguardo a questo discorso:\n<br></br>\n<b><span class=\"blu-text\">1.	Se hai delle macchie sulla pelle</b></span> non diventare matta mettendoti la protezione solare, attenzione, non sto dicendo che non la devi mettere ma se pensi che il solare eviterà che le macchie si accentuino, questo è sbagliato. \nVorrei illuminarti riguardo a questo argomento. \n<br></br>\nLe macchie, generalmente hanno due componenti, dermica ed epidermica, cioè profonda e superficiale. Nel derma ci sono dei vasi sanguini, quando si verifica la vasodilatazione per il calore prodotto dall’esposizione solare, questa vasodilatazione stimola la melanina accumulata irregolarmente evidenziando ancora di più le macchie cutanee. \nQuindi anche se metti la protezione le macchie si accentuano di più. \n<br></br>\n<b>Consigli?</b> \n<br></br>\nOmbra, ombra, ombra. Ti  abbronzerai comunque. Proteggi il tuo volto con un capello grande, occhiali da sole e  l’immancabile protezione solare. \n<br></br>\nPrendi in considerazione che se non stai facendo un protocollo con cosmetici dermocompatibili per equilibrare la fisiologia della tua cute, sarai sempre schiava di trattamenti che funzionano durante l’inverno ma le macchie riappaiono durante l’estate. \n<br></br>\n<b><span class=\"blu-text\">2.	Ricordati inoltre che prendere troppo sole</b></span> fa produrre al tuo corpo un eccesso di radicali liberi e, al di là delle rughe che si formeranno più avanti nel tempo, dovrai fare i conti anche con l’invecchiamento cellulare che accelererà proprio per non aver preso delle precauzioni.\n<br></br>\n<b>Quali possono essere le precauzioni da tenere in considerazione per nutrire la tua pelle e le tue cellule?</b>\n<br></br>\n<ul>\n<li>Bere molta acqua, si, si, almeno un litro e mezzo, anche se sembra ripetitivo.</li>\n<li>Idratare la tua pelle, tanto il viso quanto il corpo, attraverso due forme:</li>\n<br></br>\n<span class=\"blu-text\"><b>a)	utilizzando creme idratanti con principi attivi efficaci,</span></b> che si trovino nei primi 5 posti della lista di ingredienti (INCI) e senza derivati del petrolio, acrilici, ecc.\n<br></br>\n<span class=\"blu-text\"><b>b)	praticando trattamenti di medicina estetica come la bioristrutturazione</span></b> che regala una idratazione profonda e allo stesso tempo dona luce, elasticità e tonicità alla cute.\n<br></br>\n<li>Consumare frullati verdi e ricchi in antiossidanti. Non costa niente fare la colazione con un bel bicchierone di antiossidanti o almeno aggiungerlo alla colazione se mangi tanto. Fai un buon uso del file in PDF “Alimenti per ringiovanire” che ti ho regalato al momento dell’iscrizione alla newsletter.</li>\n</ul>\n<br></br> \nTi ricordo anche che nella bio del mio Instagram @dottoressaorlenazotti troverai il PDF “proteggi la tua pelle come la tua pelle protegge te” dove spiego in modo semplice l’importanza dell’uso di cosmetici dermocompatibili.\n<br></br>\nPer ultimo, ma non meno importante,  <b>pratica il ringraziamento.</b> Questa volta ringrazia in modo diverso, ringrazia per quelle cose che dai per scontato ma ti aiutano tanto nelle tue giornate,ad esempio: \n<ul>\n<li>Ringrazio per le mie ginocchia che mi permettono di camminare.</li>\n<li>Ringrazio per ogni libro che leggo.</li>\n<li>Ringrazio perché tutti i giorni respiro un’aria fresca e pulita.</li>\n</ul>\n<br></br>\nSpero che questa newsletter ti serva di ispirazione per iniziare a fare qualcosa di diverso nella tua vita.\n<br></br>\n<span class=\"orange-text\"><b><big>Desidero per te una giornata piena d’ispirazione.</span></big></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti.</span></big>\n', 1, 0),
(24, 'Rigenera la tua pelle', 'Ciao <b>VAR_NOME_CLIENTE</b>.\n<br></br>\nHai chiesto ulteriori informazioni su come funziona il programma <b>“rigenera la tua pelle”</b> e sono qua per approfondire questo argomento.\n<br></br>\nIl programma si svolgerà ONLINE e ha queste caratteristiche: \n<br></br>\n<ul>\n<li>si realizza in gruppo;</li>\n<li>è impartito in un’unica sessione;</li>\n<li>la sessione dura 1 ora e mezza;</li>\n<li>è richiesta la tua presenza ma se non puoi presenziare la registrazione sarà disponibile per 48 ore;</li>\n<li>è interattivo, cioè si possono fare domande;</li>\n<li>verranno praticati esercizi per capire come funziona la metodologia;</li>\n<li>ciò che avrai appreso potrà essere messo in pratica subito dopo la sessione;</li>\n<li>riceverai il materiale di cui avrai bisogno;</li>\n<li>avrai assistenza via email per una settimana.</li>\n</ul>\n<br></br>\nA me piace tantissimo questo programma, perché sono riuscita ad elaborare un qualcosa che è semplice da capire e soprattutto facilissimo da mettere in pratica. \n<br></br>\nTi consiglierò alcuni prodotti che uso e altri che non ho usato ma per cui ho valutato la lista di ingredienti.\n<br></br>\nQuesta sessione dura un’ora e mezza ed è così strutturata:\nUna parte teorica e una parte pratica. \n<br></br>\nNella teoria, si, sempre c’è una teoria, imparerai:\n<br></br>\n<ul>\n<li>cosa significa la compatibilità di un cosmetico con la propria pelle;</li>\n<li>a differenziare quali sono gli ingredienti compatibili con la tua pelle e quali non lo sono;</li>\n<li>quali alimenti e integratori favoriscono la riparazione della tua pelle;</li>\n<li>quali sono le cause più frequenti di disidratazione della pelle (ci scommetto che non ci crederai);</li>\n<li>quali aspetti devi tenere in conto nello scegliere un cosmetico;</li>\n<li>identificare le certificazioni che garantiscono la natura degli ingredienti di un cosmetico.</li>\n</ul>\n<br></br>\nLa parte pratica si basa in ciò che devi fare al momento della scelta dei tuoi cosmetici. Questo ti permetterà di scegliere con consapevolezza quello che usi per curare la tua pelle e se ti consigliano un cosmetico per risolvere una situazione (macchie, rughe, acne, cellulite, ecc.) potrai valutare se veramente può servire a risolvere la situazione o a creare un circolo vizioso. \n<br></br>\n<u>L’investimento: 177€</u> anziché 220€\n<br></br>\nData: 30/07/2018\n<br></br>\nOra: 18:00 (ora Italia)\n<br></br>\n<br>Il pagamento si fa tramite PayPal: orlena.zotti@gmail.com</br>\n<br>Se non hai PayPal sicuramente troveremo una soluzione.</br>\n<br></br>\n<b><span class=\"orange-text\">Ricorda che il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.</b></span>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti.</big></span>\n<br></br>\n<b>P.S: se hai qualche dubbio o domanda, scrivimi! Ti risponderò al più presto.</b>\n', 1, 0);
INSERT INTO `email_templates` (`id_template`, `nome_template`, `testo_template`, `lingua_traduzione_id`, `id_tipo_template_default`) VALUES
(25, 'Regenera tu piel', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\nHas pedido más información sobre cómo funciona el programa <b>\"regenera tu piel\"</b> y estoy aquí para profundizar más sobre este tema.\n<br></br>\nEl programa se llevará a cabo  ONLINE y tiene estas características:\n<br></br>\n<ul>\n<li>se realizará en grupo;</li>\n<li>se efectuará en una sesión;</li>\n<li>el tiempo de la sesión es de 1 hora y media;</li>\n<li>se requiere tu presencia, pero si no puedes presentarte, tendrás 48 horas para ver la grabación;</li>\n<li>recibirás todo el contenido que necesitas;</li>\n<li>es interactivo, puedes hacer preguntas;</li>\n<li>se llevarán a cabo ejercicios para que puedas entender cómo funciona la metodología;</li>\n<li>toda la información se puede poner en práctica inmediatamente después de la sesión;</li>\n<li>asistencia por email durante una semana.</li>\n<br></br>\n</ul>\nMe gusta mucho este programa, porque logré desarrollar algo que es fluido de entender y, sobre todo, muy fácil de poner en práctica.\n<br></br>\nEsta hora y media de sesión lo he estructurado así:\nUna parte teórica y una parte práctica.\n<br></br>\nDurante la teoría, sí, siempre hay una teoría, aprenderás:\n<br></br>\n<ul>\n<li>lo que significa que un cosmético sea compatible con tu piel;</li>\n<li>diferenciar qué ingredientes son compatibles con tu piel y cuáles no;</li>\n<li>qué alimentos y suplementos promueven la reparación de tu piel;</li>\n<li>cuáles son las causas más frecuentes de deshidratación de la piel (apuesto a que estas causas no te han pasado por la mente);</li>\n<li>qué aspectos debes tener en cuenta al elegir un cosmético;</li>\n<li>identificar las certificaciones que aprueban que un cosmético sea compatible con tu piel.</li>\n</ul>\n<br></br>\nLa parte práctica se basa en poner en modo ON lo que debes hacer al elegir tus cosméticos. Esto te permitirá elegir conscientemente lo que usas para cuidar tu piel y si te recomiendan un cosmético para resolver una situación (manchas, arrugas, acné, celulitis) podrás evaluar si realmente es una ayuda para resolver la situación o crear un círculo vicioso.\n<br></br>\n<u>La inversión: € 177</u> en vez de € 220 \n<br></br>\nFecha: 30/07/2018\n<br></br>\nHora: 20:00 - 8:00 pm (hora Italia)\n<br></br>\nEl pago se realiza a través de PayPal: orlena.zotti@gmail.com\n<br></br>\nSi no tiene PayPal, seguramente encontraremos una solución.\n<br></br>\n<b><span class=\"orange-text\">Recuerda que la mejor inversión que puedes hacer es en ti misma porque no hay nada que tenga más valor que tú.</b></span>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti.</big></span>\n<br></br>\n<b>P.D: si tienes alguna duda o pregunta, ¡escríbeme! Te responderé lo antes posible.</b>\n', 3, 0),
(26, 'Rigenera il tuo corpo', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nHai chiesto ulteriori informazioni su come funziona il programma <b>“rigenera il tuo corpo”</b> e sono qua per approfondire questo argomento.\n<br></br>\nIl programma si svolgerà ONLINE e ha queste caratteristiche: \n<br></br>\n<ul>\n<li>due sessioni a distanza di una settimana ogni una, ed è richiesta la tua presenza perché la sessione si svolge in diretta;</li>\n<li>è un programma da fare in gruppo;</li>\n<li>se proprio non puoi assistere, la sessione sarà registrata e disponibile per 48 ore;</li>\n<li>riceverai un workbook con tutte le istruzioni;</li>\n<li>avrai assistenza via email per 2 settimane, dove potranno essere chiariti i tuoi dubbi e fornite risposte alle tue domande;</li>\n<li>durante le sessioni non si potranno porre domande, per questo c’è l’assistenza via email.</li>\n<br></br>\n</ul>\nQuesto programma è una forma semplice per realizzare un detox del tuo organismo, cioè eliminare le tossine accumulate con il passare del tempo e che provocano disequilibrio a livello intestinale promuovendo così l’invecchiamento. La bellezza dipende in parte dalla salute dell’intestino.\n<br></br>\nPrima di finire il detox faremo una seconda sessione dove ti spiegherò cosa devi fare dopo che hai finito la parte disintossicante. Infatti dopo aver “pulito” tutto, si procederà a recuperare le corrette funzioni del tuo organismo, in modo da ottenere i benefici che si cercano.\n<br></br>\n<u>L’investimento: 159€</u> anziché 240€\n<br></br>\nData: 17/08/2018\n<br></br>\nOra: 18:00 (ora Italia)\n<br></br>\nIl pagamento si fa tramite PayPal: orlena.zotti@gmail.com\n<br></br>\nSe non hai PayPal sicuramente troveremo una soluzione.\n<br></br>\n<b><span class=\"orange-text\">Ricorda che il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.</b></span>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti.</big></span>\n<br></br>\n<b>P.S: se hai qualche dubbio o domanda, scrivimi! Ti risponderò al più presto.</b>\n', 1, 0),
(27, 'Regenera tu cuerpo', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\nHas pedido más información sobre cómo funciona el programa <b>\"regenera tu cuerpo\"</b> y estoy aquí para profundizar más sobre este tema.\n<br></br>\nEl programa se llevará a cabo ONLINE y tiene estas características:\n<br></br>\n<ul>\n<li>el tiempo de desarrollo es de un mes y tendrás mi guía a través de dos sesiones. Es importante participar en directa porque así tus preguntas serán respondidas al momento;</li>\n<li>es un programa grupal;</li>\n<li>si no puedes asistir, la sesión será grabada y estará disponible durante 48 horas;</li>\n<li>recibirás un libro de trabajo con todas las instrucciones;</li>\n<li>recibirás asistencia via email durante un mes, de esta forma podrás hacer preguntas o aclarar dudas.</li>\n<br></br>\n</ul>\nEste programa es una forma sencilla de realizar un détox de tu organismo, es decir, eliminar toxinas acumuladas con el paso del tiempo y que provocan un desequilibrio en el intestino, promoviendo así el envejecimiento. Es por eso que la belleza depende en parte de la salud del intestino.\n<br></br>\nAntes de terminar détox haremos una segunda sesión donde explicaré lo que tienes que hacer después de terminar la parte de desintoxicación, porque necesitas hacer algo después de haber \"eliminado\" todo, por lo que estarás haciendo una desintoxicación completa, apropiada y coherente con los beneficios que se buscan.\n<br></br>\n<u>La inversión: €159 inscribiendote antes del 21 de agosto,</u> en vez de €240 \n<br></br>\nFecha:04/09/2018\n<br></br>\nHora: 20:00h - 8:00 pm (hora Italia)\n<br></br>\nEl pago se realiza a través de PayPal: orlena.zotti@gmail.com\n<br></br>\nSi no tiene PayPal, seguramente encontraremos una solución.\n<br></br>\n<b><span class=\"orange-text\">Recuerda que la mejor inversión que puedes hacer es en ti misma porque no hay nada que tenga más valor que tú.</b></span>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti.</big></span>\n<br></br>\n<b>P.D: si tienes alguna duda o pregunta, ¡escríbeme! Te responderé lo antes posible.</b>\n', 3, 0),
(28, 'Mini corso gratuito', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nLo sai che la medicina estetica per me è un concetto di equilibrio. Per questa ragione abbino essa all’uso di cosmetici compatibili con la pelle.\n<br></br>\nMa cosa vuol dire compatibile con la pelle?\n<br></br>\nIn parole semplici, che i cosmetici sono fatti con ingredienti che non interferiscono nella autoregolazione della pelle. Essa ha la capacità di adattarsi ai cambiamenti esterni (sole, freddo, umidità). Quindi quando utilizzi cosmetici che interferiscono con questa capacità, ciò che accade è che pensi di usare una crema per risolvere una situazione e invece la sta provocando. Anzi tante volte riesci a vedere cambiamenti “positivi” in poco tempo ma questo non vuole dire che è la soluzione giusta. Se vuoi una conferma di quello che ti dico, smetti di usare i cosmetici per una settimana e quello che succederà sicuramente ti farà riflettere.\n<br></br>\nAllora passiamo al dunque, ti invito a partecipare al mini corso gratuito dove parlerò di questo argomento, così potrai avere una nuova coscienza sui cosmetici che utilizzi per “nutrire” la tua pelle. \n<br></br>\nE ti dico di più, c’è una soluzione per poter scegliere meglio i cosmetici, da tutto quello che usi per il benessere del tuo volto a ciò che utilizzi per il tuo corpo (includendo deodoranti e shampoo).\n<br></br>\nIl mini corso si svolgerà esclusivamente ONLINE e ha queste caratteristiche:\n<br></br>\n<li>Data: 19/07/2018</li>\n<li>Ora: 19:30 (ora Italiana)</li>\n<li>Durata: 30 min</li>\n<br></br>\n<b>Fa per te se vuoi incrementare la tua conoscienza sui cosmetici che applichi sulla tua pelle.</b>\n<br></br>\nSe proprio non puoi assistere, la sessione sarà registrata e disponibile per 48 ore;\n<br></br>\n<b><big>Il link per partecipare te lo invierò soltanto se rispondi a questa email dicendo che vuoi partecipare al mini corso gratuito.</b></big>\n<br></br>\n<b><span class=\"orange-text\">Ricorda che il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.</b></span>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti.</big></span>\n<br></br>\n<b>P.S: Non tutto quello che è bio o vegan certifica che un cosmetico sia compatibile con la pelle.</b>\n\n\n', 1, 0),
(29, 'Mini curso gratuito', 'Cia <b>VAR_NOME_CLIENTE</b>\n<br></br>\nYa sabes que la medicina estética para mí es un concepto de equilibrio. Por esta razón, la combino con el uso de cosméticos compatibles con la piel. \n<br></br>\n¿Qué significa compatible con la piel? \n<br></br>\nEn términos simples, son aquellos cosméticos que están hechos con ingredientes que no interfieren con la autorregulación de la piel, ya que ésta tiene la capacidad de adaptarse a los cambios externos (sol, frío, humedad, etc). \n<br></br>\nEntonces, cuando usas cosméticos que interfieren con esta capacidad, lo que sucede es que piensas que estás usando una crema para resolver una situación y en cambio la está empeorando y aunque, muchas veces puedes ver cambios \"positivos\" en poco tiempo, esto no significa que la situación se esté resolviendo. Si quieres confirmar esto, deja de usar tus cosméticos por una semana y lo que sucederá seguramente te hará reflexionar.\n<br></br>\n¡Ahora pasemos al punto! \n<br></br>\nTe invito a participar en el mini curso GRATUITO donde hablaré sobre este tema, para que puedas estar más al tanto de los cosméticos que usas para \"nutrir\" tu piel.\n<br></br>\nY te digo más, hay una solución para elegir adecuadamente tus cosméticos, y me refiero a estos como todo lo que utilizas para el bienestar de tu rostro y tu cuerpo (incluidos los desodorantes y champús).\n<br></br>\nEl mini curso es ONLINE y tiene estas características:\n<br></br>\n<li>Fecha: 19/07/2018</li>\n<li>Hora: 20:30 (Hora en Italia)</li>\n<li>Duración: 30 min.</li>\n<br></br>\n<b>Es para ti si quieres incrementar tu nivel de conciencia sobre los cosméticos que aplicas en tu piel.</b>\n<br></br>\nSi no puedes asistir, la sesión será grabada y estará disponible durante 48 horas;\n<br></br>\n<b><big>El enlace para participar te lo enviaré solo si respondes a este email diciendo que deseas participar en el mini curso gratuito.</b></big>\n<br></br>\n<b><span class=\"orange-text\">Recuerda que la mejor inversión que puedes hacer es en ti misma porque no hay nada que tenga más valor que tú.</b></span>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti</big></span>\n<br></br>\n<b>PD: No todo lo que dice bio o vegano certifica que un cosmético sea compatible con la piel.</b>\n', 3, 0),
(30, 'Link de enlace mini curso \"cosméticos y piel\"', 'Hola <b>VAR_NOME_CLIENTE</b>.\n<br></br>\nHoy es el día del mini curso sobre la importancia de la compatibilidad de los cosméticos con la piel.\n<br></br> \nMe siento super feliz de poder compartir estos conocimientos contigo.\n<br></br>\nTe pido por favor que abras la mente porque puede ser un poco chocante para ti el saber que estás utilizando productos en los cuales crees ciegamente porque te han vendido el concepto de biológico o vegan ok. \n<br></br>\nEl hecho de que un producto, por ejemplo, sea vegan ok no quiere decir que sea compatible con tu piel.\n<br></br>\nAquí tienes el link para conectarte: https://zoom.us/j/882298285\n<br></br>\n<b>ID Meeting:</b> 882-298-285\n<br></br>\nTe espero <span class=\"orange-text\"><b>hoy jueves 19 de julio</span></b>\n<br></br>\n<b>Hora: 8:30 pm (en Italia)</b>\n<br></br>\n<i>Y recuerda que, así como tú te alimentas, tu piel también se alimenta.</i>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti.</span></big>\n', 3, 0),
(31, 'Collegamento mini corso \"cosmetici e pelle\"', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nOggi è il giorno del mini corso sull\'importanza della compatibilità dei cosmetici con la pelle.\n<br></br>\nMi sento super felice di poter condividere questa conoscenza con te.\n<br></br>\nTi chiedo per cortesia di aprire la tua mente perché può essere un po\' scioccante sapere che stai usando prodotti in cui credi ciecamente perché ti hanno venduto il concetto di biologico o vegan ok.\n<br></br>\nIl fatto che un prodotto, ad esempio, sia vegan ok, non significa che è compatibile con la tua pelle.\n<br></br>\n<b>Ecco il link per collegarti:</b> https://zoom.us/j/207698303\n<br></br>\n<b>ID Meeting:</b> 207-698-303\n<br></br>\nTi aspetto <b><span class=\"orange-text\">oggi giovedì 19 luglio</b></span>\n<br></br>\n<b>Ore: 19:30 (in Italia)</b>\n<br></br>\n<i>E ricorda che, così come tu ti alimenti, anche la tua pelle si alimenta.</i>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti</big></span>\n', 1, 0),
(32, 'Registrazione minicorso cosmetici e pelle', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nPuoi <a href=\"http://bit.ly/minicorsoitalianocosmetici\">cliccare QUI</a> per guardare la registrazione del minicorso.\n<br></br>\n<b>Hai 48 ore per guardarlo.</b>\n<br></br>\nBuona conoscenza e consapevolezza.\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti</span></big>', 1, 0),
(33, 'Video-regalo cosméticos, piel y salud.', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\nPuedes hacer <a href=\"http://bit.ly/Videoregaloprotegetupielweb\">click AQUI</a> para ver el video-regalo.\n<br></br> \n<b>Te invito tambien a participar en el programa <a href=\"<a href=\"http://bit.ly/regeneratupiel\">Regenera tu piel</a> donde te ayudaré a incrementar tu conocimiento y consciencia a cerca de los productos que usas para tu higiene personal, protección y nutrición y como evitar desequilibrios en tu piel y en tu salud.</b> Proximamente publicaré la fecha del programa.\n<br></br>\nQue disfrutes el regalo e aumenten tus conocimientos.\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti</span></big>\n', 3, 0),
(34, 'Newsletter Rigenera la tua pelle', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nTi invito a partecipare al programma “Rigenera la tua pelle” per aiutarti ad incrementare la tua consapevolezza sui cosmetici che utilizzi per “nutrire” la tua pelle.\n<br></br>\nLo sai già che per poter avere una pelle veramente in salute devi applicare cosmetici che siano compatibile con essa. \n<br></br>\n<b>Ti faccio un esempio di cosa significa che un prodotto non sia compatibile con la pelle.</b>\n<br></br>\nHo trovato in diverse creme per il viso che si definiscono biologiche il <font color=\"red\">HEXYLENE GLYCOL</font> questo ingrediente è catalogato per tutte le certificazioni biologiche come una sostanza <b>“non ammessa”</b>. È un tipo di PEG che diminuisce la capacità della pelle per assorbire i nutrienti e in più altera il sistema endocrino. \n<br></br>\nQuello che voglio dire è che non soltanto la salute della tua pelle si altera ma metti in rischio anche la salute del tuo organismo perché applichi, senza conoscenza, cosmetici che possono causare danni su di te.\n<br></br>\nC’è una soluzione per poter scegliere meglio i cosmetici, da tutto quello che usi per il benessere del tuo volto a ciò che utilizzi per il tuo corpo (includendo deodoranti e shampoo).\n<br></br>\nQuesta <b><big>soluzione</b></big> te la trasmetto attraverso il mio programma <i><b>“Rigenera la tua pelle”</i></b> che si svolge esclusivamente ONLINE.\n<br></br> \nFai <a href=\"http://bit.ly/Rigeneralatuapelle\"><span class=\"blu-text\"><b>CLIC QUA</a></span></b> per sapere in cosa ti offro e quali sono i tuoi benefici.  Per ricevere il resto della informazione contattami.\n<br></br>\nIl programma sarà svolto il: 17/08/2018\n<br></br>\nOra: 18:30 (ora Italiana)\n<br></br>\nDurata: 1:30 min\n<br></br>\nFa per te se vuoi incrementare la tua conoscenza sui cosmetici che applichi sulla tua pelle e se valori la tua salute.\n<br></br>\n<span class=\"orange-text\"><b>Ricorda che il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.</span></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti</span></big>\n<br></br>\nP.S: Non tutto quello che è bio o vegan certifica che un cosmetico sia compatibile con la pelle.\n', 1, 0),
(35, 'Boletin Regenera tu piel', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\nTe invito a participar en el programa \"Regenera tu piel\" para ayudarte a incrementar tu conocimiento sobre los cosméticos que usas para \"nutrir\" tu piel.\n<br></br>\nYa sabes que para tener una piel verdaderamente sana debes aplicar cosméticos que sean compatibles con ella.\n<br></br>\nTe daré un ejemplo de lo que significa que un producto NO sea compatible con la piel.\n<br></br>\nEncontré el <font color=\"red\"><b>HEXYLEN GLICOL</font></b> en diferentes cremas faciales que se definen como orgánicas. Este ingrediente es catalogado en todas las certificaciones biológicas como una sustancia \"no admitida\". Es un tipo de PEG que disminuye la capacidad de la piel para absorber nutrientes y además altera el sistema endocrino.\n<br></br>\nLo que quiero decir es que no solo se altera la salud de tu piel, sino que también pones en riesgo la salud de tu cuerpo porque aplicas, sin saberlo, cosméticos que te pueden crear un daño.\n<br></br>\nHay una solución para elegir los mejores cosméticos, desde todo lo que utiliza para el bienestar de tu rostro hasta lo que usas para tu cuerpo (incluidos desodorantes y champús).\n<br></br>\nTe doy esta <b><big>solución</b></big> a través de mi programa <i><b>\"Regenera tu piel\"</i></b> que se lleva a cabo exclusivamente ONLINE.\n<br></br>\nHas <a href=\"http://bit.ly/regeneratupiel\"><b><span class=\"blu-text\">CLICK AQUÍ</a></b></span> para saber lo qué te ofrezco y cuáles son tus beneficios.  Para recibir el resto de la información contáctame.\n<br></br>\nEl programa se llevará a cabo el: 17/08/2018.\n<br></br>\nHora: 20:00 (hora italiana).\n<br></br>\nDuración: 1:30 min\n<br></br>\nEs para ti si quieres aumentar tu conocimiento sobre los cosméticos que aplicas en tu piel y si valoras tu salud.\n<br></br>\n<span class=\"orange-text\"><b>Recuerda que la mejor inversión que puedes hacer es en ti misma, porque no hay nada que tenga más valor que tu.</span></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti</span></big>\n<br></br>\nP.S: No todo lo que es orgánico o vegano certifica que un cosmético es compatible con la piel.\n', 3, 0),
(38, 'Novedades creadas pensando en ti', 'Hola VAR_NOME_CLIENTE\n<br></br>\nHoy te quiero actualizar con 2 novedades.\n<br></br>\n<b>La primera</b> es que a partir de este mes recibirás la información en un modo diferente, porque quiero entrar un poco más en confianza contigo y además pienso que es más rápido y fácil escuchar que leer.\n<br></br>\nCada mes recibirás un resumen del mes en un audio. Este resumen lo haré en base a los post de las redes sociales, escogeré 2 temas al mes de los 4 que trato, para que tengas una mejor secuencia y puedas procesar más fácilmente la información.\n<br></br> \nMe gustaría saber si te gusta escuchar la información en vez de leerla.\n<br></br>\nEn el audio de este mes hablo de:\n<br></br>\n<ul>\n<li>1. La importancia de leer la etiqueta de los cosméticos que usas.</li>\n<li>2. Cómo tonificar tu piel.</li>\n</ul>\n<br></br>\n<a href=\"https://soundcloud.com/user-816285692/importancia-de-leer-etiquetas-de-los-cosmeticos-y-como-tonificar-tu-piel\">Lo puedes escuchar clicando aquí</a> o buscándome en SoundCloud como Dottoressa Orlena Zotti.\n<br></br>\n<b>La segunda novedad</b> es que he creado el programa \"Regenera tu cuerpo\" pensando en ti. Has <a href=\"https://www.seiessenzialmentebella.com/es/programas/3/regenera-tu-cuerpo\"><big>CLICK AQUÍ</a></big> para saber de qué se trata y cuáles son los beneficios para ti. \n<br></br>\nLa fecha en que se llevará a cabo el programa es el 04/09/2018. Inscribiendote antes del 21de agosto tienes un descuento especial del 66%. Date la oportunidad de ver al final del email tu regalo.\n<br></br>\nEn resumen, este programa te ayuda a <b>desintoxicar</b> tu cuerpo y <b>reparar</b> tu intestino. \n<br></br>\nLa salud del intestino se refleja en una piel suave, lisa y tonificada, en piernas donde la celulitis y la retención de líquidos no son los protagonistas, en un rostro reposado y luminoso.\n<br></br>\nPero, ¿para qué sirve mejorar tu alimentación si la base en la que estás trabajando no está lista para recibir estos nuevos alimentos? Es como poner muebles nuevos en una sala llena de polvo, es decir, antes de decorar un espacio, tienes que limpiarlo.\n<br></br>\nLo mismo sucede con tu cuerpo. <b>Desintoxicar</b> y luego <b>reparar</b> los daños creados por las toxinas es lo que te permitirá que los cambios que introduzcas marquen la diferencia en tu bienestar.\n<br></br>\nPara empezar a hacer algo por ti, te dejo este <a href=\"https://drive.google.com/file/d/1XYZwncl6OWIF2v3ZCW7KQFTqedU3VAlD/view?usp=sharing\"><big>REGALO</a></big> donde te explico 5 claves para disminuir la inflamación de tu cuerpo.  \n<br></br>\n<span class=\"orange-text\"><b>Cuídate siempre, eres la única persona en el mundo sin la cual no puedes vivir.</span></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti</span></big>\n<br></br>\n<br></br>\n<br></br>\nSígueme por Instagram/Facebook @dottoressaorlenazotti\n', 3, 0),
(39, 'Descuento del 66% - ¡Ultimo dia!', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\nHoy es el ultimo día para inscribirte y aprovechar del 66% de descuento del programa: <a href=\"http://bit.ly/regeneratucuerpo\">Regenera tu cuerpo.</a>\n<b>Tu inversión es de 159 euros</b> en vez de 240.\n<br></br>\nRecuerda que, a través de una alimentación no adecuada, el estrés y la inflamación crónica que vive tu cuerpo día a día, se van acumulando toxinas y por esta razón realizar un détox es extraordinario para darle un respiro a tu intestino. Pero las toxinas generan un daño y cuando haces una desintoxicación sin reparar el daño producido realmente no estas aportando beneficios duraderos a tu cuerpo.\n<br></br>\nSeptiembre es el mejor período para realizar este programa. En un mes se eliminan las toxinas y se reduce el proceso inflamatorio del intestino y luego de este periodo se pueden ir implementando poco a poco nuevos hábitos que ayuden a mantener tu cuerpo sano y en equilibrio. \n<br></br>\nHaz <a href=\"http://bit.ly/5clavesinflamacionvideoregalo\">CLIC AQUÍ</a> para ver el video donde te explico porque tu cuerpo vive en inflamación crónica y que puedes hacer para reducirla. Te aconsejo de ver el video hasta el final para que puedas disfrutar de tu regalo especial.\n<br></br>\nResponde a esta email para agendar una sesión gratuita y así conversar sobre cómo te sientes en tu cuerpo cuando comes, tu nivel de energía durante el día y los beneficios que este programa te ofrece.\n<br></br>\n<b><span class=\"orange-text\">Cuídate siempre, eres la única persona en el mundo sin la cual no puedes vivir.</b></span>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti</big></span>', 3, 0),
(40, 'Como se defiende la piel del medio externo.', 'Hola VAR_NOME_CLIENTE\n<br></br>\nTe dejo el enlace para escuchar el audio de este mes donde hablo de:\n<br></br>\n<ul>\n<li>La piel y como se defiende del medio externo.</li>\n<li>Como influye una alimentación equilibrada en tu piel.</li>\n</ul>\n<br></br>\nLo puedes escuchar clicando<a href=\"http://bit.ly/Boletinagosto2018\"> AQUI</a> o buscándome en SoundCloud como Dottoressa Orlena Zotti.\n<br></br>\n<span class=\"orange-text\"><b>Cuídate siempre, eres la única persona en el mundo sin la cual no puedes vivir.</span></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti</span></big>\n<br></br>\n<br></br>\n<br></br>\nSígueme por Instagram/Facebook @dottoressaorlenazotti\n', 3, 0),
(41, 'Retreat-Scoprendo il tuo equilibrio.', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nTi va di venire in Marocco con me e Roberta Zanetti, spiritual & healing coach, tra Marrakech e un campo tendato nel deserto?\n<br></br>\nDal 21 al 25 Novenmbre 2018. Vieni a riscoprire il tuo equilibrio con noi in:\n<br></br>\n<ul>\n<li>MARRAKECH + DESERTO: <span class=\"blu-text\"><b>LA DONNA E L\'EQUILIBRIO.</span></b></li>\n</ul>\n<br></br>\nTi parleremo di equilibrio tra Corpo e Spirito e ti insegneremo a raggiungerlo.\nBeauty Masterclass + Face Yoga quotidiano + Spiritual Masterclass + Meditazioni quotidiane + Numerologia&Cabala per aiutarti a scoprire, apprezzare e valorizzare la tua bellezza, interna ed esterna.\n<br></br>\n<b>Per il programma completo, rispondimi a questa mail.</b>\n<br></br>\n<span class=\"orange-text\"><b>Ricorda che il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.</span></b>\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti.</span></big>', 1, 0),
(42, 'Programma Marrakech Novembre 2018', 'Ciao VAR_NOME_CLIENTE\n<br></br>\nPer leggere il programma fai clic <a href=\"https://drive.google.com/file/d/1jJ2lzLdWhgAUC13nDiyRssil0qzhzyHj/view?usp=sharing\">QUI.</a>\n<br></br>\nSe sei interessata in venire con noi al retreat, comunica il tuo interesse al email che trovi nel programma.\nSarà un grande piacere averti con noi.\n<br></br>\n<span clas=\"blu-text\"><big>Dottoressa Orlena Zotti.</span></big>', 3, 0),
(43, 'Programma Marrakech Novembre 2018', 'Ciao VAR_NOME_CLIENTE\n<br></br>\nPer leggere il programma fai clic <a href=\"https://drive.google.com/file/d/1LsqD8Vt4X_EvaoinKP5wbUZgD8qakYzw/view?usp=sharing\">QUI.</a>\n<br></br>\nSe sei interessata in venire con noi al retreat, comunica il tuo interesse al email che trovi nel programma.\nSarà un grande piacere averti con noi.\n<br></br>\n<span class=\"blu-text\"><big>Dottoressa Orlena Zotti.</span></big>', 1, 0),
(44, 'Link correcto para sesion gratuita - Disculpa el inconveniente. ', 'Hola VAR_NOME_CLIENTE\n<br></br>\n<b><big>*** IMPORTANTE ***</b></big>\n<br></br>\n<b><big>Disculpa, en el email anterior el link para la consulta gratuita no funciona. Te envio nuevamente el email, por si no lo has leido tengas toda la información y para que ahora si puedas participar por la sesión gratuita.</b></big>\n<br></br>\nPara mi existen dos inicios de año: enero y septiembre. Y en 3 meses queremos hacer todo lo que no nos dio tiempo hacer durante 9 meses. Pero siempre queda enero, para retomar nuevamente y con más fuerza.\n<br></br>\nEl tema que trato hoy en el audio, exactamente es lo que hacemos y nos proponemos cada enero, limpiar nuestro organismo y drenarlo de todas las toxinas que adquirimos durante las fiestas de diciembre. \n<br></br>\nPero, ¿por qué solamente limpiar, cuando puedes regenerar y rejuvenecer? Te cuento más, <a href=\"http://bit.ly/Boletinoctubre2018\"><b>CLICA AQUI</a></b>\n<br></br>\nHoy a parte del audio mensual quiero hacerte esta pregunta:\n<br></br>\n<b><big><span class=\"orange-text\">¿Te gustaría rejuvenecer tu piel?</b></big></span>\n<br></br>\nYo puedo ayudarte a tener una piel más joven, fresca, hidratada y en salud.\n<br></br>\n<b><p style=\"color:#68bf71\";>¿Cómo te puedo ayudar?</b></p> Estoy ofreciendo 8 sesiones de consultoría gratuita para ayudarte a rejuvenecer tu piel.\n<br></br>\n<b><p style=\"color:#68bf71\";>¿Te interesa?</b></p> Podemos hablar para conocerte y para que descubras como puedo ayudarte. Es una sesión sin compromiso. El número es limitado, no te quedes sin tu sesión. <a href=\"http://bit.ly/sesiongratisemail-whatsapp\"><b>CLICA AQUI</b></a> para agendar tu sesión de consultoría gratuita.\n<br></br>\n<span class=\"orange-text\"><big><b>La mejor inversión que puedes hacer es en ti misma porque no hay nada que tenga más valor que tú.</span> </big></b>\n<br></br>\n<big><span class=\"blu-text\">Dottoressa Orlena Zotti</big></span>\n', 3, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates_traduzioni`
--

CREATE TABLE `email_templates_traduzioni` (
  `id_template_trad` int(11) NOT NULL,
  `testo_preheader` text NOT NULL,
  `testo_footer_copyright` text NOT NULL,
  `testo_footer_unsubscribe` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_templates_traduzioni`
--

INSERT INTO `email_templates_traduzioni` (`id_template_trad`, `testo_preheader`, `testo_footer_copyright`, `testo_footer_unsubscribe`, `lingua_traduzione_id`) VALUES
(1, 'Vedi email online', 'TUTTI I DIRITTI RISERVATI', 'Se non vuoi più ricevere queste email per favore <span style=\"text-decoration:underline;\"><a href=\"VAR_UNSUBSCRIBE_LINK\" target=\"_blank\" style=\"text-decoration:underline; color:#ffffff;\">CANCELLATI</a></span>', 1),
(2, 'See contents online', 'ALL RIGHTS RESERVED', 'If you no longer wish to receive these emails please <span style = \"text-decoration: underline;\"> <a href=\"VAR_UNSUBSCRIBE_LINK\" target=\"_blank\" style=\"text-decoration:underline;color:#ffffff;\">UNSUBSCRIBE</a></ span>', 2),
(3, 'Ver en línea', 'TODOS LOS DERECHOS RESERVADOS', 'Si ya no desea recibir estos correos electrónicos, <span style=\"text-decoration: underline;\"><a href=\"VAR_UNSUBSCRIBE_LINK\" target=\"_blank\" style=\"text-decoration:underline;color:#ffffff;\">CANCELATE</a></ span>', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `faq`
--

CREATE TABLE `faq` (
  `id_faq` int(11) NOT NULL,
  `titolo_faq` text NOT NULL,
  `allineamento_faq` varchar(100) NOT NULL DEFAULT 'center'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `faq`
--

INSERT INTO `faq` (`id_faq`, `titolo_faq`, `allineamento_faq`) VALUES
(1, 'Il trattamento è doloroso?', 'justify'),
(2, 'Complicanze?', 'justify'),
(3, 'Quali sostanze si usano?', 'center'),
(4, 'Quante volte lo devo fare?', 'justify'),
(5, 'Quanto dura una seduta?', 'center'),
(6, 'Quando inizio a vedere i risultati?\n	', 'center'),
(7, 'Cosa succede dopo il trattamento?\n	', 'justify'),
(8, 'Quale sostanze si usano?\n	', 'justify'),
(9, 'In cosa consiste il trattamento?\n	', 'justify'),
(11, 'Quanto durano gli effetti?\n	', 'center'),
(12, 'Posso sentirmi tranquilla e sicura nei trattamenti con il botulino?\n	', 'justify'),
(13, 'Il trattamento con botulino è doloroso?\n	', 'justify'),
(14, 'Quando inizio a vedere i risultati dopo la seduta di botulino?\n	', 'justify'),
(15, 'Quanto durano gli effetti del botulino?\n	', 'justify'),
(16, 'A quale età va eseguito?\n	', 'justify'),
(17, 'Divento inespressiva?\n	', 'justify'),
(18, 'Come si svolge il trattamento con fili PDO?\n	', 'justify'),
(19, 'Il trattamento con fili PDO è doloroso?\n	', 'justify'),
(20, 'Quando inizio a vedere i risultati con fili PDO?\n	', 'justify'),
(21, 'Quanto durano i risultati dei fili PDO?\n	', 'justify'),
(22, 'Cosa devo fare dopo il trattamento con PDO?\n	', 'justify'),
(23, 'Quanto dura la seduta con Needling?\n	', 'center'),
(24, 'Il trattamento needling lo possono fare tutti?\n	', 'justify'),
(25, 'Che cosa comporta il needling?\n	', 'justify'),
(26, 'Quando inizio a vedere risultati del needling?\n	', 'center'),
(27, 'Di quante sedute needling ho bisogno?\n	', 'justify'),
(28, 'Il trattamento filler è doloroso?\n', 'justify'),
(29, 'Ho bisogno di anestesia nei filler?\n', 'justify'),
(30, 'Quanto tempo dura il risultato dei filler?\n\n', 'justify'),
(31, 'Quanto tempo ci vuole per realizzare il trattamento filler?\n', 'justify'),
(32, 'Cosa può accadere dopo il trattamento con filler?\n', 'justify'),
(33, 'Dopo il filler posso continuare con le mie attività della giornata?\n\n', 'justify'),
(34, 'Posso andare al mare subito dopo il filler?\n', 'justify'),
(35, 'Il botulino offre risultati definitivi nella iperidrosi?\n	', 'justify'),
(36, 'Quanto dura la seduta per iperidrosi?\n	\n', 'center'),
(37, 'Ha effetti avversi iperidrosi?\n	\n', 'center'),
(38, 'In cosa consiste il peeling?\n	', 'justify'),
(39, 'Si può fare il peeling in qualsiasi stagione dell’anno?\n	', 'justify'),
(40, 'Devo fare qualcosa prima di un peeling?\n	', 'justify'),
(41, 'Quanto dura una seduta di peeling?\n	', 'center'),
(42, 'Ci sono controindicazioni nei peeling?\n	\n', 'justify'),
(43, 'Quanti peeling posso fare?\n	', 'justify'),
(44, 'Il trattamento mesoterapia è doloroso?\n	', 'justify'),
(45, 'Quanto dura la seduta di mesoterapia?\n	', 'center'),
(46, 'Basta una seduta di mesoterapia per vedere i risultati?\n	', 'justify'),
(47, 'Cosa succede dopo il trattamento con mesoterapia?\n	', 'justify'),
(48, 'Si può fare la mesoterapia in qualsiasi momento?\n	', 'justify'),
(49, 'Si deve fare un mantenimento con mesoterapia?\n	', 'justify'),
(50, 'Quanti cicli di mesoterapia si possono fare in un anno?\n	', 'center'),
(51, 'Il trattamento rinofiller è doloroso?\n	', 'justify'),
(52, 'Aumenta la dimensione del naso?\n	', 'justify'),
(53, 'Quanto dura il trattamento rinofiller?\n	', 'justify'),
(54, 'Quanto dura il risultato del rinofiller?\n	', 'justify'),
(55, 'Dopo il trattamento rinofiller si deve fare qualcosa?\n	\n', 'justify'),
(56, 'Il trattamento carbossiterapia è doloroso?\n	\n', 'justify'),
(57, 'I benefici della carbossiterapia si vedono in una sola seduta?\n	', 'justify'),
(58, 'Quante sedute di carbossiterapia devo fare per avere risultati estetici?\n	', 'justify'),
(59, 'Quali zone si possono trattare con carbossiterapia?\n	\n', 'justify'),
(60, 'Ci sono dei rischi con la carbossiterapia?\n	', 'justify'),
(61, 'Può essere associata ad altri trattamenti?\n	\n', 'justify'),
(62, 'Ho le gambe gonfie, appesantite e mi fano male, posso fare questo trattamento?\n	\n', 'justify'),
(63, 'Esiste qualche controindicazione della carbossiterapia? \n	', 'justify'),
(64, 'Come si svolge il trattamento di sublimazione?\n	', 'justify'),
(65, 'Cosa accade dopo il trattamento di sublimazione?', 'list'),
(66, 'I risultati della sublimazione durano nel tempo?\n ', 'justify'),
(67, 'Si può fare in qualsiasi momento?\n\n', 'center'),
(68, 'Quali sostanze si usano nella biostimolazione?\n	', 'center');

-- --------------------------------------------------------

--
-- Struttura della tabella `faq_traduzioni`
--

CREATE TABLE `faq_traduzioni` (
  `id_faq_traduzioni` int(11) NOT NULL,
  `id_faq` int(11) NOT NULL,
  `testo_faq` text NOT NULL,
  `risposta_faq` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `faq_traduzioni`
--

INSERT INTO `faq_traduzioni` (`id_faq_traduzioni`, `id_faq`, `testo_faq`, `risposta_faq`, `lingua_traduzione_id`) VALUES
(1, 1, 'Il trattamento è doloroso?', 'Il trattamento non è doloroso, potrebbe causare leggero fastidio perché si tratta di una procedura iniettiva.', 1),
(2, 2, 'Complicanze?', 'Non esistono complicanze significative, l’unica cosa che si potrebbe presentare sono piccoli lividi o pomfi, che si risolvono in poche ore o qualche giorno e che comunque si possono facilmente coprire con il trucco.', 1),
(3, 1, 'Is the treatment painful?', 'The treatment is not painful, it may cause slight annoyance because it is an injective procedure.', 2),
(4, 2, 'Complications?', 'There are no significant complications, the only thing that could be presented is small bruises or bumps, which resolve within a few hours or a few days and which can easily be covered with the trick.', 2),
(5, 1, '¿El tratamiento es doloroso?', 'El tratamiento no es doloroso, podría causar ligero fastidio porque es un procedimiento inyectivo.', 3),
(6, 2, '¿Se pueden presentar complicaciones?', 'No existen complicaciones significativas, lo único que podría presentarse son pequeños hematomas, que se resuelven en pocas horas o algunos días  y pueden ser cubiertos facilmente con maquillaje.', 3),
(7, 3, 'Quali sostanze si usano?', 'Le sostanze che possono essere iniettate sono: acido ialuronico a bassa densità, aminoacidi, dei glicosaminoglicani, vitamine, PDRN (polidesossiribonucleotide), ecc.', 1),
(8, 3, '¿Qué sustancias se usan?\n	', 'Las sustancias que pueden ser inyectadas son: ácido hialurónico a baja densidad, aminoácidos,  glicosaminoglicanos, vitaminas, PDRN (polidexosiribonucleotidos), etc.\n', 3),
(9, 4, 'Quante volte lo devo fare?', 'Questo dipende dal bisogno della pelle. I protocolli variano infatti a seconda delle esigenze, ad esempio possono consistere in cicli di 4-6 sedute una volta alla settimana, come anche in cicli di 5 sedute ogni 21 giorni da ripetere 2 volte all’anno.', 1),
(10, 4, '¿Cuántas veces lo tengo que hacer?\n\n', 'Esto depende de la necesidad de la piel. Los protocolos varian según las exigencias de ésta, por ejemplo existen protocolos de 4-6 sesiones una vez a la semana, como ciclos de 5 sesiones cada 21 días a repetir 2 veces al año.\n', 3),
(11, 5, 'Quanto dura una seduta?', 'Dura 30 minuti.', 1),
(12, 5, '¿Cuánto tiempo dura una sesión?\n	\n', 'Dura de 20 a 30 minutos.\n', 3),
(13, 6, 'Quando inizio a vedere i risultati?\n	', 'Dopo 48 ore dalla prima seduta.\n', 1),
(14, 6, '¿Cuándo comienzo a ver resultados?\n	', 'Más o menos 48 horas después de la primera sesión.\n', 3),
(15, 7, 'Cosa succede dopo il trattamento?\n\n', 'Si possono presentare arrossamento, gonfiore o dolore localizzato nella sede d’iniezione che in genere si risolve in poco tempo. Potrebbero formarsi anche lividi. Tutto ciò si può camuffare con il trucco. ', 1),
(16, 7, '¿Qué sucede después del tratamiento?\n	', 'Se puede presentar enrojecimiento, inflamación o dolore localizado en la zona de inyección, resolviendose en poco tiempo. También pequeños hematómas. Todo esto se puede camuflar con maquillaje.', 3),
(17, 8, 'Quale sostanze si usano?\n	', 'Acido ialuronico a basso ed alto peso molecolare che permette la rimodellazione del viso e la nutrizione della pelle.\n', 1),
(18, 8, '¿Qué sustancias se usan?\n', 'Ácido hialurónico a bajo y alto peso molecolar que permiten restructurar el rostro y nutrir la piel.\n', 3),
(19, 9, 'In cosa consiste il trattamento?\n\n', 'Sono delle iniezioni che si eseguono su punti specifici sul volto. Il trattamento si realizza in un ciclo di tre sedute, due volte all’anno.\n', 1),
(20, 9, '¿En qué  consiste el tratamiento?\n	\n', 'Son inyecciones que se realizan en puntos específicos del rostro. El tratamiento se realiza en un ciclo de 3 sesiones dos veces al año.\n', 3),
(21, 11, 'Quanto durano gli effetti?\n	\n', 'In genere dura 6 mesi per ogni ciclo.\n', 1),
(22, 11, '¿Cuánto dura el efecto?\n	', 'Dura 6 meses por cada ciclo.\n', 3),
(23, 12, 'Posso sentirmi tranquilla e sicura nei trattamenti con il botulino?\n	\n', 'Anche io all’inizio dei miei studi di medicina estetica mi sono fatta tante volte la stessa domanda. Ebbene, il botulino è un trattamento sicuro, efficace ed ampiamente utilizzato non solo in questo settore. È una sostanza che proviene da un batterio, che si chiama Clostridium Botulinum, ed è in grado di paralizzare la muscolatura. In medicina estetica la sua applicazione avviene in basse percentuali perché l’obiettivo è quello di agire localmente sui muscoli coinvolti nella mimica. Esso provoca una diminuzione della forza di contrazione dei muscoli dove viene iniettato, mantenendo la tua espressività e non permettendo di accentuare la formazione delle rughe.     \n', 1),
(24, 12, '¿Me puedo sentir tranquila y segura realizando este tipo de tratamiento?\n	\n', 'Cuando inicié a estudiar medicina estética me hice muchas veces esta pregunta. Déjame decirte que, el botox es un tratamiento seguro, eficaz y ampliamente utilizado, no solo en este sector de la medicina estética. Es una sustancia que proviene de una bacteria, se llama Clostridium Botulinum, su acción es paralizar el sistema muscular. En medicina estética la dosis que se utiliza es muy baja en porcentaje. El objetivo es el de actuar localizadamente en los mùsculos de la mìmica facial. Esto provoca una diminución de la fuerza de contracciòn de los músculos donde se inyecta, de esta manera mantienes tu expresividad sin acentuar tanto las arrugas. \n', 3),
(25, 13, 'Il trattamento è doloroso?\n	', 'Il trattamento non è doloroso, potrebbe causare sensazione di bruciore per il pH variabile della soluzione fisiologica con cui viene diluito il botulino. La sensazione passa subito dopo l’iniezione.\n', 1),
(26, 13, '¿El tratamiento es doloroso?\n	', 'El tratamiento no es doloroso, podría causar una sensación no tan agradable por el pH variable de la solución fisiológica con la cual se diluye el botox. Esta sensación pasa inmediatamente luego de la inyección.\n', 3),
(27, 14, 'Quando inizio a vedere i risultati?\n	', 'I risultati si iniziano a vedere dopo 72 ore dal trattamento e passati 15 giorni devi fare una visita di controllo. \n', 1),
(28, 14, '¿Cuándo inicio a ver los resultados?\n	', 'Los resultados se inician a ver después de 72 horas del tratamiento y pasados 15 días se debe realizar una visita de control. \n', 3),
(29, 15, 'Quanto durano gli effetti?\n	', 'Ha una durata variabile tra 4 e 6 mesi, si può ripetere senza nessuna limitazione nel tempo. Da tenere in conto solo alcuni accorgimenti. \n', 1),
(30, 15, '¿Cuánto duran los efectos?\n	', 'Tiene una duración variable entre  4 e 6 meses, se puede repetir sin limitación en el tiempo,  con algunas precauciones.\n', 3),
(31, 16, 'A quale età va eseguito?\n	', 'È indicato nelle persone giovani che tendono a corrugare in modo accentuato e quindi a formare rughe permanenti e visibili nel tempo. Anche in quelle persone in cui le rughe d’espressione sono già visibili a riposo, l’effetto del botulino sarà quello di ridurre le stesse in maniera notevole. In altre parole non c’è limite di età.\n', 1),
(32, 16, '¿A partir de qué edad se puede realizar el tratamiento?\n	', 'Es indicado en las personas jóvenes que tienden a contraer los músculos en modo acentuado, originando así arrugas permanentes y visibles en el tiempo. También en las personas donde las arrugas de expresión son visibles en reposo (cuando no hay movimiento muscular), el efecto del botox es el de reducir las líneas de expresión de manera notable. En otras palabras no hay un límite de edad.\n', 3),
(33, 17, 'Divento inespressiva?\n	', 'Il botox riduce la contrattilità del muscolo ma non lo blocca, conservando la tua mimica espressiva ma riducendo la formazione delle rughe.\n', 1),
(34, 17, '¿Quedo sin expresividad?\n	', 'El bótox reduce la contracción mas no bloca el músculo, conservas tu expresividad con unas arrugas menos evidentes.\n', 3),
(35, 18, 'Come si svolge il trattamento?\n	', 'Il filo PDO viene collocato all’interno di un ago sottile e questo viene inserito dentro la pelle, dopo di che si sfila l’ago e il filo viene rilasciato al interno del tessuto.\n', 1),
(36, 18, '¿Cómo se realiza el tratamiento?\n	', 'El hilo PDO es colocado al interno de una aguja y esta viene inserida en la dermis, luego se retira la aguja y el hilo se deja al interno del tejido.\n', 3),
(37, 19, 'Il trattamento è doloroso?\n	', 'Il trattamento non è doloroso, potrebbe causare fastidio perché è si tratta di una procedura iniettiva.\n', 1),
(38, 19, '¿El tratamiento es doloroso?\n	', 'El tratamiento no es doloroso, podría causar ligero fastidio porque es un procedimiento inyectivo.\n', 3),
(39, 20, 'Quando inizio a vedere i risultati?\n	', 'I fili stimolano la produzione di collagene ed elastina, per tale motivo hanno bisogno di un periodo di tempo che va da un mese ad un mese e mezzo per rendere visibili i benefici. \n', 1),
(40, 20, '¿Cuándo comienzo a ver los resultados?\n	', 'Los hilos estimulan la producción de colágeno y elastina, por este motivo se necesita de un período de tiempo que va de un mes a un mes y medio para que los beneficios puedan ser visibles.\n', 3),
(41, 21, 'Quanto durano i risultati?\n	', 'I risultati durano circa 6-8 mesi, consiglio sempre di fare una seconda seduta vicina al termine di questo periodo al fine di prolungare così l’effetto di questo trattamento. \n', 1),
(42, 21, '¿Cuánto duran los resultados?\n	', 'Los resultados duran de 6-8 mesi, aconsejo hacer siempre una segunda sesión cuando esta por terminar este período, para prolongar de esta manera los efectos del tratamiento.\n', 3),
(43, 22, 'Cosa devo fare dopo il trattamento?\n\n	', 'Post trattamento non c’è nessuna prescrizione che si debba seguire. Puoi precedere vivendo con normalità la tua giornata.\n', 1),
(44, 22, '¿Qué debo hacer después del tratamiento?\n	', 'No se debe realizar nada, simplemente continuar con la normalidad de las actividades previstas para el día.\n', 3),
(45, 23, 'Quanto dura la seduta?\n	', 'Dipende dell’area da trattare, tra 40 e 60 min.\n', 1),
(46, 23, '¿Cuánto dura la sesión?\n	\n', 'Depende de la zona de tratamiento, entre 40 – 60 min.\n', 3),
(47, 24, 'Il trattamento lo possono fare tutti?\n	', 'Si, tranne le persone che presentano lesioni nella zona che si dovrebbe trattare.\n', 1),
(48, 24, '¿El tratamiento puede ser realizado en todas las personas?\n	\n', 'Sí, indiferentemente del fototipo de la piel. No se realiza en casos como lesiones  en el área que se debe tratar.\n', 3),
(49, 25, 'Che cosa comporta?\n	', 'Il trattamento si esegue dopo l’applicazione di anestesia topica, si prosegue facendo delle micro iniezioni multiple e l’area trattata diventerà rossa. Dopo di che si prosegue con la fase di raffreddamento e si applica una crema lenitiva. L’arrossamento, che può essere anche intenso, dura da 24 a 48 ore. Questo tipo di trattamento andrà fatto preferibilmente durante il periodo invernale. \n', 1),
(50, 25, '¿En qué consiste?\n	', 'El tratamiento se realiza después de la aplicación de anestesía tópica, luego de realizar las micro inyecciones de manera multiple el área tratada se presentará un color rojo intenso. Se procede a la fase de enfriamiento y aplicación de una crema hidratante que disminuye la sensación de calor. El enrojecimiento dura de 24 a 48 horas. Este tipo de tratamiento se realiza durante el periodo invernal. \n', 3),
(51, 26, 'Quando inizio a vedere risultati?\n	', 'Dopo 2- 3 mesi dall’inizio del trattamento.\n', 1),
(52, 26, '¿Cuándo se comienzan a ver los resultados?\n	', 'Después de la 2-3 sesión de tratamiento.\n', 3),
(53, 27, 'Di quante sedute ho bisogno?\n	', 'Consiglio di fare 6 sedute, per quanto riguarda il viso, 1 volta al mese. Per altre aree specifiche del corpo si devono effettuare alcune sedute in più.\n', 1),
(54, 27, '¿Cuántas seiones se deben realizar?\n	', 'Lo aconsejable son 6 sesiones, en el rostro, a distancia de 1 mes cada una. Para otras àreas especìficas del cuerpo se debe adicionar algunas sesiones màs.\n', 3),
(55, 28, 'Il trattamento è doloroso?\n', 'Come in ogni procedura iniettiva si può sentire fastidio nella zona a trattare.\n', 1),
(56, 28, '¿El tratamiento es doloroso?\n', 'El tratamiento no es doloroso, podría causar ligero fastidio porque es un procedimiento inyectivo.\n', 3),
(57, 29, 'Ho bisogno di anestesia?\n', 'Non c’è bisogno di anestesia topica perché quasi tutti i prodotti per filler contengono un anestetico all’interno. Solo in zone particolarmente sensibili come le labbra si applica anestesia. \n', 1),
(58, 29, '¿Se necesita usar un anestésico?\n', 'No hay necesidad de usar un anestésico. Casi todos los productos para filler contienen anestésico en su composisción. Yo particularmente aplico anestesia tópica en la zona de los labios, ya que es muy sensible. \n', 3),
(59, 30, 'Quanto tempo dura il risultato?\n\n', 'Dipende dalla zona, il risultato può durare da 4 a 8 mesi.\n', 1),
(60, 30, '¿Cuánto tiempo dura el resultado?\n', 'Depende de la zona, el resultato puede durar entre 4 a 8 meses.\n', 3),
(61, 31, 'Quanto tempo ci vuole per realizzare il trattamento?\n', 'In genere mezz’ora, ma dipende sempre dalla zona da trattare.\n', 1),
(62, 31, '¿Cuánto tiempo se necesita para realizar el tratamiento?\n', 'Generalmente 30-40 minutos. Depende siempre del área a tratar.\n', 3),
(63, 32, 'Cosa può accadere dopo il trattamento?\n', 'Lividi, gonfiore, arrossamento. Il gonfiore si presenta perché l’acido ialuronico richiama acqua nella zona trattata, questo effetto si risolve in poco tempo ponendo in luce l’ottimo risultato degli effetti del filler.\n', 1),
(64, 32, '¿Qué sucede después del tratamiento?\n', 'Se podrían presentar hematomas, inflamación y enrojecimiento. El edema se presenta ya que el ácido hialurónico hace un llamado de agua en la zona tratada. Pocas horas después de este hallazgo, se evidenciaran los óptimos resultados del tratamiento.  \n', 3),
(65, 33, 'Dopo il filler posso continuare con le mie attività della giornata?\n', 'Assolutamente sì! Puoi continuare le tue attività normalmente, dopo il trattamento sei presentabile per una vita socievole.\n', 1),
(66, 33, '¿Después del filler puedo continuar con las actividades del día?\n', 'Sí, puedes continuar con las actividades planificadas, eres socialmente activa. Teniendo encuenta que saunas y piscina las debes suspender por ese día. \n', 3),
(67, 34, 'Posso andare al mare subito dopo?\n', 'Sarebbe meglio esporsi dopo un giorno del trattamento, a meno che si presentino dei lividi, in quel caso è meglio evitare il sole e comunque proteggere la zona interessata dal livido, perché il sole può provocare una macchia.', 1),
(68, 34, '¿Puedo ir a la playa inmediatamente después?\n', 'Si, aunque es mejor esperar a un día después del trattamiento, a menos que se evidencien hematomas, en este caso es mejor evitar exponerse al sol. Este puede provocar en el lugar del hematoma  una mancha.', 3),
(69, 35, 'Il botulino offre risultati definitivi?\n	', 'I risultati non sono definitivi, è necessario sottoporsi a periodici trattamenti per ottenere un risultato prolungato, almeno una volta all’anno.\n', 1),
(70, 35, '¿Este tratamiento me da resultados definitivos?\n	', 'Los resultados no son definitivos, es necesario repetir el tratamiento regularmente para prolungar los resultados. Es un tratamiento que va realizado al menos una vez al año.	\n', 3),
(71, 36, 'Quanto dura la seduta?\n	\n', 'Ha una durata di 40 - 60 minuti approssimativamente. \n', 1),
(72, 36, '¿Cuànto tiempo dura la sesiòn?\n	', 'Unos 40-60 minutos aproximadamente.\n', 3),
(73, 37, 'Ha effetti avversi?\n	', 'Non comporta nessun effetto avverso.\n', 1),
(74, 37, '¿Se pueden presentar efectos secundarios?\n	', 'No comporta efectos secundarios.\n', 3),
(75, 38, 'In cosa consiste?\n	', 'Elimina l’eccesso di cellule morte superficiali dello strato corneo favorendo la protezione della pelle. Rende la pelle più pulita e ossigenata. Promuove il rinnovamento cellulare e la produzione di nuovo collagene ed elastina da parte dei fibroblasti.\n', 1),
(76, 38, '¿Qué me proporciona el tratamiento?\n	', 'Elimina el exeso de células muertas superficiales del estrato corneo favoreciendo la protección de la piel. Concede una piel más limpia y oxigenada. Promueve la renovación celular y la producción de colageno nuevo y de elastina por parte de los fibroblastos. \n', 3),
(77, 39, 'Si può fare in qualsiasi stagione dell’anno?\n	', 'Ci sono peeling fotosensibilizzanti che si effettuano solo durante l’inverno e ci sono peeling che si possono fare anche in periodo estivo.\n', 1),
(78, 39, '¿Se puede hacer en cualquier período del año?\n	', 'Existen peeling fotosensibilizantes que se efectuan solo durante el invierno o con indicaciones muy precisas a realizar después del tratamiento. Durante el verano se pueden realizar peeling más ligeros sin la preocupación de las consecuencias del sol.\n', 3),
(79, 40, 'Devo fare qualcosa prima di un peeling?\n	', 'Durante il periodo invernale consiglio di applicare per 2 settimane prima del peeling una crema a base di alfa-idrossi-acido, che favorisce i benefici di questo trattamento. Invece durante il periodo estivo non è necessario fare qualcosa prima della procedura.\n', 1),
(80, 40, '¿Debo hacer una actividad preparatoria para realizar este tratamiento?\n	', 'Durante el período invernal aconsejo de aplicar durantes 2 semanas antes del peeling una crema a base di alfa-idroxi-ácido, que favorece los beneficios de este tratamiento y evita consecuencias como las quemaduras.\n', 3),
(81, 41, 'Quanto dura una seduta?\n	\n', 'In genere 30 minuti.\n', 1),
(82, 41, '¿Cuánto tiempo dura una sesión?\n	\n', 'Genaralmente 30 minutos.\n', 3),
(83, 42, 'Ci sono controindicazioni?\n	', 'In generale è sconsigliato se sono presenti nell’area di trattamento irritazioni cutanee evidenti o patologie particolari della pelle.\n', 1),
(84, 42, '¿Hay alguna contraindicación?\n	', 'No se debe realizar el tratamientos en las áreas de irritación o donde se presente una lesión.\n', 3),
(85, 43, 'Quanti peeling posso fare?\n	', 'Dipende dal tipo di peeling che si effettua e da quello che si vuole ottenere. Se ne può fare solo uno ogni tanto per dare più luce al volto, oppure cicli di 3-5 peeling ad intervalli che vanno da 15 giorni ad un mese per lavorare sull’uniformità del colorito, su macchie e rughe.\n', 1),
(86, 43, '¿Cuántos peeling puedo hacerme?\n	', 'Depende del tipo de peeling que se efectua y de lo que se quiere obtener. Se puede realizar un peeling de vez en cuando para dar luz a la piel o ciclos de 3 -5 peeling con intervalos de 15 a 30 días cuando lo que se quiere trabajar es la uniformidad del color, las discromias y las arrugas.\n', 3),
(87, 44, 'Il trattamento è doloroso?\n	\n', 'Non è doloroso, ma si deve considerare che come tutti i trattamenti iniettivi potrebbe essere un po’ fastidioso.\n', 1),
(88, 44, '¿El tratamiento es doloroso?\n	\n', 'El tratamiento no es doloroso, podría causar ligero fastidio porque es un procedimiento inyectivo.\n', 3),
(89, 45, 'Quanto dura la seduta?\n	', 'In genere 30 minuti.\n', 1),
(90, 45, '¿Cuánto tiempo dura la sesión?\n	', 'Genralmente de unos 30 minutos.\n', 3),
(91, 46, 'Basta una seduta per vedere i risultati?\n	', 'Per ottenere benefici da questo trattamento si devono implementare dei cicli che possono variare per numero di sedute tra 4 e 10 una volta a settimana a seconda della zona da trattare. \n', 1),
(92, 46, '¿Con una sesión basta para ver los resultados?\n	', 'Para obtener los beneficios de esta técnica se deben implementar ciclos inyectivos que pueden variar en número de 4 a 10 sesiones una vez a la semana según sea la zona a tratar.\n', 3),
(93, 47, 'Cosa succede dopo il trattamento?\n	', 'Si può presentare gonfiore e lividi. Con alcune tecniche è necessario creare dei pomfi che durano un paio di giorni.\n', 1),
(94, 47, '¿Qué sucede después del tratamiento?\n	', 'Se podrían presentar inflamación y hematomas. En algunas técnicas es necesario crear pequeños abultamientos que duran pocos días.\n', 3),
(95, 48, 'Si può fare in qualsiasi momento?\n	\n', 'Si! Non c’è un periodo specifico dell’anno per fare questo trattamento. Solo si deve fare attenzione se si formano i lividi di non prendere sole direttamente sulla zona trattata.\n', 1),
(96, 48, '¿El tratamiento se puede realizar en cualquier momento?\n	', '¡Sí! En cualquier período del año este tratamiento puede ser realizado. Si se forman hematómas, prevenir la exposición solar directamente en la zona.\n', 3),
(97, 49, 'Si deve fare un mantenimento?\n	\n', 'Più che un mantenimento, si realizzano sedute di richiamo, che permettono di mantenere i risultati.\n', 1),
(98, 49, '¿Debo realizar algún tipo de mantenimiento después que termino el ciclo?\n\n', '¡Sì! El mantenimiento consiste en repetir el ciclo.\n', 3),
(99, 50, 'Quanti cicli si devono fare in un anno?\n	', 'Da 2 a 3 cicli all\'anno, dipende dalle tue esigenze.\n', 1),
(100, 50, '¿Cuántos ciclos se realizan al año?\n	\n', 'Entre 2 a 3 ciclos al año, dependiendo de tus exigencias.\n', 3),
(101, 51, 'Il trattamento è doloroso?\n	', 'Non è doloroso, la procedura è fatta attraverso una cannula, per cui il trattamento risulta poco invasivo, facile e veloce da eseguire.\n', 1),
(102, 51, '¿El tratamiento es doloroso?\n	', 'No es doloroso, es un pocedimiento fácil y rápido de realizar.\n', 3),
(103, 52, 'Aumenta la dimensione del naso?\n	', 'Effettivamente sì, perché si sta utilizzando un prodotto per riempire spazi vuoti che però all’effetto ottico crea più armonia con la linearità del naso.\n', 1),
(104, 52, '¿La nariz aumenta de dimensión?\n	', 'Efectivamente sí, porque el ácido hialurónico se utiliza para rellenar los espacios vacios, pero al efecto óptico crea armonía y un mejor perfil.\n', 3),
(105, 53, 'Quanto dura il trattamento?\n	', 'È un trattamento abbastanza veloce e semplice da fare. È possibile realizzarlo in 30 – 45 minuti.\n', 1),
(106, 53, '¿Cuánto tiempo dura la sesión?\n	\n', 'Es posible realizar el tratamiento en unos 30 a 45 minutos.	\n', 3),
(107, 54, 'Quanto dura il risultato?\n	', 'Può durare anche un anno o un po’ di più. La velocità d\'assorbimento del prodotto è variabile di una persona ad altra.\n', 1),
(108, 54, '¿Cuánto tiempo dura el resultado?\n	\n', 'Puede durar un año/año y medio. La velocidad de absorción del producto es variable de una persona a otra.\n', 3),
(109, 55, 'Dopo il trattamento si deve fare qualcosa?\n	', 'Non usare occhiali per 48 ore e non toccare la zona trattata.\n', 1),
(110, 55, '¿Después del tratamiento es necesario hacer algo?\n	', 'No usar anteojos o lentes de sol por 48 horas y no tocar la zona tratada.\n', 3),
(111, 56, 'Il trattamento è doloroso?\n	', 'Il trattamento si basa sull’iniezione di un gas e questo causa una dolenza che dipende dalla sensibilità individuale. Tante persone lo tollerano molto bene.\n', 1),
(112, 56, '¿El tratamiento es doloroso?\n	', 'El tratamiento se basa en la inyección de un gas y esto puede causar un ligero malestar dependiendo de la sensibilidad de cada quien.\n', 3),
(113, 57, 'I benefici si vedono in una sola seduta?\n	', 'L’unico beneficio che si può ottenere in una sola seduta è quello di sentire le gambe più leggere e sgonfie, se non si fanno altre sedute questo effetto non si mantiene. \n', 1),
(114, 57, '¿Los beneficios se pueden ver desde la primera sesión?\n	', 'El único beneficio que se puede apreciar es “sentir” las piernas mucho más ligeras y menos dolorosas, debido a la reducción de la inflamación. Se debe continuar a realizar las sesiones para poder mantener este efecto. \n', 3),
(115, 58, 'Quante sedute devo fare per avere risultati estetici?\n	', 'Questo trattamento si fa in cicli di 10 sedute una volta a settimana e per iniziare a vedere benefici estetici bisogna di arrivare alla 4° - 5°. Poi occorre fare delle sedute di mantenimento una volta al mese.\n', 1),
(116, 58, '¿Cuántas sesiones se deben realizar para obtener resultados estéticos?\n	', 'Este tratamiento consiste en un ciclo de 10 sesiones a realizar una vez a la semana. Los beneficios estéticos se empiezan a notar después de la 4° - 5° sesión. Terminado el ciclo se realiza el mantenimiento una vez al mes.\n', 3),
(117, 59, 'Quali zone si possono trattare?\n	\n', 'Viso, collo, décolleté, gambe, addome e qualsiasi altra parte del corpo.\n', 1),
(118, 59, '¿Qué zonas se pueden tratar?\n	', 'Rostro, cuello, escote, piernas, abdomen y cualquier otra parte parte del cuerpo.\n', 3),
(119, 60, 'Ci sono dei rischi?\n	\n', 'No. Tuttavia il rossore localizzato e piccoli ematomi nella zona trattata sono comuni, ma si riassorbono velocemente.\n', 1),
(120, 60, 'Se pueden presentar riesgos?\n	.\n', 'No. Aunque si se puede presentar enrojecimiento localizado y pequeños hematomas en la zona tratada.\n', 3),
(121, 61, 'Può essere associata ad altri trattamenti?\n	\n', 'Si, la carbossiterapia funziona molto bene come potenziatore dei benefici di altri trattamenti di medicina estetica.\n', 1),
(122, 61, '¿Puede ser asociada a otros tipos de tratamientos?\n	', 'Sí, la carboxiterapia funziona muy bien como potenciador de los beneficios de otros tratamientos de la medicina estética como por ejemplo la mesoterapia. \n', 3),
(123, 62, 'Ho le gambe gonfie, appesantite e mi fano male, posso fare questo trattamento?\n	\n', 'Assolutamente sì, dalla prima seduta troverai molto sollievo, avrai la sensazione di camminare fra le nuvole.\n', 1),
(124, 62, 'Las pierna y/o tobillos se me inflaman y me duelen, ¿este trattamiento me ayudará?\n	\n', 'Sí, es una de las indicaciones principales de este tratamiento, desde la primera sesión tendrás la sensación de caminar entre las nubes.\n', 3),
(125, 63, 'Esiste qualche controindicazione? \n	', 'Sì. Persone con insufficienza epatica, respiratoria, renale o cardiaca, diabete, cancro, disturbi della circolazione, epilessia, asma attivo e problemi o infezioni nella zona da trattare potrebbero non essere qualificati per ricevere questo trattamento. Le donne in gravidanza o allattamento non possono applicare questo trattamento.\n', 1),
(126, 63, '¿El tratamiento esta contraindicado en algun caso?\n	', 'Sí. Personas con insuficiencia hepática (hígado), respiratoria, renal (riñón) y cardiaca, diabetes, cáncer, trastornos de la circulación, epilepsia, otros problemas de corazón, hígado y riñón, asma activa y problemas o infecciones en el área a ser tratada podrían no estar calificados para recibir este tratamiento. Las mujeres embarazadas o en periodo de lactancia tampoco podrán aplicarse este tratamiento.\n', 3),
(127, 64, 'Come si svolge il trattamento?\n	', 'In alcuni casi, potrebbe essere preferibile l\'applicazione di anestesia topica sulla zona prima di realizzare il trattamento. Consiste nell’effettuare piccoli spot a poca distanza tra di loro, esposti direttamente sulla superficie cutanea. Questo permette di accorciare la cute in eccesso e conservarne la perfetta plasticità. Il numero di sedute è variabile a seconda di ciò che si vuole trattare e viene stabilito dopo una accurata analisi.\n', 1),
(128, 64, '¿Cómo se realiza el tratamiento?\n	 ', 'Consiste en efectuar pequeños spot a poca distancia entre ellos sobre la superficie cutánea. Esto permite acortar la piel en ecceso y conservar su perfecta plasticidad. El número de seiones es variable, depende de lo que se quiere tratar y se establece después de un análisis completo.\n', 3),
(129, 65, 'Cosa accade dopo il trattamento?', '<ul>\n<li>Sulla zona trattata sarà visibile un sottile strato più scuro prodotto della sublimazione del tessuto cutaneo.</li>\n<li>Nelle ore successive ad ogni seduta potrebbe presentarsi edema localizzato che si riassorbirà in qualche giorno. </li>\n<li>A distanza di pochi giorni si formerà una crosticina da non rimuovere, essa cadrà entro 7-10 giorni. </li>\n<li>Si deve pulire con cura e con una detersione molto delicata. </li>\n<li>Si deve evitare il più possibile il contatto diretto e costante con i raggi del sole e proteggere con cura l’area trattata.</li> \n<li>Dopo la caduta delle crosticine l’area trattata si presenterà di colore rosaceo e piano piano riacquisterà la sua normale colorazione.</li>\n</ul>', 1),
(130, 65, '¿Qué sucede después del tratamiento?', '<ul align=\"center\"\">\n<li>En la zona tratada será visible un estrato fino y oscuro producto de la sublimación del tejido cutáneo.</li>\n<li>En las horas sucesivas al tratamiento se presentará edema localizado que tomará un par de días en reabsorberse. </li>\n<li>Se formará una crosta que no debe ser removida, esta caerá sola en unos 7 a 10 días.  </li>\n<li>Se debe limpiar con mucha atención y muy delicadamente. </li>\n<li>Se debe evitar el contacto directo y constante con los rayos del sol y se debe proteger la zona. </li>\n<li>Al caer la crosta la piel se presenta enrojecida, poco a poco adquistará el color normal de la piel.</li>\n</ul>', 3),
(131, 66, 'I risultati durano nel tempo?\n', 'Il risultato è definitivo e duraturo. Tuttavia i tempi di permanenza dei risultati dipendono molto dall’individuo, tipologia di pelle e dallo stile di vita. Con il passare del tempo, per il naturale processo d’invecchiamento, la pelle potrebbe nuovamente presentare inestetismi e cedimenti, quindi è necessario consigliare trattamenti preventivi.\n', 1),
(132, 66, '¿Cuánto tiempo dura el resultado?\n', 'Es definitivo. \n<br></br>\nEn caso de la blefaroplastica, se tiene que tomar en cuenta que con el pasar del tiempo, por el proceso natural de envejecimiento la piel nuevamente cederá. Se aconsejan siempre tratamientos preventivos.\n', 3),
(133, 67, 'Si può fare in qualsiasi momento?\n\n', 'Assolutamente no, questo è un trattamento che viene realizzato solo durante il periodo invernale.\n', 1),
(134, 67, '¿Se puede realizar en cualquier periodo del año?\n	', 'Es mejor realizarlo en periodo invernal y con una adecuada protección.\n', 3),
(135, 68, 'Quali sostanze si usano?\n	', 'Le sostanze che possono essere iniettate sono: acido ialuronico a bassa densità, aminoacidi, dei glicosaminoglicani, vitamine, PDRN (polidesossiribonucleotide), ecc.\n', 1),
(136, 68, '¿Qué sustancias se usan?\n	', 'Las sustancias que pueden ser inyectadas son: ácido hialurónico a baja densidad, aminoácidos,  glicosaminoglicanos, vitaminas, PDRN (polidexosiribonucleotidos), etc.\n', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'customers', 'Customer of ecommerce shop');

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue`
--

CREATE TABLE `lingue` (
  `id_lingue` int(11) NOT NULL,
  `nome_lingue` varchar(50) NOT NULL,
  `abbr_lingue` varchar(10) NOT NULL,
  `locale_paypal_lingue` varchar(10) NOT NULL,
  `codice_ci` varchar(150) NOT NULL,
  `stato_lingua` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue`
--

INSERT INTO `lingue` (`id_lingue`, `nome_lingue`, `abbr_lingue`, `locale_paypal_lingue`, `codice_ci`, `stato_lingua`) VALUES
(1, 'ITALIANO', 'IT', 'it_IT', 'italian', 1),
(2, 'ENGLISH', 'EN', 'en_GB', 'english', 0),
(3, 'ESPANOL', 'ES', 'es_ES', 'spanish', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine`
--

CREATE TABLE `pagine` (
  `id_pagina` int(11) NOT NULL,
  `nome_pagina` varchar(50) NOT NULL,
  `url_pagina` varchar(50) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `controller` varchar(250) NOT NULL,
  `tipo_pagina` varchar(25) NOT NULL,
  `trad_code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine`
--

INSERT INTO `pagine` (`id_pagina`, `nome_pagina`, `url_pagina`, `id_lingua`, `controller`, `tipo_pagina`, `trad_code`) VALUES
(1, 'default page', 'default_page', 1, 'frontend/Home', 'statica', ''),
(2, 'Home', 'it/home', 1, 'frontend/Home', 'statica', ''),
(3, 'Su di me', 'it/sudime', 1, 'frontend/Home/sudime', 'statica', ''),
(4, 'Mini biografia', 'it/minibiografia', 1, 'frontend/Home/minibiografia', 'statica', ''),
(5, 'Il mio lavoro', 'it/ilmiolavoro', 1, 'frontend/Home/ilmiolavoro', 'statica', ''),
(6, 'Privacy', 'it/privacy', 1, 'frontend/Home/privacy', 'statica', ''),
(7, 'Contattami', 'it/contattami', 1, 'frontend/Home/contattami', 'statica', ''),
(9, 'Programmi', 'it/programmi/(:any)/(:any)', 1, 'frontend/Programmi/detail/$1/$2', 'dinamica', 'programmi'),
(10, 'Trattamenti', 'it/trattamenti/(:any)/(:any)', 1, 'frontend/Trattamenti/detail/$1/$2', 'dinamica', 'trattamenti'),
(11, 'Home', 'en/home', 2, 'frontend/Home', 'statica', ''),
(12, 'About me', 'en/aboutme', 2, 'frontend/Home/sudime', 'statica', ''),
(13, 'Mini biography', 'en/minibiography', 2, 'frontend/Home/minibiografia', 'statica', ''),
(14, 'My work', 'en/mywork', 2, 'frontend/Home/ilmiolavoro', 'statica', ''),
(15, 'Privacy', 'en/privacy', 2, 'frontend/Home/privacy', 'statica', ''),
(16, 'Contact me', 'en/contactme', 2, 'frontend/Home/contattami', 'statica', ''),
(18, 'Programs', 'en/programs/(:any)/(:any)', 2, 'frontend/Programmi/detail/$1/$2', 'dinamica', 'programmi'),
(19, 'Treatments', 'en/treatments/(:any)/(:any)', 2, 'frontend/Trattamenti/detail/$1/$2', 'dinamica', 'trattamenti'),
(20, 'Inicio', 'es/inicio', 3, 'frontend/Home', 'statica', ''),
(21, 'Sobre mi', 'es/sobremi', 3, 'frontend/Home/sudime', 'statica', ''),
(22, 'Mini biografía', 'es/minibiografía', 3, 'frontend/Home/minibiografia', 'statica', ''),
(23, 'Mi trabajo', 'es/mitrabajo', 3, 'frontend/Home/ilmiolavoro', 'statica', ''),
(24, 'Politica de privacidad', 'es/politicadeprivacidad', 3, 'frontend/Home/privacy', 'statica', ''),
(25, 'Contactame', 'es/contactame', 3, 'frontend/Home/contattami', 'statica', ''),
(27, 'Programas', 'es/programas/(:any)/(:any)', 3, 'frontend/Programmi/detail/$1/$2', 'dinamica', 'programmi'),
(28, 'Tratamientos', 'es/tratamientos/(:any)/(:any)', 3, 'frontend/Trattamenti/detail/$1/$2', 'dinamica', 'trattamenti'),
(31, 'Gratis', 'it/gratis', 1, 'frontend/Home/gratis', 'statica', ''),
(32, 'Gratis', 'es/gratis', 3, 'frontend/Home/gratis', 'statica', ''),
(33, 'Retreats', 'it/retreats/(:any)', 1, 'frontend/Retreats/detail/$1', 'dinamica', 'retreats'),
(34, 'Escapadas', 'es/escapadas/(:any)', 3, 'frontend/Retreats/detail/$1', 'dinamica', 'retreats');

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine_contenuti`
--

CREATE TABLE `pagine_contenuti` (
  `id_pc` int(11) NOT NULL,
  `pc_page_code` varchar(150) NOT NULL,
  `pc_titolo_1` varchar(250) NOT NULL,
  `pc_immagine_1` varchar(250) NOT NULL,
  `pc_testo_body_1` longtext NOT NULL,
  `pc_titolo_2` varchar(250) NOT NULL,
  `pc_immagine_2` varchar(250) NOT NULL,
  `pc_testo_body_2` longtext NOT NULL,
  `pc_testo_bold_1` text NOT NULL,
  `pc_testo_bold_2` text NOT NULL,
  `pc_testo_bold_3` text NOT NULL,
  `pc_top_image` varchar(250) NOT NULL,
  `pc_top_image_mobile` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine_contenuti`
--

INSERT INTO `pagine_contenuti` (`id_pc`, `pc_page_code`, `pc_titolo_1`, `pc_immagine_1`, `pc_testo_body_1`, `pc_titolo_2`, `pc_immagine_2`, `pc_testo_body_2`, `pc_testo_bold_1`, `pc_testo_bold_2`, `pc_testo_bold_3`, `pc_top_image`, `pc_top_image_mobile`, `id_lingua`) VALUES
(1, 'HOME', 'Home', 'tiracconto3.jpg', 'Ti racconto...<br><br>Una gran parte delle tue paure riguardo la medicina estetica, vengono alimentate dalla società stessa, dove ti fanno vedere orrori o ti fanno credere che la bellezza fisica non conta.<br><br>\n    Per me conta (e siamo sinceri, perché vivi in un mondo che è materia), ma conta quando hai in <span class=\"blu-text\"><b>equilibrio la bellezza esterna e quella interiore.</b></span><br>\n    Forse non conosci la Medicina Estetica e se la “conosci” (e questo non significa che sai veramente di cosa si tratta), hai paura perché pensi che ti gonfierà, ti trasformerà o diventerai un’altra.<br><br>\n    Io <span class=\"orange-text\"><b>ti offro armonia ed equilibrio</b></span>, rimarrai chi sei fisicamente ma ti vedrai e ti vedranno <span class=\"blu-text\"><b>meglio, riposata e fantastica</b></span> e se me lo permetti ti guiderò verso la tua rinnovazione interna.\n    <br><span class=\"orange-text\"><b>Rompi le credenze limitanti</b></span>, per quello che riguarda la bellezza esteriore, <span class=\"blu-text\"><b>non avere vergogna</b></span> di sentirti bene dentro della tua propria pelle e cercare di sentirti più bella.<br><br>\n    <span class=\"orange-text\"><b>Fai quello che senti</b></span> di fare, <span class=\"blu-text\"><b>ma sentilo con il cuore</b></span> e vivi con piacere <span class=\"orange-text\"><b>l\'armonia esterna e l\'equilibrio interno</b></span> che potresti raggiungere se veramente lo vuoi.<br><br><span class=\"blu-text text-24\"><b>Tutto dipende da te!</b></span>', '', '', '', 'Ciao, sono Orlena Zotti, medico estetico, creatrice di <span class=\"orange-text\"><b>\"Sei Essenzialmente Bella\"</b></span>.<br>Aiuto le donne a trovare la stabilità e rafforzare l\'amor proprio attraverso l\'armonia fisica.<br>Amo fornire strumenti alle donne che vogliono un cambiamento e cercano di connettersi con l\'essenza della bellezza.', '<span class=\"text-24\"><b>Cosa è la medicina estetica?</b></span><br>\nLa definirei come tutto quello che è alternativo alla chirurgia plastica per armonizzare la bellezza fisica, tramite trattamenti realizzati con procedure poco o per niente invasive.', '“La Medicina Estetica è un viaggio, un percorso, che si realizza passo a passo.<br>\nL’obbiettivo è farti sentire bene dentro della tua pelle in modo naturale ed equilibrato.”', '98898-ina-10.png', '20fa5-ima-10.png', 1),
(2, 'HOME', 'Home', 'tiracconto3.jpg', 'I tell you ...<br><br>A lot of your fears about aesthetic medicine are fueled by the company itself, where they show you horrors or make you believe that physical beauty does not matter.<br/><br/>\r\n    For me it counts (and we are honest, because you live in a world that is matter), but it matters when you are in the <span class=\"blu-text\"><b>balance between outer and inner beauty.</b></span><br>\r\n    Maybe you do not know the Aesthetic Medicine and if you \"know\" (and this does not mean you really know what it is), you\'re afraid because you think it will swell, turn you or you will become another.<br><br>\r\n    <span class=\"orange-text\"><b>I offer you harmony and balance</b></span>, you will be who you are physically but you will see and will see you <span class=\"blu-text\"> better, rested and fantastic</b></span> and if you let me, I will guide you to your inner renewal.\r\n    <br><b> Break the Limiting Beliefs </b> </ span>, as far as the outside beauty is concerned, <span class = \"orange-text\"> <b> do not have shame </b></span> to feel good inside your own skin and try to feel more beautiful. <br> <br>\r\n    <span class=\"orange-text\"><b>Do what you feel</b></span> do <span class=\"blu-text\"><b> but feel it with your heart </b></span> and you will enjoy <span class=\"orange-text\"><b>external harmony and interior renewal</b></span> that you could achieve if you really want it.<br><br><span class=\"blu-text text-24\"><b>Everything depends on you!</b></span>', '', '', '', 'Hello, I\'m Orlena Zotti, aesthetic, creator of <span class=\"orange-text\"><b>\"Sei essenzialmente bella\"</b></span>.<br/>Helping women find stability and strengthen love through physical harmony. <br> I love to provide tools to women who want a change and seek to connect with the essence of beauty.', '<span class=\"text-24\"><b>What is aesthetic medicine?</b></span><br>\r\nI would define it as all that is alternative to plastic surgery to harmonize physical beauty, through treatments carried out with little or no invasive procedures.', '“Aesthetic Medicine is a journey, a journey that takes place step by step.<br/>\r\nThe goal is to make you feel more beautiful by believing others that you simply rested.”', '', '', 2),
(3, 'HOME', 'Home', 'tiracconto3.jpg', 'Te cuento...<br><br>Una gran parte de tus miedos a la medicina estética se alimenta de la sociedad en la que vives, que te hace ver los resultados más inapropiados provocados por quien no tiene la capacidad y/o preparación en esta disciplina o quien te hace creer que la belleza física no cuenta.<br><br>\n    Para mí cuenta (y seamos sinceras, vives en un mundo que es materia), pero cuenta cuando se tiene en <span class=\"blu-text\"><b>equilibrio la belleza externa y la belleza interna.</b></span><br>\n    Quizás no conoces la Medicina Estética y si la “conoces” (esto no significa que sabes verdaderamente de que se trata), sientes miedo porque piensas que te inflamarás, te transformarás o te convertirás en otra.<br><br>\n    Yo <span class=\"orange-text\"><b>te ofrezco armonía y equilibrio</b></span>, serás quien eres fisicamente, pero te verás y te verán <span class=\"blu-text\"><b>mejor, reposada, fresca y fantástica </b></span> y si me lo permites te guiaré a tu renacer interior.\n    <br><span class=\"orange-text\"><b>Rompe las creencias limitantes</b></span>, sobre la bellezza externa,<span class=\"blu-text\"><b>no te avergüences </b></span> de sentirte bien dentro de tu propia piel y de buscar ayuda para sentirte más bonita.<br><br>\n    <span class=\"orange-text\"><b>Haz aquello que sientes</b></span> que es mejor para tí, <span class=\"blu-text\"><b>pero siéntelo con el corazón</b></span> y vive con placer la <span class=\"orange-text\"><b>armonía externa y equilibrio interno</b></span> que podrías lograr si así lo quieres.<br><br><span class=\"blu-text text-24\"><b>¡Todo depende de tí!</b></span>', '', '', '', 'Hola, soy Orlena Zotti, medico estético, mi negocio se llama <span class=\"orange-text\"><b>\"Eres Esencialmente Bella\"</b></span>.<br>Yo ayudo a las mujeres a sentirse bien dentro de su piel a través del amor propio y el cuidado propio. Amo ofrecer herramientas a las mujeres que quieren un cambio y buscan conectar con la esencia de la belleza.', '<span class=\"text-24\"><b>¿Qué es la medicina estética para mi?</b></span><br>\nLa definiría como un conjunto de tratamientos médicos, realizados con procedimientos poco o para nada invasivos, que son alternativos a la cirugía plástica, logrando así armonizar la belleza física.', '“La Medicina Estética es un viaje, un recorrido, que se realiza paso a paso.\nEl objetivo es hacerte sentir bien dentro de tu propia piel en un modo natural y equilibrado.”', '6dc2c-ina-11.png', '7250b-ima-11.png', 3),
(4, 'SUDIME', 'Su di me', 'sudime.jpg', '<p align=\"justify\">A questo paese mi ha portato la solitudine, il sentirmi come un pesce fuor d\'acqua, la sensazione di non appartenere al luogo in cui mi trovavo né alla maniera di fare la medicina estetica come la facevo. Ho pensato che cambiare ambiente poteva far migliorare la situazione, ma questo non è successo.\n                        </p>\n                        <p align=\"justify\">\n                            Allora ho iniziato la mia ricerca di tutto ciò che mi avrebbe aiutato a capire cosa volevo veramente, cosa mi rendeva felice e, soprattutto, cosa sono venuta a fare in questo mondo.\n                        </p>\n                        <p align=\"justify\">\n                        	Prima di tutto, ho deciso di ampliare le mie competenze come medico estetico ed estendere la conoscenza ad altri orizzonti del benessere globale, così ho intrapreso la lettura di molti libri sull’evoluzione spirituale, ho scoperto il mondo della meditazione, ho frequentato diversi corsi di crescita personale e su come nutrire il mio corpo e la mia anima. Tutto questo si è aggiunto alla mia passione, la Medicina Estetica.\n                        </p>\n                        <p align=\"justify\">\n                        	Così mi è sorto il desiderio di applicare alla medicina estetica questo viaggio interiore e ho cominciato ad unificare tutti gli strumenti che ho sperimentato in programmi specifici. Questo mi ha permesso di creare un modo per offrire aiuto alle donne, armonizzando la bellezza esteriore ed equilibrando la bellezza interiore, accorpando due mondi che la società ha venduto come diversi, ma che per me sono inseparabili perché entrambi si nutrono equilibratamente.\n                        </p>', 'Mini biografia', '', '<p>\n                       Ho conseguito la laurea di Medico Chirurgo in Venezuela (il mio paese di nascita) nel 2005, subito dopo ho conosciuto il mondo della medicina estetica e me ne sono innamorata, tanto che ne ho fatto la mia unica professione.\n                    </p>\n                    <p>\n                        Sono arrivata in Italia nel 2010 e ho seguito la procedura per il riconoscimento della mia laurea in questo paese, che porto nel mio sangue grazie a mio nonno.\n                    </p>\n                    <p>\n                        Per perfezionare le mie conoscenze in questo campo ho seguito un percorso quadriennale di studi conseguendo il Diploma di Medicina Estetica della Fondazione Internazionale Fatebenefratelli a Roma.\n                    </p>\n                    <p>\n                        Ora mi trovo in Sardegna, dove, grazie al mio patrimonio di conoscenze, svolgo il mio lavoro offrendo un concetto di medicina estetica unico ed equilibrato. \n                    </p>', '', '', '', '', '', 1),
(5, 'SUDIME', 'About me', 'sudime.jpg', '<br/><p align=\"justify\">Here loneliness has been brought to me, feeling like a fish out of water, feeling not to belong to the place where I was or how to do aesthetic medicine as I did. I thought that changing the environment could improve the situation, but that did not happen.\r\n                        </p>\r\n						<br/>\r\n                        <p align=\"justify\">\r\n                            Then I started my search for everything that would help me to understand what I really wanted, what made me happy and, above all, what I came to do in this world.\r\n                        </p>\r\n						<br/>\r\n                        <p align=\"justify\">\r\n                        	First of all, I decided to expand my skills as an aesthetic doctor and extend knowledge to other horizons of global well-being, so I started reading many books about spiritual evolution, I discovered the world of meditation, I attended several courses of personal growth and how to nourish my body and soul. All this has been added to my passion, Aesthetic Medicine.\r\n                        </p>\r\n						<br/>\r\n                        <p align=\"justify\">\r\n                        	So I had the desire to apply this inner journey to aesthetic medicine and I began to unify all the tools I experienced in specific programs. This has allowed me to create a way of offering women\'s help, harmonizing the exterior beauty and balancing the inner beauty, by merging two worlds that the society has sold as different, but which for me are inseparable because they both nourish equilibrally.\r\n                        </p>', 'Mini Biografy', '', '<p>\r\n                       I graduated from the Medical Surgeon in Venezuela (my birth country) in 2005, soon after I met the world of aesthetic medicine and I fell in love with him so much that I did my only job.\r\n                    </p>\r\n                    <p>\r\n                        I arrived in Italy in 2010 and followed the procedure for the recognition of my degree in this country, which I bring in my blood thanks to my grandfather.\r\n                    </p>\r\n                    <p>\r\n                        To refine my knowledge in this field I followed a four-year course of study by completing the Diploma of Aesthetic Medicine of the Fatebenefratelli International Foundation in Rome.\r\n                    </p>\r\n                    <p>\r\n                        Now I am in Sardinia, where, thanks to my knowledge of wealth, I do my job by offering a concept of unique and balanced aesthetic medicine.\r\n                    </p>', '', '', '', '', '', 2),
(6, 'SUDIME', 'Sobre mi', 'sudime.jpg', '<p align=\"justify\"><br>A este país (Italia) me trajo la soledad, el sentirme un pez fuera del agua, el sentir que no pertenecía al lugar donde me encontraba, ni a la manera de hacer la medicina estética que hacía, pensé que cambiando de ambiente podría mejorar el como me sentía, pero esto no sucedió.\n                        </p>\n                        <p align=\"justify\">\n                            Entonces comencé a buscar lo que fuera que me ayudara para comprender que quería de verdad, que me hacía feliz y sobre todo que vine a hacer en este mundo.\n                        </p>\n                        <p align=\"justify\">\n                        	Inicié perfeccionando mis conocimientos como médico estético, leí muchos libros sobre evolución espiritual, conocí el mundo de la meditación, hice muchos cursos de crecimiento personal y de como nutrir mi cuerpo y todo esto se sumó a mi pasión: la medicina estética.\n                        </p>\n                        <p align=\"justify\">\n                        	Así me invadieron las ganas de aplicar a la medicina estética este recorrido interno y fue cuando empecé a unificar en programas todas las herramientas que he experimentado.<br><br>Esto me ha permitido crear una forma de ofrecer ayuda a las mujeres, armonizando la belleza externa y equilibrando la belleza interna, acomplando dos mundos que la sociedad a vendido como diferentes pero que para mí son inseparables porque los dos se nutren equilibradamente.\n                        </p>', 'Mini biografía', '', '<p>\n                      Me gradué de Médico Cirujano en Venezuela (mi país de nacimiento) en el 2005, casi inmediatamente después, conocí el mundo de la medicina estética y me enamoré de ella, tanto que la trasformé en mi única profesión.\n                    </p>\n                    <p>\n                        Legué a Italia en el 2010 y realicé todo el procedimiento para validar mi título en este país, que llevo en mi sangre gracias a mi abuelo.\n                    </p>\n                    <p>\n                        Para perfeccionar mis conocimientos en este campo seguí un curso de cuatro años opteniendo un Diploma de Medicina Estética de la Fondazione Internazionale Fatebenefratelli en Roma.\n                    </p>\n                    <p>\n                      Ahora me encuentro en Cerdeña, donde, gracias a mi patrimonio de conocimientos, desarrollo mi trabajo ofreciendo un concepto de medicina estetica único y equilibrado.\n                    </p>\n                    <p>\n                        ', '', '', '', '', '', 3),
(7, 'MIOLAVORO', 'Il mio lavoro', 'ilmiolavoro.jpg', '<p align=\"justify\">\n                           Integrare la bellezza fisica e la crescita interna è una vera e propria missione, perché in questo risveglio alla consapevolezza che stiamo avendo, \"il nostro corpo è il nostro tempio\", ma attualmente, in questo risveglio, questa frase serve solo se ti prendi cura del tuo corpo attraverso l’alimentazione e l\'attività fisica, in questo modo ti puoi mantenere in equilibrio.\n                        </p>\n                        <p align=\"justify\">\n                            Ma come si può includere la bellezza in questo momento di presa di consapevolezza?\nPer molti anni la bellezza fisica è stata stigmatizzata perché per il pensiero comune non importa come ti vedi (brutta o carina) o ti senti, se non chi sei e come sei. Per questo quello che io propongo è armonizzare la tua bellezza, ad esempio far diventare le tue rughe meno evidenti e far diventare il tuo volto più riposato e fresco. Non avere vergogna di volerti guardare meglio e di sentirti bene dentro la tua pelle.\n                        </p>\n                        <p align=\"justify\">\n                        	Regala al tuo corpo e al tuo volto l\'aria fresca che ha perso nel corso degli anni, ed è solo questo, aria fresca, non è una ricostruzione, non è magia, non è diventare qualcun altro. Non significa smettere di essere chi sei. Ti aiuterò a ritrovare la bellezza che non vedi o che pensi di aver perso. Ti aiuterò ad iniziare a vedere con occhi diversi, con occhi d’amore per te stessa.\n                        </p>\n                        <p align=\"justify\">\n                        	La bellezza che per molte persone si sprigiona da dentro verso fuori per la maggior parte si riflette dall\'esterno verso l\'interno; perché molte volte guardare allo specchio quello che si può ottenere fa risplendere la tua luce interiore con più intensità e ti aiuta a vedere ciò che era nascosto, dal passare degli anni, dalla mancanza d’amore per te stessa,  dalla mancanza di tempo.\n                        </p>', '', '', '', 'Ricordati che il tuo corpo è il tuo tempio e tante volte l\'attività fisica e il mangiare sano non è sufficiente, è anche importante per il tuo tempio un soffio d’aria fresca.', '', '', '', '', 1),
(8, 'MIOLAVORO', 'My work', 'ilmiolavoro.jpg', '<br/><p align=\"justify\">\r\n                           Integrating physical beauty and internal growth is a real mission, because in this awakening to the awareness that we are having, \"our body is our temple,\" but in this awakening, this phrase only serves if you take care of your body through nutrition and physical activity, in this way you can keep it in balance.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                            But how can beauty be included in this awareness-raising moment?\r\nFor many years, physical beauty has been stigmatized because for common thinking it does not matter how you see (ugly or pretty) or you feel, if you are not who you are and how are you. For this, what I propose is to harmonize your beauty, for example to make your wrinkles less noticeable and make your face more rested and cool. Do not be ashamed of wanting to look better and feel good inside your skin.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                        	Give your body and your face fresh air that has lost over the years, and that\'s just this, fresh air, it\'s not a reconstruction, it\'s no magic, it\'s not becoming someone else. It does not mean to stop being who you are. I will help you find the beauty you do not see or think you\'ve lost. I will help you begin to see with different eyes, with eyes of love for yourself.\r\n                        </p>\r\n						<br/>\r\n                        <p align=\"justify\">\r\n                        	The beauty that for many people emanates from the inside out is mostly reflected from the outside inward; because many times you look in the mirror what you can get brings your inner light shining with more intensity and helps you to see what was hidden from years of lack of love for yourself by the lack of time.\r\n                        </p>', '', '', '', 'Remember that your body is your temple and many times physical activity and healthy eating is not enough, it is also important for your temple to breathe fresh air.', '', '', '', '', 2),
(9, 'MIOLAVORO', 'Mi trabajo', 'ilmiolavoro.jpg', '<p align=\"justify\">\n                            Es una verdadera misión  integrar belleza física y crecimiento interno,  porque en este despertar de consciencia que estamos teniendo,  \"nuestro cuerpo es nuestro templo\". Pero actualmente en este despertar este frase sirve solo si tú cuidas tu cuerpo desde la alimentación y la actividad física, de esta manera te mantendrás en equilibrio contigo misma.\n                        </p>\n                        <p align=\"justify\"> \n¿Pero cómo uno puede incluir la bellezza en este tiempo de tomar consciencia?  \nPor muchos años la belleza física ha sido atacada y ha sido tomada como superficial, porque no importa el como te veas (fea o bonita) o te sientas,  si no quién eres y cómo eres. Por eso lo que yo propongo, por ejemplo, es armonizar tu belleza,  que las arrugas sean menos evidentes y que tu rostro se vea más reposado y fresco. No te avergüences por querer verte mejor y sentirte bien dentro de tu propia piel.\n                        </p>\n                        <p align=\"justify\">\n                        	Regala a tu cuerpo y a tu rostro ese aire fresco que ha perdido con los años y solo es eso,  aire fresco,  no es una reconstrucción,  no es magia,  no es convertirte en otra persona. No es que dejes de ser quien eres. Yo te ayudo a encontrar esa belleza que no ves o esa belleza que piensas haber perdido. Te ayudo a que te empieces a ver con otros ojos,  con ojos de amor por ti misma.\n                        </p>\n                        <p align=\"justify\">\n                        	Lo que a muchos le funciona de adentro hacia fuera a otros les funciona de manera increíble de afuera hacia adentro; porque muchas veces viendo al espejo lo que se puede obtener eso hace que la luz interna brille  con más intensidad y que empieces a ver lo que estaba escondido,  por el desgaste de los años,  por la falta de amor a tí misma,  por falta de tiempo.\n                        </p>', '', '', '', 'Recuerda que tu cuerpo es tu templo y que muchas veces la actividad física y la alimentación más saludable no basta,  también es importante para tu templo el aire fresco.', '', '', '', '', 3),
(10, 'PRIVACY', 'Privacy', '', '<p align=\"justify\">Ai sensi del decreto legislativo 30 giugno 2003 n. 196, Sei essenzialmente bella, in qualità di Titolare autonomo del trattamento, è tenuta a fornire alcune informazioni riguardanti l\'utilizzo dei dati personali da Lei forniti, ovvero altrimenti acquisiti nell\'ambito della rispettiva attività.\n<br></br>\nTitolare del trattamento dei dati: Dottoressa Orlena Zotti. Via sumirago, 31, 00188. Roma \n<br></br>\nFonte dei dati personali.\nI dati personali cui ci si riferisce sono raccolti direttamente da questo sito web. Tutti questi dati verranno trattati nel rispetto della citata legge e degli obblighi di riservatezza cui si è sempre ispirata l\'attività dello studio.</p>\n                    <p align=\"justify\">\n                       <b>Verranno raccolti i seguenti dati:</b>\n                       <ul class=\"privacy-ul\">\n                        <li>indirizzo IP dell\'utente;</li>\n                        <li>indirizzo e-mail personale;</li>\n                        <li>indirizzo URL di provenienza;</li>\n                        <li>numero di telefono</li>\n                        <li>nome</li>\n                        <li>Paese -città</li>\n                        <li>cognome</li>\n                       </ul>\n                    </p>\n                    <p align=\"left\">\n                       <b>Finalità del trattamento cui sono destinati i dati I dati personali da Lei forniti:</b>\n                       <ul class=\"privacy-ul\">\n                       	<li>completamento e supporto dell\'accesso;</li>\n                        <li>per eseguire obblighi di legge;</li>\n                        <li>per esigenze di tipo operativo e gestionale</li>\n                        <li>per inziative e invio di newsletters riservate agli iscritti al sito</li>\n					  </ul>\n                    </p>\n                    <p align=\"justify\">Modalità di trattamento dei dati\nIn relazione alle indicate finalità,il trattamento dei dati personali avviene mediante strumenti manuali, informatici e telematici con logiche strettamente correlate alle finalità stesse e, comunque, in modo da garantire la sicurezza e la riservatezza dei dati stessi. Il trattamento dei dati avverrà mediante strumenti idonei a garantirne la sicurezza e la riservatezza e potrà essere effettuato anche attraverso strumenti automatizzati atti a memorizzare, gestire e trasmettere i dati stessi. I dati forniti non verranno ceduti e/o rivenduti a soggetti terzi.<br><br>\nDiritti di cui all\'art.7 La informiamo, infine, che l\'art. 7 del decreto legislativo 196/2003 conferisce agli interessati l\'esercizio di specifici diritti. In particolare, l\'interessato può ottenere dal Titolare la conferma dell\'esistenza o no di propri dati personali e che tali dati vengano messi a sua disposizione in forma intelligibile. L\'interessato può altresì chiedere di conoscere l\'origine dei dati personali, la finalità e modalità del trattamento; la logica applicata in caso di trattamento effettuato con l\'ausilio di strumenti elettronici; gli estremi identificativi del titolare e del responsabile; di ottenere la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge nonché l\'aggiornamento, la rettificazione o, se vi è interesse, l\'integrazione dei dati; di opporsi, per motivi legittimi, al trattamento di dati che lo riguardano anche ai fini di invio di materiale pubblicitario o di vendita diretta.<br><br>I diritti in oggetto potranno essere esercitati, anche per il tramite di un incaricato, mediante richiesta rivolta al responsabile nominato con lettera. Nell\'esercizio dei diritti, l\'interessato può conferire per iscritto, delega o procura a persone fisiche, enti, associazioni od organismi. L\'interessato può, altresì, farsi assistere da una persona di fiducia. Ulteriori informazioni potranno essere rischieste, per iscritto, via email a sei@seiessenzialmentebella.com. Sede legale: via sumirago, 31, 00188. Roma. Italia.\n                    </p>\n					<p align=\"justify\">\n					<b>Informazioni sui cookie</b><br/>\n					I cookie sono stringhe di testo (piccole porzioni di informazioni) che vengono memorizzati su computer, tablet, smartphone, notebook, da riutilizzare nel corso della medesima visita (cookie di sessione) o per essere ritrasmessi agli stessi siti in una visita successiva.<br/><br/>Ai sensi dell\'art. 13 del D.l.vo n. 196/2003 questo sito utilizza unicamente cookie tecnici od a questi assimilati, che non richiedono un preventivo consenso; si tratta di cookie necessari, indispensabili per il corretto funzionamento del sito, servono per effettuare la navigazione.\n					</p>\n', '', '', '', '', '', '', '', '', 1),
(11, 'PRIVACY', 'Privacy', '', '<p align=\"justify\">Pursuant to Legislative Decree no. 196, You are essentially beautiful, as a sole proprietor of the treatment, you are required to provide some information regarding the use of the personal data you provide, or otherwise acquired in the course of your business. Source of personal data.\r\nThe personal data we refer to is collected directly from this website. All of these data will be processed in compliance with the said law and the confidentiality requirements that have always been inspired by the activity of the study.</p>\r\n					<p align=\"justify\">\r\n                       <b>The following data will be collected:</b>\r\n                       <ul class=\"privacy-ul\">\r\n                        <li>user IP address</li>\r\n                        <li>ersonal email address</li>\r\n                        <li>source URL</li>\r\n                        <li>phone number</li>\r\n                        <li>name</li>\r\n                        <li>surname</li>\r\n                       </ul>\r\n                    </p>\r\n					\r\n					<p align=\"left\">\r\n                       <b>Purpose of the treatment to which the data is intended The personal data you provide:</b>\r\n                       <ul class=\"privacy-ul\">\r\n                       	<li>completion and Access Support;</li>\r\n                        <li>to enforce legal obligations;</li>\r\n                        <li>for operational and management needs</li>\r\n                        <li>for promotions and promotional communications reserved for members of the site</li>\r\n					  </ul>\r\n                    </p>\r\n                    <p align=\"justify\">How data is processed in relation to the aforementioned purposes, the processing of personal data is done through manual, computer and telematic tools with logic strictly related to the purposes themselves and, in any case, in order to guarantee the security and confidentiality of the data themselves. Data processing will be carried out by means of appropriate tools to ensure its security and confidentiality, and it can also be carried out by means of automated tools for storing, managing and transmitting the data itself. The provided data will not be transferred and / or resold to third parties\r\nThe rights referred to in art. 7 We inform you, finally, that art. 7 of Legislative Decree 196/2003 grants the persons concerned the exercise of specific rights. In particular, the data subject may obtain from the Registrar the confirmation of the existence or not of his / her personal data and that such data shall be made available to him in an intelligible form. The person concerned may also ask to know the origin of personal data, the purpose and the manner of processing; the logic applied in the case of processing carried out with the aid of electronic instruments; the identification details of the holder and the manager; to obtain the cancellation, transformation into anonymous form or the blocking of the data processed in violation of the law as well as the updating, correction or, if there is interest, the integration of the data; to oppose, for legitimate reasons, the processing of data concerning him also for the purpose of sending advertising material or direct sale. <br> <br> The rights in question may also be exercised, through an agent as well request addressed to the person named by letter. In the exercise of rights, the person concerned may confer in writing, delegate or procure to natural persons, bodies, associations or bodies. The person concerned can also be assisted by a trusted person. Further information can be obtained, in writing, at the headquarters of the medical office, located in Rome, Via delle Belle Arti, 7 - 00196\r\n                    </p>\r\n					<p align=\"justify\">\r\n					<b>About cookies</b><br/>\r\n					Cookies are text strings (small portions of information) that are stored on computers, tablets, smartphones, notebooks, reusable during the same visit (session cookies) or retransmitted to the same sites on a subsequent.<br/><br/>Visit. art. 13 of Legislative Decree no. 196/2003 this site uses only technical cookies or similar assimilates, which do not require prior consent; these are the necessary cookies, essential for the proper functioning of the site, they serve to make navigation.\r\n					</p>', '', '', '', '', '', '', '', '', 2),
(12, 'PRIVACY', 'Politica de privacidad', '', '<p align=\"justify\">De conformidad con el Decreto Legislativo no. 196, Sei essenzialmente Bella,  es autorizado por el usuario al uso de la información personal que proporcione, dentro de la actividad. Fuente de datos personales. Los datos personales a los que nos referimos se recogen directamente en este sitio web. Todos estos datos serán procesados en cumplimiento de dicha ley y los requisitos de confidencialidad que siempre se han inspirado en la actividad del estudio.</p>\n                    <p align=\"justify\">\n                       <b>Se recogerán los siguientes datos:</b>\n                       <ul class=\"privacy-ul\">\n                        <li>dirección IP del usuario;</li>\n                        <li>dirección de correo electrónico personal;</li>\n                        <li>URL de origen;</li>\n                        <li>número de teléfono</li>\n                        <li>nombre</li>\n                        <li>pais></li>\n                        <li>apellido</li>\n                       </ul>\n                    </p>\n                    <p align=\"left\">\n                       <b>Finalidad del tratamiento al que se destinan los datos personales que usted proporciona:</b>\n                       <ul class=\"privacy-ul\">\n                       	<li>apoyo y complemento del acceso;</li>\n                        <li>para hacer cumplir las obligaciones legales;</li>\n                        <li>para las necesidades operacionales y de gestión;</li>\n                        <li>para promociones y comunicaciones promocionales reservadas a los miembros del sitio;</li>\n					  </ul>\n                    </p>\n                    <p align=\"justify\">MMétodos de procesamiento de datos en relación con las finalidades indicadas, el tratamiento de datos personales son tratados con herramientas manuales, equipo y datos con lógica estrictamente relacionados con los mismos fines y, de todos modos, a fin de garantizar la seguridad y confidencialidad de los datos. Los datos se procesan utilizando medios adecuados para garantizar la seguridad y la confidencialidad y se pueden realizar usando herramientas automatizadas para almacenar, gestionar y transmitir los datos. Los datos proporcionados no serán cedidos y / o revendidos a terceros.<br><br>\nLos derechos a que se refiere el artículo 7. Le informamos, que el art. 7 del Decreto Legislativo 196/2003 otorga a los interesados el ejercicio de derechos específicos. En particular, el interesado podrá obtener del registrador la confirmación de la existencia o no de sus datos personales y que dichos datos se pondrán a su disposición de forma inteligible. El interesado también puede solicitar conocer el origen de los datos personales, la finalidad y la forma de tratamiento; la lógica aplicada en el caso de la transformación realizada con ayuda de instrumentos electrónicos; los datos de identificación del titular y del administrador; para obtener la cancelación, la transformación en forma anónima o el bloqueo de los datos tratados en violación de la ley y la actualización, corrección o, si está interesado, la integración de los datos; para oponerse, por motivos legítimos, al tratamiento de los datos que le conciernen también con el fin de enviar material publicitario o ventas directas.<br><br>Los derechos en cuestión también podrán ejercerse, incluso a través de una persona encargada, mediante una solicitud dirigida a la persona designada por carta. En el ejercicio de los derechos, la persona interesada podrá conferir por escrito, delegar o procurar a personas físicas, organismos, asociaciones u organismos. Sede legal: Via sumirago,31,00188. Roma. Italia\n                    </p>\n					<p align=\"justify\">\n					<b>Acerca de las cookies</b><br/>\n					Las cookies son cadenas de texto (pequeñas porciones de información) almacenadas en computadoras, tabletas, smartphones, portátiles, reutilizables durante la misma visita (cookies de sesión) o retransmitidas en los mismos sitios en una visita posterior.<br/><br/>Según el art. 13 del Decreto Legislativo núm. 196/2003 este sitio usa solamente cookies tecnicos o relacionadas a estos que no requieren consentimiento previo; son necesarios para el buen funcionamiento del web, sirven para hacer la navegación.\n					</p>', '', '', '', '', '', '', '', '', 3),
(13, 'GRATIS', 'Gratis', '', '<p align=\"justify\">Gratis ita</p>\n', '', '', '', '', '', '', '', '', 1),
(14, 'GRATIS', 'Gratis', '', '<p align=\"justify\">Gratis es</p>', '', '', '', '', '', '', '', '', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `programmi`
--

CREATE TABLE `programmi` (
  `id_programma` int(11) NOT NULL,
  `titolo_programma` varchar(500) NOT NULL,
  `stato_programma` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `programmi`
--

INSERT INTO `programmi` (`id_programma`, `titolo_programma`, `stato_programma`) VALUES
(1, 'Un soffio d\'aria fresca ', 3),
(3, 'Rigenera il tuo corpo', 3),
(4, 'Alimenta corpo e anima', 1),
(5, 'La tua valutazione online', 1),
(6, 'Brilla intensamente', 1),
(7, 'Rigenera la tua pelle', 3),
(8, 'Regalo dedicado a ti', 3),
(9, 'Desintoxícate, embellece y rejuvenece.', 1),
(10, 'Prueba Viajes', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `programmi_benefici`
--

CREATE TABLE `programmi_benefici` (
  `id_programmi_benefici` int(11) NOT NULL,
  `id_programma` int(11) NOT NULL,
  `id_benefici` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `programmi_benefici`
--

INSERT INTO `programmi_benefici` (`id_programmi_benefici`, `id_programma`, `id_benefici`) VALUES
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 6, 13),
(10, 6, 15),
(11, 6, 16),
(12, 6, 17),
(13, 6, 18),
(14, 6, 19),
(55, 1, 110),
(56, 1, 112),
(57, 1, 114),
(58, 3, 121),
(59, 3, 125),
(60, 3, 123),
(61, 3, 117),
(62, 3, 124),
(63, 3, 122),
(64, 3, 119),
(65, 3, 115),
(66, 3, 118),
(67, 3, 120),
(68, 3, 116),
(69, 7, 131),
(70, 7, 127),
(71, 7, 128),
(72, 7, 129),
(73, 7, 130),
(74, 7, 126),
(75, 7, 132),
(76, 4, 133),
(77, 4, 136),
(79, 4, 141),
(80, 4, 135),
(81, 4, 140),
(82, 4, 139),
(83, 4, 138),
(84, 4, 137),
(85, 4, 142),
(86, 4, 134),
(87, 7, 143),
(88, 9, 121),
(89, 9, 125),
(90, 9, 131),
(91, 9, 123),
(92, 9, 117),
(93, 9, 127),
(94, 9, 124),
(95, 9, 128),
(96, 9, 129),
(97, 9, 122),
(98, 9, 115),
(99, 9, 130),
(100, 9, 126),
(101, 9, 132),
(102, 9, 118),
(103, 9, 120),
(104, 9, 116);

-- --------------------------------------------------------

--
-- Struttura della tabella `programmi_gallery`
--

CREATE TABLE `programmi_gallery` (
  `id_programmi_gallery` int(11) NOT NULL,
  `id_programma` int(11) NOT NULL,
  `img_gallery` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `programmi_gallery`
--

INSERT INTO `programmi_gallery` (`id_programmi_gallery`, `id_programma`, `img_gallery`) VALUES
(1, 1, 'prog_1.jpg'),
(2, 1, 'prog_2.jpg'),
(3, 1, 'prog_3.jpg'),
(4, 1, 'prog_4.jpg'),
(7, 1, '024b0-guance-web.png'),
(10, 1, '8dee7-labios3.jpg'),
(12, 1, '7f65c-btx1.jpg'),
(13, 1, '01818-piernas.jpg'),
(15, 4, 'c9239-manioca-ita.png'),
(16, 4, '19912-pancakes-esp.png'),
(17, 4, 'bf086-crespelle-ita.png'),
(18, 4, '8ac5c-yuca-esp.png'),
(19, 4, 'a499d-lasagna-ita.png'),
(20, 4, '715c4-lasagna-esp.png'),
(21, 4, '8b276-spaghuettini-ita.png'),
(22, 4, 'c7167-tortillas-quinoa-esp.png');

-- --------------------------------------------------------

--
-- Struttura della tabella `programmi_traduzioni`
--

CREATE TABLE `programmi_traduzioni` (
  `id_programmi_traduzioni` int(11) NOT NULL,
  `id_programma` int(11) NOT NULL,
  `frase_programma` text NOT NULL,
  `nome_programma` varchar(500) NOT NULL,
  `descrizione_programma` text NOT NULL,
  `immagine_programma` varchar(250) NOT NULL,
  `immagine_programma_mobile` varchar(250) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `programmi_traduzioni`
--

INSERT INTO `programmi_traduzioni` (`id_programmi_traduzioni`, `id_programma`, `frase_programma`, `nome_programma`, `descrizione_programma`, `immagine_programma`, `immagine_programma_mobile`, `lingua_traduzione_id`) VALUES
(1, 1, '“Sentiti più bella, fresca e riposata”.', 'Un soffio d\'aria fresca ', 'In questo spazio vorrei condividere con te il mio modo di applicare la medicina estetica.\nPer me è uno strumento di armonizzazione e si basa nel realizzare il trattamento giusto per te nel tempo più appropriato, vuol dire che questi trattamenti sono personalizzati e si realizzano ogni uno al suo tempo.\n<br></br>\nNella valutazione non si deve dare attenzione soltanto al sintomo, come diciamo noi medici, ma anche all’origine dello stesso perché così i risultati che si ottengono regaleranno benessere e saranno duraturi nel tempo.\n<br></br>\n<strong>Il mio concetto è donare a te un soffio d’aria fresca, armonizzando e valorizzando la bellezza che esiste già e lavorando sulla sua accettazione.</strong>\n<br></br>\nQuando sei te stessa ti accetti così come sei. Questo però non implica che devi invecchiare male. Puoi invecchiare bene coccolando e nutrendo la tua pelle, dandogli quello di cui ha bisogno perché con il passare del tempo, la vita frenetica e stressante, lo ha perso. Questo è un esempio di cosa significa amore verso te stessa.\n<br></br>\nQuando ami te stessa, allora puoi prenderti cura di te, armonizzando la tua bellezza fisica ed quella interna, sentendoti bene dentro la tua pelle e rompendo i paradigmi su ciò che possono pensare le persone intorno a te.\n<br></br>\n<strong><span class=\"blu-text\">Ricorda che il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.</strong></span>\n', '1b4e5-ina-08.png', '15583-ima-08.png', 1),
(3, 3, '“Il corpo è l’involucro della tua anima”.', 'Rigenera il tuo corpo', '<br></br>\n<strong><big><span class=\"orange-text\">Lo sai che la tua bellezza dipende anche dall’intestino?</strong></big></span>\n<br></br>\nÈ importante e benefico per il tuo organismo aiutarlo nel processo d’infiammazione e ossidazione nel quale si trova continuamente, che viene accentuato da diversi fattori, come una alimentazione senza consapevolezza, un eccesso d’attività fisica senza contrastare il suo effetto ossidativo, lo stress, l’uso di cosmetici contenenti sostanze che acutizzano il processo, ecc.\n<br></br>\nPer ridurre l’infiammazione dell’organismo si deve iniziare a lavorare dall’intestino, attraverso uno schema per rimuovere, riparare e ripopolare che porterà ad ottenere un intestino che lavora efficacemente. \n<br></br>\nÈ fondamentale che tu sappia che l’invecchiamento che vedi sulla tua pelle è dovuto anche a questo processo, ma la buona notizia e che esiste una soluzione e si basa nello sfruttare in modo positivo le funzioni del tuo corpo.\n<br></br>\n<strong>La soluzione te la offro in questo programma il cui compito esclusivo è quello di disintossicare e rigenerare il tuo organismo.</strong> \n<br></br>\nÈ molto utile anche quando si realizzano trattamenti di medicina estetica per trattare l’adiposità localizzata e/o cellulite e gli stessi trattamenti a loro volta contribuiscono in modo efficace nell’eliminazione delle tossine.\n<br></br>\n“Rigenera la tua pelle” è indicato alle donne che:\n<ul style=\"font-size:22px;\">\n<li>si prendono cura di sé;</li>\n<li>cercano benessere interno;</li>\n<li>vogliono ridurre lo stress del proprio corpo;</li>\n<li>vogliono rallentare il processo d’invecchiamento;</li>\n<li>non presentano patologie importanti;</li>\n<li>cercano di dare al proprio corpo un aiuto consapevole.</li>\n</ul>\n<br/>\n<p>Il programma si realizza esclusivamente ONLINE e ha queste caratteristiche:</p>\n<ul style=\"font-size:22px;\">\n<li>due sessioni a distanza di una settimana ogni una, ed è richiesta la tua presenza perché la sessione si svolge in diretta;</li>\n<li>è un programma da fare in gruppo;</li>\n<li>se proprio non puoi assistere, la sessione sarà registrata e disponibile per 48 ore;</li>\n<li>riceverai un workbook con tutte le istruzioni;</li>\n<li>avrai assistenza via email per 2 settimane, dove potranno essere chiariti i tuoi dubbi e fornite risposte alle tue domande;</li>\n<li>durante le sessioni non si potranno porre domande, per questo c’è l’assistenza via email.</li>\n</ul>\n<br/>\n<p>Alla fine di questo percorso avrai l’acceso prioritario al programma <strong><span class=\"orange-text\"><a href=\"https://www.seiessenzialmentebella.com/it/programmi/4/alimenta-corpo-e-anima\">“Alimenta corpo e anima”</strong></span></a>, il quale è personalizzato e ti aiuterà a prenderti cura di te attraverso una alimentazione consapevole e mirata a mantenere il tuo equilibrio interno.</p>\n<p>\n<strong><span class=\"blu-text\">Ricorda che il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.</strong></span>\n<br></br>\n<strong>Altro?</strong> Se vuoi sapere di più, <strong>contattami.</strong> Andando a fondo pagina troverai come farlo.</p>', 'c70df-ina-01.png', '81140-ima-01.png', 1),
(4, 4, '“Ritrova una migliore relazione con il cibo e vivi la tua esperienza”.', 'Alimenta corpo e anima', '<br></br>\nSe sei arrivata qui hai l’intenzione di investire su te stessa perché non c’è altro che abbia valore più di te. \n<br></br>\nAlimenta la tua anima investendo su di te e credendo in quello che vuoi raggiungere.\n<br></br>\nAlimenta il tuo corpo eliminando tutto ciò che non ti permette stare in equilibrio e introducendo nuove abitudini che abbiano l’origine in una alimentazione consapevole.\n<br></br>\nCominci ad avere un’alimentazione consapevole quando sai cosa puoi ottenere da un alimento. Ma questo, cosa significa?\n<br></br>\nSignifica che puoi essere certa di quali sono i cibi che:\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>ti donano energia;</li>\n<li>ti provocano stanchezza;</li>\n<li>aiutano a rallentare l’invecchiamento;</li>\n<li>mettono a posto l’intestino per promuovere la bellezza e l’armonia del tuo corpo <a href=\"https://www.seiessenzialmentebella.com/it/programmi/3/rigenera-il-tuo-corpo\"><strong>(clicca qui per scoprire cosa voglio dire);</li></a></strong>\n<li>ti sgonfiano e ti fanno sentire più leggera;</li>\n<li>ti aiutano a dimagrire;</li>\n<li>combattono la depressione;</li>\n<li>fanno che la tua pelle sia più luminosa e sana;</li>\n<li>ti proteggono dal sole;</li>\n<br></br>\n</ul>\n<p>\ncosì alla fine non soltanto mangerai per il piacere di farlo ma lo farai con un proposito ben preciso.\n<br></br>\nSe vuoi che la tua alimentazione agisca positivamente nella tua vita, questo è il programma che fa per te.</p>\n<br></br>\n<p>Il programma si svolgerà ONLINE e ha queste caratteristiche:</p>\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>è personalizzato;</li>\n<li>due sessioni al mese per una durata di quattro mesi;</li>\n<li>ogni sessione dura tra i 60 ai 90 minuti;</li>\n<li>le settimane in cui non si svolgono le sessioni, riceverai materiale di supporto da mettere in pratica;</li>\n<li>quando vorrai e sentirai il bisogno hai a disposizione 2 sessioni online personalizzate;</li>\n<li>avrai assistenza via whatsapp durante tutto il percorso;</li>\n</ul>\n<br></br>\n<p>\nRicorda, quello che importa è la consapevolezza dell’equilibrio.\n<br></br>\n<strong><span class=\"blu-text\">Il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.<strong></span>\n<br></br>\n<b>Altro?</b> Se vuoi sapere di più, <b>contattami.</b> Andando a fondo pagina troverai come farlo.</p>\n', 'da3e4-ina-03.png', '67a39-ima-03.png', 1),
(5, 5, '“La distanza non è un ostacolo quando vuoi sapere di più”.', 'La tua valutazione online', '<br></br>\n<strong><span class=\"orange-text\"><big>Se non stai vicina a me e non hai possibilità d’incontrarmi di persona, ti offro:</strong></span></big>\n<br></br>\n+ indicazioni su come armonizzare il tuo corpo ed il tuo volto con un approccio olistico;\n<br></br>\n+ di aiutarti a scoprire quali trattamenti di medicina estetica sono più adatti per te;\n<br></br>\n+ un piano di lavoro personalizzato.</br><p>\n<br></br>\n<strong>In questo modo otterrai la conoscenza su come raggiungere la migliore versione di te e la tranquillità di sapere quali sono i trattamenti e prodotti di origine certificata più adatti a te.</strong>\n<br></br>\n<span class=\"blu-text\"><b>Riceverai come bonus un video con diversi esercizi che ti aiuteranno a ringiovanire la tua pelle.</span></b>', '61cab-1950_500_valutazione.png', 'cef63-580_700_valutazione.png', 1),
(7, 1, '“ Siéntete más bella, fresca y reposada”.', 'Un toque de aire fresco', 'En este espacio quiero compartir contigo cómo me gusta aplicar la medicina estética.\n<br></br>\nPara mí es una herramienta de armonización y se basa en hacer el tratamiento adecuado para ti, la cantidad indicada y en el tiempo apropiado, significa que estos tratamientos son personalizados y cada uno se realizara en su momento.\n<br></br>\nPero no solo debemos prestarle atención al síntoma, como decimos los médicos, sino también al origen del mismo, obteniendo así resultados que te causarán bienestar y serán más duraderos en el tiempo.\n<br></br>\n<strong>Mi concepto es darte, un toque de aire fresco, armonizando y valorizando la belleza que ya existe y trabajando además en tu aceptación.</strong>\n<br></br>\nCuando eres tú misma, te aceptas tal como eres. Pero esto no implica que tengas que envejecer mal  Puedes envejecer bien mimando y nutriendo tu piel, dándole lo que necesita porque con el paso del tiempo, actualmente acelerado, lo ha perdido. Este es un ejemplo de lo que significa \"amor propio\".\n<br></br>\nCuando te amas a ti misma practicas el cuidado propio, armonizando tu belleza física e interna, sintiéndote bien dentro de tu piel y rompiendo los paradigmas de lo que las personas a tu alrededor pueden pensar.\n<br></br>\n<strong><span class=\"blu-text\">Recuerda que la mejor inversión que puedes hacer es en ti misma porque no hay nada que tenga más valor que tú.</strong></span>\n', '6196a-ina-09.png', 'a393e-ima-09.png', 3),
(8, 6, 'Sé consciente de tu luz y de tu sombra y empieza a brillar', 'Brilla intensamente', 'Una de las cosas más maravillosas que tienes como ser humano son tus sombras, porque cuando te haces consciente de ellas logras entenderte mejor y al abrazarlas y respetarlas se convierten en tu motor, en tu impulso, para hacerte brillar desde tu luz.<br><br><b>¿Cómo lo haces, cómo creas consciencia?</b><br>A través de la Numerología Cuántica se descodifica toda la información contenida en tus números, haciendote consciente de: quien eres: desde tu esencia, tu luz y tus sombras, lo que vienes a trabajar en esta vida, que obtáculos deberías trascender, que instrumentos traes de tu vida pasada para emplear en esta vida, cual es el regalo o Don que te ha sido dado para estar aquí, éste es tu herramienta principal para salir de cualquier circunstacia que te toque vivir.<br><br>La Numerología te ayuda a tener consciencia plena de quien eres y así brillar intensamente y cuando lo haces te sientes ùnica, verdadera, genuina y entiendes lo importante que eres como pieza de engranaje en este mundo maravilloso brillando desde tu propia luz.<br><br>\nCuando reconoces tus sombras, no quiere decir que las resuelves y ya nunca más estarán en tu vida. Tus sombras son tuyas, te pertenecen, lo que superas son las circunstancias que vives desde ellas y a medida que elevas tus niveles de conciencia, será más fácil para ti reconocerlas y hacer paces con ellas, empoderandote así de tu propia luz y no dejando que sean tus sombras las que se empoderen de ti.<br><br>Te recuerdo que la numerología cuántica te ayuda a hacerte consciente y a vibrar en los diferentes niveles de consciencia y así como tú evolocionas, tus números también lo hacen para ayudarte a vibrar con más intensidad.<br><br>\nLa manera ideal de ser cocreadora es saber desde donde estás creando tu vida, si desde tu luz o desde tus sombras y ciertamente para vivir la vida que quieres será necesario que tomes consciencia para empezar a crear en positivo y con magnetismo.<br><br> \n<strong>Inspirate e inicia a tener consciencia de tu luz y de tu sombra y empieza a brillar.</strong>\n\n', '9396f-1950_500_brilla.png', '7ed32-580_700_brilla.png', 3),
(9, 0, 'Il corpo è l’involucro della tua anima', 'Innamorati di te 2', 'Questo programma mi permetterà di conoscere cosa vorresti migliorare di te.  Attraverso tecniche di valorizzazione personale e trattamenti di medicina estetica ti aiuterò a raggiungere il tuo obiettivo, armonizzando ed equilibrando l’aspetto che non ti fa sentire bene dentro la tua pelle, ridandoti quella sensazione di benessere interiore.\n\nSvilupperò un piano specifico per te e per i tuoi bisogni. Contemporaneamente, ponendo a frutto la mia esperienza personale, ti guiderò e sosterrò con alcuni strumenti, che ho applicato sulla mia persona, che ti aiuteranno a sviluppare un legame più forte con te stessa e trovare quella pace interna di cui hai bisogno.\n\nIl programma dura 4 mesi, tempo sufficiente per iniziare a conoscere il tuo valore personale.\n', '', '', 1),
(10, 0, '\"El cuerpo es el hogar de tu alma.\"', 'Innamorate di te 2', 'En este programa te ofrezco una evaluación que me permitirá conocer que es lo que quieres mejorar en tí y a través de técnicas de valor personal y tratamientos de medicina estética, te ayudaré a lograr tu objetivo, armonizando y equilibrando ese aspecto con el que no te sientes a gusto, ridandote esa sensación de bienestar interno.\nRealizaré un plan a ejecutar específico para tí y tus necesidades. Contemporaneamente, desde mi experiencia personal te guiaré y te apoyaré con algunas herramientas que he aplicado en mi para que te conectes contigo misma y te encamines a esa paz interna que estas buscando, tomando en cuenta siempre que somos seres bioindividuales.\nEl programa tiene una duracion de 4 meses, período suficiente para que empieces a conocer tu valor personal.\nEn el caso que no te puedas realizar tratamientos conmigo, te ofrezco <u>tu evaluación online</u> que puedes encontrar en la sección de programas y así saber cuales son los beneficions para tí. \nAunque no puedas realizar los tratamientos de medicina estética, podemos realizar online la otra parte de este programa que se enfoca en el valor personal.  \n', '', '', 3),
(11, 0, '“Si cambias el modo de ver las cosas, cambiarás la manera de vivir tu vida”. ', 'Innamorati di te 2', 'En este programa te ofrezco una evaluación que me permitirá conocer que es lo que quieres mejorar en tí y a través de técnicas de valor personal y tratamientos de medicina estética, te ayudaré a lograr tu objetivo, armonizando y equilibrando ese aspecto con el que no te sientes a gusto, ridandote esa sensación de bienestar interno.\nRealizaré un plan a ejecutar específico para tí y tus necesidades. Contemporaneamente, desde mi experiencia personal te guiaré y te apoyaré con algunas herramientas que he aplicado en mi para que te conectes contigo misma y te encamines a esa paz interna que estas buscando, tomando en cuenta siempre que somos seres bioindividuales.\nEl programa tiene una duracion de 4 meses, período suficiente para que empieces a conocer tu valor personal.\nEn el caso que no te puedas realizar tratamientos conmigo, te ofrezco <u>tu evaluación online</u> que puedes encontrar en la sección de programas y así saber cuales son los beneficions para tí. \nAunque no puedas realizar los tratamientos de medicina estética, podemos realizar online la otra parte de este programa que se enfoca en el valor personal.\n', '', '', 3),
(13, 3, '“El cuerpo es el hogar de tu alma”.', 'Regenera tu cuerpo', '<br></br>\n<strong><big><span class=\"orange-text\">¿Sabes que tu belleza depende del intestino?</strong></big></span>\n<br></br>\nEs importante y beneficioso para tu organismo ayudarlo en el proceso de inflamación y oxidación, en el que se encuentra en continuación y que se acentúa por diferentes factores, tales como una alimentación sin conciencia, el exceso de actividad física sin contrarrestar su efecto oxidativo,  el estrés , el uso de cosméticos con la presencia de sustancias que agudizan el proceso, etc.\n<br></br>\nPara reducir la inflamación del organismo se debe empezar a trabajar desde el intestino, a través de un esquema para remover, reparar y repoblar, del cual se obtendrá un intestino capaz de funcionar eficazmente.\n <br></br>\nEs esencial que sepas que el envejecimiento que ves en tu piel se debe a este proceso, pero hay una solución y se basa en emplear de modo positivo las funciones de tu cuerpo.\n<br></br>\n<strong>La solución te la doy en este programa y su única tarea es desintoxicar y regenerar tu cuerpo.</strong>\n<br></br>\nTambién es muy útil cuando se realizan tratamientos de medicina estética para tratar la adiposidad localizada  y/o la celulitis y  a su vez, los tratamientos influyen en modo eficaz en la eliminación de toxinas.\n<br></br>\n\"Regenera tu cuerpo\" está indicado para mujeres que:\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>practican el cuidado propio;</li>\n<li>buscan el bienestar interno;</li>\n<li>quieren reducir el estrés de su cuerpo;</li>\n<li>quieren ralentizar el proceso de envejecimiento;</li>\n<li>no presentan patologías importantes;</li>\n<li>quieren ayudar a su cuerpo de forma conciente.</li>\n</ul>\n<br></br>\n<p>El programa se lleva a cabo exclusivamente  ONLINE y tiene estas características:</p>\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>el tiempo de desarrollo es de un mes y tendrás mi guía a través de dos sesiones. Es importante participar en directa porque así tus preguntas serán respondidas al momento;</li>\n<li>es un programa grupal;</li>\n<li>si no puedes asistir, la sesión será grabada y estará disponible durante 48 horas;</li>\n<li>recibirás un libro de trabajo con todas las instrucciones;</li>\n<li>recibirás asistencia via email durante un mes, de esta forma podrás hacer preguntas o aclarar dudas.</li>\n</ul>\n<br></br>\n<p>Al finalizar el mes, tendrás el acceso prioritario al programa <strong><span class=\"orange-text\"><a href=\"https://www.seiessenzialmentebella.com/es/programas/4/alimenta-corpo-e-anima\">\"Nutre cuerpo y alma”</strong></span></a> el cual es personalizado y te ayudaré a desarrollar una alimentación cociente con el objetivo de mantener el equilibrio en las funciones de tu organismo.</p>\n<br></br>\n<p>\n<strong><span class=\"blu-text\">Recuerda que la mejor inversión que puedes hacer es en ti misma porque no hay nada que tenga más valor que tú.</strong></span>\n<br></br>\n<strong>Algo más?</strong> Si quieres saber más,<strong> contáctame.</strong> Al final de la página, encontrarás cómo hacerlo.</p>\n<br></br>\n\n', 'c100b-ina-02.png', '73e58-ima-02.png', 3),
(14, 4, '“Mejora tu relación con los alimentos y vive tu experiencia”.', 'Nutre cuerpo y alma', '<br></br>\nSi has llegado hasta aquí, tienes la intención de invertir en ti misma porque no hay nada que tenga más valor que tú.\n<br></br>\nAlimenta tu alma invirtiendo en ti misma y creyendo en lo que deseas lograr.\n<br></br>\nAlimenta tu cuerpo mediante la eliminación de todo lo que no le permite estar en equilibrio y la introducción de nuevos hábitos que tengan origen en una alimentación consciente.\n<br></br>\nPracticas una alimentación consciente cuando sabes lo que puedes obtener a través de los alimentos. Pero esto, ¿qué significa?\n<br></br>\nSignifica que puedes estar segura de cuáles son los alimentos que:\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>te dan energía; </li>\n<li>te causan cansancio;</li>\n<li>ayudan a retrasar el envejecimiento;</li>\n<li>ponen en marcha el intestino para promover la belleza y la armonía de tu cuerpo <a href=\"https://www.seiessenzialmentebella.com/es/programas/3/regenera-tu-cuerpo\">(has clic aquí para saber a qué me refiero);</a></li>\n<li>te desinflaman y te hacen sentir más ligera;</li>\n<li>te ayudan a perder peso;</li>\n<li>te ayudan a mejorar tu estado de ánimo;</li>\n<li>hacen que tu piel sea luminosa y saludable;</li>\n<li>te protegen del sol;</li>\n</ul>\n<br></br>\n<p>\nasí que al final no sólo comerás por el placer de hacerlo también lo harás con un propósito determinado.\n<br></br>\nSi deseas que la alimentación influya positivamente en tu vida, este es el programa para ti.\n<br></br>\n</p>\n<p>El programa se lleva a cabo exclusivamente ONLINE y tiene estas características:</p>\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>es personalizado;</li>\n<li>dos sesiones al mes durante cuatro meses;</li>\n<li>cada sesión dura entre 60 y 90 minutos;</li>\n<li>las semanas en que no se realizan las sesiones, recibirás material de apoyo para poner en práctica;</li>\n<li>cuando quieras y sientas la necesidad tendrás a disposición 2 sesiones online personalizadas;</li>\n<li>recibirás asistencia por whatsapp durante todo el camino.</li>\n</ul>\n<br></br>\n<p>\nRecuerda, lo que importa es la conciencia del equilibrio.\n<br></br>\n<strong><span class=\"blu-text\">La mejor inversión que puedes hacer es en ti misma porque no hay nada que tenga más valor que tú.</strong></span>\n<br></br>\n<strong>Algo más?</strong> Si quieres saber más,<strong> contáctame.</strong> Al final de la página, encontrarás cómo hacerlo.</p>\n', '11db0-ina-04.png', '85595-ima-04.png', 3),
(15, 5, '“La distancia no es un obstáculo quando quieres saber más”.', 'Evaluación online', '<br></br>\n<span class=\"orange-text\"><strong><big>Si no estas cerca de mi y no tienes la posibilidad de encontrarme en persona, te ofrezco:</span></strong></big>\n<br></br>\n+ indicaciones sobre como armonizar tu cuerpo y rostro con un enfoque holístico;\n<br></br> \n+ ayudarte a obtener el conocimiento de cuales son los tratamientos de medicina estética adaptos a tus necesidades;\n<br></br>\n+ un plan de trabajo personalizado.\n<br></br>\n<strong>De este modo obtendrás los conocimientos para lograr la mejor versión de tí y la tranquilidad de saber cuales son los tratamientos y productos de origen certificada adecuados para tí.</strong>\n<br></br>\n<span class=\"blu-text\"><b>Como bonus obtendrás un video con diferentes ejercicios que te ayudarán a rejuvenecer tu piel.</span></b>\n', '4bdd1-ina-05.png', '43314-ima-05.png', 3),
(16, 7, 'Per una pelle sana usa cosmetici compatibili con la tua pelle.', 'Rigenera la tua pelle', '<br></br>\n<strong><span class=\"orange-text\">Sapevi che una delle cause d’invecchiamento, macchie e disidratazione della tua pelle, potrebbero essere le creme che usi?</strong></span>\n<br></br>\nLa prima cosa da sapere è che non devi fidarti dei cosmetici dove c’è scritto bio. La seconda è che devi sempre leggere l’etichetta del prodotto per sapere quello che veramente contiene e se fa per te.\n<br></br>\n<strong>Un cosmetico non compatibile con la tua pelle è potenzialmente aggressivo per essa.</strong>\n<br></br>\nQuello che dovresti iniziare a fare è incrementare il tuo livello di consapevolezza e cominciare a prenderti veramente cura di te, perché non conta soltanto quello che accade alla tua pelle, ma è anche indispensabile capire quello che succede all’interno del proprio corpo.\n<br></br>\nTi domanderai come puoi leggere l’etichetta di un cosmetico se non capisci quello che c’è scritto. Si, lo so che gli ingredienti sono impronunciabili e logicamente non li consci, ma esiste un modo per poterlo fare ed io te lo insegnerò attraverso questo programma “Rigenera la tua pelle”.\n<br></br>\nCosa imparerai? Sicuramente non i nomi impronunciabili.\n<br></br>\nIn un modo facile e didattico imparerai:\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>cosa significa la compatibilità di un cosmetico con la propria pelle;</li>\n<li>a differenziare quali sono gli ingredienti compatibili con la tua pelle e quali non lo sono;</li>\n<li>quali alimenti e integratori favoriscono la riparazione della tua pelle;</li>\n<li>quali sono le cause più frequenti di disidratazione della pelle (ci scommetto che non ci crederai);</li>\n<li>quali aspetti devi tenere in conto nello scegliere un cosmetico;</li>\n<li>identificare le certificazioni che garantiscono la natura degli ingredienti di un cosmetico.</li>\n</ul>\n<br></br>\n<p>\nCome si svolgerà il programma?\n<br></br>\nIl programma si svolgerà ONLINE e ha queste caratteristiche:</p> \n<br></br>\n<ul style=\"font-size:22px;\">\n<li>si realizza in gruppo;</li>\n<li>è impartito in un’unica sessione;</li>\n<li>la sessione dura 1 ora e mezza;</li>\n<li>è richiesta la tua presenza ma se non puoi presenziare la registrazione sarà disponibile per 48 ore;</li>\n<li>è interattivo, cioè si possono fare domande;</li>\n<li>verranno praticati esercizi per capire come funziona la metodologia;</li>\n<li>ciò che avrai appreso potrà essere messo in pratica subito dopo la sessione;</li>\n<li>riceverai il materiale di cui avrai bisogno;</li>\n<li>avrai assistenza via email per una settimana.</li>\n</ul>\n<br></br>\n<p><span class=\"orange-text\">Alla fine di questo programma sarai capace di scegliere i cosmetici compatibili con la tua pelle, applicando la metodologia proposta, con il fine di ristabilire l’equilibrio fisiologico della tua cute.</p></span>\n<br></br>\n<p>Questo programma è rivolto alle donne che:</p>\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>si prendono cura di sé stesse;</li>\n<li>vogliono imparare a scegliere un cosmetico compatibile con la propria pelle;</li>\n<li>vogliono essere più consapevoli su come nutrire l’organo più grande del proprio corpo;</li>\n<li>abbiano nozioni basilari nell’uso del computer.</li>\n<br></br>\n</ul>\n<p>\n<strong><span class=\"blu-text\">Ricorda che il migliore investimento che puoi fare è quello su te stessa, perché non c’è nient’altro che abbia valore più di te.</strong></span>\n<br></br>\n<strong>Altro?</strong> Se vuoi sapere di più, <strong>contattami.</strong> Andando a fondo pagina troverai come farlo.</p>\n\n', '9b8c5-ina-06.png', '4c410-ima-06.png', 1),
(17, 7, 'Para tener una piel saludable usa cosméticos compatibles con ella.', 'Regenera tu piel', '<br></br>\n<strong><span class=\"orange-text\">¿Sabías que una de las causas del envejecimiento, manchas y deshidratación de tu piel podrían ser las cremas que usas?</strong></span>\n<br></br>\nLo primero que debes saber es que no debes confiar en los cosméticos que dicen ser biológicos. Lo segundo es que siempre debes leer la etiqueta del producto para saber qué contiene realmente y si es adecuado para ti.\n<br></br>\n<strong>Un cosmético que no es compatible con tu piel es potencialmente agresivo para ella.</strong>\n<br></br>\nLo que deberías comenzar a hacer es aumentar su nivel de conciencia y comenzar realmente a cuidar de ti, porque no es solo lo que le sucede a tu piel, sino que también es esencial saber lo que podría estar sucediendo dentro de tu cuerpo.\n<br></br>\nTe preguntarás ¿cómo puedo leer la etiqueta de un cosmético si no entiendo lo que está escrito? Sí, sé que los ingredientes son impronunciables y muchos de ellos no los conoces, pero hay una manera de hacerlo y te enseñaré a través de este programa \"Regenera tu piel\".\n<br></br>\n¿Qué vas a aprender? Definitivamente no son los nombres impronunciables.\n<br></br>\nDe forma fácil y didáctica aprenderás:\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>lo que significa que un cosmético sea compatible con tu piel;</li>\n<li>diferenciar qué ingredientes son compatibles con tu piel y cuáles no;</li>\n<li>qué alimentos y suplementos promueven la reparación de tu piel;</li>\n<li>cuáles son las causas más frecuentes de deshidratación de la piel (apuesto que estas causas no te han pasado por la mente);</li>\n<li>qué aspectos debes tener en cuenta al elegir un cosmético;</li>\n<li>identificar las certificaciones que aprueban que un cosmético sea compatible con tu piel.</li>\n</ul>\n<br></br>\n<p>\n¿Cómo se llevará a cabo el programa?\n<br></br>\nEl programa se llevará a cabo  ONLINE y tiene estas características:\n</p>\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>se realizará en grupo;</li>\n<li>se efectuará en una sesión;</li>\n<li>el tiempo de la sesión es de 1 hora y media;</li>\n<li>se requiere tu presencia, pero si no puedes presentarte, tendrás 48 horas para ver la grabación;</li>\n<li>recibirás todo el contenido que necesitas;</li>\n<li>es interactivo, puedes hacer preguntas;</li>\n<li>se llevarán a cabo ejercicios para que puedas entender cómo funciona la metodología;</li>\n<li>toda la información se puede poner en práctica inmediatamente después de la sesión;</li>\n<li>asistencia por email durante una semana.</li>\n</ul>\n<br></br>\n<p><span class=\"orange-text\">Al final de este programa, tendrás la posibilidad de elegir los cosméticos compatibles con tu piel, aplicando la metodología propuesta, con el objetivo de restablecer el equilibrio fisiológico de tu piel.</p></span>\n<br></br>\n<p>Este programa está dirigido a mujeres que:</p>\n<br></br>\n<ul style=\"font-size:22px;\">\n<li>practican el cuidado propio;</li>\n<li>quieren aprender a elegir un cosmético compatible con su piel;</li>\n<li>quieren estar más conscientes de cómo alimentar el órgano más grande de su cuerpo;</li>\n<li>quieren aprender a escoger los cosméticos para sus bebés;</li>\n<li>tengan una noción básica sobre el uso de la computadora.</li>\n<br></br>\n</u>\n<p>\n<strong><span class=\"blu-text\">Recuerda que la mejor inversión que puedes hacer es en ti misma porque no hay nada que tenga más valor que tú.</strong></span>\n<br></br>\n<strong>Algo más?</strong> Si quieres saber más, <strong>contáctame.</strong> Al final de la página, encontrarás cómo hacerlo.</p>\n', '1658c-ina-07.png', '2bbf5-ima-07.png', 3),
(19, 8, 'Los regalos son para incrementar tu conciencia y tus conocimientos.', 'Regalo dedicado a ti', 'Este regalo lo he realizado con la intención de ayudarte a incrementar tu nivel de conciencia sobre los cosméticos que usas para tu cuidado propio. De este tipo de cosas se habla muy poco y no se tiene mucho conocimiento de ello.\n<br></br>\nAprender sobre esto es algo que me ha ayudado a mejorar la salud de mi piel, sufría de dermatitis atópica y tenia que ser esclava del aplicarme constantemente cremas que me “hidrataran” la piel, hasta que comprendí que una piel equilibrada funciona correctamente y cumple su función: <b>protegernos del medio externo.</b>\n<br></br>\nActualmente en el mercado hay infinidad de cosméticos para nuestro cuidado personal: <i>shampoo, acondicionador, crema para el cuerpo y rostro, desodorantes, maquillaje,</i> es todo un mundo. La mayoría de estos productos contienen ingredientes que <b>desequilibran el sistema hormonal, alteran la piel y están relacionados con el desarrollo de cáncer en nuestro cuerpo,</b> todo esto <b><span class=\"orange-text\">comprobado científicamente.</span></b>\n<br></br>\nUsar cosméticos que dicen ser biológicos o naturales no es la solución. Antes, por falta de conocimiento, confiaba en marcas catalogadas como naturales, pero cuando aprendí a leer los ingredientes me di cuenta que muchos de los que estaban presentes en la lista de mis cosméticos estaban catalogados como “no admitidos” en los sistemas de certificaciones que aseguran que una sustancia no agrede nuestro cuerpo o la naturaleza.\n<br></br>\nTe invito a abrir tu mente para que empieces a conocer el mundo de los cosméticos.\n<br></br>\n<p align=\"center\">Para descargar tu regalo has <big><b><a href=\"http://bit.ly/Videoregaloprotegetupielweb\">CLICK AQUÍ.</a></big></b></p>\n', 'a29d5-prueba-regalo-web-2.png', '24964-regalo-dedicado-a-ti-mobil-1-mobil.png', 3),
(20, 9, 'Atrévete y rompe con los paradigmas.', 'Desintoxícate, embellece y rejuvenece.', '¿Sabes que tu belleza depende de 2 factores importantes?\n<br></br>\nEstos factores son: tu intestino y tu piel.\n<br></br>\n<span class=\"orange-text\"><b>Tu intestino:</span></b> es fundamental ayudar a tu cuerpo en el proceso de inflamación en el que se encuentra continuamente y que se acentúa por diferentes motivos, tales como una alimentación sin conciencia, el exceso de actividad física, el estrés, hormonas, fármacos, estilo de vida, el uso de cosméticos con la presencia de sustancias que agudizan el proceso, etc.\n<br></br>\nPara reducir la inflamación del organismo se debe empezar a trabajar desde el intestino. Un intestino saludable se refleja en una piel joven y bella.\n<br></br>\n<span class=\"orange-text\"><b>Tu piel:</span></b> una de las cosas que influye mucho en su función es el medio externo, el frío, el calor, el sol, la contaminación, la humedad, etc y los productos que utilizas para tu cuidado personal: los cosméticos como el champú, crema para el cuerpo y el rostro, desodorante, maquillaje, etc\n<br></br>\nEsto quiere decir que la piel se adapta a todo lo que sucede en la superficie. Cuando respetamos la superficie de la piel su respuesta será: una piel sana, uniforme, hidratada, tonificada y sin imperfecciones. Cuando se disturban sus funciones regulares, su respuesta será: poros dilatados, machas, acné, envejecimiento precoz.\n<br></br>\nEn resumen, el envejecimiento que ves en tu piel se debe a todo este proceso, pero <strong>hay una solución.</strong> \n<br></br>\n<strong>La solución te la doy en este programa:</strong> Desintoxícate, embellece y rejuvenece. Una forma para rejuvenecer tu piel y hacer florecer tu bienestar.\n<br></br>\n<strong>El programa está dividido en 5 módulos:</strong>\n<br></br>\n<b>Módulo 1</b>\n<br></br>\nTrabajaremos en la estrategia para desintoxicar y recuperar el equilibrio de tu piel y de tu organismo.\n<br></br>\nDe forma fácil y didáctica aprenderás:\n<ul style=\"font-size:22px;\">\n<li>lo que significa que un cosmético sea compatible con tu piel;</li>\n<li>diferenciar qué ingredientes son compatibles con tu piel y cuáles no;</li>\n<li>qué alimentos y suplementos promueven la reparación de tu piel;</li>\n<li>cuáles son las causas más frecuentes de deshidratación de la piel (apuesto que estas causas no te han pasado por la mente);</li>\n<li>qué aspectos debes tener en cuenta al elegir un cosmético;</li>\n<li>identificar las certificaciones que aprueban que un cosmético sea compatible con tu piel.</li>\n</ul>\n<br></br>\n<p><b>Módulo 2</b></p>\n<p>Aquí trabajaremos en la estrategia para desintoxicar tu cuerpo:\nTodos los insumos que necesitas para realizar la práctica y ponerte en acción.</p>\n<br></br>\n<p><b>Módulo 3</b></p>\n<p>Aquí trabajaremos en la estrategia para regenerar tu cuerpo:\nReparar los daños causados por las toxinas y por el proceso de inflamación crónica.</p>\n<br></br>\n<p><b>Módulo 4:</b></p>\n<p>Masterclass: Inflamación crónica: cuáles son sus causas y cómo podemos hacer para disminuirla.</p>\n<br></br>\n<p><b>Módulo 5:</b></p>\n<p>Conclusión del programa. Hablaremos de cómo has llevado este proceso y como a través de tu proceso puedes ayudar a otros.</p>\n<br></br>\n<p>El programa se lleva a cabo exclusivamente en <b>ONLINE</b> y tiene estas características:</p>\n<ul style=\"font-size:22px;\">\n<li>dura 40 días + 10 días para integración de los bonus;</li>\n<li>se desarrolla en cinco sesiones que serán programadas en su momento, y es requerida tu presencia;</li>\n<li>es un programa grupal;</li>\n<li>si no puedes asistir, la sesión será grabada y estará disponible durante 48 horas;</li>\n<li>recibirás todo el contenido que necesitas;</li>\n<li>recibirás asistencia WhatsApp durante 50 días;</li>\n<li>es interactivo, puedes hacer preguntas, las cuales responderé al final de cada sesión;</li>\n<li>se llevarán a cabo ejercicios para que puedas entender cómo funciona la metodología (modulo 1);</li>\n<li>toda la información se puede poner en práctica inmediatamente después de la sesión.</li>\n</ul>\n<br></br>\n<p>¿Para quién está indicado? está indicado para mujeres que:</p>\n<ul style=\"font-size:22px;\">\n<li>practican el cuidado propio;</li>\n<li>buscan el bienestar interno;</li>\n<li>quieren reducir el estrés de su cuerpo;</li>\n<li>quieren ralentizar el proceso de envejecimiento;</li>\n<li>no presentan patologías importantes;</li>\n<li>quieren ayudar a su cuerpo de forma conciente;</li>\n<li>quieren aprender a elegir un cosmético compatible con su piel;</li>\n<li>quieren estar más conscientes de cómo alimentar el órgano más grande de su cuerpo;</li>\n<li>quieren aprender a escoger los cosméticos para sus bebés;</li>\n<li>tengan una noción básica sobre el uso de la computadora.</li>\n</ul>\n<br></br>\n<p><span class=\"orange-text\"><b><big>Bonus:</span></b></big>\n<br></br>\n<b>1. Una Masterclass con una invitada especial.</b> El tema a tratar es la ansiedad que se puede presentar antes los cambios de alimentación y como regular esa ansiedad.\n<br></br>\n<b>2. Un PDF con lista de probióticos para tu piel y tu intestino</b> y algunas recetas para fermentar vegetales.\n<br></br>\n<b>3. Una sesión privada conmigo para evaluar como puedes proseguir después de este programa,</b> cómo te gustaría continuar o saber como puedes continuar a introducir hábitos que te ayuden a nutrirte en cuerpo y alma para tu crecimiento y beneficio propio y de todos los que están a tu alrededor.</p>\n\n\n\n\n\n', '8ef13-ina-02.png', '02901-ima-02.png', 3),
(21, 10, '“No hay nada más profundo de aquello que se refleja en la superficie” Friedrich Hegel', 'El poder de tu Diosa', '<p><ul style=\"font-size:26px;\"><span class=\"orange-text\"><center> <b>ALICANTE 28 MAR al 02 APR 2019</b></center></span></ul></p>\n<br></br>\n<br></br>\n<p><ul style=\"font-size:22px;\"><center>¿Cómo pueden el amor propio y el autocuidado influir en tu belleza?</center></ul></p>\n<br></br>\n<body>\n<div align=\"center\">\n<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/An_ALeYikeg\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>\n</div>\n</body>\n<br></br>\nPrueba imagen\n<center><img width=\"360\" height=\"215\" src=\"https://drive.google.com/file/d/1B2KcOsXyAraox2V9I9pEz6WBpbl8hNwT/view?usp=sharing\" ></img></center>\n\n', '015cf-ina-05.png', 'e19fc-ima-05.png', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `programmi_trattamenti`
--

CREATE TABLE `programmi_trattamenti` (
  `id_programmi_trattamenti` int(11) NOT NULL,
  `id_programma` int(11) NOT NULL,
  `id_trattamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `programmi_trattamenti`
--

INSERT INTO `programmi_trattamenti` (`id_programmi_trattamenti`, `id_programma`, `id_trattamento`) VALUES
(1, 1, 1),
(11, 1, 2),
(12, 1, 3),
(13, 1, 12),
(14, 1, 5),
(15, 1, 8),
(18, 1, 11),
(19, 1, 13);

-- --------------------------------------------------------

--
-- Struttura della tabella `retreats`
--

CREATE TABLE `retreats` (
  `retreats_id` int(11) NOT NULL,
  `retreats_name` varchar(255) NOT NULL,
  `retreats_text` longtext NOT NULL,
  `retreats_lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `retreats`
--

INSERT INTO `retreats` (`retreats_id`, `retreats_name`, `retreats_text`, `retreats_lang_id`) VALUES
(1, 'Viaggio nel mondo mio', 'Testo del viaggio', 1),
(2, 'Viaje en mi mondo', 'Viaje text', 3),
(3, 'Prova di retreats', '<p>\n	<em>asdasdas</em></p>\n', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_descrizione`
--

CREATE TABLE `stato_descrizione` (
  `stato_descrizione_id` int(11) NOT NULL,
  `stato_descrizione_text` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_descrizione`
--

INSERT INTO `stato_descrizione` (`stato_descrizione_id`, `stato_descrizione_text`) VALUES
(1, 'ATTIVO'),
(2, 'CANCELLATO'),
(3, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `studi_medici`
--

CREATE TABLE `studi_medici` (
  `id_studio_medico` int(11) NOT NULL,
  `citta` varchar(200) NOT NULL,
  `indirizzo` varchar(250) NOT NULL,
  `lat` float NOT NULL,
  `long` float NOT NULL,
  `stato` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `studi_medici`
--

INSERT INTO `studi_medici` (`id_studio_medico`, `citta`, `indirizzo`, `lat`, `long`, `stato`) VALUES
(1, 'Guasila', 'Via Roma, 30 - 09040 Guasila', 39.5605, 9.04277, 1),
(2, 'Nuoro', 'Via Angioj, 12 - 08100 (NU)', 40.3219, 9.33272, 1),
(3, 'Cagliari', 'Viale Marconi, 26 - 09131 (CA)', 39.2265, 9.12979, 3),
(4, 'Tortolì', 'Via Bertulottu - 08048 (OG)', 39.9263, 9.6779, 1),
(5, 'Oristano', 'Via Giovanni XXIII, 10 - 09170 (OR)', 39.9039, 8.58714, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_template_default`
--

CREATE TABLE `tipo_template_default` (
  `id_tipo_template_default` int(11) NOT NULL,
  `desc_tipo_template_default` varchar(255) NOT NULL,
  `nome_tipo_template_default` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_template_default`
--

INSERT INTO `tipo_template_default` (`id_tipo_template_default`, `desc_tipo_template_default`, `nome_tipo_template_default`) VALUES
(1, 'Template di email benvenuto per chi si iscrive alla newsletter', 'NEWSLETTER_BENVENUTO'),
(2, 'Email il risposta alla richiesta di contatto dal modulo del sito', 'CONTATTO_RICVUTO');

-- --------------------------------------------------------

--
-- Struttura della tabella `traduzioni_file_lang`
--

CREATE TABLE `traduzioni_file_lang` (
  `id_tfl` int(11) NOT NULL,
  `LANGUAGE_NAME` varchar(250) NOT NULL,
  `LABEL_SURNAME` varchar(255) NOT NULL,
  `LABEL_CITY` varchar(255) NOT NULL,
  `LANGUAGE_ABBR` varchar(250) NOT NULL,
  `LANGUAGE_ID` varchar(250) NOT NULL,
  `LANGUAGE_LOCALE_PAYPAL` varchar(250) NOT NULL,
  `LANGUAGE_CI_CODE` varchar(250) NOT NULL,
  `MENU_HOME` varchar(250) NOT NULL,
  `MENU_ABOUTME` varchar(250) NOT NULL,
  `MENU_BIO` varchar(250) NOT NULL,
  `MENU_MYWORK` varchar(250) NOT NULL,
  `MENU_PROGRAMS` varchar(250) NOT NULL,
  `MENU_TREATMENTS` varchar(250) NOT NULL,
  `MENU_STUDIES` varchar(250) NOT NULL,
  `MENU_CONTACTME` varchar(250) NOT NULL,
  `MENU_LANGUAGE` varchar(250) NOT NULL,
  `MENU_PRIVACY` varchar(250) NOT NULL,
  `MENU_GRATIS` varchar(250) NOT NULL,
  `MENU_RETREATS` varchar(250) NOT NULL,
  `SUBSCRIBE_BTN` varchar(250) NOT NULL,
  `SEND_MESSAGE_BTN` varchar(250) NOT NULL,
  `SEND_AREYOUSURE_BTN` varchar(250) NOT NULL,
  `SUBSCRIBE_NEWSLETTER_H1` varchar(250) NOT NULL,
  `SUBSCRIBE_NEWSLETTER_DESC` varchar(250) NOT NULL,
  `SUBSCRIBE_NEWSLETTER_NAME` varchar(250) NOT NULL,
  `SUBSCRIBE_NEWSLETTER_EMAIL` varchar(250) NOT NULL,
  `SUBSCRIBE_NEWSLETTER_PRIVACY` varchar(255) NOT NULL,
  `PROGRAM_TREATMENTS` varchar(250) NOT NULL,
  `PROGRAM_TREATMENTS_DESC` varchar(250) NOT NULL,
  `BENEFITS_QUESTION` varchar(250) NOT NULL,
  `CONTACTME_DESC` varchar(250) NOT NULL,
  `LABEL_NAME` varchar(250) NOT NULL,
  `LABEL_EMAIL` varchar(250) NOT NULL,
  `LABEL_PHONE` varchar(250) NOT NULL,
  `LABEL_LOCATION` varchar(250) NOT NULL,
  `LABEL_MESSAGE` varchar(250) NOT NULL,
  `LABEL_CHOOSE` varchar(250) NOT NULL,
  `LABEL_FAQ` varchar(250) NOT NULL,
  `LABEL_UNSUBSCRIBE` varchar(250) NOT NULL,
  `MSG_REQUIRED_NAME` varchar(250) NOT NULL,
  `MSG_REQUIRED_SURNAME` varchar(255) NOT NULL,
  `MSG_REQUIRED_CITY` varchar(255) NOT NULL,
  `MSG_REQUIRED_EMAIL` varchar(250) NOT NULL,
  `MSG_VALID_EMAIL` varchar(250) NOT NULL,
  `MSG_REQUIRED_PHONE` varchar(250) NOT NULL,
  `MSG_VALID_PHONE` varchar(250) NOT NULL,
  `MSG_REQUIRED_OFFICE` varchar(250) NOT NULL,
  `MSG_REQUIRED_MESSAGE` varchar(250) NOT NULL,
  `MSG_REQUIRED_CHECK` varchar(255) NOT NULL,
  `MSG_SUCCESS_CONTACT` varchar(250) NOT NULL,
  `MSG_FAILURE_CONTACT` varchar(250) NOT NULL,
  `MSG_SUCCESS_NEWSLETTER` varchar(250) NOT NULL,
  `MSG_UNIQUE_NEWSLETTER` varchar(250) NOT NULL,
  `MSG_UNSUBSCRIBE_DONE` varchar(250) NOT NULL,
  `MSG_UNSUBSCRIBE_NOTFOUND` varchar(250) NOT NULL,
  `HOME_SERVICIOS_PATH` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `traduzioni_file_lang`
--

INSERT INTO `traduzioni_file_lang` (`id_tfl`, `LANGUAGE_NAME`, `LABEL_SURNAME`, `LABEL_CITY`, `LANGUAGE_ABBR`, `LANGUAGE_ID`, `LANGUAGE_LOCALE_PAYPAL`, `LANGUAGE_CI_CODE`, `MENU_HOME`, `MENU_ABOUTME`, `MENU_BIO`, `MENU_MYWORK`, `MENU_PROGRAMS`, `MENU_TREATMENTS`, `MENU_STUDIES`, `MENU_CONTACTME`, `MENU_LANGUAGE`, `MENU_PRIVACY`, `MENU_GRATIS`, `MENU_RETREATS`, `SUBSCRIBE_BTN`, `SEND_MESSAGE_BTN`, `SEND_AREYOUSURE_BTN`, `SUBSCRIBE_NEWSLETTER_H1`, `SUBSCRIBE_NEWSLETTER_DESC`, `SUBSCRIBE_NEWSLETTER_NAME`, `SUBSCRIBE_NEWSLETTER_EMAIL`, `SUBSCRIBE_NEWSLETTER_PRIVACY`, `PROGRAM_TREATMENTS`, `PROGRAM_TREATMENTS_DESC`, `BENEFITS_QUESTION`, `CONTACTME_DESC`, `LABEL_NAME`, `LABEL_EMAIL`, `LABEL_PHONE`, `LABEL_LOCATION`, `LABEL_MESSAGE`, `LABEL_CHOOSE`, `LABEL_FAQ`, `LABEL_UNSUBSCRIBE`, `MSG_REQUIRED_NAME`, `MSG_REQUIRED_SURNAME`, `MSG_REQUIRED_CITY`, `MSG_REQUIRED_EMAIL`, `MSG_VALID_EMAIL`, `MSG_REQUIRED_PHONE`, `MSG_VALID_PHONE`, `MSG_REQUIRED_OFFICE`, `MSG_REQUIRED_MESSAGE`, `MSG_REQUIRED_CHECK`, `MSG_SUCCESS_CONTACT`, `MSG_FAILURE_CONTACT`, `MSG_SUCCESS_NEWSLETTER`, `MSG_UNIQUE_NEWSLETTER`, `MSG_UNSUBSCRIBE_DONE`, `MSG_UNSUBSCRIBE_NOTFOUND`, `HOME_SERVICIOS_PATH`, `id_lingua`) VALUES
(1, 'ITALIANO', 'Cognome', 'Città', 'IT', '1', 'it_IT', 'italian', 'Home', 'Su di me', 'Mini biografia', 'Il mio lavoro', 'Programmi', 'Trattamenti', 'Studi medici', 'Contattami', 'Lingua', 'Privacy', 'Gratis', 'Retreats', 'ISCRIVITI', 'INVIA MESSAGGIO', 'SEI SICURO ?', 'ISCRIVITI ALLA NEWSLETTER', 'Inizia a ricevere consigli, ispirazioni e informazioni che ti potranno aiutare a raggiungere una vita con maggiori soddisfazioni.', 'Inserisci il tuo nome', 'Inserisci la tua email', 'Acconsento al trattamento dei dati e confermo la privacy policy*', 'Trattamenti del programma', 'Clicca sui link per vedere i dettagli dei singoli trattamenti', 'Quali sono i benefici per te ?', 'Se hai qualche domanda, se vuoi prenotare il trattamento o chiedere qualche informazione.', 'Nome', 'Email', 'Telefono', 'Studio di zona', 'Messaggio', 'Scegli...', 'Domande frequenti', 'Cancellati', 'Inserisci il tuo nome', 'Inserisci il tuo cognome', 'Inserisci la città', 'Inserisci la tua email', 'Inserisci una email valida', 'Inserisci il tuo numero di telefono', 'Inserisci un numero valido', 'Seleziona uno studio medico', 'Inserisci il messaggio', 'Devi acconsentire al trattamento dei dati personali', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'Abbiamo riscontrato un problema nell\'invio del mesaggio. Riprova!', 'Adesso sei iscritto alla newsletter. Grazie !', 'L\'indirizzo email è già iscritto alla newsletter', 'La tua email/iscrizione è stata rimossa. Grazie!', 'Il contatto richiesto non è attualmente registrato', 'servicios.png', 1),
(2, 'ENGLISH', 'Surname', 'City', 'EN', '2', 'en_GB', 'english', 'Home', 'About me', 'Mini biography', 'My work', 'Programs', 'Treatments', 'Medical offices', 'Contact me', 'Language', 'Privacy', '', '', 'SUBSCRIBE', 'SEND MESSAGE', 'ARE YOU SURE ?', 'SUBSCRIBE TO NEWSLETTER', 'Get started with tips, inspirations and information that will help you live a life with greater satisfaction.', 'Insert your name', 'Insert your email', '', 'Treatments of the program', 'Click on the links to see the details of individual treatments', 'What are the benefits for you?', 'If you have any questions, whether you want to book the treatment or ask for some information.', 'Name', 'Email', 'Phone', 'Medical office', 'Message', 'Choose...', 'Frequent questions', 'Unsubscribe', 'Insert your name', '', '', 'Insert your email', 'Insert a valid email address', 'Insert your phone number', 'Inserit a valid number', 'Choose a office', 'Insert a message', '', 'Your message has been sent correctly. Thank you !', 'Sorry it seems that our mail server is not responding. Please try again later!', 'Now you are subscribed to the newsletter. Thank you !', 'The email address is already subscribed to the newsletter.', 'Your email/subscription was removed. Thank you!', 'The required contact is not currently registered', 'servicios.png', 2),
(3, 'ESPANOL', 'Apellido', 'País', 'ES', '3', 'es_ES', 'spanish', 'Inicio', 'Sobre mi', 'Mini biografía', 'Mi trabajo', 'Programas', 'Tratamientos', 'Consultorio Médico', 'Contáctame', 'Idiomas', 'Politica de privacidad', 'Gratis', 'Escapadas', 'Sí, quiero unirme', 'Envía mensaje', '¿Estás seguro?', 'Inscríbete a los boletines', 'Comienza a recibir consejos, inspiración e información que te pueden ayudar a tener una vida llena de satisfacción.', 'Tu nombre', 'Tu email', 'Doy mi consentimiento para el procesamiento de datos y confirmo la política de privacidad*', 'Tratamientos del programa', 'Haz click sobre el link para ver los detalles de cada tratamiento', '&#191;Qué beneficios tiene para tí ?', 'Si tienes preguntas, quieres una cita o quieres pedir información.', 'Nombre', 'Email', 'Teléfono', 'Consultorio Médico Zona', 'Mensaje', 'Elige...', 'Preguntas Frecuentes', 'Darse de baja', 'Escribe tu nombre', 'Escribe tu Apellido', 'Escribe tu pàis', 'Escribe tu email', 'Escribe un email válido', 'Escribe tu teléfono', 'Escribe un número válido', 'Selecciona un consultorio médico', 'Escribe el mensaje', '\r\nDebes dar tu consentimiento al tratamiento y datos personales', 'Tu mensaje ha sido enviado correctamente. ¡ Gracias !', 'Hemos encontrado un problema en el envío del mensaje. ¡Intenta otra vez!', 'Ahora estás suscrito al boletín. Gracias!', 'Su dirección de correo electrónico ya está incluida en el boletín.', 'Su correo electrónico / registro ha sido eliminado. Gracias!', 'El contacto requerido no está registrado actualmente', 'servicios.png', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `trattamenti`
--

CREATE TABLE `trattamenti` (
  `id_trattamento` int(11) NOT NULL,
  `titolo_trattamento` varchar(250) NOT NULL,
  `stato_trattamento` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `trattamenti`
--

INSERT INTO `trattamenti` (`id_trattamento`, `titolo_trattamento`, `stato_trattamento`) VALUES
(1, 'Biostimolazione', 1),
(2, 'Bioristrutturazione', 1),
(3, 'Botox', 1),
(4, 'Iperidrosi con Botox', 1),
(5, 'Fili PDO', 1),
(6, 'Epilazione laser', 3),
(7, 'Needling medico', 3),
(8, 'Filler', 1),
(9, 'Peeling', 3),
(10, 'Mesoterapia', 1),
(11, 'Rinofiller', 1),
(12, 'Carbossiterapia', 1),
(13, 'Sublimazione dermica', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `trattamenti_benefici`
--

CREATE TABLE `trattamenti_benefici` (
  `id_trattamenti_benefici` int(11) NOT NULL,
  `id_trattamento` int(11) NOT NULL,
  `id_benefici` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `trattamenti_benefici`
--

INSERT INTO `trattamenti_benefici` (`id_trattamenti_benefici`, `id_trattamento`, `id_benefici`) VALUES
(1, 1, 8),
(3, 1, 10),
(4, 1, 11),
(5, 1, 12),
(7, 2, 28),
(8, 2, 31),
(9, 2, 29),
(10, 2, 30),
(11, 3, 51),
(12, 3, 52),
(13, 3, 50),
(14, 3, 49),
(15, 4, 53),
(16, 4, 54),
(17, 4, 55),
(18, 4, 56),
(19, 5, 57),
(20, 5, 61),
(22, 5, 58),
(23, 5, 60),
(24, 5, 66),
(25, 7, 64),
(26, 7, 66),
(27, 7, 67),
(28, 7, 63),
(29, 7, 62),
(30, 7, 65),
(31, 8, 72),
(32, 8, 70),
(33, 8, 51),
(34, 8, 68),
(35, 8, 71),
(36, 9, 75),
(37, 9, 76),
(38, 9, 73),
(39, 9, 74),
(40, 10, 80),
(41, 10, 78),
(42, 10, 79),
(43, 10, 77),
(44, 10, 81),
(45, 11, 85),
(46, 11, 86),
(47, 11, 87),
(48, 11, 83),
(49, 11, 84),
(50, 12, 97),
(51, 12, 94),
(52, 12, 96),
(53, 12, 91),
(54, 12, 92),
(55, 12, 52),
(56, 12, 95),
(57, 12, 90),
(58, 12, 93),
(59, 12, 88),
(60, 13, 98),
(61, 13, 100),
(62, 13, 102),
(63, 13, 99),
(64, 13, 101);

-- --------------------------------------------------------

--
-- Struttura della tabella `trattamenti_faq`
--

CREATE TABLE `trattamenti_faq` (
  `id_trattamenti_faq` int(11) NOT NULL,
  `id_trattamento` int(11) NOT NULL,
  `id_faq` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `trattamenti_faq`
--

INSERT INTO `trattamenti_faq` (`id_trattamenti_faq`, `id_trattamento`, `id_faq`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 13, 64),
(4, 13, 65),
(5, 13, 66),
(6, 13, 67),
(7, 12, 60),
(8, 12, 63),
(9, 12, 62),
(10, 12, 57),
(11, 12, 56),
(12, 12, 61),
(13, 12, 59),
(14, 12, 58),
(15, 12, 5),
(16, 11, 52),
(17, 11, 55),
(18, 11, 51),
(19, 11, 54),
(20, 11, 53),
(21, 10, 46),
(22, 10, 47),
(23, 10, 44),
(24, 10, 50),
(25, 10, 45),
(26, 10, 49),
(27, 10, 48),
(28, 9, 42),
(29, 9, 40),
(30, 9, 38),
(31, 9, 43),
(32, 9, 41),
(33, 9, 39),
(34, 8, 32),
(35, 8, 33),
(36, 8, 29),
(37, 8, 28),
(38, 8, 34),
(39, 8, 31),
(40, 8, 30),
(46, 5, 18),
(47, 5, 22),
(48, 5, 19),
(49, 5, 20),
(50, 5, 21),
(51, 4, 37),
(52, 4, 35),
(53, 4, 36),
(54, 3, 16),
(55, 3, 17),
(56, 3, 13),
(57, 3, 12),
(58, 3, 14),
(59, 3, 5),
(60, 3, 15),
(61, 2, 7),
(62, 2, 1),
(63, 2, 9),
(64, 2, 8),
(65, 2, 5),
(66, 2, 11),
(67, 1, 7),
(68, 1, 8),
(69, 1, 6),
(70, 1, 4),
(71, 1, 5),
(72, 7, 25),
(73, 7, 27),
(74, 7, 24),
(75, 7, 26),
(76, 7, 23);

-- --------------------------------------------------------

--
-- Struttura della tabella `trattamenti_traduzioni`
--

CREATE TABLE `trattamenti_traduzioni` (
  `id_trattamenti_traduzioni` int(11) NOT NULL,
  `id_trattamento` int(11) NOT NULL,
  `nome_trattamento` varchar(250) NOT NULL,
  `descrizione_trattamento` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `trattamenti_traduzioni`
--

INSERT INTO `trattamenti_traduzioni` (`id_trattamenti_traduzioni`, `id_trattamento`, `nome_trattamento`, `descrizione_trattamento`, `lingua_traduzione_id`) VALUES
(1, 1, 'Biostimolazione', 'È una procedura micro-iniettiva che si utilizza per stimolare il derma agendo sulla produzione di sostanze come il collagene e l’acido ialuronico che garantiscono l\'elasticità e la tonicità della pelle.', 1),
(2, 2, 'Bioristrutturazione', 'Consiste in un mix di ialuronici, ad alto e basso peso molecolare a lunga durata, associati ad una tecnica iniettiva innovativa che donano piacevoli e naturali effetti estetici.', 1),
(3, 3, 'Botox', 'Consiste in una tecnica iniettiva che agisce riducendo in modo selettivo la forza contrattile dei muscoli coinvolti nella mimica.', 1),
(4, 4, 'Iperidrosi con Botox', 'In parole più semplici <i>sudorazione eccessiva.</i> È un evento fisiologico che colpisce generalmente gli uomini, ma non risparmia neanche le donne. Si verifica in maggior misura nelle ascelle.', 1),
(5, 5, 'Fili PDO', 'Sono fili riassorbibili che vengono inseriti nel derma tramite aghi sottili. La sua azione è migliorare la tonicià della pelle. Si utilizza nelle lassità cutanee del viso e di aree specifiche del corpo (interno braccia, interno cosce, addome, ginocchia).', 1),
(6, 6, 'Epilazione laser', 'L’energia del laser diodo danneggia i follicoli dei peli ed altera la relativa capacità di svilupparne altri. Col tempo i peli cadranno e la crescita sarà rallentata fino alla progressiva scomparsa.', 1),
(7, 7, 'Needling medico', 'La cute è sottoposta a microperforazioni multiple, effettuate tramite una pennetta dotata di piccoli aghi (1,5 mm), previa applicazione di anestesia topica. L’obbiettivo è quello di attivare la produzione di collagene ed elastina attraverso la stimolazione del tuo tessuto.', 1),
(8, 8, 'Filler', 'Questo trattamento permette di armonizzare e rallentare l’invecchiamento fisiologico della pelle. Il prodotto più utilizzato è l’acido ialuronico e viene applicato attraverso diverse tecniche iniettive, in maggior misura sul volto.\n<br></br>\nIl <strong>beneficio</strong> più importante è un volto più <strong>fresco, equilibrato e riposato</strong>, questo si ottiene attraverso la stimolazione che realizza l\'acido ialuronico nei tessuti.', 1),
(9, 9, 'Peeling', 'Consiste nell’applicazione di uno o più acidi (miscelati) come l’acido mandelico, piruvico, salicilico, cogico, ecc. in grado di attivare la produzione di collagene ed elastina per rivitalizzare e ringiovanire la pelle.', 1),
(10, 10, 'Mesoterapia', 'È una tecnica micro-iniettiva per somministrare un prodotto (farmaco o sostaza omeopatica) a livello intradermico in un’area anatomica ben definita sul volto e/o sul corpo.', 1),
(11, 11, 'Rinofiller', 'È una tecnica che permette di migliorare in maniera concreta e rapida il profilo del naso, senza ricorrere ad interventi chirurgici. Il trattamento prevede l’uso di filler (acido ialuronico) che riempiono le aree irregolari, alzano la radice del naso o sollevano la punta.  ', 1),
(12, 12, 'Carbossiterapia', 'Somministrazione sottocutanea, tramite micro-iniezioni, di anidride carbonica. Questo provoca la liberazione di ossigeno nell’organismo ottenendo diversi benefici.', 1),
(13, 13, 'Sublimazione dermica (Blefaroplastica non chirurgica)', 'Questa è una tecnica che permette di accorciare la cute in eccesso delle palpebre senza dover incidere per asportarla. Si effettua senza anestesia (se non locale con pomate a base di lidocaina), non necessita di sutura.\n<br></br>\nSi usa anche per asportare tutto quello che potrebbe sporgere dalla pelle, come fibromi, verruche, xantelasmi, ecc.\n', 1),
(14, 1, 'Biostimulation', 'It is an injectable treatment that is used to stimulate the dermis by acting on the production of substances such as collagen and hyaluronic acid that guarantee the elasticity and tonicity of the skin.', 2),
(15, 1, 'Bioestimulación', 'Es un procedimiento que se realiza a través de microinyecciones para estimular la dermis favoreciendo la producción de sustancias como colágeno y ácido hialurónico que garantizan la elasticidad y tonicidad de la piel.', 3),
(16, 2, 'Biorestructuración', 'Consiste en un mix de acido hialurónico a bajo y alto peso molecular y de larga duración que asociado a una técnica inyectiva novedosa donan un natural y agradable efecto estético.', 3),
(17, 3, 'Botox', 'Es un tratamiento sealizado a través de técnicas inyectivas. Su principal acción es reducir de manera selectiva la fuerza de contracción de los músculos que intervienen en la mímica facial.', 3),
(18, 4, 'Hiperidrosis con Botox', 'En palabras simples, <i>sudoración eccesiva.</i> Es un evento fisiológico que se presenta mayormente en los hombres, aunque si muchas mujeres no escapan de ello. Se  verifica en mayor medida en las axilas.', 3),
(19, 5, 'Hilos PDO', 'Es un procedimiento que se realiza con hilos absorbibles que se insertan en la dermis a través de agujas muy delgadas, su acción se basa en mejorar la tonicidad de la piel. Se utiliza en la relajación cutánea del rostro y de áreas específicas del cuerpo (interno de los brazos, interno de las piernas, abdome, rodillas).', 3),
(20, 6, 'Depilación Laser', 'La energía del laser de diodo daña el folículo de los vellos y altera la relativa capacidad para desarrollar otros. Con el tiempo los vellos caerán y su crecimiento se enlentecerá hasta su progresiva desaparición.', 3),
(21, 7, 'Needling Médico', 'La piel es expuesta a microperforaciones múltiples, efectuadas a través de una especie de lápiz con agujas ultra finas y aplicación previa de anestesía tópica. El objetivo es el de activar la producción de cológeno y elastina para obtener una serie de resultados beneficiosos.', 3),
(22, 8, 'Filler ', 'Este tratamiento ayuda a armonizar y desacelerar el envejecimiento fisiológico de la piel. El producto màs utilizado es el àcido hialurónico y se aplica a través de técnicas inyectivas.\n<br></br>\nEl <strong>beneficio</strong> más importante es el resultado de un rostro más <strong>fresco, equilibrado y reposado,</strong> esto se obtiene a través de la estimulación que produce el ácido hialurónico en los tejidos.', 3),
(23, 9, 'Peeling', 'Consiste en la aplicación de uno o más ácidos (mezclados) como el ácido mandelico, pirúvico, glicólico, salicílico, etc, en grado de activar la producción de colágeno y elastina para revitalizar y rejuvenecer la piel.', 3),
(24, 10, 'Mesoterapia', 'Es una técnica microinyectiva a través de la cual se suministras farmacos o productos homeopáticos a nivel intradérmico en una zona anatómica definida en el rostro y/o el cuerpo.', 3),
(25, 11, 'Rinofiller', 'Es una técnica que permite mejorar de manera concreta y rápida el perfil de la nariz, sin recurrir a la intervención quirúrgica. El tratamiento prevee el uso de ácido hialurónico para rellenar el área irregular y/o alzar la punta de la nariz.', 3),
(26, 12, 'Carboxiterapia', 'Se trata de la suministración subcutánea de dióxido de carbono a través de pequeñas infiltraciones en los tejidos afectos. Esto provoca la liberación de oxígeno en el organismo obteniendo diferentes beneficios.', 3),
(27, 13, 'Sublimación Dérmica (Blefaroplástica sin cirugía)', 'Se trata de una técnica que permite acortar la piel, de los parpados superiores, en ecceso, sin tener que realizar un intervento quirúrgico. Se efectúa aplicando una pomada anestética y luego se procede a la sublimación de la piel. No se realizan suturas. \n<br></br>\nTambién se usa para eliminar todo lo que pueda sobre salir de la piel, como pequeños fibromas, verrugas, xantelasma, etc.\n', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'roberto.rossi77@gmail.com', '$2y$08$thYAVQ43pc.dHmdNDhQz/evLOaAKHbthgR/dkfc7MIjpEwCcDYdMy', '', 'roberto.rossi77@gmail.com', '', NULL, NULL, NULL, 1268889823, 1534767520, 1, 'Roberto', 'Rossi', 'ADMIN', '4234234'),
(3, '127.0.0.1', 'sei@seiessenzialmentebella.com', '$2y$08$FY9Hvlbhhg8tIQVWagvKDegFz0xIICm8uqOiduDibtEvTsEaNtve6', NULL, 'sei@seiessenzialmentebella.com', NULL, 'CjsboKf1IOZHPmrbpLT9tub695a8165e4e961a7a', 1509700421, NULL, 1508231534, 1541407802, 1, 'Orlena', 'Zotti', NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(4, 3, 1);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `benefici`
--
ALTER TABLE `benefici`
  ADD PRIMARY KEY (`id_benefici`);

--
-- Indici per le tabelle `benefici_traduzioni`
--
ALTER TABLE `benefici_traduzioni`
  ADD PRIMARY KEY (`id_benefici_traduzioni`);

--
-- Indici per le tabelle `commenti`
--
ALTER TABLE `commenti`
  ADD PRIMARY KEY (`id_commento`);

--
-- Indici per le tabelle `constants_framework`
--
ALTER TABLE `constants_framework`
  ADD PRIMARY KEY (`id_cf`);

--
-- Indici per le tabelle `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  ADD PRIMARY KEY (`id_contatto`);

--
-- Indici per le tabelle `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  ADD PRIMARY KEY (`id_contatto_newsletter`);

--
-- Indici per le tabelle `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id_template`);

--
-- Indici per le tabelle `email_templates_traduzioni`
--
ALTER TABLE `email_templates_traduzioni`
  ADD PRIMARY KEY (`id_template_trad`);

--
-- Indici per le tabelle `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indici per le tabelle `faq_traduzioni`
--
ALTER TABLE `faq_traduzioni`
  ADD PRIMARY KEY (`id_faq_traduzioni`);

--
-- Indici per le tabelle `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lingue`
--
ALTER TABLE `lingue`
  ADD PRIMARY KEY (`id_lingue`);

--
-- Indici per le tabelle `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `pagine`
--
ALTER TABLE `pagine`
  ADD PRIMARY KEY (`id_pagina`);

--
-- Indici per le tabelle `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  ADD PRIMARY KEY (`id_pc`);

--
-- Indici per le tabelle `programmi`
--
ALTER TABLE `programmi`
  ADD PRIMARY KEY (`id_programma`);

--
-- Indici per le tabelle `programmi_benefici`
--
ALTER TABLE `programmi_benefici`
  ADD PRIMARY KEY (`id_programmi_benefici`);

--
-- Indici per le tabelle `programmi_gallery`
--
ALTER TABLE `programmi_gallery`
  ADD PRIMARY KEY (`id_programmi_gallery`);

--
-- Indici per le tabelle `programmi_traduzioni`
--
ALTER TABLE `programmi_traduzioni`
  ADD PRIMARY KEY (`id_programmi_traduzioni`);

--
-- Indici per le tabelle `programmi_trattamenti`
--
ALTER TABLE `programmi_trattamenti`
  ADD PRIMARY KEY (`id_programmi_trattamenti`);

--
-- Indici per le tabelle `retreats`
--
ALTER TABLE `retreats`
  ADD PRIMARY KEY (`retreats_id`);

--
-- Indici per le tabelle `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  ADD PRIMARY KEY (`stato_descrizione_id`);

--
-- Indici per le tabelle `studi_medici`
--
ALTER TABLE `studi_medici`
  ADD PRIMARY KEY (`id_studio_medico`);

--
-- Indici per le tabelle `tipo_template_default`
--
ALTER TABLE `tipo_template_default`
  ADD PRIMARY KEY (`id_tipo_template_default`);

--
-- Indici per le tabelle `traduzioni_file_lang`
--
ALTER TABLE `traduzioni_file_lang`
  ADD PRIMARY KEY (`id_tfl`);

--
-- Indici per le tabelle `trattamenti`
--
ALTER TABLE `trattamenti`
  ADD PRIMARY KEY (`id_trattamento`);

--
-- Indici per le tabelle `trattamenti_benefici`
--
ALTER TABLE `trattamenti_benefici`
  ADD PRIMARY KEY (`id_trattamenti_benefici`);

--
-- Indici per le tabelle `trattamenti_faq`
--
ALTER TABLE `trattamenti_faq`
  ADD PRIMARY KEY (`id_trattamenti_faq`);

--
-- Indici per le tabelle `trattamenti_traduzioni`
--
ALTER TABLE `trattamenti_traduzioni`
  ADD PRIMARY KEY (`id_trattamenti_traduzioni`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `benefici`
--
ALTER TABLE `benefici`
  MODIFY `id_benefici` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT per la tabella `benefici_traduzioni`
--
ALTER TABLE `benefici_traduzioni`
  MODIFY `id_benefici_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT per la tabella `commenti`
--
ALTER TABLE `commenti`
  MODIFY `id_commento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT per la tabella `constants_framework`
--
ALTER TABLE `constants_framework`
  MODIFY `id_cf` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT per la tabella `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  MODIFY `id_contatto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT per la tabella `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  MODIFY `id_contatto_newsletter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=464;

--
-- AUTO_INCREMENT per la tabella `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT per la tabella `email_templates_traduzioni`
--
ALTER TABLE `email_templates_traduzioni`
  MODIFY `id_template_trad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT per la tabella `faq_traduzioni`
--
ALTER TABLE `faq_traduzioni`
  MODIFY `id_faq_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT per la tabella `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `lingue`
--
ALTER TABLE `lingue`
  MODIFY `id_lingue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `pagine`
--
ALTER TABLE `pagine`
  MODIFY `id_pagina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT per la tabella `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  MODIFY `id_pc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT per la tabella `programmi`
--
ALTER TABLE `programmi`
  MODIFY `id_programma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT per la tabella `programmi_benefici`
--
ALTER TABLE `programmi_benefici`
  MODIFY `id_programmi_benefici` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT per la tabella `programmi_gallery`
--
ALTER TABLE `programmi_gallery`
  MODIFY `id_programmi_gallery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT per la tabella `programmi_traduzioni`
--
ALTER TABLE `programmi_traduzioni`
  MODIFY `id_programmi_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT per la tabella `programmi_trattamenti`
--
ALTER TABLE `programmi_trattamenti`
  MODIFY `id_programmi_trattamenti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT per la tabella `retreats`
--
ALTER TABLE `retreats`
  MODIFY `retreats_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `studi_medici`
--
ALTER TABLE `studi_medici`
  MODIFY `id_studio_medico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `tipo_template_default`
--
ALTER TABLE `tipo_template_default`
  MODIFY `id_tipo_template_default` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `traduzioni_file_lang`
--
ALTER TABLE `traduzioni_file_lang`
  MODIFY `id_tfl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `trattamenti`
--
ALTER TABLE `trattamenti`
  MODIFY `id_trattamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT per la tabella `trattamenti_benefici`
--
ALTER TABLE `trattamenti_benefici`
  MODIFY `id_trattamenti_benefici` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT per la tabella `trattamenti_faq`
--
ALTER TABLE `trattamenti_faq`
  MODIFY `id_trattamenti_faq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT per la tabella `trattamenti_traduzioni`
--
ALTER TABLE `trattamenti_traduzioni`
  MODIFY `id_trattamenti_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
