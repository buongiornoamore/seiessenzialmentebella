-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2018 alle 09:45
-- Versione del server: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `seiessen_framework`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `benefici`
--

CREATE TABLE IF NOT EXISTS `benefici` (
  `id_benefici` int(11) NOT NULL,
  `titolo_benefici` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `benefici`
--

INSERT INTO `benefici` (`id_benefici`, `titolo_benefici`) VALUES
(1, 'Una valutazione mirata sull’aspetto fisico che senti in disarmonia'),
(3, 'Raggiungere la migliore versione di te dal punto di vista fisico'),
(4, 'Un volto più fresco e riposato'),
(5, 'Un risultato naturale, senza la percezione di aver fatto un trattamento di medicina estetica'),
(6, 'Equilibrio della tua immagine corporea'),
(7, 'Un piano d’azione personalizzato'),
(8, 'Migliora l’aspetto cutaneo'),
(10, 'Idrata'),
(11, 'Contrasta i processi d’invecchiamento'),
(12, 'Pelle più fresca'),
(13, 'Creas consciencia de lo que viniste a hacer, de tu misión de vida'),
(15, 'Reconoces tus sombras y te haces consciente de ellas'),
(16, 'reconoces tu luz '),
(17, 'brillas con luz propia'),
(18, 'te haces consciente de cuales son tus dones y de como utilizarlos'),
(19, 'te reencuentras con tus aprendizajes y los pones en práctica'),
(28, 'Dona freschezza e luminosità.\n'),
(29, 'Effetto tensore.'),
(30, 'Rimodella il viso.\n\n'),
(31, 'Effetto bio-rigenerante creando sostegno in profondità.'),
(32, 'Raggiungere la migliore versione di te esternamente ed internamente.'),
(33, 'Conseguire un risultato naturale, senza la percezione di aver fatto un trattamento di medicina estetica'),
(34, 'Rivalutare (se c’è l’hai) o conoscere (se non c’è l’hai) la cosmesi appropriata per te'),
(35, 'Dare equilibrio alla tua vita.'),
(37, 'Integrare i risultati di armonizzazione del tuo volto con il benessere della tua vita'),
(38, 'Armonizzare il tuo volto con un approccio olistico'),
(39, 'Rinnovare la tua immagine corporea.'),
(40, 'Sentirti a tuo agio dentro la tua pelle.'),
(41, 'Sentirti più leggera'),
(42, 'Migliorare la tua digestione.'),
(43, 'Disintossicare il tuo sistema digestivo.'),
(44, 'Stabilire una relazione di benessere con il cibo.'),
(45, 'Scegliere cosa comprare e non comprare tra i vari prodotti alimentari.'),
(46, 'Identificare gli alimenti che nutrono la tua anima.'),
(48, 'Capire come avere più energia.'),
(49, 'Volto riposato'),
(50, 'Rughe meno evidenti'),
(51, 'Attenua i solchi profondi'),
(52, 'Pelle più liscia'),
(53, 'Ascelle più fresche e asciutte'),
(54, 'Più comodità nel muovere le braccia'),
(55, 'Riduzione dei batteri che creano il cattivo odore. '),
(56, 'Una vita più socievole e meno imbarazzante.'),
(57, 'Azione liftante.'),
(58, 'Migliora l’elasticità.'),
(60, 'Ringiovanimento cutaneo'),
(61, 'L’attività giornaliera si può riprendere da subito.'),
(62, 'Migliora le rughe peribuccali e perioculari. '),
(63, 'Migliora le cicatrici post-acneiche.'),
(64, 'Attenua le macchie superficiali della pelle.'),
(65, 'Migliora le smagliature.'),
(66, 'Migliora la texture della pelle.'),
(67, 'Migliora la tonicità del collo e palpebre superiori.'),
(68, 'Attenuare le rughe (guance, contorno labbra e occhi, collo, decolleté).'),
(69, 'Attenuare i solchi (naso-labiale, marionetta).'),
(70, 'Armonizzare le labbra.'),
(71, 'Ripristinare la zona malare e zigomatica.'),
(72, 'Armonizzare il contorno mandibolare.'),
(73, 'Migliora la texture e la luminosità della cute.'),
(74, 'Migliora le macchie.'),
(75, 'Azione antibatterica ottenendo risultati positivi sull’acne.'),
(76, 'Equilibra la produzione di sebo.'),
(77, 'Riduce l’adiposità localizzata, permettendo di armonizzare il corpo.'),
(78, 'Rassoda la pelle.'),
(79, 'Riduce la cellulite'),
(80, 'Migliora la ritenzione idrica.'),
(81, 'Rinforza il cuoio capelluto nei trattamenti di caduta di capelli. '),
(83, 'Un naso più omogeneo'),
(84, 'Uniformità della linea nasale, senza “gobba”.'),
(85, 'Miglioramento del profilo.'),
(86, 'Procedura senza intervento chirurgico.'),
(87, 'Ritorno alla vita normale, subito dopo il trattamento.'),
(88, 'Tonicità cutanea'),
(90, 'Riduzione della cellulite.'),
(91, 'Miglioramento delle smagliature.'),
(92, 'Miglioramento sulle rughe superficiali.'),
(93, 'Riduzione dell’adiposità localizzata sui fianchi, addome, cosce.'),
(94, 'Diminuisce la ritenzione dei liquidi.'),
(95, 'Previene la formazione di capillari.'),
(96, 'Migliora le gambe pesanti o gonfie, dando sensazione di leggerezza. '),
(97, 'Aiuta la cicatrizzazione delle ulcere.'),
(98, 'Accorciamento dell’eccesso di pelle sugli occhi.'),
(99, 'Occhi più aperti e armonici.'),
(100, 'Apparenza più fresca.'),
(101, 'Pelle liscia e pulita libera di fibromi o verruche.'),
(102, 'Non è un intervento chirurgico, per cui non invasivo.'),
(103, 'Raggiungere la migliore versione di te internamente ed anche esternamente.'),
(104, ' Lasciar andare il passato.'),
(105, 'Rimuovere le convinzioni limitanti che stanno bloccando la tua energia e felicità.'),
(106, 'Dare equilibrio alla tua vita.'),
(107, 'Conoscere come puoi vivere una vita che ti dia soddisfazione.'),
(108, 'Essere consapevole della tua luce ma anche delle tue ombre e come esse possono aiutarti a vibrare alto.'),
(109, 'Integrare questi risultati nella tua vita.');

-- --------------------------------------------------------

--
-- Struttura della tabella `benefici_traduzioni`
--

CREATE TABLE IF NOT EXISTS `benefici_traduzioni` (
  `id_benefici_traduzioni` int(11) NOT NULL,
  `id_benefici` int(11) NOT NULL,
  `testo_benefici` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `benefici_traduzioni`
--

INSERT INTO `benefici_traduzioni` (`id_benefici_traduzioni`, `id_benefici`, `testo_benefici`, `lingua_traduzione_id`) VALUES
(1, 1, 'Una valutazione mirata sull’aspetto fisico che senti in disarmonia.', 1),
(3, 3, 'Raggiungere la migliore versione di te dal punto di vista fisico.', 1),
(4, 4, 'Un volto più fresco e riposato.', 1),
(5, 5, 'Un risultato naturale, senza la percezione di aver fatto un trattamento di medicina estetica.', 1),
(6, 6, 'Equilibrio della tua immagine corporea.', 1),
(7, 7, 'Un piano d’azione personalizzato.', 1),
(8, 8, 'improves the skin''s appearance', 2),
(10, 10, 'Idrata.', 1),
(11, 11, 'Contrasta i processi d’invecchiamento.', 1),
(12, 12, 'Pelle più fresca.', 1),
(13, 1, 'a targeted assessment of the physical appearance that you feel disagreeable', 2),
(15, 3, 'Obtener la mejor versión de tí esternamente e internamente', 3),
(16, 5, 'Conseguir un resultato natural, sin la percepción de haber hecho un tratamiento de medicina estética.', 3),
(17, 1, 'Una evaluación sobre ese aspecto físico que sientes en desarmonía.', 3),
(18, 4, 'Un rostro fresco y reposado.', 3),
(19, 6, 'Equilibrar tu imagen corporal.', 3),
(20, 7, 'Un plan de acción personalizado.', 3),
(21, 8, 'Migliora il tuo aspetto cutaneo.', 1),
(22, 8, 'Mejora el aspecto cutáneo.', 3),
(24, 10, 'Hidrata.', 3),
(25, 11, 'Contrarresta el proceso de envejecimiento.', 3),
(27, 12, 'Piel más fresca.', 3),
(28, 13, 'Creas consciencia de lo que viniste a hacer, de tu misión de vida.', 3),
(30, 15, 'Reconoces tus sombras y te haces consciente de ellas.', 3),
(31, 16, 'Reconoces tu luz.', 3),
(32, 17, 'Brillas con luz propia.', 3),
(33, 18, 'Te haces consciente de cuales son tus dones y de como utilizarlos.', 3),
(34, 19, 'Te reencuentras con tus aprendizajes y los pones en práctica.', 3),
(35, 32, 'Raggiungere la migliore versione di te esternamente ed internamente.', 1),
(36, 32, 'Obtener la mejor versión de tí esternamente e internamente.', 3),
(37, 33, 'Conseguire un risultato naturale, senza la percezione di aver fatto un trattamento di medicina estetica.', 1),
(38, 33, 'Conseguir un resultato natural, sin la percepción de haber hecho un tratamiento de medicina estética.', 3),
(39, 34, 'Rivalutare (se c’è l’hai) o conoscere (se non c’è l’hai) la cosmesi appropriata per te.', 1),
(40, 34, 'Reevaluar (si lo tienes) o conocer (si no lo tienes) los cométicos apropiados para tí.', 3),
(41, 35, 'Dare equilibrio alla tua vita.', 1),
(42, 35, 'Conceder equilibrio a tu vida.', 3),
(45, 37, 'Integrare i risultati di armonizzazione del tuo volto con il benessere della tua vita.', 1),
(46, 37, 'Integrar los resultados de armominía de tu rostro con el bienestar de tu vida. ', 3),
(47, 38, 'Armonizzare il tuo volto con un approccio olistico.', 1),
(48, 38, 'Armonizar tu rostro con un enfoque holístico.', 3),
(51, 39, 'Rinnovare la tua immagine corporea.', 1),
(52, 39, 'Renovar tu imagen corporal.', 3),
(53, 40, 'Sentirti a tuo agio dentro la tua pelle.', 1),
(54, 40, 'Sentirte esplendida dentro de tu propia piel.', 3),
(55, 41, 'Sentirti più leggera.', 1),
(56, 41, 'Sentire más ligera.', 3),
(57, 42, 'Migliorare la tua digestione.', 1),
(58, 42, 'Mejorar tu digestión.', 3),
(59, 43, 'Disintossicare il tuo sistema digestivo.', 1),
(60, 43, 'Desintoxicar tu sistema digestivo.', 3),
(61, 44, 'Stabilire una relazione di benessere con il cibo.', 1),
(62, 44, 'Establecer una relación de bienestar con los alimentos.', 3),
(63, 45, 'Scegliere cosa comprare e non comprare tra i vari prodotti alimentari.', 1),
(64, 45, 'Saber escoger que comprar y que no comprar entre los productos alimentares.', 3),
(65, 46, 'Identificare gli alimenti che nutrono la tua anima.', 3),
(66, 46, 'Identificar los alimentos que nutren tu alma.', 3),
(68, 48, 'Capire come avere più energia.', 1),
(69, 48, 'Descubrir como obtener mayor energía.', 3),
(70, 52, 'Pelle più liscia', 1),
(71, 52, 'Piel más lisa.', 3),
(72, 51, 'Attenua i solchi profondi.', 1),
(73, 51, 'Atenúa los sulcos profundos.', 3),
(74, 50, 'Rughe meno evidenti.', 1),
(75, 50, 'Arrugas menos evidentes.', 3),
(76, 49, 'Volto riposato.', 1),
(77, 49, 'Rostro reposado.', 3),
(78, 28, 'Dona freschezza e luminosità.', 1),
(79, 28, 'Dona frescura y luminosidad.', 3),
(80, 29, 'Effetto tensore.', 1),
(81, 29, 'Efecto tensor.', 3),
(82, 30, 'Rimodella il viso.', 1),
(83, 30, 'Remodela el rostro.', 3),
(84, 31, 'Effetto bio-rigenerante creando sostegno in profondità.', 1),
(85, 31, 'Efecto bio-regenerante creando un soporte en profondidad.', 3),
(86, 53, 'Ascelle più fresche e asciutte.', 1),
(87, 53, 'Axilas más frescas y secas.', 3),
(88, 54, 'Più comodità nel muovere le braccia.', 1),
(89, 54, 'Mayor libertad en el movimiento de los brazos.', 3),
(90, 55, 'Riduzione dei batteri che creano il cattivo odore. ', 1),
(91, 55, 'Reducción de bacterias que crean mal olor. ', 3),
(92, 56, 'Una vita più socievole e meno imbarazzante.', 1),
(93, 56, 'Una vida más social y menos embarazosa.', 3),
(94, 57, 'Azione liftante.', 1),
(95, 57, 'Acción liftante.', 3),
(96, 58, 'Migliora l’elasticità.', 1),
(97, 58, 'Mejora la elasticidad.', 3),
(100, 60, 'Ringiovanimento cutaneo.', 1),
(101, 60, 'Rejuvenecimiento  cutáneo.', 3),
(102, 61, 'L’attività giornaliera si può riprendere da subito.', 1),
(103, 61, 'Las actividades del día se pueden continuar inmendiatamente despuès del tratamiento.', 3),
(104, 62, 'Migliora le rughe peribuccali e perioculari. ', 1),
(105, 62, 'Mejora las arrugas alrededor de la boca y ojos. ', 3),
(106, 63, 'Migliora le cicatrici post-acneiche.', 1),
(107, 63, 'Mejora las cicatrices del acnè.', 3),
(108, 64, 'Attenua le macchie superficiali della pelle.', 1),
(109, 64, 'Atenùa las discromias superficiales de la piel.', 3),
(110, 65, 'Migliora le smagliature.', 1),
(111, 65, 'Mejora las estrìas.', 3),
(112, 66, 'Migliora la texture della pelle.', 1),
(113, 66, 'Mejora la textura de la piel.', 3),
(114, 67, 'Migliora la tonicità del collo e palpebre superiori.', 1),
(115, 67, 'Ayuda en la tonificaciòn de cuello y párpados superiores.', 3),
(116, 68, 'Attenuare le rughe (guance, contorno labbra e occhi, collo, decolleté).', 1),
(117, 68, 'Atenúa las arrugas (mejillas, contorno de los labios y ojos, cuello, escote).', 3),
(118, 69, 'Attenuare i solchi (naso-labiale, marionetta).', 1),
(119, 69, 'Atenúa los sulcos (naso-labiales, marioneta).', 3),
(120, 70, 'Armonizzare le labbra.', 1),
(121, 70, 'Armoniza los labios.', 3),
(122, 71, 'Ripristinare la zona malare e zigomatica.', 1),
(123, 71, 'Restablece el área malar y pómulos.', 3),
(124, 72, 'Armonizzare il contorno mandibolare.', 1),
(125, 72, 'Armoniza el contorno mandibular.', 3),
(126, 73, 'Migliora la texture e la luminosità della cute.', 1),
(127, 73, 'Mejora la textura y la luminosidad de la piel.', 3),
(128, 74, 'Migliora le macchie.', 1),
(129, 74, 'Mejora las discromías (manchas).', 3),
(130, 75, 'Azione antibatterica ottenendo risultati positivi sull’acne.', 1),
(131, 75, 'Acción antibacterica proporcionando resultados positivos en el acné.', 3),
(132, 76, 'Equilibra la produzione di sebo.', 1),
(133, 76, 'Equilibra la producción de sebo.', 3),
(134, 77, 'Riduce l’adiposità localizzata, permettendo di armonizzare il corpo.', 1),
(135, 77, 'Reduce la adiposidad localizda, permitiendo la armonización del cuerpo.', 3),
(136, 78, 'Rassoda la pelle.', 1),
(137, 78, 'Tonifica la piel.', 3),
(138, 79, 'Riduce la cellulite.', 1),
(139, 79, 'Reduce la celulitis.', 3),
(140, 80, 'Migliora la ritenzione idrica.', 1),
(141, 80, 'Mejora la retención hídrica.', 3),
(142, 81, 'Rinforza il cuoio capelluto nei trattamenti di caduta di capelli. ', 1),
(143, 81, 'Refuerza el cuero cabelludo en los tratamientos de caída del cabello. ', 3),
(145, 83, 'Un naso più omogeneo.', 1),
(146, 83, 'Una narìz homogénea.', 3),
(147, 84, 'Uniformità della linea nasale, senza “gobba”.', 1),
(148, 84, 'Uniformidad de la linea nasal, sin “protuberancia”.', 3),
(149, 85, 'Miglioramento del profilo.', 1),
(150, 85, 'Mejora el perfil.', 3),
(151, 86, 'Procedura senza intervento chirurgico.', 1),
(152, 86, 'Procedimiento sin intervención quirúrgica.', 3),
(153, 87, 'Ritorno alla vita normale, subito dopo il trattamento.', 1),
(154, 87, 'Vida socialmente activa inmediatamente después del tratamiento.', 3),
(155, 88, 'Tonicità cutanea.', 1),
(156, 88, 'Tonicidad cutánea', 3),
(159, 90, 'Riduzione della cellulite.', 1),
(160, 90, 'Reducción de la celulitis.', 3),
(161, 91, 'Miglioramento delle smagliature.', 1),
(162, 91, 'Mejora las estrias.', 3),
(163, 92, 'Miglioramento sulle rughe superficiali.', 1),
(164, 92, 'Mejora las arrugas superficiales.', 3),
(165, 93, 'Riduzione dell’adiposità localizzata sui fianchi, addome, cosce.', 1),
(166, 93, 'Reducción de la adiposidad localizada.', 3),
(167, 94, 'Diminuisce la ritenzione dei liquidi.', 1),
(168, 94, 'Diminuye la retención de líquidos.', 3),
(169, 95, 'Previene la formazione di capillari.', 1),
(170, 95, 'Previene la formación de capilares.', 3),
(171, 96, 'Migliora le gambe pesanti o gonfie, dando sensazione di leggerezza. ', 1),
(172, 96, 'Mejora las piernas pesadas e inflamadas, regalando bienestar en el movimiento. ', 3),
(173, 97, 'Aiuta la cicatrizzazione delle ulcere.', 1),
(174, 97, 'Ayuda a la cicatrización de úlceras.', 3),
(175, 98, 'Accorciamento dell’eccesso di pelle sugli occhi.', 1),
(176, 98, 'Acorta el ecceso de piel de los parpados.', 3),
(177, 99, 'Occhi più aperti e armonici.', 1),
(178, 99, 'Ojos más abiertos y armoniosos.', 3),
(179, 100, 'Apparenza più fresca.', 1),
(180, 100, 'Apariencia más fresca.', 3),
(181, 101, 'Pelle liscia e pulita libera di fibromi o verruche.', 1),
(182, 101, 'Piel más lisa y limpia libre de verrugas.', 3),
(183, 102, 'Non è un intervento chirurgico, per cui non invasivo.', 1),
(184, 102, 'No es un procedimiento quirúrgico, por ende no es invasivo.', 3),
(185, 103, 'Raggiungere la migliore versione di te internamente ed anche esternamente.', 1),
(186, 103, 'Lograr la mejor versión de tí internamente y también externamente.', 3),
(187, 104, ' Lasciar andare il passato.', 1),
(188, 104, 'Dejar ir el pasado.', 3),
(189, 105, 'Rimuovere le convinzioni limitanti che stanno bloccando la tua energia e felicità.', 1),
(190, 105, 'Remover las creencias limitantes que estan bloqueando tu energía y felicidad.', 3),
(191, 106, 'Dare equilibrio alla tua vita.', 1),
(192, 106, 'Dar equilibrio a tu vida.', 3),
(193, 107, 'Conoscere come puoi vivere una vita che ti dia soddisfazione.', 1),
(194, 107, 'Conocer cómo puedes vivir una vida que te de satisfacción.', 3),
(195, 108, 'Essere consapevole della tua luce ma anche delle tue ombre e come esse possono aiutarti a vibrare alto.', 1),
(196, 108, 'Ser consciente de tu luz y también de tus sombras y como pueden ayudarte a vibrar alto.', 3),
(197, 109, 'Integrare questi risultati nella tua vita.', 1),
(198, 109, 'Integrar los resultados en tu vida.', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `commenti`
--

CREATE TABLE IF NOT EXISTS `commenti` (
  `id_commento` int(11) NOT NULL,
  `nome_commento` varchar(250) NOT NULL,
  `testo_commento` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `commenti`
--

INSERT INTO `commenti` (`id_commento`, `nome_commento`, `testo_commento`, `lingua_traduzione_id`) VALUES
(1, ' Privacy della paziente', 'Ho fatto una consulenza di medicina estetica per curiosità. Il mio obiettivo era quello di togliere stanchezza al viso, per ottenere un aspetto più fresco ma naturale. La dottoressa mi ha proposto una seduta di biostimolazione con acido ialuronico e successivamente due sedute di filler. Sono pienamente soddisfatta del risultato. Competente e professionale la consiglio pienamente\n\n', 1),
(2, 'Antonina Caddeo', 'Grazie alla dottoressa Orlena che è un fantastico medico estetico ma è un altrettanto fantastico medico dell’anima, posso dire che lei mi ha dato armonia al mio fisico ma soprattutto ha cambiato profondamente il modo che avevo di vedere me stessa. Grazie al suo aiuto sono un''altra persona più sicura, più decisa, più forte con un''autostima che non ho mai avuto. ', 1),
(3, 'Anonimo', 'Ho iniziato a vedere, mio malgrado, il viso sciupato, questo purtroppo mi ha resa infelice. Ho saputo che c''era una dottoressa che si occupa di medicina estetica e mi sono subito rivolta a lei. Ora posso realmente constatare un cambiamento quasi miracoloso sul viso. Mi sento rinata e consiglio a tutti di lasciar stare i pregiudizi e prendersi cura del proprio corpo .', 1),
(4, 'Erika Montero', 'Escuchar a Orlena describir la vibración que tienen los números de mi vida fue como una inyección de energía, que representó una guía con la que me sentí identificada, ampliando así mi punto de vista para mejorar ciertas características y para afianzar otras. Es un regalo de autoconocimiento que recomiendo.\n\n', 3),
(5, 'Geraldine Betancourt', 'Orlena Zotti, es una mujer que logra una alquimia única entre su saber científico y espiritual. Gracias a ella, ahora tengo una guía , un manual precioso, que incluye mis sombras, mis aspectos menos conocidos y trabajados, que me permiten seguir mi proyecto de vida. ', 3),
(6, 'Privacy della paziente', 'Faccio sempre tanto sport e mantengo una alimentazione sana, ma i risultati non sono mai stati buoni per quello che riguarda alla ritenzione e cosce piene di buchi. Dopo di aver fatto dei trattamenti con la Dott.ssa Orlena ho ottenuto risultati visibili da avermi cambiato una taglia di pantaloni. Sono molto contenta dei risultati di questo percorso che sto facendo.', 1),
(7, 'Claudia Fernandez', 'El aporte de Orlena fue maravilloso para mi vida. Algo así como poder ver en perspectiva mis lugares más luminosos y los más oscuros. Me ha sido muy útil y ha aumentado mi confianza. Recomiendo totalmente el programa “Brilla Intensamente”.', 3),
(8, 'Dulce Huerta', 'El programa “Brilla intensamente” es una herramienta que me ha ayudado a conocerme mejor desde mi luz y mi sombra y me ha recordado que la energía no se crea ni se destruye, solo se transforma, lo cual me hace estar consciente y me da la guía para ser cada día mi mejor versión.', 3),
(9, 'Cinzia Passera', 'La dottoressa Orlena è un vulcano di istinto, intuito ed energia ma anche professionalità, competenza e preparazione.\nIl suo concetto di ''''avere cura'''' abbraccia l''individuo nella sua totalità senza trascurare l''aspetto più vero, profondo e interiore. Perché è lì che c''è la nostra essenza e lei sa come valorizzarla. Si serve della scienza, ma è alla nostra anima che parla. Orlena è la solarità che ci accoglie, con il suo calore latino, per ascoltarci, consigliarci, guidarci.', 1),
(10, 'Jessica Alzini', 'Ho conosciuto Orlena tramite un suo video postato in Facebook. Mi è piaciuto fin da subito e gli argomenti trattati erano molto interessanti. Ho intrapreso con lei un percorso personale che mi ha dato modo di scoprire cose nuove e nuovi mezzi per migliorarmi. Grazie per la tua empatia, professionalità, semplicità e spontaneità.', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `constants_framework`
--

CREATE TABLE IF NOT EXISTS `constants_framework` (
  `id_cf` int(11) NOT NULL,
  `cf_name` varchar(100) NOT NULL,
  `cf_value` varchar(250) NOT NULL,
  `cf_description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `constants_framework`
--

INSERT INTO `constants_framework` (`id_cf`, `cf_name`, `cf_value`, `cf_description`) VALUES
(1, 'SITE_URL_PATH', 'https://www.seiessenzialmentebella.com', 'Indirizzo online del sito'),
(2, 'SITE_TITLE_NAME', 'Sei essenzialmente bella', 'Titolo del sito online'),
(3, 'SMTP_HOST_CUSTOM', 'secureit18.sgcpanel.com', 'Indirizzo del server SMTP'),
(4, 'SMTP_USER_CUSTOM', 'sei@seiessenzialmentebella.com', 'Utente del server SMTP'),
(5, 'SMTP_PASS_CUSTOM', 'oz17@Sei', 'Password del server SMTP'),
(6, 'DB_HOSTNAME', '77.104.188.118', 'Indirizzo del DB'),
(7, 'DB_USERNAME', 'seiessen', 'Utente del DB'),
(8, 'DB_PASSWORD', 'sei17@Cpanel', 'Password del DB'),
(9, 'DB_DATABASE', 'seiessen_framework', 'Nome del DB'),
(10, 'COMPANY_NAME', 'Seiessenzialmentebella', 'Nome della società/ditta'),
(11, 'COMPANY_EMAIL', 'sei@seiessenzialmentebella.com', 'Email della società/ditta'),
(12, 'COMPANY_COPYRIGHT', '&copy; 2017 Seiessenzialmentebella', 'Testo footer copyright'),
(13, 'COMPANY_PHONE', '+39 3923576272', 'Telefono società/ditta'),
(14, 'COMPANY_ADDRESS', 'Sardegna', 'Indirizzo società/ditta'),
(15, 'GPLUS_LINK', '', 'Link social G+'),
(16, 'YOUTUBE_LINK', '', 'Link social YOUTUBE'),
(17, 'PINTEREST_LINK', '', 'Link social PINTEREST'),
(18, 'TWITTER_LINK', '', 'Link social TWITTER'),
(19, 'FACEBOOK_LINK', 'https://www.facebook.com/dottoressaorlenazotti/', 'Link social FACEBOOK'),
(20, 'INSTAGRAM_LINK', 'https://www.instagram.com/dottoressaorlenazotti/', 'Link social INSTAGRAM'),
(21, 'GOOGLE_ANALITYCS_ID', 'UA-108374307-1', 'UID Google Analitycs'),
(22, 'PAYPAL_EMAIL', '', 'Email di Paypal per i pagamenti'),
(23, 'PAYPAL_ENV', '', 'Environment di paypal sviluppo/produzione'),
(24, 'PAYPAL_SANDBOX_CLIENT_ID', '', 'Cliend ID sandbox di Paypal'),
(25, 'PAYPAL_LIVE_CLIENT_ID', '', 'Cliend ID sandbox di Paypal LIVE'),
(26, 'STRIPE_PK', '', 'Stipe Public Key'),
(27, 'STRIPE_SK', '', 'Stripe Secret Key');

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_moduli`
--

CREATE TABLE IF NOT EXISTS `contatti_moduli` (
  `id_contatto` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `id_studio_medico` int(11) NOT NULL,
  `messaggio` text NOT NULL,
  `provenienza` varchar(250) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_moduli`
--

INSERT INTO `contatti_moduli` (`id_contatto`, `nome`, `email`, `telefono`, `id_studio_medico`, `messaggio`, `provenienza`, `data`, `stato_contatto`, `data_unsubscribe`, `id_lingua`) VALUES
(5, 'Veronica', 'veronica.ls@hotmail.com', '', 0, 'Espana', '', '0000-00-00 00:00:00', 1, NULL, 3),
(9, 'Nena', 'orlenazp@hotmail.com', '', 0, 'Vorrei sapere come funziona', 'Innamorati di te 2', '2018-01-21 23:00:00', 1, NULL, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_newsletter`
--

CREATE TABLE IF NOT EXISTS `contatti_newsletter` (
  `id_contatto_newsletter` int(11) NOT NULL,
  `nome_contatto` varchar(250) NOT NULL,
  `email_contatto` varchar(250) NOT NULL,
  `data_contatto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_newsletter`
--

INSERT INTO `contatti_newsletter` (`id_contatto_newsletter`, `nome_contatto`, `email_contatto`, `data_contatto`, `stato_contatto`, `data_unsubscribe`, `lingua_traduzione_id`) VALUES
(11, 'STEFANIA', 'stefi.estrella@hotmail.it', '2018-02-02 02:47:00', 1, NULL, 1),
(12, 'Maria Grazia Catarinicchia', 'mrosolino@alice.it', '2018-02-02 02:51:33', 1, NULL, 1),
(13, 'Elena', 'elenabazzana@yahoo.it', '2018-02-02 05:07:35', 1, NULL, 1),
(17, 'Cinzia Passera', 'cinziaopassera@gmail.com', '2018-02-02 07:53:00', 1, NULL, 1),
(18, 'Misay silva', 'misay@hotmail.com', '2018-02-02 09:48:44', 1, NULL, 3),
(19, 'Silvia Adriana Mundarain Trujillo ', 'mundarainsilvia@hotmail.com', '2018-02-02 10:04:50', 1, NULL, 3),
(20, 'Carolina', 'carolmundarain@hotmail.com', '2018-02-02 10:54:21', 1, NULL, 3),
(21, 'Stefania', 'stefaniat@yahoo.it', '2018-02-02 11:15:36', 1, NULL, 1),
(22, 'Claudia', 'ianniclaudia77@gmail.com', '2018-02-02 11:54:18', 1, NULL, 1),
(23, 'Claudia Fernández', 'claudiafernandeze@gmail.com', '2018-02-02 12:08:33', 1, NULL, 3),
(24, 'Linda Cattaneo', 'linda.c.cattaneo@gmail.com', '2018-02-02 12:17:21', 1, NULL, 1),
(25, 'Erika', 'erikamontero1@gmail.com', '2018-02-02 12:24:46', 1, NULL, 3),
(26, 'Alessandra', 'alessandra@omodei.it', '2018-02-02 12:54:35', 1, NULL, 1),
(27, 'Paola', 'paolina77@gmail.com', '2018-02-02 12:59:33', 1, NULL, 1),
(28, 'Dedoro Schinelli Claudia', 'dedoroclaudia@gmail.com', '2018-02-02 13:58:12', 1, NULL, 1),
(29, 'Silvia', 'silviapi@tiscali.it', '2018-02-02 15:10:14', 1, NULL, 1),
(30, 'Erika', 'bertocci.erika@gmail.com', '2018-02-02 16:26:00', 1, NULL, 1),
(31, 'Barbara ', 'barbararjpg@yahoo.it', '2018-02-02 16:31:14', 1, NULL, 1),
(32, 'Chiara violino', 'violinochiara@gmail.com', '2018-02-02 17:16:28', 1, NULL, 1),
(33, 'chantal', 'chantal.pasquettaz@gmail.com', '2018-02-03 00:50:32', 1, NULL, 1),
(34, 'Laura ', 'laura.mobilee@gmail.com', '2018-02-03 02:06:33', 1, NULL, 1),
(35, 'Alice', 'alicemoro.ali@gmail.com', '2018-02-03 02:24:15', 1, NULL, 1),
(36, 'Jessica ', 'x.jessica@virgilio.it', '2018-02-03 09:57:28', 1, NULL, 1),
(37, 'Federica Cinus', 'fedecinus78@libero.it', '2018-02-03 13:07:39', 1, NULL, 1),
(38, 'Deborah', 'deborahforzanini@gmail.com', '2018-02-03 13:35:20', 1, NULL, 1),
(39, 'Daniela', 'luglio73@libero.it', '2018-02-04 08:12:09', 1, NULL, 1),
(40, 'Raffaella', 'raffy.medina@gmail.com', '2018-02-04 13:41:03', 1, NULL, 1),
(41, 'Agostina Sol ', 'agostina.cisella@gmail.com', '2018-02-04 16:59:26', 1, NULL, 1),
(42, 'roberta', 'zanetti.rz@gmail.com', '2018-02-04 22:57:01', 1, NULL, 1),
(43, 'Rosalba', 'rosalba_piar@yahoo.it', '2018-02-05 07:17:18', 1, NULL, 1),
(44, 'Fátima', 'fvasquezv79@gmail.com', '2018-02-08 22:14:34', 1, NULL, 3),
(45, 'Virginia ', 'virgy1266@gmail.com', '2018-02-10 09:29:41', 1, NULL, 1),
(46, 'Lara', 'laraferr69@gmail.com', '2018-02-10 13:50:02', 1, NULL, 1),
(47, 'Luisa Elena Perez', 'lelenapv@hotmail.com', '2018-02-09 23:00:00', 1, NULL, 3),
(48, 'Iolanda', 'ferraroiolanda@gmail.com', '2018-02-15 06:34:50', 1, NULL, 1),
(49, 'Anita angiolini', 'sales@angiolinianita.com', '2018-02-16 07:27:53', 1, NULL, 1),
(50, 'Tiziana', 'info@tizianarinaldi.it', '2018-02-17 01:49:07', 1, NULL, 1),
(51, 'Vera', 'verasganga@gmail.com', '2018-02-21 00:39:18', 1, NULL, 1),
(52, 'María Alejandra Monteverde', 'mariamonteverdembs@gmail.com', '2018-02-25 23:00:00', 1, NULL, 3),
(53, 'Roberto', 'roberto.rossi77@gmail.com', '2018-03-14 23:00:00', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id_template` int(11) NOT NULL,
  `nome_template` varchar(250) NOT NULL,
  `testo_template` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL,
  `id_tipo_template_default` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_templates`
--

INSERT INTO `email_templates` (`id_template`, `nome_template`, `testo_template`, `lingua_traduzione_id`, `id_tipo_template_default`) VALUES
(1, 'Benvenuto', 'Ciao <b>VAR_NOME_CLIENTE</b>,\n<br><br>\n<br>grazie per esserti iscritto a <a href="https://www.seiessenzialmentebella.com/it/home target="_blank"><span class="orange-text">Sei Essenzialmente Bella.</a></span><br><br>\n<p align="left">Volevo parlare un po’ con te dei miei 3 pilastri fondamentali nel lavoro:</p>\n<br>\n<p><ul align="left">\n<li>La Medicina Estetica, la mia passione da 12 anni. Un mondo in cui attraverso l’armonizzazione della bellezza, ho integrato due concetti: bellezza esterna e bellezza interna.</li> \n<li>L’alimentazione, una esperienza realmente vissuta che voglio condividere con te, guidandoti per capire come si sente il tuo corpo con quello che mangi, cosa ti dà energia e come integrare in maniera equilibrata i tuoi nutrienti, raggiungendo così il tuo benessere generale e il miglioramento della tua immagine corporea.</li>\n<li>La valorizzazione personale, una forma per renderti nella tua vita più serena, più presente, più amorevole e compassionevole con te stessa e soprattutto più felice. </li>\n</ul>\n<p align="justify">\nCome regalo di benvenuto voglio offrirti una valutazione di medicina estetica online gratuita, che durerà 20 minuti e parleremo di quella particolare situazione che vorresti risolvere o migliorare. Questo regalo non è trasferibile (valida fino al 30 Giugno del 2018). Devi solo rispondere a questa email chiedendo la tua valutazione per programmare l''appuntamento. Goditi questa opportunità, fai il primo passo per conoscere più in profondità quello che ti posso offrire. \n<br></br>\nTi voglio regalare anche una guida che ho realizzato con tutto il cuore per te: <a href="https://drive.google.com/file/d/1Oo2fw2T8gvm-6XG_gwYA6E-UgDTV621O/view?usp=sharing">Alimenti per ringiovanire</a>, questo è solo un piccolo assaggio di come puoi  avere una alimentazione consapevole.\n </p>\n<br>\n<p align="justify">\nUna volta al mese riceverai una newsletter, e pertanto, mi farebbe tanto piacere sapere su quale argomento vorresti avere più informazioni. \n</p>\n<br><br>\n<p align="justify" class="orange-text">\n<b>Desidero per te una giornata piena d’ispirazione.</b>\n </p>\n<br><br>\n<p align="justify"><b>Dottoressa Orlena Zotti</b></p>\n\n', 1, 1),
(2, 'Welcome', 'Hello <b>VAR_NOME_CLIENTE</b>,<br>thank you for subscribing to Six Essentially Beautiful\n<p> I wanted to talk to you about my 3 core pillars in the work: </ p>\n- Aesthetic Medicine, my passion for 12 years. A world where, through the harmonization of beauty, I have integrated two concepts: external beauty and inner beauty.\n- Power, a really experienced experience that I want to share with you, guiding you to understand how you feel your body with what you eat, what gives you energy and how to integrate your nutrients in a balanced way, thus achieving your overall well-being the improvement of your bodily image.\n- Personal enhancement, a form to make your life more serene, more present, more loving and compassionate with yourself and above all happier.\nAs a welcome gift I want to offer you a 10% discount voucher that will only be valid once for the first treatment you will do with me. It is not cumulative, it is not transferable.\n \nEnjoy this opportunity, take the first step to learn more deeply what I can offer you.\n\nYou will receive a newsletter twice a month, so I would be very happy to know what argument you would like to have more information.\n\nI wish for you a day full of inspiration.\n\nDr. Orlena Zotti\n', 2, 1),
(3, 'Bienvenido', 'Bienvenido VAR_NOME_CLIENTE,\n<br><br>\n<br>gracias por haberte inscrito en <a href="https://www.seiessenzialmentebella.com/it/home"><span class="orange-text">Sei Essenzialmente Bella.</a></span><br><br>\n<p aligh="left">Quería hablar un poquito contigo acerca de mis 3 pilares de trabajo:</p>\n<br>\n<p><ul align="left">\n<li>La Medicina Estética, mi pasión desde hace 12 años. Un mundo en el cual a través de la armonización de la belleza, he integrado dos conceptos: belleza externa y belleza interna.</li>\n<li>La alimentación, una experiencia vivida que quiero compartir contigo, serè tu guia en la interpretación de cómo se siente en tu cuerpo lo que comes, que alimentos te dan energía y cómo integrar de forma equilibrada tus nutrientes. Logrando así, tu bienestar general y mejorando tu imagen corporal.</li>\n<li>La valoración personal, una forma de hacerte dentro de tu vida, màs conciente, más serena, más presente, más amorosa y compasiva contigo misma y más feliz.</li>\n</ul>\n<br>\n<p align="justify">\nComo regalo de bienvenida te ofrezco una evaluación de medicina estetica online gratuita, que durará 20 minutos y hablaremos de esa situación en particular que quisieras resolver o mejorar. Este regalo no es transferible (valida hasta el 30 de junio del 2018). Solo tienes que responder a esta email para concordar tu cita. <strong>Disfrúta de esta oportunidad, da el primer paso para conocer más en profundidad lo que te puedo ofrecer.</strong>\n<br></br>\nTe quiero regalar también una guía que he realizado con todo el corazón para tí: <a href="https://drive.google.com/file/d/1sN4a-6BHCPgtUqdkyaNezms71CiuZZpc/view?usp=sharing">Alimentos para rejuvenecer</a>, esto es solo una pequeña prueba de como puedes tener una alimentación consciente.\n</p>\n<br>\n<p align="justify">\nUna vez al mes recibirás un boletín, por lo tanto, me gustaría saber sobre cual tema te gustaría tener información, ¿cuáles son tus intereses?\n</p>\n<br><br>\n<p align="justify" class="orange-text"><strong>¡Te deseo un dia lleno de inspiración!</p></strong>\n<br><br>\n<p align="justify"><strong>Doctora Orlena Zotti.</strong></p>\n', 3, 1),
(4, 'LINK', '<a href="url del link" target="_blank">TESTO VISIBILE</a>', 1, 0),
(5, 'Contatto ricevuto dal sito', 'Ciao VAR_NOME_CLIENTE,\nti ringraziamo per averci contattato.', 1, 2),
(6, 'Contacto recibido de la web', 'Ciao VAR_NOME_CLIENTE, gracias por contactarnos.', 3, 2),
(7, 'Il tuo corpo è il tuo tempio 2', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\n<i><strong>“Il tuo corpo è il tuo tempio 2”</i></strong> è un programma da svolgere presso il mio studio dove si procederà alla realizzazione di trattamenti di medicina estetica dedicati al corpo insieme allo sviluppo di un protocollo che servirà da supporto a questi trattamenti. \n<br></br>\nL’applicazione di trattamenti di medicina estetica si concretizza con l’obbiettivo di trattare la cellulite, il rilassamento cutaneo, l’adiposità localizzata, il gonfiore alle gambe e/o piedi e l’indolenzimento, migliorare il microcircolo e l’ossigenazione dei tessuti ottenendo come risultato una miglior qualità della cute ed un risveglio a livello cellulare.\n<br></br>\nIl protocollo di supporto si realizza con l’obbiettivo di rigenerare le cellule dell’organismo e anche creare un livello di consapevolezza che aiuti a capire che il mangiare è una azione che permette di nutrire le cellule in modo che il corpo e tutte le sue funzioni siano efficaci aiutandoci anche a rallentare l’invecchiamento cellulare. \n<br></br>\n<strong>Quanto tempo dura questo programma?</strong>\n<br>Dipende dai trattamenti di medicina estetica che si applicheranno. In genere i trattamenti corporei hanno bisogno di tempo per raggiungere un risultato. Se ad esempio il trattamento viene praticato per una insufficienza venosa, che porta generalmente ad avere gambe pesanti, gonfie e indolenzite, allora i risultati sono evidenti dalla prima seduta e andranno effettuate diverse sedute successive per mantenere il risultato nel tempo.\nTornando alla parte estetica, è necessaria più di una seduta perché si tratta di un processo di rigenerazione dal momento che si sta lavorando non solo sulla parte esterna del corpo ma anche all’interno dell’organismo.</br>\n<br></br>\n<strong>Con quale frequenza devo fare le sedute?</strong>\n<br>I trattamenti corporei richiedono una frequenza settimanale. Ma tutto dipende dal tipo di trattamento indicato per la parte che si vuole migliorare.</br>\n<br></br>\n<strong>I trattamenti sono dolorosi?</strong>\n<br>I trattamenti si applicano per via iniettiva e il dolore o il fastidio dipendono dalla sensibilità individuale di ciascuna persona. In genere sono trattamenti ben tollerati.</br>\n<br></br> \nI trattamenti che ti offro sono:\n<br></br>\n<strong>Carbossiterapia & Ossigenoterapia</strong>\n<br></br>\n<strong>Mesoterapia lipolitica, tensioattiva e drenante.</strong>\n<br></br>\n<strong>Bioristrutturazione</strong> a base di acido ialuronico per tonificare la cute.\n<br></br>\n<strong>Fili PDO</strong> per tonificare la cute e rigenerare il tessuto.\n<br></br>\n<i>Apro una parentesi sulla mesoterapia, la quale è una tecnica iniettiva che può essere praticata in qualsiasi zona del corpo, incluso il volto. Consiste nell’applicazione intradermica di una sostanza che poi si diffonderà lentamente nel tessuto sottostante con un’azione più efficace rispetto ad un’applicazione topica o ad una somministrazione sistemica.</i>\n<br></br>\n<strong>Il costo?</strong>\n<br></br>\nIl protocollo di supporto ha un costo di 220 euro.\n<br></br>\nI trattamenti di medicina estetica hanno prezzi diversi. La cosa migliore che ti posso offrire è una valutazione che mi aiuterà a decidere quali sono i trattamenti più indicati per te. \n<br></br>\nSe ancora permane qualsiasi dubbio mi puoi contattare rispondendo a questa email.\n<br></br>\n<span class="orange-text"><b>Desidero per te una giornata piena d’ispirazione.</span></b>\n<br></br>\n<span class="blu-text">Dottoressa Orlena Zotti.</span>\n\n\n\n', 1, 0),
(8, 'Il tuo corpo è il tuo tempio 2', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\n<i><strong>“Il tuo corpo è il tuo tempio 2"</i></strong> es un programa presidencial donde se procederá con la realización simultánea de tratamientos de medicina estética dedicados al cuerpo y un protocolo de soporte para estos tratamientos.\n<br></br>\nLa aplicación de tratamientos de medicina estética se realiza con el objetivo de tratar la celulitis, flacidez de la piel, la adiposidad localizada, inflamación y /o dolor en las piernas y pies, mejorar la microcirculación y la oxigenación de los tejidos obteniendo como resultado mejor calidad de la piel y una regeneración a nivel celular.\n<br></br>\nEl protocolo de soporte se realiza con el objetivo de regenerar las células del cuerpo y también crear un nivel de conciencia que ayude a entender que el comer es una acción que nos permite alimentar a las células de nuestro cuerpo logrando que sus funciones sean eficaces y ayudando a desacelerar el envejecimiento celular.\n<br></br>\n<strong>¿Cuánto dura este programa?</strong>\n<br></br>\nDepende de los tratamientos de medicina estética que se llevarán a cabo. Generalmente los tratamientos corporales necesitan tiempo para lograr un resultado, teniendo en cuenta que si el tratamiento se realiza para una insuficiencia venosa profunda que se manifiesta con piernas pesadas, inflamadas y dolientes, entonces los resultados se sienten desde la primera sesión y se necesitará hacer varias sesiones para mantener el resultado a lo largo del tiempo.\n\nVolviendo a la parte estética, se debe realizar más de una sesión, ya que es precisamente un proceso de regeneración, tomando en cuenta que no sólo se está trabajando en el exterior del cuerpo si no también el interior de éste.\n<br></br>\n<strong>¿Con qué frecuencia debo realizar las sesiones?</strong>\n<br></br>\nLos tratamientos corporales requieren una frecuencia semanal. Pero esto dependerá del tipo de tratamiento indicado para lo que quiere mejorar.\n<br></br>\n<strong>¿Los tratamientos son dolorosos?</strong>\n<br></br>\nLos tratamientos se aplican por vía inyectiva y el dolor o la incomodidad dependerán de la sensibilidad de cada persona. Generalmente son tratamientos bien tolerados.\n<br></br>\nTe hago saber los tratamientos que te ofrezco:\n<br></br>\n<strong>Carboxiterapía & oxígenoterapía</strong>\n<br></br>\n<strong>Mesoterapia lipolítica, tensioactiva y drenante.</strong>\n<br></br>\n<strong>Biorestructuración</strong> a base de ácido hialurónico para tonificar la piel.\n<br></br>\n<strong>Los hilos PDO</strong> para tonificar la piel y regenerar el tejido.\n<br></br>\n<i>Hago un paréntesis para entender el concepto: "La mesoterapia es una técnica inyectiva que puede realizarse en cualquier área del cuerpo, incluido el rostro. Es la aplicación intradérmica de una sustancia que luego se extenderá lentamente hacia el tejido subyacente con una acción más efectiva proporcionada de una aplicación tópica o administración sistémica ".</i>\n<br></br>\n<strong>¿El costo?</strong>\n<br></br>\nEl protocolo de soporte tiene un valor de 220 euros.\n<br></br>\nLos tratamientos de medicina estética tienen diversos precios. Lo mejor que puedo ofrecerte es hacer una evaluación que me ayudará a decidir cuales son los tratamientos indicados para ti.\n<br></br>\nSi aún tienes algún tipo de duda, puedes contactarme respondiendo a este correo electrónico.\n<br></br>\n<span class="orange-text"><b>¡Te deseo un día lleno de inspiración!</span></b>\n<br></br>\n<span class="blu-text">Dottoressa Orlena Zotti.</span>\n\n\n', 3, 0),
(9, 'Nutre corpo e anima 3', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\nIl programma<i><strong> “Nutre corpo e anima 3”</i></strong>, si svolge in sessioni individuali online. Ha una durata di 4 mesi ed è suddiviso in 5 moduli. \n<br></br>\nLe sessioni online  avranno una cadenza di 15 giorni ed una durata da 60 a 90 minuti ciascuna. \n<br><br>\nIl contatto avverrà tramite <strong>Skype:<big> sei essenzialmente bella.</strong></big> Ti consiglio di aggiungere il contatto alla tua lista così lo avrai a portata di mano quando cominceremmo.\n<br></br>\n<u>Contenuto: </u>\n<br></br>\n<strong>Modulo 1 (2 sessioni)</strong>\n<br></br>\nCarboidrati, lipidi e proteine:\n<br></br>\n<ul>\n<li>Come agiscono nell’organismo.</li>\n<li>Quali sono i benefici.</li>\n<li>Piatto consapevole e salutare </li>\n<li>Gruppi di alimenti </li>\n<li>Esperimenti e consigli</li>\n</ul>\n<br></br>\n<strong>Modulo 2 (2 sessioni)</strong>\n<br></br>\nFondamentalmente parleremo dei cibi dell’anima. Individueremo l’area della tua vita che ti genera insoddisfazione e definiremo come potresti iniziare ad agire per ottenere un risultato diverso. \n<br></br>\n“Non puoi risolvere un problema con lo stesso tipo di pensiero che hai usato per crearlo.” Albert Einstein.\n<br></br>\n<strong>Modulo 3 (2 sessioni)</strong>\n<br></br>\nImparerai a vedere il cibo che mangi in maniera differente, saranno i primi passi per iniziare a mangiare con consapevolezza.\nParleremmo di energia: Quali alimenti ti possono aiutare ad incrementare l’energia e quali invece la riducono.\n<br></br>\nVedremmo come puoi migliorare la tua digestione e come potresti ottenere benefici dai superfood.\n<br></br>\nConoscerai gli additivi alimentari.\n<br></br>\nDiventerai consapevole di come si sviluppa l’obesità ed il sovrappeso.\n<br></br>\n<strong>Modulo 4</strong>\n<br></br>\nLa relazione che intercorre tra consapevolezza piena e alimentazione e i suoi benefici.\n<br></br>\n<strong>Modulo 5</strong>\n<br></br>\nCome leggere le etichette degli alimenti. Conoscere come è fatto un prodotto non solo è importante nella cosmesi ma anche in quello che mangi, perché alla fine l’obbiettivo principale di questa azione è nutrire le nostre cellule.\n<br></br>\nPurificherai e disintossicherai il tuo organismo.\n<br></br>\n<big><b>In regalo con il programma avrai:</big></b>\n<br></br>\na)	4 Ebooks: Osa e crea / Crostata autunnale / Risveglio energetico / Nettalingua\n<br></br>\nb)	Ricette varie per implementare le novità\n<br></br>\nIl programma si compone di una parte teorica, per capire come funzionano gli alimenti nel nostro corpo, ed una parte pratica per imparare a riconoscere come il tuo corpo risponde agli alimenti.\nLe sessioni sono individuali e le date saranno programmate prima d’iniziare il percorso. Eventuali variazioni della data e dell’ora dovrai comunicarmele con un anticipo di almeno 48 ore, per poter riprogrammare la sessione.\n<br></br>\n<b>Il pagamento avverrà tramite PayPal</b>: orlena.zotti@gmail.com\n<br></br>\nIl costo di questo programma, pagando in un''unica soluzione, è di 582 Euro (la commissione PayPal la pago io).\n<br></br>\nHai l’opzione di poter frazionare il pagamento in 4 rate, il cui pagamento dovrà essere effettuato prima di iniziare ogni modulo. Se non viene saldata la rata non si potrà accedere al modulo. La quarta rata permette l’accesso al 4° e 5° modulo.\n<br></br>\n<ul>\n<li>1° = 210 + 5.9% commissione di PayPal</li>\n<li>2° = 150 + 5.9% commissione di PayPal</li>\n<li>3°= 150 + 5.9% commissione di PayPal</li>\n<li>4°= 150 + 5.9% commissione di PayPal</li>\n</ul>\n<br></br>\nSpero che le informazioni siano state complete e chiare.\n<br></br>\n<b><span class="orange-text">Desidero per te una giornata piena d’ispirazione!</b></span>\n<br></br>\n<span class="blu-text">Dottoressa Orlena Zotti.</span>\n\n\n\n\n', 1, 0),
(10, 'Nutre corpo e anima 3', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\n<br>El programa <i><b>“Nutre corpo e anima 3”</i></b> se lleva a cabo en sesiones individuales online. Tiene una duración de 4 meses y un contenido dividido en 5 módulos.</br>\n<br>Cada 15 días realizaremos sesiones que durarán entre 60 a 90 minutos.</br>\n<br>Las sesiones se realizarán a través de <strong>Skype:<big> sei essenzialmente bella.</strong></big> Te aconsejo de agregar el contacto en tu lista, así lo tendrás a la mano cuando iniciemos.</br>\n<br></br>\n<u>Contenido:</u>\n<br></br>\n<b>Módulo 1 (2 sesiones)</b>\n<br></br>\nCarbohidratos, lípidos y proteínas:\n<br></br>\n<ul>\n<li>¿Cómo funcionan en el cuerpo?</li>\n<li>¿Cómo te benefician?</li>\n<li>Un plato consciente y saludable</li>\n<li>Grupos de alimentos</li>\n<li>Experimentos y consejos</li>\n</ul>\n<br></br>\n<b>Módulo 2 (2 sesiones)</b>\n<br></br>\nBásicamente hablaremos sobre los alimentos del alma. El área de tu vida donde vives desde la insatisfacción y cómo podrías empezar a actuar para obtener un resultado diferente.\n<br></br>\n"No se puede resolver un problema con el mismo tipo de pensamiento que se ha usado para crearlo". Albert einstein.\n<br></br>\n<b>Módulo 3 (2 sesiones)</b>\n<br></br>\nAprenderás a ver de una manera diferente  los alimentos que comes, serán los primeros pasos para comenzar una alimentación consciente.\n<br></br>\nHablaremos de energía: ¿qué alimentos pueden ayudarte a aumentar la energía y cuáles pueden disminuirla?\n<br></br>\n¿Cómo puedes mejorar su digestión y cómo puedes obtener los beneficios de los superalimentos?\n<br></br>\nAditivos alimentarios.\n<br></br>\n¿Por qué se desarrollan la obesidad y el sobrepeso?\n<br></br>\n<b>Módulo 4</b>\n<br></br>\nEstar presente cuando te alimentas y los beneficios que te brinda.\n<br></br>\n<b>Módulo 5</b>\n<br></br>\n¿Cómo leer una etiqueta y para qué te sirve? Leer como es hecho un producto no sólo es importante en la cosmética, también debes crear consciencia sobre la forma en la que estás alimentando tu cuerpo, porque al final el objetivo principal de alimentación es nutrir nuestras células.\n<br></br>\nPrepárate para purificar y desintoxicar tu cuerpo.\n<br></br>\n<b><big>El programa tiene estos regalos para ti:</b></big>\n<br></br>\na) 3 Ebooks: Atrévete y crea / Despierta energía / Limpiador de lengua.\n<br></br>\nb) Varias recetas para implementar las noticias\n<br></br>\n\nEl programa consta de una parte teórica, para entender como funcionan los alimentos en nuestro cuerpo (global) y una parte práctica, de esta manera, aprenderá a reconocer cómo responde tu cuerpo a través de lo que comes (individual).\n<br></br>\nComo las sesiones son individuales, las fechas se programarán antes de comenzar el recorrido. En el momento en el cual se deban presentar modificaciones en los días y horarios preestablecidos se debe avisar con un tiempo de anticipación de 48 horas, para la reprogramación. \n<br></br>\n<strong>El pago se realiza por PayPal:</strong> orlena.zotti@gmail.com\n<br></br>\nEl costo de este programa en pago único es de 582 euros (la comisión de PayPal la pago yo)\n<br></br>\nTambién tienes la opción de hacer el pago en cuotas, éstas serán pagadas antes de comenzar cada módulo, si no se cancela la cuota no será enviado el módulo. La 4° cuota permite el acceso al 4to y 5to modulo.\n<br></br>\n<ul>\n<li>1º = 210 + 5,9% de comisión de PayPal</li>\n<li>2º = 150 + 5,9% de comisión de PayPal</li>\n<li>3º = 150 + 5,9% de comisión de PayPal</li>\n<li>4° = 150 + 5,9% de comisión de PayPal</li>\n</ul>\n<br></br>\nSi queda algún tipo de dudas, puedes responder este email.\n<br></br>\n<strong><span class="orange-tect">¡Te deseo un día lleno de inspiración!</strong></span>\n<br></br>\n<span class="blu-text">Dottoressa Orlena Zotti.</span>\n\n\n\n\n\n', 3, 0),
(11, 'La tua valutazione online', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\n<i><b>"La tua valutazione online"</i></b> è un programma che viene offerto alle persone che non possono fare una visita presso il mio studio, ma che sentono il desiderio di conoscere cosa potrebbero fare per migliorare ciò che non soddisfa sia del proprio volto che del corpo.\n<br></br>\nLa seduta avverrà tramite <strong>Skype:<big> sei essenzialmente bella.</big></strong> Ti consiglio di aggiungere il contatto alla tua lista così lo avrai subito a disposizione quando cominceremmo.\n<br></br>\nDue giorni prima di iniziare la visita dovrai inviare le foto del volto e/o del tuo corpo, a seconda dell’area di cui desideri parlare. \n<br></br>\n<b>Le foto del volto devono essere scattate:</b>\n<br></br>\n<ul>\n<li>con buona luce \n<li>1 foto di fronte\n<li>1 foto per ogni profilo\n<li>deve essere visibile tutto il volto\n</ul>\n<br></br>\n<b>Le foto del corpo, a seconda della parte interessata, devono essere scattate:</b>\n<br></br>\n<ul>\n<li>addome</li>\n<br>un’immagine frontale ed una per ogni profilo. Se si tratta di rilassamento della cute addominale, la foto deve essere scattata con il torace piegato un po’ in avanti, in modo che il rilassamento si possa apprezzare meglio.</br>\n<li>le gambe e glutei </li>\n<br>ogni profilo (sia frontale che laterale)</br>\n</ul>\n<br></br>\n<b>La foto di una altra zona specifica che vorresti trattare.</b>\n<br></br>\nLa visita durerà 40 - 60 minuti e dopo aver valutato la tua situazione ti indicherò quali sono i trattamenti che potresti effettuare, ti spiegherò in cosa consistono e ti consiglierò i prodotti che si possono trovare a livello mondiale.  In questo modo sarai più informata e ti sentirai più tranquillità nel momento in cui sceglierai il tuo medico estetico per il trattamento. Se posso indirizzarti da un collega che si trovi nella tua zona lo farò con vero piacere.\n<br></br>\n<big><a href="https://drive.google.com/drive/folders/1IasP80NDC2Sq_7ukbof_VmNdAh4Kr2CU?usp=sharing">Cliccando QUA</a></big> troverai esempi di come dovresti scattare le foto.\n<br></br>\nIl costo di<i><b> “La tua valutazione online”</i></b> è di 50 Euro (+ 5,9% commissione PayPal)\n<br></br>\nIl pagamento deve essere effettuato <b>almeno 24 ore prima della sessione</b> di valutazione tramite PayPal all’indirizzo: orlena.zotti@gmail.com  \n<br></br>\nSe hai dubbi puoi rispondere a questa email.\n<br></br>\n<strong><span class="orange-text">Desidero per te una giornata piena d’ispirazione!</strong></span>\n<br></br>\n<span class="blu-text">Dottoressa Orlena Zotti.</span>\n\n\n\n', 1, 0),
(12, 'La tua valutazione online', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\n<i><b>“La tua valutazione online”</i></b> es un programa que se ofrece a las personas que no pueden realizar una consulta presencial, pero que tienen el deseo de saber lo que podrían hacer para mejorar ese algo que no le gusta, ya sea la cara o el cuerpo.\n<br></br>\nLa evaluación sera realizada por <b>Skype: <big>sei essenzialmente bella.</big></b> Te aconsejo que agragues este contacto a tu lista, así lo tendrás a la mano cuando iniciemos.\n<br></br>\nDos días antes de la consulta, deberás enviar una foto de tu rostro y / o  cuerpo, según la zona de la cual desee hablar.\n<br></br>\n<b>La foto de la cara debe ser tomada:</b>\n<br></br>\n<ul>\n<li>Con luz</li>\n<li>1 foto frontal</li>\n<li>foto para cada perfil</li>\n<li>Se debe ver toda la cara</li>\n</ul>\n<br></br>\n<b>La foto del cuerpo debe ser tomada de manera que sea vea toda la zona:</b>\n<br></br>\n<ul>\n<li>abdomen</li> \n<br>frontal y cada lado. Si tu situación es falta de tonicidad de la piel abdominal, la foto debe tomarse con el tronco inclinado un poco hacia adelante para que se pueda apreciar mejor la poca tonicidad.</br>\n</ul>\n<br></br>\n<ul>\n<li>piernas y glúteos </li>\n<br>foto de cada perfil.</br>\n</ul>\n<br></br>\n<b>La foto de un área específica que quisieras tratar.</b>\n<br></br>\nLa consulta durará entre 40-60 minutos, y después de evaluar tu situación, te hare saber cuáles son los tratamientos que se pueden realizar, te voy a explicar en qué consisten y te diré los nombres de productos que se pueden encontrar a nivel mundial. De esta forma, estarás informada y te sentirás más tranquila al elegir tu médico estético tratante. Y si está en mis manos te pondré en contacto con un colega que está en tu zona geográfica.\n<br></br>\n<bi><a href="https://drive.google.com/open?id=1IasP80NDC2Sq_7ukbof_VmNdAh4Kr2CU">Haciendo click  AQUI,</big></a> encontrarás ejemplos de cómo debes tomar las fotos.\n<br></br>\nEl costo de "La tua valutazione online" es de 50 euros (+ 5,9% de comisión de PayPal)\n<br></br>\nEl pago se debe<b> 48 horas antes de la sesión</b> a través de PayPal a la dirección: orlena.zotti@gmail.com\n<br></br>\nSi aún tienes dudas, puede responder a este correo electrónico.\n<br></br>\n<b><span class="orange-text">¡Te deseo un día lleno de inspiración!</b></span>\n<br></br>\n<span class="blu-text">Dottoressa Orlena Zotti.</span>\n\n\n\n', 3, 0),
(13, 'Innamorati di te 2', 'Ciao <b>VAR_NOME_CLIENTE</b>\n<br></br>\n<i><b>"Innamorati di te 2"</i></b> è un programma che ha una data specifica d’inizio la quale verrà pubblicizzata sul sito web.\n<br></br>\nQuando saremmo vicini alla data d’inizio ti invieremmo un’email con le informazioni su come si svolgerà questo programma e su come fare ad iscriversi. \n<br></br>\nSeguendolo avrai la possibilità di cominciare a conoscere le tue potenzialità ed esprimere quello che veramente sei.\n<br></br>\n<b><span class="orange-text">Desidero per te una giornata piena d’ispirazione.</b></span>\n<br></br>\n<span class="blu-text">Dottoressa Orlena Zotti. </span>\n\n', 1, 0),
(14, 'Innamorati di te 2', 'Hola <b>VAR_NOME_CLIENTE</b>\n<br></br>\n<i><b>“Innamorati di te 2”</i></b> es un programa que tiene una fecha de inicio específica que será publicada en la página web.\n<br></br>\nCuando estemos cerca de la fecha de inicio, te enviaremos un email con información sobre cómo proceder con este programa y qué debes hacer para inscribirte.\n<br></br>\nSiguiendo este camino comenzarás a conocer tu potencial y mostrarte como realmente eres.\n<br></br>\n<b><span class="orange-text"¡Te deseo un día lleno de inspiración!</b></span>\n<br></br>\n<span class="blu-text">Dottoressa Orlena Zotti.</span>\n', 3, 0),
(15, 'Newsletter febrero 2018', 'Ciao, <b>VAR_NOME_CLIENTE</b>\n<br></br>\nParliamo di armonia:\n<br></br>\nPossiamo accettare ciò che ci capita con una sorta di rassegnazione o fare in modo che quello che ci rende felici accada nella nostra vita. Possiamo anche vivere con entusiasmo o sconforto, con allegria o tristezza. Il punto è quello di essere consapevole che la scelta è sempre nostra e come vogliamo vivere la nostra vita dipende solo da noi.\n<br></br>\nInizia a vedere la vita in modo diverso da quello che sei abituata, inizia ad uscire da quello stato di zombi dove ti hanno insegnato che le cose non si mettono in discussione. Non esiste una verità assoluta, l’unica verità assoluta che esiste sta dentro di te perché solo tu sai che cosa ti aiuta a vibrare alto e in armonia.\n<br></br>\nCome già sai uno degli strumenti che uso per dare armonia, in questo caso alla parte fisica del nostro essere, è la medicina estetica. Il botulino è uno dei trattamenti che più mi piace per dare quella apparenza riposata al volto. \n<br></br>\nTi domanderai come funziona. Ha una durata di 90 giorni nell’organismo ma il risultato di questo trattamento dura da 4 a 6 mesi. Ti spiego, la funzione del botulino è quella di diminuire la forza di contrazione dei muscoli mimici, dopo che il botulino viene eliminato dall’organismo, il rilassamento muscolare si mantiene grazie in parte alla memoria cellulare. \n<br></br>\nIl botulino non gonfia e non si usa per riempire, il suo utilizzo si basa soltanto sulla diminuzione della contrazione dei muscoli mimici.\n<br></br>\nPer sapere di più sul botulino puoi premere <span class="orange-text"><a href="https://www.seiessenzialmentebella.com/it/trattamenti/3/botox">QUI.</a></span>\n<br></br>\nAttraverso quello che mangi puoi raggiungere la tua giusta proporzione di armonia del tuo corpo tramite la rigenerazione cellulare.\n<br></br>\nIl mangiare è uno degli atti attraverso il quale il nostro corpo si mantiene giovane, con energia e in salute, perché le cellule dell’organismo prendono quello che serve per la sua rigenerazione da ciò che mangiamo. Lo sapevi che le cellule si rigenerano tutti i giorni? Compromettiti con te stessa e fai il primo passo per avere una alimentazione consapevole.\n<br></br>\nSe ti interessa sapere di più, ti posso aiutare e ti invito a conoscere il programma <strong>“Nutre corpo e anima 3”</strong> facendo clic <span class="orange-text"><a href="https://www.seiessenzialmentebella.com/it/programmi/4/nutre-corpo-e-anima-3">QUI.</a></span>\n<br></br>\nVivi in armonia in tutti i tuoi corpi: fisico, mentale, emozionale, spirituale ed energetico.\n<br></br>\n<span class="orange-text"><b>Desidero per te una giornata piena d’ispirazione e armonia.</a></b>\n<br></br>\n<span class="blu-text">Dottoressa Orlena Zotti.</a>\n \n\n', 1, 0),
(16, 'Boletín Febrero 2018', 'Hola, <b>VAR_NOME_CLIENTE</b>\n<br></br>\nHablemos de armonía:\n<br></br>\nPodemos aceptar lo que nos sucede con una especie de resignación o asegurarnos de que aquello que nos hace felices suceda en nuestras vidas. También podemos vivir con entusiasmo o desaliento, con alegría o tristeza. El punto es ser consciente de que la elección siempre es nuestra y que la forma en que queremos vivir depende solo de nosotras.\n<br></br>\nComienza a ver la vida de forma diferente a lo que estás acostumbrada, comienza a salir de ese estado zombi donde te enseñaron que las cosas no se cuestionaban. No hay una verdad absoluta, la única verdad absoluta que existe está dentro de ti porque solo tú sabes lo que te ayuda a vibrar alto y en armonía.\n<br></br>\nComo ya sabes, una de las herramientas que utilizo para dar armonía, en este caso, a la parte física de nuestro ser, es la medicina estética. El Botox es uno de los tratamientos que más me gusta para darle esa apariencia relajada y reposada al rostro.\n<br></br>\nTe preguntarás cómo funciona. El producto dura 90 días en el cuerpo pero el resultado de este tratamiento dura de 4 a 6 meses. Te explico, la función del Botox es disminuir la fuerza de contracción de los músculos mímicos, después de que el Botox es eliminado del cuerpo, la relajación muscular se mantiene gracias en parte a la memoria celular.\n<br></br>\nEl Botox no te hincha y no se usa para rellenar, su uso se basa únicamente en la disminución de la contracción de los músculos mímicos.\n<br></br>\nPara saber más sobre este tratamiento, puede presionar <a href="https://www.seiessenzialmentebella.com/es/tratamientos/3/botox">AQUÍ.</a>\n<br></br>\nGracias a lo que comes puedes alcanzar la proporción correcta de armonía en tu cuerpo a través de la regeneración celular.\n<br></br>\nComer es uno de los actos por los cuales nuestro cuerpo se mantiene joven, enérgico y saludable, porque las células del organismo toman de lo que comemos lo que se necesita para su regeneración. ¿Sabías que las células se regeneran todos los días? Comprométete y da el primer paso para tener una alimentación consciente.\n<br></br>\nSi quieres saber más, te puedo ayudar invitándote a que conozcas el programa <strong>"Nutre corpo e anima 3"</strong> haciendo clic <a href="https://www.seiessenzialmentebella.com/es/programas/4/nutre-corpo-e-anima-3">AQUÍ.</a>\n<br></br>\nVive en armonía con todos tus cuerpos: físico, mental, emocional, espiritual y  energético.\n<br></br>\n<b><span class="orange-text">Te deseo un día lleno de inspiración y armonía.</b></span>\n<br></br>\n<span class="blu-text">Dra. Orlena Zotti.</span>\n', 3, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates_traduzioni`
--

CREATE TABLE IF NOT EXISTS `email_templates_traduzioni` (
  `id_template_trad` int(11) NOT NULL,
  `testo_preheader` text NOT NULL,
  `testo_footer_copyright` text NOT NULL,
  `testo_footer_unsubscribe` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_templates_traduzioni`
--

INSERT INTO `email_templates_traduzioni` (`id_template_trad`, `testo_preheader`, `testo_footer_copyright`, `testo_footer_unsubscribe`, `lingua_traduzione_id`) VALUES
(1, 'Vedi email online', 'TUTTI I DIRITTI RISERVATI', 'Se non vuoi più ricevere queste email per favore <span style="text-decoration:underline;"><a href="VAR_UNSUBSCRIBE_LINK" target="_blank" style="text-decoration:underline; color:#ffffff;">CANCELLATI</a></span>', 1),
(2, 'See contents online', 'ALL RIGHTS RESERVED', 'If you no longer wish to receive these emails please <span style = "text-decoration: underline;"> <a href="VAR_UNSUBSCRIBE_LINK" target="_blank" style="text-decoration:underline;color:#ffffff;">UNSUBSCRIBE</a></ span>', 2),
(3, 'Ver en línea', 'TODOS LOS DERECHOS RESERVADOS', 'Si ya no desea recibir estos correos electrónicos, <span style="text-decoration: underline;"><a href="VAR_UNSUBSCRIBE_LINK" target="_blank" style="text-decoration:underline;color:#ffffff;">CANCELATE</a></ span>', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `id_faq` int(11) NOT NULL,
  `titolo_faq` text NOT NULL,
  `allineamento_faq` varchar(100) NOT NULL DEFAULT 'center'
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `faq`
--

INSERT INTO `faq` (`id_faq`, `titolo_faq`, `allineamento_faq`) VALUES
(1, 'Il trattamento è doloroso?', 'justify'),
(2, 'Complicanze?', 'justify'),
(3, 'Quali sostanze si usano?', 'center'),
(4, 'Quante volte lo devo fare?', 'justify'),
(5, 'Quanto dura una seduta?', 'center'),
(6, 'Quando inizio a vedere i risultati?\n	', 'center'),
(7, 'Cosa succede dopo il trattamento?\n	', 'justify'),
(8, 'Quale sostanze si usano?\n	', 'justify'),
(9, 'In cosa consiste il trattamento?\n	', 'justify'),
(11, 'Quanto durano gli effetti?\n	', 'center'),
(12, 'Posso sentirmi tranquilla e sicura nei trattamenti con il botulino?\n	', 'justify'),
(13, 'Il trattamento con botulino è doloroso?\n	', 'justify'),
(14, 'Quando inizio a vedere i risultati dopo la seduta di botulino?\n	', 'justify'),
(15, 'Quanto durano gli effetti del botulino?\n	', 'justify'),
(16, 'A quale età va eseguito?\n	', 'justify'),
(17, 'Divento inespressiva?\n	', 'justify'),
(18, 'Come si svolge il trattamento con fili PDO?\n	', 'justify'),
(19, 'Il trattamento con fili PDO è doloroso?\n	', 'justify'),
(20, 'Quando inizio a vedere i risultati con fili PDO?\n	', 'justify'),
(21, 'Quanto durano i risultati dei fili PDO?\n	', 'justify'),
(22, 'Cosa devo fare dopo il trattamento con PDO?\n	', 'justify'),
(23, 'Quanto dura la seduta con Needling?\n	', 'center'),
(24, 'Il trattamento needling lo possono fare tutti?\n	', 'justify'),
(25, 'Che cosa comporta il needling?\n	', 'justify'),
(26, 'Quando inizio a vedere risultati del needling?\n	', 'center'),
(27, 'Di quante sedute needling ho bisogno?\n	', 'justify'),
(28, 'Il trattamento filler è doloroso?\n', 'justify'),
(29, 'Ho bisogno di anestesia nei filler?\n', 'justify'),
(30, 'Quanto tempo dura il risultato dei filler?\n\n', 'justify'),
(31, 'Quanto tempo ci vuole per realizzare il trattamento filler?\n', 'justify'),
(32, 'Cosa può accadere dopo il trattamento con filler?\n', 'justify'),
(33, 'Dopo il filler posso continuare con le mie attività della giornata?\n\n', 'justify'),
(34, 'Posso andare al mare subito dopo il filler?\n', 'justify'),
(35, 'Il botulino offre risultati definitivi nella iperidrosi?\n	', 'justify'),
(36, 'Quanto dura la seduta per iperidrosi?\n	\n', 'center'),
(37, 'Ha effetti avversi iperidrosi?\n	\n', 'center'),
(38, 'In cosa consiste il peeling?\n	', 'justify'),
(39, 'Si può fare il peeling in qualsiasi stagione dell’anno?\n	', 'justify'),
(40, 'Devo fare qualcosa prima di un peeling?\n	', 'justify'),
(41, 'Quanto dura una seduta di peeling?\n	', 'center'),
(42, 'Ci sono controindicazioni nei peeling?\n	\n', 'justify'),
(43, 'Quanti peeling posso fare?\n	', 'justify'),
(44, 'Il trattamento mesoterapia è doloroso?\n	', 'justify'),
(45, 'Quanto dura la seduta di mesoterapia?\n	', 'center'),
(46, 'Basta una seduta di mesoterapia per vedere i risultati?\n	', 'justify'),
(47, 'Cosa succede dopo il trattamento con mesoterapia?\n	', 'justify'),
(48, 'Si può fare la mesoterapia in qualsiasi momento?\n	', 'justify'),
(49, 'Si deve fare un mantenimento con mesoterapia?\n	', 'justify'),
(50, 'Quanti cicli di mesoterapia si possono fare in un anno?\n	', 'center'),
(51, 'Il trattamento rinofiller è doloroso?\n	', 'justify'),
(52, 'Aumenta la dimensione del naso?\n	', 'justify'),
(53, 'Quanto dura il trattamento rinofiller?\n	', 'justify'),
(54, 'Quanto dura il risultato del rinofiller?\n	', 'justify'),
(55, 'Dopo il trattamento rinofiller si deve fare qualcosa?\n	\n', 'justify'),
(56, 'Il trattamento carbossiterapia è doloroso?\n	\n', 'justify'),
(57, 'I benefici della carbossiterapia si vedono in una sola seduta?\n	', 'justify'),
(58, 'Quante sedute di carbossiterapia devo fare per avere risultati estetici?\n	', 'justify'),
(59, 'Quali zone si possono trattare con carbossiterapia?\n	\n', 'justify'),
(60, 'Ci sono dei rischi con la carbossiterapia?\n	', 'justify'),
(61, 'Può essere associata ad altri trattamenti?\n	\n', 'justify'),
(62, 'Ho le gambe gonfie, appesantite e mi fano male, posso fare questo trattamento?\n	\n', 'justify'),
(63, 'Esiste qualche controindicazione della carbossiterapia? \n	', 'justify'),
(64, 'Come si svolge il trattamento di sublimazione?\n	', 'justify'),
(65, 'Cosa accade dopo il trattamento di sublimazione?', 'list'),
(66, 'I risultati della sublimazione durano nel tempo?\n ', 'justify'),
(67, 'Si può fare in qualsiasi momento?\n\n', 'center'),
(68, 'Quali sostanze si usano nella biostimolazione?\n	', 'center');

-- --------------------------------------------------------

--
-- Struttura della tabella `faq_traduzioni`
--

CREATE TABLE IF NOT EXISTS `faq_traduzioni` (
  `id_faq_traduzioni` int(11) NOT NULL,
  `id_faq` int(11) NOT NULL,
  `testo_faq` text NOT NULL,
  `risposta_faq` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `faq_traduzioni`
--

INSERT INTO `faq_traduzioni` (`id_faq_traduzioni`, `id_faq`, `testo_faq`, `risposta_faq`, `lingua_traduzione_id`) VALUES
(1, 1, 'Il trattamento è doloroso?', 'Il trattamento non è doloroso, potrebbe causare leggero fastidio perché si tratta di una procedura iniettiva.', 1),
(2, 2, 'Complicanze?', 'Non esistono complicanze significative, l’unica cosa che si potrebbe presentare sono piccoli lividi o pomfi, che si risolvono in poche ore o qualche giorno e che comunque si possono facilmente coprire con il trucco.', 1),
(3, 1, 'Is the treatment painful?', 'The treatment is not painful, it may cause slight annoyance because it is an injective procedure.', 2),
(4, 2, 'Complications?', 'There are no significant complications, the only thing that could be presented is small bruises or bumps, which resolve within a few hours or a few days and which can easily be covered with the trick.', 2),
(5, 1, '¿El tratamiento es doloroso?', 'El tratamiento no es doloroso, podría causar ligero fastidio porque es un procedimiento inyectivo.', 3),
(6, 2, '¿Se pueden presentar complicaciones?', 'No existen complicaciones significativas, lo único que podría presentarse son pequeños hematomas, que se resuelven en pocas horas o algunos días  y pueden ser cubiertos facilmente con maquillaje.', 3),
(7, 3, 'Quali sostanze si usano?', 'Le sostanze che possono essere iniettate sono: acido ialuronico a bassa densità, aminoacidi, dei glicosaminoglicani, vitamine, PDRN (polidesossiribonucleotide), ecc.', 1),
(8, 3, '¿Qué sustancias se usan?\n	', 'Las sustancias que pueden ser inyectadas son: ácido hialurónico a baja densidad, aminoácidos,  glicosaminoglicanos, vitaminas, PDRN (polidexosiribonucleotidos), etc.\n', 3),
(9, 4, 'Quante volte lo devo fare?', 'Questo dipende dal bisogno della pelle. I protocolli variano infatti a seconda delle esigenze, ad esempio possono consistere in cicli di 4-6 sedute una volta alla settimana, come anche in cicli di 5 sedute ogni 21 giorni da ripetere 2 volte all’anno.', 1),
(10, 4, '¿Cuántas veces lo tengo que hacer?\n\n', 'Esto depende de la necesidad de la piel. Los protocolos varian según las exigencias de ésta, por ejemplo existen protocolos de 4-6 sesiones una vez a la semana, como ciclos de 5 sesiones cada 21 días a repetir 2 veces al año.\n', 3),
(11, 5, 'Quanto dura una seduta?', 'Dura 30 minuti.', 1),
(12, 5, '¿Cuánto tiempo dura una sesión?\n	\n', 'Dura de 20 a 30 minutos.\n', 3),
(13, 6, 'Quando inizio a vedere i risultati?\n	', 'Dopo 48 ore dalla prima seduta.\n', 1),
(14, 6, '¿Cuándo comienzo a ver resultados?\n	', 'Más o menos 48 horas después de la primera sesión.\n', 3),
(15, 7, 'Cosa succede dopo il trattamento?\n\n', 'Si possono presentare arrossamento, gonfiore o dolore localizzato nella sede d’iniezione che in genere si risolve in poco tempo. Potrebbero formarsi anche lividi. Tutto ciò si può camuffare con il trucco. ', 1),
(16, 7, '¿Qué sucede después del tratamiento?\n	', 'Se puede presentar enrojecimiento, inflamación o dolore localizado en la zona de inyección, resolviendose en poco tiempo. También pequeños hematómas. Todo esto se puede camuflar con maquillaje.', 3),
(17, 8, 'Quale sostanze si usano?\n	', 'Acido ialuronico a basso ed alto peso molecolare che permette la rimodellazione del viso e la nutrizione della pelle.\n', 1),
(18, 8, '¿Qué sustancias se usan?\n', 'Ácido hialurónico a bajo y alto peso molecolar que permiten restructurar el rostro y nutrir la piel.\n', 3),
(19, 9, 'In cosa consiste il trattamento?\n\n', 'Sono delle iniezioni che si eseguono su punti specifici sul volto. Il trattamento si realizza in un ciclo di tre sedute, due volte all’anno.\n', 1),
(20, 9, '¿En qué  consiste el tratamiento?\n	\n', 'Son inyecciones que se realizan en puntos específicos del rostro. El tratamiento se realiza en un ciclo de 3 sesiones dos veces al año.\n', 3),
(21, 11, 'Quanto durano gli effetti?\n	\n', 'In genere dura 6 mesi per ogni ciclo.\n', 1),
(22, 11, '¿Cuánto dura el efecto?\n	', 'Dura 6 meses por cada ciclo.\n', 3),
(23, 12, 'Posso sentirmi tranquilla e sicura nei trattamenti con il botulino?\n	\n', 'Anche io all’inizio dei miei studi di medicina estetica mi sono fatta tante volte la stessa domanda. Ebbene, il botulino è un trattamento sicuro, efficace ed ampiamente utilizzato non solo in questo settore. È una sostanza che proviene da un batterio, che si chiama Clostridium Botulinum, ed è in grado di paralizzare la muscolatura. In medicina estetica la sua applicazione avviene in basse percentuali perché l’obiettivo è quello di agire localmente sui muscoli coinvolti nella mimica. Esso provoca una diminuzione della forza di contrazione dei muscoli dove viene iniettato, mantenendo la tua espressività e non permettendo di accentuare la formazione delle rughe.     \n', 1),
(24, 12, '¿Me puedo sentir tranquila y segura realizando este tipo de tratamiento?\n	\n', 'Cuando inicié a estudiar medicina estética me hice muchas veces esta pregunta. Déjame decirte que, el botox es un tratamiento seguro, eficaz y ampliamente utilizado, no solo en este sector de la medicina estética. Es una sustancia que proviene de una bacteria, se llama Clostridium Botulinum, su acción es paralizar el sistema muscular. En medicina estética la dosis que se utiliza es muy baja en porcentaje. El objetivo es el de actuar localizadamente en los mùsculos de la mìmica facial. Esto provoca una diminución de la fuerza de contracciòn de los músculos donde se inyecta, de esta manera mantienes tu expresividad sin acentuar tanto las arrugas. \n', 3),
(25, 13, 'Il trattamento è doloroso?\n	', 'Il trattamento non è doloroso, potrebbe causare sensazione di bruciore per il pH variabile della soluzione fisiologica con cui viene diluito il botulino. La sensazione passa subito dopo l’iniezione.\n', 1),
(26, 13, '¿El tratamiento es doloroso?\n	', 'El tratamiento no es doloroso, podría causar una sensación no tan agradable por el pH variable de la solución fisiológica con la cual se diluye el botox. Esta sensación pasa inmediatamente luego de la inyección.\n', 3),
(27, 14, 'Quando inizio a vedere i risultati?\n	', 'I risultati si iniziano a vedere dopo 72 ore dal trattamento e passati 15 giorni devi fare una visita di controllo. \n', 1),
(28, 14, '¿Cuándo inicio a ver los resultados?\n	', 'Los resultados se inician a ver después de 72 horas del tratamiento y pasados 15 días se debe realizar una visita de control. \n', 3),
(29, 15, 'Quanto durano gli effetti?\n	', 'Ha una durata variabile tra 4 e 6 mesi, si può ripetere senza nessuna limitazione nel tempo. Da tenere in conto solo alcuni accorgimenti. \n', 1),
(30, 15, '¿Cuánto duran los efectos?\n	', 'Tiene una duración variable entre  4 e 6 meses, se puede repetir sin limitación en el tiempo,  con algunas precauciones.\n', 3),
(31, 16, 'A quale età va eseguito?\n	', 'È indicato nelle persone giovani che tendono a corrugare in modo accentuato e quindi a formare rughe permanenti e visibili nel tempo. Anche in quelle persone in cui le rughe d’espressione sono già visibili a riposo, l’effetto del botulino sarà quello di ridurre le stesse in maniera notevole. In altre parole non c’è limite di età.\n', 1),
(32, 16, '¿A partir de qué edad se puede realizar el tratamiento?\n	', 'Es indicado en las personas jóvenes que tienden a contraer los músculos en modo acentuado, originando así arrugas permanentes y visibles en el tiempo. También en las personas donde las arrugas de expresión son visibles en reposo (cuando no hay movimiento muscular), el efecto del botox es el de reducir las líneas de expresión de manera notable. En otras palabras no hay un límite de edad.\n', 3),
(33, 17, 'Divento inespressiva?\n	', 'Il botox riduce la contrattilità del muscolo ma non lo blocca, conservando la tua mimica espressiva ma riducendo la formazione delle rughe.\n', 1),
(34, 17, '¿Quedo sin expresividad?\n	', 'El bótox reduce la contracción mas no bloca el músculo, conservas tu expresividad con unas arrugas menos evidentes.\n', 3),
(35, 18, 'Come si svolge il trattamento?\n	', 'Il filo PDO viene collocato all’interno di un ago sottile e questo viene inserito dentro la pelle, dopo di che si sfila l’ago e il filo viene rilasciato al interno del tessuto.\n', 1),
(36, 18, '¿Cómo se realiza el tratamiento?\n	', 'El hilo PDO es colocado al interno de una aguja y esta viene inserida en la dermis, luego se retira la aguja y el hilo se deja al interno del tejido.\n', 3),
(37, 19, 'Il trattamento è doloroso?\n	', 'Il trattamento non è doloroso, potrebbe causare fastidio perché è si tratta di una procedura iniettiva.\n', 1),
(38, 19, '¿El tratamiento es doloroso?\n	', 'El tratamiento no es doloroso, podría causar ligero fastidio porque es un procedimiento inyectivo.\n', 3),
(39, 20, 'Quando inizio a vedere i risultati?\n	', 'I fili stimolano la produzione di collagene ed elastina, per tale motivo hanno bisogno di un periodo di tempo che va da un mese ad un mese e mezzo per rendere visibili i benefici. \n', 1),
(40, 20, '¿Cuándo comienzo a ver los resultados?\n	', 'Los hilos estimulan la producción de colágeno y elastina, por este motivo se necesita de un período de tiempo que va de un mes a un mes y medio para que los beneficios puedan ser visibles.\n', 3),
(41, 21, 'Quanto durano i risultati?\n	', 'I risultati durano circa 6-8 mesi, consiglio sempre di fare una seconda seduta vicina al termine di questo periodo al fine di prolungare così l’effetto di questo trattamento. \n', 1),
(42, 21, '¿Cuánto duran los resultados?\n	', 'Los resultados duran de 6-8 mesi, aconsejo hacer siempre una segunda sesión cuando esta por terminar este período, para prolongar de esta manera los efectos del tratamiento.\n', 3),
(43, 22, 'Cosa devo fare dopo il trattamento?\n\n	', 'Post trattamento non c’è nessuna prescrizione che si debba seguire. Puoi precedere vivendo con normalità la tua giornata.\n', 1),
(44, 22, '¿Qué debo hacer después del tratamiento?\n	', 'No se debe realizar nada, simplemente continuar con la normalidad de las actividades previstas para el día.\n', 3),
(45, 23, 'Quanto dura la seduta?\n	', 'Dipende dell’area da trattare, tra 40 e 60 min.\n', 1),
(46, 23, '¿Cuánto dura la sesión?\n	\n', 'Depende de la zona de tratamiento, entre 40 – 60 min.\n', 3),
(47, 24, 'Il trattamento lo possono fare tutti?\n	', 'Si, tranne le persone che presentano lesioni nella zona che si dovrebbe trattare.\n', 1),
(48, 24, '¿El tratamiento puede ser realizado en todas las personas?\n	\n', 'Sí, indiferentemente del fototipo de la piel. No se realiza en casos como lesiones  en el área que se debe tratar.\n', 3),
(49, 25, 'Che cosa comporta?\n	', 'Il trattamento si esegue dopo l’applicazione di anestesia topica, si prosegue facendo delle micro iniezioni multiple e l’area trattata diventerà rossa. Dopo di che si prosegue con la fase di raffreddamento e si applica una crema lenitiva. L’arrossamento, che può essere anche intenso, dura da 24 a 48 ore. Questo tipo di trattamento andrà fatto preferibilmente durante il periodo invernale. \n', 1),
(50, 25, '¿En qué consiste?\n	', 'El tratamiento se realiza después de la aplicación de anestesía tópica, luego de realizar las micro inyecciones de manera multiple el área tratada se presentará un color rojo intenso. Se procede a la fase de enfriamiento y aplicación de una crema hidratante que disminuye la sensación de calor. El enrojecimiento dura de 24 a 48 horas. Este tipo de tratamiento se realiza durante el periodo invernal. \n', 3),
(51, 26, 'Quando inizio a vedere risultati?\n	', 'Dopo 2- 3 mesi dall’inizio del trattamento.\n', 1),
(52, 26, '¿Cuándo se comienzan a ver los resultados?\n	', 'Después de la 2-3 sesión de tratamiento.\n', 3),
(53, 27, 'Di quante sedute ho bisogno?\n	', 'Consiglio di fare 6 sedute, per quanto riguarda il viso, 1 volta al mese. Per altre aree specifiche del corpo si devono effettuare alcune sedute in più.\n', 1),
(54, 27, '¿Cuántas seiones se deben realizar?\n	', 'Lo aconsejable son 6 sesiones, en el rostro, a distancia de 1 mes cada una. Para otras àreas especìficas del cuerpo se debe adicionar algunas sesiones màs.\n', 3),
(55, 28, 'Il trattamento è doloroso?\n', 'Come in ogni procedura iniettiva si può sentire fastidio nella zona a trattare.\n', 1),
(56, 28, '¿El tratamiento es doloroso?\n', 'El tratamiento no es doloroso, podría causar ligero fastidio porque es un procedimiento inyectivo.\n', 3),
(57, 29, 'Ho bisogno di anestesia?\n', 'Non c’è bisogno di anestesia topica perché quasi tutti i prodotti per filler contengono un anestetico all’interno. Solo in zone particolarmente sensibili come le labbra si applica anestesia. \n', 1),
(58, 29, '¿Se necesita usar un anestésico?\n', 'No hay necesidad de usar un anestésico. Casi todos los productos para filler contienen anestésico en su composisción. Yo particularmente aplico anestesia tópica en la zona de los labios, ya que es muy sensible. \n', 3),
(59, 30, 'Quanto tempo dura il risultato?\n\n', 'Dipende dalla zona, il risultato può durare da 4 a 8 mesi.\n', 1),
(60, 30, '¿Cuánto tiempo dura el resultado?\n', 'Depende de la zona, el resultato puede durar entre 4 a 8 meses.\n', 3),
(61, 31, 'Quanto tempo ci vuole per realizzare il trattamento?\n', 'In genere mezz’ora, ma dipende sempre dalla zona da trattare.\n', 1),
(62, 31, '¿Cuánto tiempo se necesita para realizar el tratamiento?\n', 'Generalmente 30-40 minutos. Depende siempre del área a tratar.\n', 3),
(63, 32, 'Cosa può accadere dopo il trattamento?\n', 'Lividi, gonfiore, arrossamento. Il gonfiore si presenta perché l’acido ialuronico richiama acqua nella zona trattata, questo effetto si risolve in poco tempo ponendo in luce l’ottimo risultato degli effetti del filler.\n', 1),
(64, 32, '¿Qué sucede después del tratamiento?\n', 'Se podrían presentar hematomas, inflamación y enrojecimiento. El edema se presenta ya que el ácido hialurónico hace un llamado de agua en la zona tratada. Pocas horas después de este hallazgo, se evidenciaran los óptimos resultados del tratamiento.  \n', 3),
(65, 33, 'Dopo il filler posso continuare con le mie attività della giornata?\n', 'Assolutamente sì! Puoi continuare le tue attività normalmente, dopo il trattamento sei presentabile per una vita socievole.\n', 1),
(66, 33, '¿Después del filler puedo continuar con las actividades del día?\n', 'Sí, puedes continuar con las actividades planificadas, eres socialmente activa. Teniendo encuenta que saunas y piscina las debes suspender por ese día. \n', 3),
(67, 34, 'Posso andare al mare subito dopo?\n', 'Sarebbe meglio esporsi dopo un giorno del trattamento, a meno che si presentino dei lividi, in quel caso è meglio evitare il sole e comunque proteggere la zona interessata dal livido, perché il sole può provocare una macchia.', 1),
(68, 34, '¿Puedo ir a la playa inmediatamente después?\n', 'Si, aunque es mejor esperar a un día después del trattamiento, a menos que se evidencien hematomas, en este caso es mejor evitar exponerse al sol. Este puede provocar en el lugar del hematoma  una mancha.', 3),
(69, 35, 'Il botulino offre risultati definitivi?\n	', 'I risultati non sono definitivi, è necessario sottoporsi a periodici trattamenti per ottenere un risultato prolungato, almeno una volta all’anno.\n', 1),
(70, 35, '¿Este tratamiento me da resultados definitivos?\n	', 'Los resultados no son definitivos, es necesario repetir el tratamiento regularmente para prolungar los resultados. Es un tratamiento que va realizado al menos una vez al año.	\n', 3),
(71, 36, 'Quanto dura la seduta?\n	\n', 'Ha una durata di 40 - 60 minuti approssimativamente. \n', 1),
(72, 36, '¿Cuànto tiempo dura la sesiòn?\n	', 'Unos 40-60 minutos aproximadamente.\n', 3),
(73, 37, 'Ha effetti avversi?\n	', 'Non comporta nessun effetto avverso.\n', 1),
(74, 37, '¿Se pueden presentar efectos secundarios?\n	', 'No comporta efectos secundarios.\n', 3),
(75, 38, 'In cosa consiste?\n	', 'Elimina l’eccesso di cellule morte superficiali dello strato corneo favorendo la protezione della pelle. Rende la pelle più pulita e ossigenata. Promuove il rinnovamento cellulare e la produzione di nuovo collagene ed elastina da parte dei fibroblasti.\n', 1),
(76, 38, '¿Qué me proporciona el tratamiento?\n	', 'Elimina el exeso de células muertas superficiales del estrato corneo favoreciendo la protección de la piel. Concede una piel más limpia y oxigenada. Promueve la renovación celular y la producción de colageno nuevo y de elastina por parte de los fibroblastos. \n', 3),
(77, 39, 'Si può fare in qualsiasi stagione dell’anno?\n	', 'Ci sono peeling fotosensibilizzanti che si effettuano solo durante l’inverno e ci sono peeling che si possono fare anche in periodo estivo.\n', 1),
(78, 39, '¿Se puede hacer en cualquier período del año?\n	', 'Existen peeling fotosensibilizantes que se efectuan solo durante el invierno o con indicaciones muy precisas a realizar después del tratamiento. Durante el verano se pueden realizar peeling más ligeros sin la preocupación de las consecuencias del sol.\n', 3),
(79, 40, 'Devo fare qualcosa prima di un peeling?\n	', 'Durante il periodo invernale consiglio di applicare per 2 settimane prima del peeling una crema a base di alfa-idrossi-acido, che favorisce i benefici di questo trattamento. Invece durante il periodo estivo non è necessario fare qualcosa prima della procedura.\n', 1),
(80, 40, '¿Debo hacer una actividad preparatoria para realizar este tratamiento?\n	', 'Durante el período invernal aconsejo de aplicar durantes 2 semanas antes del peeling una crema a base di alfa-idroxi-ácido, que favorece los beneficios de este tratamiento y evita consecuencias como las quemaduras.\n', 3),
(81, 41, 'Quanto dura una seduta?\n	\n', 'In genere 30 minuti.\n', 1),
(82, 41, '¿Cuánto tiempo dura una sesión?\n	\n', 'Genaralmente 30 minutos.\n', 3),
(83, 42, 'Ci sono controindicazioni?\n	', 'In generale è sconsigliato se sono presenti nell’area di trattamento irritazioni cutanee evidenti o patologie particolari della pelle.\n', 1),
(84, 42, '¿Hay alguna contraindicación?\n	', 'No se debe realizar el tratamientos en las áreas de irritación o donde se presente una lesión.\n', 3),
(85, 43, 'Quanti peeling posso fare?\n	', 'Dipende dal tipo di peeling che si effettua e da quello che si vuole ottenere. Se ne può fare solo uno ogni tanto per dare più luce al volto, oppure cicli di 3-5 peeling ad intervalli che vanno da 15 giorni ad un mese per lavorare sull’uniformità del colorito, su macchie e rughe.\n', 1),
(86, 43, '¿Cuántos peeling puedo hacerme?\n	', 'Depende del tipo de peeling que se efectua y de lo que se quiere obtener. Se puede realizar un peeling de vez en cuando para dar luz a la piel o ciclos de 3 -5 peeling con intervalos de 15 a 30 días cuando lo que se quiere trabajar es la uniformidad del color, las discromias y las arrugas.\n', 3),
(87, 44, 'Il trattamento è doloroso?\n	\n', 'Non è doloroso, ma si deve considerare che come tutti i trattamenti iniettivi potrebbe essere un po’ fastidioso.\n', 1),
(88, 44, '¿El tratamiento es doloroso?\n	\n', 'El tratamiento no es doloroso, podría causar ligero fastidio porque es un procedimiento inyectivo.\n', 3),
(89, 45, 'Quanto dura la seduta?\n	', 'In genere 30 minuti.\n', 1),
(90, 45, '¿Cuánto tiempo dura la sesión?\n	', 'Genralmente de unos 30 minutos.\n', 3),
(91, 46, 'Basta una seduta per vedere i risultati?\n	', 'Per ottenere benefici da questo trattamento si devono implementare dei cicli che possono variare per numero di sedute tra 4 e 10 una volta a settimana a seconda della zona da trattare. \n', 1),
(92, 46, '¿Con una sesión basta para ver los resultados?\n	', 'Para obtener los beneficios de esta técnica se deben implementar ciclos inyectivos que pueden variar en número de 4 a 10 sesiones una vez a la semana según sea la zona a tratar.\n', 3),
(93, 47, 'Cosa succede dopo il trattamento?\n	', 'Si può presentare gonfiore e lividi. Con alcune tecniche è necessario creare dei pomfi che durano un paio di giorni.\n', 1),
(94, 47, '¿Qué sucede después del tratamiento?\n	', 'Se podrían presentar inflamación y hematomas. En algunas técnicas es necesario crear pequeños abultamientos que duran pocos días.\n', 3),
(95, 48, 'Si può fare in qualsiasi momento?\n	\n', 'Si! Non c’è un periodo specifico dell’anno per fare questo trattamento. Solo si deve fare attenzione se si formano i lividi di non prendere sole direttamente sulla zona trattata.\n', 1),
(96, 48, '¿El tratamiento se puede realizar en cualquier momento?\n	', '¡Sí! En cualquier período del año este tratamiento puede ser realizado. Si se forman hematómas, prevenir la exposición solar directamente en la zona.\n', 3),
(97, 49, 'Si deve fare un mantenimento?\n	\n', 'Più che un mantenimento, si realizzano sedute di richiamo, che permettono di mantenere i risultati.\n', 1),
(98, 49, '¿Debo realizar algún tipo de mantenimiento después que termino el ciclo?\n\n', '¡Sì! El mantenimiento consiste en repetir el ciclo.\n', 3),
(99, 50, 'Quanti cicli si devono fare in un anno?\n	', 'Da 2 a 3 cicli all''anno, dipende dalle tue esigenze.\n', 1),
(100, 50, '¿Cuántos ciclos se realizan al año?\n	\n', 'Entre 2 a 3 ciclos al año, dependiendo de tus exigencias.\n', 3),
(101, 51, 'Il trattamento è doloroso?\n	', 'Non è doloroso, la procedura è fatta attraverso una cannula, per cui il trattamento risulta poco invasivo, facile e veloce da eseguire.\n', 1),
(102, 51, '¿El tratamiento es doloroso?\n	', 'No es doloroso, es un pocedimiento fácil y rápido de realizar.\n', 3),
(103, 52, 'Aumenta la dimensione del naso?\n	', 'Effettivamente sì, perché si sta utilizzando un prodotto per riempire spazi vuoti che però all’effetto ottico crea più armonia con la linearità del naso.\n', 1),
(104, 52, '¿La nariz aumenta de dimensión?\n	', 'Efectivamente sí, porque el ácido hialurónico se utiliza para rellenar los espacios vacios, pero al efecto óptico crea armonía y un mejor perfil.\n', 3),
(105, 53, 'Quanto dura il trattamento?\n	', 'È un trattamento abbastanza veloce e semplice da fare. È possibile realizzarlo in 30 – 45 minuti.\n', 1),
(106, 53, '¿Cuánto tiempo dura la sesión?\n	\n', 'Es posible realizar el tratamiento en unos 30 a 45 minutos.	\n', 3),
(107, 54, 'Quanto dura il risultato?\n	', 'Può durare anche un anno o un po’ di più. La velocità d''assorbimento del prodotto è variabile di una persona ad altra.\n', 1),
(108, 54, '¿Cuánto tiempo dura el resultado?\n	\n', 'Puede durar un año/año y medio. La velocidad de absorción del producto es variable de una persona a otra.\n', 3),
(109, 55, 'Dopo il trattamento si deve fare qualcosa?\n	', 'Non usare occhiali per 48 ore e non toccare la zona trattata.\n', 1),
(110, 55, '¿Después del tratamiento es necesario hacer algo?\n	', 'No usar anteojos o lentes de sol por 48 horas y no tocar la zona tratada.\n', 3),
(111, 56, 'Il trattamento è doloroso?\n	', 'Il trattamento si basa sull’iniezione di un gas e questo causa una dolenza che dipende dalla sensibilità individuale. Tante persone lo tollerano molto bene.\n', 1),
(112, 56, '¿El tratamiento es doloroso?\n	', 'El tratamiento se basa en la inyección de un gas y esto puede causar un ligero malestar dependiendo de la sensibilidad de cada quien.\n', 3),
(113, 57, 'I benefici si vedono in una sola seduta?\n	', 'L’unico beneficio che si può ottenere in una sola seduta è quello di sentire le gambe più leggere e sgonfie, se non si fanno altre sedute questo effetto non si mantiene. \n', 1),
(114, 57, '¿Los beneficios se pueden ver desde la primera sesión?\n	', 'El único beneficio que se puede apreciar es “sentir” las piernas mucho más ligeras y menos dolorosas, debido a la reducción de la inflamación. Se debe continuar a realizar las sesiones para poder mantener este efecto. \n', 3),
(115, 58, 'Quante sedute devo fare per avere risultati estetici?\n	', 'Questo trattamento si fa in cicli di 10 sedute una volta a settimana e per iniziare a vedere benefici estetici bisogna di arrivare alla 4° - 5°. Poi occorre fare delle sedute di mantenimento una volta al mese.\n', 1),
(116, 58, '¿Cuántas sesiones se deben realizar para obtener resultados estéticos?\n	', 'Este tratamiento consiste en un ciclo de 10 sesiones a realizar una vez a la semana. Los beneficios estéticos se empiezan a notar después de la 4° - 5° sesión. Terminado el ciclo se realiza el mantenimiento una vez al mes.\n', 3),
(117, 59, 'Quali zone si possono trattare?\n	\n', 'Viso, collo, décolleté, gambe, addome e qualsiasi altra parte del corpo.\n', 1),
(118, 59, '¿Qué zonas se pueden tratar?\n	', 'Rostro, cuello, escote, piernas, abdomen y cualquier otra parte parte del cuerpo.\n', 3),
(119, 60, 'Ci sono dei rischi?\n	\n', 'No. Tuttavia il rossore localizzato e piccoli ematomi nella zona trattata sono comuni, ma si riassorbono velocemente.\n', 1),
(120, 60, 'Se pueden presentar riesgos?\n	.\n', 'No. Aunque si se puede presentar enrojecimiento localizado y pequeños hematomas en la zona tratada.\n', 3),
(121, 61, 'Può essere associata ad altri trattamenti?\n	\n', 'Si, la carbossiterapia funziona molto bene come potenziatore dei benefici di altri trattamenti di medicina estetica.\n', 1),
(122, 61, '¿Puede ser asociada a otros tipos de tratamientos?\n	', 'Sí, la carboxiterapia funziona muy bien como potenciador de los beneficios de otros tratamientos de la medicina estética como por ejemplo la mesoterapia. \n', 3),
(123, 62, 'Ho le gambe gonfie, appesantite e mi fano male, posso fare questo trattamento?\n	\n', 'Assolutamente sì, dalla prima seduta troverai molto sollievo, avrai la sensazione di camminare fra le nuvole.\n', 1),
(124, 62, 'Las pierna y/o tobillos se me inflaman y me duelen, ¿este trattamiento me ayudará?\n	\n', 'Sí, es una de las indicaciones principales de este tratamiento, desde la primera sesión tendrás la sensación de caminar entre las nubes.\n', 3),
(125, 63, 'Esiste qualche controindicazione? \n	', 'Sì. Persone con insufficienza epatica, respiratoria, renale o cardiaca, diabete, cancro, disturbi della circolazione, epilessia, asma attivo e problemi o infezioni nella zona da trattare potrebbero non essere qualificati per ricevere questo trattamento. Le donne in gravidanza o allattamento non possono applicare questo trattamento.\n', 1),
(126, 63, '¿El tratamiento esta contraindicado en algun caso?\n	', 'Sí. Personas con insuficiencia hepática (hígado), respiratoria, renal (riñón) y cardiaca, diabetes, cáncer, trastornos de la circulación, epilepsia, otros problemas de corazón, hígado y riñón, asma activa y problemas o infecciones en el área a ser tratada podrían no estar calificados para recibir este tratamiento. Las mujeres embarazadas o en periodo de lactancia tampoco podrán aplicarse este tratamiento.\n', 3),
(127, 64, 'Come si svolge il trattamento?\n	', 'In alcuni casi, potrebbe essere preferibile l''applicazione di anestesia topica sulla zona prima di realizzare il trattamento. Consiste nell’effettuare piccoli spot a poca distanza tra di loro, esposti direttamente sulla superficie cutanea. Questo permette di accorciare la cute in eccesso e conservarne la perfetta plasticità. Il numero di sedute è variabile a seconda di ciò che si vuole trattare e viene stabilito dopo una accurata analisi.\n', 1),
(128, 64, '¿Cómo se realiza el tratamiento?\n	 ', 'Consiste en efectuar pequeños spot a poca distancia entre ellos sobre la superficie cutánea. Esto permite acortar la piel en ecceso y conservar su perfecta plasticidad. El número de seiones es variable, depende de lo que se quiere tratar y se establece después de un análisis completo.\n', 3),
(129, 65, 'Cosa accade dopo il trattamento?', '<ul>\n<li>Sulla zona trattata sarà visibile un sottile strato più scuro prodotto della sublimazione del tessuto cutaneo.</li>\n<li>Nelle ore successive ad ogni seduta potrebbe presentarsi edema localizzato che si riassorbirà in qualche giorno. </li>\n<li>A distanza di pochi giorni si formerà una crosticina da non rimuovere, essa cadrà entro 7-10 giorni. </li>\n<li>Si deve pulire con cura e con una detersione molto delicata. </li>\n<li>Si deve evitare il più possibile il contatto diretto e costante con i raggi del sole e proteggere con cura l’area trattata.</li> \n<li>Dopo la caduta delle crosticine l’area trattata si presenterà di colore rosaceo e piano piano riacquisterà la sua normale colorazione.</li>\n</ul>', 1),
(130, 65, '¿Qué sucede después del tratamiento?', '<ul align="center"">\n<li>En la zona tratada será visible un estrato fino y oscuro producto de la sublimación del tejido cutáneo.</li>\n<li>En las horas sucesivas al tratamiento se presentará edema localizado que tomará un par de días en reabsorberse. </li>\n<li>Se formará una crosta que no debe ser removida, esta caerá sola en unos 7 a 10 días.  </li>\n<li>Se debe limpiar con mucha atención y muy delicadamente. </li>\n<li>Se debe evitar el contacto directo y constante con los rayos del sol y se debe proteger la zona. </li>\n<li>Al caer la crosta la piel se presenta enrojecida, poco a poco adquistará el color normal de la piel.</li>\n</ul>', 3),
(131, 66, 'I risultati durano nel tempo?\n', 'Il risultato è definitivo e duraturo. Tuttavia i tempi di permanenza dei risultati dipendono molto dall’individuo, tipologia di pelle e dallo stile di vita. Con il passare del tempo, per il naturale processo d’invecchiamento, la pelle potrebbe nuovamente presentare inestetismi e cedimenti, quindi è necessario consigliare trattamenti preventivi.\n', 1),
(132, 66, '¿Cuánto tiempo dura el resultado?\n', 'Es definitivo. \n<br></br>\nEn caso de la blefaroplastica, se tiene que tomar en cuenta que con el pasar del tiempo, por el proceso natural de envejecimiento la piel nuevamente cederá. Se aconsejan siempre tratamientos preventivos.\n', 3),
(133, 67, 'Si può fare in qualsiasi momento?\n\n', 'Assolutamente no, questo è un trattamento che viene realizzato solo durante il periodo invernale.\n', 1),
(134, 67, '¿Se puede realizar en cualquier periodo del año?\n	', 'Es mejor realizarlo en periodo invernal y con una adecuada protección.\n', 3),
(135, 68, 'Quali sostanze si usano?\n	', 'Le sostanze che possono essere iniettate sono: acido ialuronico a bassa densità, aminoacidi, dei glicosaminoglicani, vitamine, PDRN (polidesossiribonucleotide), ecc.\n', 1),
(136, 68, '¿Qué sustancias se usan?\n	', 'Las sustancias que pueden ser inyectadas son: ácido hialurónico a baja densidad, aminoácidos,  glicosaminoglicanos, vitaminas, PDRN (polidexosiribonucleotidos), etc.\n', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'customers', 'Customer of ecommerce shop');

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue`
--

CREATE TABLE IF NOT EXISTS `lingue` (
  `id_lingue` int(11) NOT NULL,
  `nome_lingue` varchar(50) NOT NULL,
  `abbr_lingue` varchar(10) NOT NULL,
  `locale_paypal_lingue` varchar(10) NOT NULL,
  `codice_ci` varchar(150) NOT NULL,
  `stato_lingua` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue`
--

INSERT INTO `lingue` (`id_lingue`, `nome_lingue`, `abbr_lingue`, `locale_paypal_lingue`, `codice_ci`, `stato_lingua`) VALUES
(1, 'ITALIANO', 'IT', 'it_IT', 'italian', 1),
(2, 'ENGLISH', 'EN', 'en_GB', 'english', 1),
(3, 'ESPANOL', 'ES', 'es_ES', 'spanish', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine`
--

CREATE TABLE IF NOT EXISTS `pagine` (
  `id_pagina` int(11) NOT NULL,
  `nome_pagina` varchar(50) NOT NULL,
  `url_pagina` varchar(50) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `controller` varchar(250) NOT NULL,
  `tipo_pagina` varchar(25) NOT NULL,
  `trad_code` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine`
--

INSERT INTO `pagine` (`id_pagina`, `nome_pagina`, `url_pagina`, `id_lingua`, `controller`, `tipo_pagina`, `trad_code`) VALUES
(1, 'default page', 'default_page', 1, 'frontend/Home', 'statica', ''),
(2, 'Home', 'it/home', 1, 'frontend/Home', 'statica', ''),
(3, 'Su di me', 'it/sudime', 1, 'frontend/Home/sudime', 'statica', ''),
(4, 'Mini biografia', 'it/minibiografia', 1, 'frontend/Home/minibiografia', 'statica', ''),
(5, 'Il mio lavoro', 'it/ilmiolavoro', 1, 'frontend/Home/ilmiolavoro', 'statica', ''),
(6, 'Privacy', 'it/privacy', 1, 'frontend/Home/privacy', 'statica', ''),
(7, 'Contattami', 'it/contattami', 1, 'frontend/Home/contattami', 'statica', ''),
(8, 'Studi medici', 'it/studimedici', 1, 'frontend/Home/studimedici', 'statica', ''),
(9, 'Programmi', 'it/programmi/(:any)/(:any)', 1, 'frontend/Programmi/detail/$1/$2', 'dinamica', 'programmi'),
(10, 'Trattamenti', 'it/trattamenti/(:any)/(:any)', 1, 'frontend/Trattamenti/detail/$1/$2', 'dinamica', 'trattamenti'),
(11, 'Home', 'en/home', 2, 'frontend/Home', 'statica', ''),
(12, 'About me', 'en/aboutme', 2, 'frontend/Home/sudime', 'statica', ''),
(13, 'Mini biography', 'en/minibiography', 2, 'frontend/Home/minibiografia', 'statica', ''),
(14, 'My work', 'en/mywork', 2, 'frontend/Home/ilmiolavoro', 'statica', ''),
(15, 'Privacy', 'en/privacy', 2, 'frontend/Home/privacy', 'statica', ''),
(16, 'Contact me', 'en/contactme', 2, 'frontend/Home/contattami', 'statica', ''),
(17, 'Medical offices', 'en/medicaloffices', 2, 'frontend/Home/studimedici', 'statica', ''),
(18, 'Programs', 'en/programs/(:any)/(:any)', 2, 'frontend/Programmi/detail/$1/$2', 'dinamica', 'programmi'),
(19, 'Treatments', 'en/treatments/(:any)/(:any)', 2, 'frontend/Trattamenti/detail/$1/$2', 'dinamica', 'trattamenti'),
(20, 'Inicio', 'es/inicio', 3, 'frontend/Home', 'statica', ''),
(21, 'Sobre mi', 'es/sobremi', 3, 'frontend/Home/sudime', 'statica', ''),
(22, 'Mini biografía', 'es/minibiografía', 3, 'frontend/Home/minibiografia', 'statica', ''),
(23, 'Mi trabajo', 'es/mitrabajo', 3, 'frontend/Home/ilmiolavoro', 'statica', ''),
(24, 'Politica de privacidad', 'es/politicadeprivacidad', 3, 'frontend/Home/privacy', 'statica', ''),
(25, 'Contactame', 'es/contactame', 3, 'frontend/Home/contattami', 'statica', ''),
(26, 'Consultorio Médico', 'es/consultoriomedico', 3, 'frontend/Home/studimedici', 'statica', ''),
(27, 'Programas', 'es/programas/(:any)/(:any)', 3, 'frontend/Programmi/detail/$1/$2', 'dinamica', 'programmi'),
(28, 'Tratamientos', 'es/tratamientos/(:any)/(:any)', 3, 'frontend/Trattamenti/detail/$1/$2', 'dinamica', 'trattamenti');

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine_contenuti`
--

CREATE TABLE IF NOT EXISTS `pagine_contenuti` (
  `id_pc` int(11) NOT NULL,
  `pc_page_code` varchar(150) NOT NULL,
  `pc_titolo_1` varchar(250) NOT NULL,
  `pc_immagine_1` varchar(250) NOT NULL,
  `pc_testo_body_1` longtext NOT NULL,
  `pc_titolo_2` varchar(250) NOT NULL,
  `pc_immagine_2` varchar(250) NOT NULL,
  `pc_testo_body_2` longtext NOT NULL,
  `pc_testo_bold_1` text NOT NULL,
  `pc_testo_bold_2` text NOT NULL,
  `pc_testo_bold_3` text NOT NULL,
  `pc_top_image` varchar(250) NOT NULL,
  `pc_top_image_mobile` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine_contenuti`
--

INSERT INTO `pagine_contenuti` (`id_pc`, `pc_page_code`, `pc_titolo_1`, `pc_immagine_1`, `pc_testo_body_1`, `pc_titolo_2`, `pc_immagine_2`, `pc_testo_body_2`, `pc_testo_bold_1`, `pc_testo_bold_2`, `pc_testo_bold_3`, `pc_top_image`, `pc_top_image_mobile`, `id_lingua`) VALUES
(1, 'HOME', 'Home', 'tiracconto3.jpg', 'Ti racconto...<br><br>Una gran parte delle tue paure riguardo la medicina estetica, vengono alimentate dalla società stessa, dove ti fanno vedere orrori o ti fanno credere che la bellezza fisica non conta.<br><br>\n    Per me conta (e siamo sinceri, perché vivi in un mondo che è materia), ma conta quando hai in <span class="blu-text"><b>equilibrio la bellezza esterna e quella interiore.</b></span><br>\n    Forse non conosci la Medicina Estetica e se la “conosci” (e questo non significa che sai veramente di cosa si tratta), hai paura perché pensi che ti gonfierà, ti trasformerà o diventerai un’altra.<br><br>\n    Io <span class="orange-text"><b>ti offro armonia ed equilibrio</b></span>, rimarrai chi sei fisicamente ma ti vedrai e ti vedranno <span class="blu-text"><b>meglio, riposata e fantastica</b></span> e se me lo permetti ti guiderò verso la tua rinnovazione interna.\n    <br><span class="orange-text"><b>Rompi le credenze limitanti</b></span>, per quello che riguarda la bellezza esteriore, <span class="blu-text"><b>non avere vergogna</b></span> di sentirti bene dentro della tua propria pelle e cercare di sentirti più bella.<br><br>\n    <span class="orange-text"><b>Fai quello che senti</b></span> di fare, <span class="blu-text"><b>ma sentilo con il cuore</b></span> e vivi con piacere <span class="orange-text"><b>l''armonia esterna e l''equilibrio interno</b></span> che potresti raggiungere se veramente lo vuoi.<br><br><span class="blu-text text-24"><b>Tutto dipende da te!</b></span>', '', '', '', 'Ciao, sono Orlena Zotti, medico estetico, creatrice di <span class="orange-text"><b>"Sei Essenzialmente Bella"</b></span>.<br>Aiuto le donne a trovare la stabilità e rafforzare l''amor proprio attraverso l''armonia fisica.<br>Amo fornire strumenti alle donne che vogliono un cambiamento e cercano di connettersi con l''essenza della bellezza.', '<span class="text-24"><b>Cosa è la medicina estetica?</b></span><br>\nLa definirei come tutto quello che è alternativo alla chirurgia plastica per armonizzare la bellezza fisica, tramite trattamenti realizzati con procedure poco o per niente invasive.', '“La Medicina Estetica è un viaggio, un percorso, che si realizza passo a passo.<br>\nL’obbiettivo è farti sentire più bella facendo credere agli altri che ti sei semplicemente riposata.”', '1950_500.png', '580_700.png', 1),
(2, 'HOME', 'Home', 'tiracconto3.jpg', 'I tell you ...<br><br>A lot of your fears about aesthetic medicine are fueled by the company itself, where they show you horrors or make you believe that physical beauty does not matter.<br/><br/>\r\n    For me it counts (and we are honest, because you live in a world that is matter), but it matters when you are in the <span class="blu-text"><b>balance between outer and inner beauty.</b></span><br>\r\n    Maybe you do not know the Aesthetic Medicine and if you "know" (and this does not mean you really know what it is), you''re afraid because you think it will swell, turn you or you will become another.<br><br>\r\n    <span class="orange-text"><b>I offer you harmony and balance</b></span>, you will be who you are physically but you will see and will see you <span class="blu-text"> better, rested and fantastic</b></span> and if you let me, I will guide you to your inner renewal.\r\n    <br><b> Break the Limiting Beliefs </b> </ span>, as far as the outside beauty is concerned, <span class = "orange-text"> <b> do not have shame </b></span> to feel good inside your own skin and try to feel more beautiful. <br> <br>\r\n    <span class="orange-text"><b>Do what you feel</b></span> do <span class="blu-text"><b> but feel it with your heart </b></span> and you will enjoy <span class="orange-text"><b>external harmony and interior renewal</b></span> that you could achieve if you really want it.<br><br><span class="blu-text text-24"><b>Everything depends on you!</b></span>', '', '', '', 'Hello, I''m Orlena Zotti, aesthetic, creator of <span class="orange-text"><b>"Sei essenzialmente bella"</b></span>.<br/>Helping women find stability and strengthen love through physical harmony. <br> I love to provide tools to women who want a change and seek to connect with the essence of beauty.', '<span class="text-24"><b>What is aesthetic medicine?</b></span><br>\r\nI would define it as all that is alternative to plastic surgery to harmonize physical beauty, through treatments carried out with little or no invasive procedures.', '“Aesthetic Medicine is a journey, a journey that takes place step by step.<br/>\r\nThe goal is to make you feel more beautiful by believing others that you simply rested.”', '1950_500.png', '580_700.png', 2),
(3, 'HOME', 'Home', 'tiracconto3.jpg', 'Te cuento...<br><br>Una gran parte de tus miedos a la medicina estética se alimenta de la sociedad en la que vives, que te hace ver los resultados más inapropiados provocados por quien no tiene la capacidad y/o preparación en esta disciplina o quien te hace creer que la belleza física no cuenta.<br><br>\n    Para mí cuenta (y seamos sinceras, vives en un mundo que es materia), pero cuenta cuando se tiene en <span class="blu-text"><b>equilibrio la belleza externa y la belleza interna.</b></span><br>\n    Quizás no conoces la Medicina Estética y si la “conoces” (esto no significa que sabes verdaderamente de que se trata), sientes miedo porque piensas que te inflamarás, te transformarás o te convertirás en otra.<br><br>\n    Yo <span class="orange-text"><b>te ofrezco armonía y equilibrio</b></span>, serás quien eres fisicamente, pero te verás y te verán <span class="blu-text"><b>mejor, reposada, fresca y fantástica </b></span> y si me lo permites te guiaré a tu renacer interior.\n    <br><span class="orange-text"><b>Rompe las creencias limitantes</b></span>, sobre la bellezza externa,<span class="blu-text"><b>no te avergüences </b></span> de sentirte bien dentro de tu propia piel y de buscar ayuda para sentirte más bonita.<br><br>\n    <span class="orange-text"><b>Haz aquello que sientes</b></span> que es mejor para tí, <span class="blu-text"><b>pero siéntelo con el corazón</b></span> y vive con placer la <span class="orange-text"><b>armonía externa y equilibrio interno</b></span> que podrías lograr si así lo quieres.<br><br><span class="blu-text text-24"><b>¡Todo depende de tí!</b></span>', '', '', '', 'Hola, soy Orlena Zotti, medico estético, mi negocio se llama <span class="orange-text"><b>"Sei Essenzialmente Bella"</b></span>.<br>Yo ayudo a las mujeres a encontrar su estabilidad y reforzar su amor propio a través de la armonía física. Amo ofrecer herramientas a las mujeres que quieren un cambio y buscan conectar con la esencia de la belleza.', '<span class="text-24"><b>¿Qué es la medicina estética para mi?</b></span><br>\nLa definiría como un conjunto de tratamientos médicos, realizados con procedimientos poco o para nada invasivos, que son alternativos a la cirugía plástica, logrando así armonizar la belleza física.', '“La Medicina Estética es un viaje, un recorrido, que se realiza paso a paso.\nEl objetivo es hacerte sentir más bella haciéndole creer a los demás que simplemente te has reposado”', '1950_500.png', '580_700.png', 3),
(4, 'SUDIME', 'Su di me', 'sudime.jpg', '<p align="justify">Qui mi ha portato la solitudine, il sentirmi come un pesce fuor d''acqua, la sensazione di non appartenere al luogo in cui mi trovavo né alla maniera di fare la medicina estetica come la facevo. Ho pensato che cambiare ambiente poteva far migliorare la situazione, ma questo non è successo.\r\n                        </p>\r\n                        <p align="justify">\r\n                            Allora ho iniziato la mia ricerca di tutto ciò che mi avrebbe aiutato a capire cosa volevo veramente, cosa mi rendeva felice e, soprattutto, cosa sono venuta a fare in questo mondo.\r\n                        </p>\r\n                        <p align="justify">\r\n                        	Prima di tutto, ho deciso di ampliare le mie competenze come medico estetico ed estendere la conoscenza ad altri orizzonti del benessere globale, così ho intrapreso la lettura di molti libri sull’evoluzione spirituale, ho scoperto il mondo della meditazione, ho frequentato diversi corsi di crescita personale e su come nutrire il mio corpo e la mia anima. Tutto questo si è aggiunto alla mia passione, la Medicina Estetica.\r\n                        </p>\r\n                        <p align="justify">\r\n                        	Così mi è sorto il desiderio di applicare alla medicina estetica questo viaggio interiore e ho cominciato ad unificare tutti gli strumenti che ho sperimentato in programmi specifici. Questo mi ha permesso di creare un modo per offrire aiuto alle donne, armonizzando la bellezza esteriore ed equilibrando la bellezza interiore, accorpando due mondi che la società ha venduto come diversi, ma che per me sono inseparabili perché entrambi si nutrono equilibratamente.\r\n                        </p>', 'Mini biografia', '', '<p>\r\n                       Ho conseguito la laurea di Medico Chirurgo in Venezuela (il mio paese di nascita) nel 2005, subito dopo ho conosciuto il mondo della medicina estetica e me ne sono innamorata, tanto che ne ho fatto la mia unica professione.\r\n                    </p>\r\n                    <p>\r\n                        Sono arrivata in Italia nel 2010 e ho seguito la procedura per il riconoscimento della mia laurea in questo paese, che porto nel mio sangue grazie a mio nonno.\r\n                    </p>\r\n                    <p>\r\n                        Per perfezionare le mie conoscenze in questo campo ho seguito un percorso quadriennale di studi conseguendo il Diploma di Medicina Estetica della Fondazione Internazionale Fatebenefratelli a Roma.\r\n                    </p>\r\n                    <p>\r\n                        Ora mi trovo in Sardegna, dove, grazie al mio patrimonio di conoscenze, svolgo il mio lavoro offrendo un concetto di medicina estetica unico ed equilibrato. \r\n                    </p>', '', '', '', '', '', 1),
(5, 'SUDIME', 'About me', 'sudime.jpg', '<br/><p align="justify">Here loneliness has been brought to me, feeling like a fish out of water, feeling not to belong to the place where I was or how to do aesthetic medicine as I did. I thought that changing the environment could improve the situation, but that did not happen.\r\n                        </p>\r\n						<br/>\r\n                        <p align="justify">\r\n                            Then I started my search for everything that would help me to understand what I really wanted, what made me happy and, above all, what I came to do in this world.\r\n                        </p>\r\n						<br/>\r\n                        <p align="justify">\r\n                        	First of all, I decided to expand my skills as an aesthetic doctor and extend knowledge to other horizons of global well-being, so I started reading many books about spiritual evolution, I discovered the world of meditation, I attended several courses of personal growth and how to nourish my body and soul. All this has been added to my passion, Aesthetic Medicine.\r\n                        </p>\r\n						<br/>\r\n                        <p align="justify">\r\n                        	So I had the desire to apply this inner journey to aesthetic medicine and I began to unify all the tools I experienced in specific programs. This has allowed me to create a way of offering women''s help, harmonizing the exterior beauty and balancing the inner beauty, by merging two worlds that the society has sold as different, but which for me are inseparable because they both nourish equilibrally.\r\n                        </p>', 'Mini Biografy', '', '<p>\r\n                       I graduated from the Medical Surgeon in Venezuela (my birth country) in 2005, soon after I met the world of aesthetic medicine and I fell in love with him so much that I did my only job.\r\n                    </p>\r\n                    <p>\r\n                        I arrived in Italy in 2010 and followed the procedure for the recognition of my degree in this country, which I bring in my blood thanks to my grandfather.\r\n                    </p>\r\n                    <p>\r\n                        To refine my knowledge in this field I followed a four-year course of study by completing the Diploma of Aesthetic Medicine of the Fatebenefratelli International Foundation in Rome.\r\n                    </p>\r\n                    <p>\r\n                        Now I am in Sardinia, where, thanks to my knowledge of wealth, I do my job by offering a concept of unique and balanced aesthetic medicine.\r\n                    </p>', '', '', '', '', '', 2),
(6, 'SUDIME', 'Sobre mi', 'sudime.jpg', '<p align="justify"><br>Aquí me trajo la soledad, el sentirme un pez fuera del agua, el sentir que no pertenecía al lugar donde me encontraba, ni a la manera de hacer la medicina estética que hacía, pensé que cambiando de ambiente podría mejorar el como me sentía, pero esto no sucedió.\n                        </p>\n                        <p align="justify">\n                            Entonces comencé a buscar lo que fuera que me ayudara para comprender que quería de verdad, que me hacía feliz y sobre todo que vine a hacer en este mundo.\n                        </p>\n                        <p align="justify">\n                        	Inicié perfeccionando mis conocimientos como médico estético, leí muchos libros sobre evolución espiritual, conocí el mundo de la meditación, hice muchos cursos de crecimiento personal y de como nutrir mi cuerpo y todo esto se sumó a mi pasión: la medicina estética.\n                        </p>\n                        <p align="justify">\n                        	Así me invadieron las ganas de aplicar a la medicina estética este recorrido interno y fue cuando empecé a unificar en programas todas las herramientas que he experimentado.<br><br>Esto me ha permitido crear una forma de ofrecer ayuda a las mujeres, armonizando la belleza externa y equilibrando la belleza interna, acomplando dos mundos que la sociedad a vendido como diferentes pero que para mí son inseparables porque los dos se nutren equilibradamente.\n                        </p>', 'Mini biografía', '', '<p>\n                      Me gradué de Médico Cirujano en Venezuela (mi país de nacimiento) en el 2005, casi inmediatamente después, conocí el mundo de la medicina estética y me enamoré de ella, tanto que la trasformé en mi única profesión.\n                    </p>\n                    <p>\n                        Legué a Italia en el 2010 y realicé todo el procedimiento para validar mi título en este país, que llevo en mi sangre gracias a mi abuelo.\n                    </p>\n                    <p>\n                        Para perfeccionar mis conocimientos en este campo seguí un curso de cuatro años opteniendo un Diploma de Medicina Estética de la Fondazione Internazionale Fatebenefratelli en Roma.\n                    </p>\n                    <p>\n                      Ahora me encuentro en Cerdeña, donde, gracias a mi patrimonio de conocimientos, desarrollo mi trabajo ofreciendo un concepto de medicina estetica único y equilibrado.\n                    </p>\n                    <p>\n                        ', '', '', '', '', '', 3),
(7, 'MIOLAVORO', 'Il mio lavoro', 'ilmiolavoro.jpg', '<p align="justify">\r\n                           Integrare la bellezza fisica e la crescita interna è una vera e propria missione, perché in questo risveglio alla consapevolezza che stiamo avendo, "il nostro corpo è il nostro tempio", ma attualmente, in questo risveglio, questa frase serve solo se ti prendi cura del tuo corpo attraverso l’alimentazione e l''attività fisica, in questo modo ti puoi mantenere in equilibrio.\r\n                        </p>\r\n                        <p align="justify">\r\n                            Ma come si può includere la bellezza in questo momento di presa di consapevolezza?\r\nPer molti anni la bellezza fisica è stata stigmatizzata perché per il pensiero comune non importa come ti vedi (brutta o carina) o ti senti, se non chi sei e come sei. Per questo quello che io propongo è armonizzare la tua bellezza, ad esempio far diventare le tue rughe meno evidenti e far diventare il tuo volto più riposato e fresco. Non avere vergogna di volerti guardare meglio e di sentirti bene dentro la tua pelle.\r\n                        </p>\r\n                        <p align="justify">\r\n                        	Regala al tuo corpo e al tuo volto l''aria fresca che ha perso nel corso degli anni, ed è solo questo, aria fresca, non è una ricostruzione, non è magia, non è diventare qualcun altro. Non significa smettere di essere chi sei. Ti aiuterò a ritrovare la bellezza che non vedi o che pensi di aver perso. Ti aiuterò ad iniziare a vedere con occhi diversi, con occhi d’amore per te stessa.\r\n                        </p>\r\n                        <p align="justify">\r\n                        	La bellezza che per molte persone si sprigiona da dentro verso fuori per la maggior parte si riflette dall''esterno verso l''interno; perché molte volte guardare allo specchio quello che si può ottenere fa risplendere la tua luce interiore con più intensità e ti aiuta a vedere ciò che era nascosto, dal passare degli anni, dalla mancanza d’amore per te stessa,  dalla mancanza di tempo.\r\n                        </p>', '', '', '', 'Ricordati che il tuo corpo è il tuo tempio e tante volte l''attività fisica e il mangiare sano non è sufficiente, è anche importante per il tuo tempio un soffio d’aria fresca.', '', '', '', '', 1),
(8, 'MIOLAVORO', 'My work', 'ilmiolavoro.jpg', '<br/><p align="justify">\r\n                           Integrating physical beauty and internal growth is a real mission, because in this awakening to the awareness that we are having, "our body is our temple," but in this awakening, this phrase only serves if you take care of your body through nutrition and physical activity, in this way you can keep it in balance.\r\n                        </p>\r\n                        <p align="justify">\r\n                            But how can beauty be included in this awareness-raising moment?\r\nFor many years, physical beauty has been stigmatized because for common thinking it does not matter how you see (ugly or pretty) or you feel, if you are not who you are and how are you. For this, what I propose is to harmonize your beauty, for example to make your wrinkles less noticeable and make your face more rested and cool. Do not be ashamed of wanting to look better and feel good inside your skin.\r\n                        </p>\r\n                        <p align="justify">\r\n                        	Give your body and your face fresh air that has lost over the years, and that''s just this, fresh air, it''s not a reconstruction, it''s no magic, it''s not becoming someone else. It does not mean to stop being who you are. I will help you find the beauty you do not see or think you''ve lost. I will help you begin to see with different eyes, with eyes of love for yourself.\r\n                        </p>\r\n						<br/>\r\n                        <p align="justify">\r\n                        	The beauty that for many people emanates from the inside out is mostly reflected from the outside inward; because many times you look in the mirror what you can get brings your inner light shining with more intensity and helps you to see what was hidden from years of lack of love for yourself by the lack of time.\r\n                        </p>', '', '', '', 'Remember that your body is your temple and many times physical activity and healthy eating is not enough, it is also important for your temple to breathe fresh air.', '', '', '', '', 2),
(9, 'MIOLAVORO', 'Mi trabajo', 'ilmiolavoro.jpg', '<p align="justify">\n                            Es una verdadera misión  integrar belleza física y crecimiento interno,  porque en este despertar de consciencia que estamos teniendo,  "nuestro cuerpo es nuestro templo". Pero actualmente en este despertar este frase sirve solo si tú cuidas tu cuerpo desde la alimentación y la actividad física, de esta manera te mantendrás en equilibrio contigo misma.\n                        </p>\n                        <p align="justify"> \n¿Pero cómo uno puede incluir la bellezza en este tiempo de tomar consciencia?  \nPor muchos años la belleza física ha sido atacada y ha sido tomada como superficial, porque no importa el como te veas (fea o bonita) o te sientas,  si no quién eres y cómo eres. Por eso lo que yo propongo, por ejemplo, es armonizar tu belleza,  que las arrugas sean menos evidentes y que tu rostro se vea más reposado y fresco. No te avergüences por querer verte mejor y sentirte bien dentro de tu propia piel.\n                        </p>\n                        <p align="justify">\n                        	Regala a tu cuerpo y a tu rostro ese aire fresco que ha perdido con los años y solo es eso,  aire fresco,  no es una reconstrucción,  no es magia,  no es convertirte en otra persona. No es que dejes de ser quien eres. Yo te ayudo a encontrar esa belleza que no ves o esa belleza que piensas haber perdido. Te ayudo a que te empieces a ver con otros ojos,  con ojos de amor por ti misma.\n                        </p>\n                        <p align="justify">\n                        	Lo que a muchos le funciona de adentro hacia fuera a otros les funciona de manera increíble de afuera hacia adentro; porque muchas veces viendo al espejo lo que se puede obtener eso hace que la luz interna brille  con más intensidad y que empieces a ver lo que estaba escondido,  por el desgaste de los años,  por la falta de amor a tí misma,  por falta de tiempo.\n                        </p>', '', '', '', 'Recuerda que tu cuerpo es tu templo y que muchas veces la actividad física y la alimentación más saludable no basta,  también es importante para tu templo el aire fresco.', '', '', '', '', 3),
(10, 'PRIVACY', 'Privacy', '', '<p align="justify">Ai sensi del decreto legislativo 30 giugno 2003 n. 196, Sei essenzialmente bella, in qualità di Titolare autonomo del trattamento, è tenuta a fornire alcune informazioni riguardanti l''utilizzo dei dati personali da Lei forniti, ovvero altrimenti acquisiti nell''ambito della rispettiva attività.\r\nFonte dei dati personali.\r\nI dati personali cui ci si riferisce sono raccolti direttamente da questo sito web. Tutti questi dati verranno trattati nel rispetto della citata legge e degli obblighi di riservatezza cui si è sempre ispirata l''attività dello studio.</p>\r\n                    <p align="justify">\r\n                       <b>Verranno raccolti i seguenti dati:</b>\r\n                       <ul class="privacy-ul">\r\n                        <li>indirizzo IP dell''utente;</li>\r\n                        <li>indirizzo e-mail personale;</li>\r\n                        <li>indirizzo URL di provenienza;</li>\r\n                        <li>numero di telefono</li>\r\n                        <li>nome</li>\r\n                        <li>cognome</li>\r\n                       </ul>\r\n                    </p>\r\n                    <p align="left">\r\n                       <b>Finalità del trattamento cui sono destinati i dati I dati personali da Lei forniti:</b>\r\n                       <ul class="privacy-ul">\r\n                       	<li>completamento e supporto dell''accesso;</li>\r\n                        <li>per eseguire obblighi di legge;</li>\r\n                        <li>per esigenze di tipo operativo e gestionale</li>\r\n                        <li>per inziative e comunicazioni promozionali riservate agli iscritti al sito</li>\r\n					  </ul>\r\n                    </p>\r\n                    <p align="justify">Modalità di trattamento dei dati\r\nIn relazione alle indicate finalità,il trattamento dei dati personali avviene mediante strumenti manuali, informatici e telematici con logiche strettamente correlate alle finalità stesse e, comunque, in modo da garantire la sicurezza e la riservatezza dei dati stessi. Il trattamento dei dati avverrà mediante strumenti idonei a garantirne la sicurezza e la riservatezza e potrà essere effettuato anche attraverso strumenti automatizzati atti a memorizzare, gestire e trasmettere i dati stessi. I dati forniti non verranno ceduti e/o rivenduti a soggetti terzi.<br><br>\r\nDiritti di cui all''art.7 La informiamo, infine, che l''art. 7 del decreto legislativo 196/2003 conferisce agli interessati l''esercizio di specifici diritti. In particolare, l''interessato può ottenere dal Titolare la conferma dell''esistenza o no di propri dati personali e che tali dati vengano messi a sua disposizione in forma intelligibile. L''interessato può altresì chiedere di conoscere l''origine dei dati personali, la finalità e modalità del trattamento; la logica applicata in caso di trattamento effettuato con l''ausilio di strumenti elettronici; gli estremi identificativi del titolare e del responsabile; di ottenere la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge nonché l''aggiornamento, la rettificazione o, se vi è interesse, l''integrazione dei dati; di opporsi, per motivi legittimi, al trattamento di dati che lo riguardano anche ai fini di invio di materiale pubblicitario o di vendita diretta.<br><br>I diritti in oggetto potranno essere esercitati, anche per il tramite di un incaricato, mediante richiesta rivolta al responsabile nominato con lettera. Nell''esercizio dei diritti, l''interessato può conferire per iscritto, delega o procura a persone fisiche, enti, associazioni od organismi. L''interessato può, altresì, farsi assistere da una persona di fiducia. Ulteriori informazioni potranno essere rischieste, per iscritto, presso la sede dello studio medico, sita in Roma, Via delle Belle Arti, 7 - 00196\r\n                    </p>\r\n					<p align="justify">\r\n					<b>Informazioni sui cookie</b><br/>\r\n					I cookie sono stringhe di testo (piccole porzioni di informazioni) che vengono memorizzati su computer, tablet, smartphone, notebook, da riutilizzare nel corso della medesima visita (cookie di sessione) o per essere ritrasmessi agli stessi siti in una visita successiva.<br/><br/>Ai sensi dell''art. 13 del D.l.vo n. 196/2003 questo sito utilizza unicamente cookie tecnici od a questi assimilati, che non richiedono un preventivo consenso; si tratta di cookie necessari, indispensabili per il corretto funzionamento del sito, servono per effettuare la navigazione.\r\n					</p>\r\n', '', '', '', '', '', '', '', '', 1),
(11, 'PRIVACY', 'Privacy', '', '<p align="justify">Pursuant to Legislative Decree no. 196, You are essentially beautiful, as a sole proprietor of the treatment, you are required to provide some information regarding the use of the personal data you provide, or otherwise acquired in the course of your business. Source of personal data.\r\nThe personal data we refer to is collected directly from this website. All of these data will be processed in compliance with the said law and the confidentiality requirements that have always been inspired by the activity of the study.</p>\r\n					<p align="justify">\r\n                       <b>The following data will be collected:</b>\r\n                       <ul class="privacy-ul">\r\n                        <li>user IP address</li>\r\n                        <li>ersonal email address</li>\r\n                        <li>source URL</li>\r\n                        <li>phone number</li>\r\n                        <li>name</li>\r\n                        <li>surname</li>\r\n                       </ul>\r\n                    </p>\r\n					\r\n					<p align="left">\r\n                       <b>Purpose of the treatment to which the data is intended The personal data you provide:</b>\r\n                       <ul class="privacy-ul">\r\n                       	<li>completion and Access Support;</li>\r\n                        <li>to enforce legal obligations;</li>\r\n                        <li>for operational and management needs</li>\r\n                        <li>for promotions and promotional communications reserved for members of the site</li>\r\n					  </ul>\r\n                    </p>\r\n                    <p align="justify">How data is processed in relation to the aforementioned purposes, the processing of personal data is done through manual, computer and telematic tools with logic strictly related to the purposes themselves and, in any case, in order to guarantee the security and confidentiality of the data themselves. Data processing will be carried out by means of appropriate tools to ensure its security and confidentiality, and it can also be carried out by means of automated tools for storing, managing and transmitting the data itself. The provided data will not be transferred and / or resold to third parties\r\nThe rights referred to in art. 7 We inform you, finally, that art. 7 of Legislative Decree 196/2003 grants the persons concerned the exercise of specific rights. In particular, the data subject may obtain from the Registrar the confirmation of the existence or not of his / her personal data and that such data shall be made available to him in an intelligible form. The person concerned may also ask to know the origin of personal data, the purpose and the manner of processing; the logic applied in the case of processing carried out with the aid of electronic instruments; the identification details of the holder and the manager; to obtain the cancellation, transformation into anonymous form or the blocking of the data processed in violation of the law as well as the updating, correction or, if there is interest, the integration of the data; to oppose, for legitimate reasons, the processing of data concerning him also for the purpose of sending advertising material or direct sale. <br> <br> The rights in question may also be exercised, through an agent as well request addressed to the person named by letter. In the exercise of rights, the person concerned may confer in writing, delegate or procure to natural persons, bodies, associations or bodies. The person concerned can also be assisted by a trusted person. Further information can be obtained, in writing, at the headquarters of the medical office, located in Rome, Via delle Belle Arti, 7 - 00196\r\n                    </p>\r\n					<p align="justify">\r\n					<b>About cookies</b><br/>\r\n					Cookies are text strings (small portions of information) that are stored on computers, tablets, smartphones, notebooks, reusable during the same visit (session cookies) or retransmitted to the same sites on a subsequent.<br/><br/>Visit. art. 13 of Legislative Decree no. 196/2003 this site uses only technical cookies or similar assimilates, which do not require prior consent; these are the necessary cookies, essential for the proper functioning of the site, they serve to make navigation.\r\n					</p>', '', '', '', '', '', '', '', '', 2),
(12, 'PRIVACY', 'Politica de privacidad', '', '<p align="justify">De conformidad con el Decreto Legislativo no. 196, Sei essenzialmente Bella,  es autorizado por el usuario al uso de la información personal que proporcione, dentro de la actividad. Fuente de datos personales. Los datos personales a los que nos referimos se recogen directamente en este sitio web. Todos estos datos serán procesados en cumplimiento de dicha ley y los requisitos de confidencialidad que siempre se han inspirado en la actividad del estudio.</p>\r\n                    <p align="justify">\r\n                       <b>Se recogerán los siguientes datos:</b>\r\n                       <ul class="privacy-ul">\r\n                        <li>dirección IP del usuario;</li>\r\n                        <li>dirección de correo electrónico personal;</li>\r\n                        <li>URL de origen;</li>\r\n                        <li>número de teléfono</li>\r\n                        <li>nombre</li>\r\n                        <li>apellido</li>\r\n                       </ul>\r\n                    </p>\r\n                    <p align="left">\r\n                       <b>Finalidad del tratamiento al que se destinan los datos personales que usted proporciona:</b>\r\n                       <ul class="privacy-ul">\r\n                       	<li>apoyo y complemento del acceso;</li>\r\n                        <li>para hacer cumplir las obligaciones legales;</li>\r\n                        <li>para las necesidades operacionales y de gestión;</li>\r\n                        <li>para promociones y comunicaciones promocionales reservadas a los miembros del sitio;</li>\r\n					  </ul>\r\n                    </p>\r\n                    <p align="justify">MMétodos de procesamiento de datos en relación con las finalidades indicadas, el tratamiento de datos personales son tratados con herramientas manuales, equipo y datos con lógica estrictamente relacionados con los mismos fines y, de todos modos, a fin de garantizar la seguridad y confidencialidad de los datos. Los datos se procesan utilizando medios adecuados para garantizar la seguridad y la confidencialidad y se pueden realizar usando herramientas automatizadas para almacenar, gestionar y transmitir los datos. Los datos proporcionados no serán cedidos y / o revendidos a terceros.<br><br>\r\nLos derechos a que se refiere el artículo 7. Le informamos, que el art. 7 del Decreto Legislativo 196/2003 otorga a los interesados el ejercicio de derechos específicos. En particular, el interesado podrá obtener del registrador la confirmación de la existencia o no de sus datos personales y que dichos datos se pondrán a su disposición de forma inteligible. El interesado también puede solicitar conocer el origen de los datos personales, la finalidad y la forma de tratamiento; la lógica aplicada en el caso de la transformación realizada con ayuda de instrumentos electrónicos; los datos de identificación del titular y del administrador; para obtener la cancelación, la transformación en forma anónima o el bloqueo de los datos tratados en violación de la ley y la actualización, corrección o, si está interesado, la integración de los datos; para oponerse, por motivos legítimos, al tratamiento de los datos que le conciernen también con el fin de enviar material publicitario o ventas directas.<br><br>Los derechos en cuestión también podrán ejercerse, incluso a través de una persona encargada, mediante una solicitud dirigida a la persona designada por carta. En el ejercicio de los derechos, la persona interesada podrá conferir por escrito, delegar o procurar a personas físicas, organismos, asociaciones u organismos.\r\n                    </p>\r\n					<p align="justify">\r\n					<b>Acerca de las cookies</b><br/>\r\n					Las cookies son cadenas de texto (pequeñas porciones de información) almacenadas en computadoras, tabletas, smartphones, portátiles, reutilizables durante la misma visita (cookies de sesión) o retransmitidas en los mismos sitios en una visita posterior.<br/><br/>Según el art. 13 del Decreto Legislativo núm. 196/2003 este sitio usa solamente cookies tecnicos o relacionadas a estos que no requieren consentimiento previo; son necesarios para el buen funcionamiento del web, sirven para hacer la navegación.\r\n					</p>', '', '', '', '', '', '', '', '', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `programmi`
--

CREATE TABLE IF NOT EXISTS `programmi` (
  `id_programma` int(11) NOT NULL,
  `titolo_programma` varchar(500) NOT NULL,
  `stato_programma` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `programmi`
--

INSERT INTO `programmi` (`id_programma`, `titolo_programma`, `stato_programma`) VALUES
(1, 'Un soffio di aria fresca 2', 1),
(2, 'Innamorati di te 2', 1),
(3, 'Il tuo corpo è il tuo tempio 2', 1),
(4, 'Nutre corpo e anima 3', 1),
(5, 'La tua valutazione online', 1),
(6, 'Brilla intensamente', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `programmi_benefici`
--

CREATE TABLE IF NOT EXISTS `programmi_benefici` (
  `id_programmi_benefici` int(11) NOT NULL,
  `id_programma` int(11) NOT NULL,
  `id_benefici` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `programmi_benefici`
--

INSERT INTO `programmi_benefici` (`id_programmi_benefici`, `id_programma`, `id_benefici`) VALUES
(1, 1, 1),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 6, 13),
(10, 6, 15),
(11, 6, 16),
(12, 6, 17),
(13, 6, 18),
(14, 6, 19),
(30, 1, 38),
(31, 3, 48),
(32, 3, 35),
(33, 3, 43),
(34, 3, 46),
(35, 3, 42),
(36, 3, 39),
(37, 3, 45),
(38, 3, 40),
(39, 3, 41),
(40, 3, 44),
(41, 4, 48),
(42, 4, 35),
(43, 4, 43),
(44, 4, 46),
(45, 4, 42),
(46, 4, 41),
(47, 4, 44),
(48, 2, 104),
(49, 2, 106),
(50, 2, 108),
(51, 2, 109),
(52, 2, 103),
(53, 2, 105),
(54, 2, 107);

-- --------------------------------------------------------

--
-- Struttura della tabella `programmi_gallery`
--

CREATE TABLE IF NOT EXISTS `programmi_gallery` (
  `id_programmi_gallery` int(11) NOT NULL,
  `id_programma` int(11) NOT NULL,
  `img_gallery` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `programmi_gallery`
--

INSERT INTO `programmi_gallery` (`id_programmi_gallery`, `id_programma`, `img_gallery`) VALUES
(1, 1, 'prog_1.jpg'),
(2, 1, 'prog_2.jpg'),
(3, 1, 'prog_3.jpg'),
(4, 1, 'prog_4.jpg'),
(7, 1, '024b0-guance-web.png'),
(10, 1, '8dee7-labios3.jpg'),
(12, 1, '7f65c-btx1.jpg'),
(13, 1, '01818-piernas.jpg'),
(15, 4, 'c9239-manioca-ita.png'),
(16, 4, '19912-pancakes-esp.png'),
(17, 4, 'bf086-crespelle-ita.png'),
(18, 4, '8ac5c-yuca-esp.png'),
(19, 4, 'a499d-lasagna-ita.png'),
(20, 4, '715c4-lasagna-esp.png'),
(21, 4, '8b276-spaghuettini-ita.png'),
(22, 4, 'c7167-tortillas-quinoa-esp.png');

-- --------------------------------------------------------

--
-- Struttura della tabella `programmi_traduzioni`
--

CREATE TABLE IF NOT EXISTS `programmi_traduzioni` (
  `id_programmi_traduzioni` int(11) NOT NULL,
  `id_programma` int(11) NOT NULL,
  `frase_programma` text NOT NULL,
  `nome_programma` varchar(500) NOT NULL,
  `descrizione_programma` text NOT NULL,
  `immagine_programma` varchar(250) NOT NULL,
  `immagine_programma_mobile` varchar(250) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `programmi_traduzioni`
--

INSERT INTO `programmi_traduzioni` (`id_programmi_traduzioni`, `id_programma`, `frase_programma`, `nome_programma`, `descrizione_programma`, `immagine_programma`, `immagine_programma_mobile`, `lingua_traduzione_id`) VALUES
(1, 1, '“Sentiti più bella, fresca e riposata”.', 'Un soffio di aria fresca 2', 'Ti offro una valutazione focalizzata su ciò che non ti piace del tuo viso, quei particolari che non vuoi guardare allo specchio e che senti che ti tolgono bellezza. Quindi procederò alla correzione, eseguirò il trattamento più adatto, armonizzando la tua bellezza e donandoti più luce.\n<br/><br/>\nNei successivi 15 giorni programmeremo una visita di controllo in cui valuterò i risultati e realizzerò il piano di trattamento specifico per le tue esigenze, in modo da poter dare un soffio d’aria fresca al tuo viso.\n<br></br>\nSe non puoi raggiungermi fisicamente, e quindi sarai impossibilitata a realizzare i trattamenti di medicina estetica con me, ti propongo di seguire il programma <span class="blu-text"><b>“La tua valutazione online”.</span></b> Troverai all’interno dell’elenco dei programmi i benefici che potrà portarti. ', '5f132-1950_500_aria_fresca.png', '2ca1b-580_700_aria_fresca.png', 1),
(2, 2, '“Cambiando il modo di vedere le cose, cambierà la forma di vivere la tua vita”.', 'Innamorati di te 2', 'Questo programma <b>online</b> ti aiuterà a realizzare dei cambiamenti nella tua esistenza, attraverso tecniche di valorizzazione personale, armonizzando ed equilibrando quelle aree della tua vita che ti fanno sentire insoddisfatta, raggiungendo così il benessere interiore.\n<br></br>\nTi guiderò nel riconoscimento delle tue convinzioni limitanti e di come potresti trasformare quelle convinzioni di maniera tale che tu possa riprendere il tuo potere e lasciar andare tutto quello che blocca la tua energia e la tua felicità.\n<br></br>\nTi fornirò strumenti che ti aiuteranno a sviluppare un legame più forte con te stessa, riconoscere chi sei (e non quello che dovresti essere), indirizzando il lavoro interiore per risplendere attraverso la tua propria luce.  \n<br></br>\nSarano 5 settimane di lavoro, di guida e di ricerca interna, perché tutto inizia da te. Non c’è bacchetta magica, i risultati li vedrai nella misura in cui troverai il giusto compromesso con te stessa. <b>Ti ricordo che la bellezza esterna è un riflesso del tuo stato interiore, sia a livello cellulare (materia) che a livello emozionale (spirito).</b>\n<br></br>\n <span class="orange-text"><big><b> Vuoi sapere cosa ti aspetta ogni settimana?</span></big></b>\n<br></br>\n<big><strong>Settimana 1:</big></strong> \n<big><br><i>In quale area della tua vita trovi insoddisfazione, sii sincerità con te stessa perché questo è il punto di partenza.</big></i></br>\n<big><strong>Settimana 2: </big></strong>\n<big><br><i>Sii consapevole delle tue reali limitazioni! Quanto sei disposta a fare per riconoscerle?</big></i></br>\n<big><strong>Settimana 3: </big></strong>\n<big><br><i>Lascia andare il passato… quello che ti blocca e non ti permette di vibrare alto.</big></i></br>\n<big><strong>Settimana 4:</big></strong> \n<big><br><i>Crea e organizza un piano d’azione. Andiamo verso la ripresa di te!</big></i></br>\n<big><strong>Settimana 5:</big></strong>\n<big><br><i>Sii consapevole della tua luce ma anche delle tue ombre, ti aiuterà a fare il salto.</big></i></br>\n<br></br>\n<b> <span class="blu-text"><big>Le date d’inizio di questo programma saranno pubblicate prossimamente.</b></span></big>\n', 'dbdd1-1950_500_innamorati.png', 'caf52-580_700_innamorati.png', 1),
(3, 3, '“Il corpo è l’involucro della tua anima”.', 'Il tuo corpo è il tuo tempio 2', 'Questo programma inizia con una valutazione orientata su ciò che vuoi migliorare del tuo corpo. Analizzerò la tua alimentazione e ti farò eseguire dei semplici esercizi che mi aiuteranno a capire qual è la tua relazione con il cibo. Successivamente realizzerò un programma medico estetico appositamente per te. \n<br></br>\nTi aiuterò a riconoscere il linguaggio del tuo corpo e gradualmente sperimenterai cose che non sapevi di te, che ti permetteranno di raggiungere un nuovo stile di vita, sentirti con più energia, più attiva e più leggera ed in più migliorerà la tua immagine corporea.\n<br/><br/>\n<b>Il tempo di applicazione di questo programma è di 4 mesi.</b>\n<br/><br/>\nRicordati che il programma <strong><span class="blu-text">“Il corpo è il tuo tempio”</span></strong> di <strong><span class="orange-text"><big>Sei Essenzialmente Bella</strong></span></big> è un viaggio, che si realizza passo a passo godendoti il camino per arrivare all’obbiettivo. \n<br></br>\nQuesto programma si realizza insieme al programma <span class="blu-text"><b>"Nutre corpo e anima 3"</span></b> che viene eseguito <span class="blu-text"><big><b>ONLINE.</span></big></b>\n<br></br>\nSe non puoi raggiungermi fisicamente, e quindi sarai impossibilitata a realizzare i trattamenti di medicina estetica con me, ti propongo di seguire il programma <b><span class="blu-text">“La tua valutazione online”.</b></span> Troverai all’interno dell’elenco dei programmi i benefici che potrà portarti. ', '68696-1950_500_corpo_tempio.png', '87351-580_700_corpo_tempio.png', 1),
(4, 4, '“Ritrova una migliore relazione con il cibo e vivi la tua esperienza”.', 'Nutre corpo e anima 3', 'In questo programma sarò una tua guida, ti aiuterò a conoscere quali alimenti ti fanno sentire l’energia e la sensazione di benessere nel tuo corpo e quali ti possono creare gonfiore, pesantezza o farti stare male. L’obbiettivo non è tanto conoscere gli alimenti se non riconoscere i segnali del tuo corpo, in questo modo svilupperai la capacità di seguire il tuo camino nell’alimentazione senza aver bisogno di una persona che lo faccia per te.\n<br></br>\nIl cibo è un qualcosa che ti dovrebbe dare energia, non restartela, per cui se dopo pranzo ti senti stanca e con sonno c’è qualcosa che non va come dovrebbe. Questo ti potrebbe sembrare normale perché è ciò che pensa normalmente il comune denominatore nella società.\n<br></br> \nSei un essere individuale ed è meglio imparare a leggere i segnali del tuo corpo, sapere cosa sta accadendo e cosa lo ha originato. In questo modo il tuo intestino lavorerà meglio, senza attivare processi d’infiammazione che sono all’origine del sovrappeso, dell’obesità nonché dell’invecchiamento cutaneo.  \n<br></br>\nTi propongo d’iniziare un percorso di 4 mesi che <b>cambierà la tua relazione con il cibo e con te stessa</b> e <b>migliorerà certamente la tua immagine corporea.</b>\n<br></br> \n<strong>Questo programma si esegue</strong> <big>ONLINE.</big>\n<br></br>', 'a1f8e-1950_500_nutre.png', '68be5-580_700_nutre.png', 1),
(5, 5, '“La distanza non è un ostacolo quando vuoi sapere di più”.', 'La tua valutazione online', '<br></br>\n<strong><span class="orange-text"><big>Se non stai vicina a me e non hai possibilità d’incontrarmi di persona, ti offro:</strong></span></big>\n<br></br>\n+ indicazioni su come armonizzare il tuo corpo ed il tuo volto con un approccio olistico;\n<br></br>\n+ di aiutarti a scoprire quali trattamenti di medicina estetica sono più adatti per te;\n<br></br>\n+ un piano di lavoro personalizzato.</br><p>\n<br></br>\n<strong>In questo modo otterrai la conoscenza su come raggiungere la migliore versione di te e la tranquillità di sapere quali sono i trattamenti e prodotti di origine certificata più adatti a te.</strong>\n', '61cab-1950_500_valutazione.png', 'cef63-580_700_valutazione.png', 1),
(6, 1, 'Feel more beautiful, fresh and rested.', 'Un soffio di aria fresca 2', 'I offer you a focus on what you do not like about your face, those details that you do not want to look in the mirror and feel that they take away your beauty. Then I will proceed to the correction, I will perform the most appropriate treatment, harmonizing your beauty and giving you more light. <br/> <br/> In the next 15 days we will schedule a checkup where I will evaluate the results and realize the specific treatment plan for your needs, so you can give a breath of fresh air to your face.', 'a7067-1950_500_aria_fresca.png', '9a788-580_700_aria_fresca.png', 2),
(7, 1, '“ Siéntete más bella, fresca y reposada”.', 'Un soffio di aria fresca 2', 'Con este programa tendrás una evaluación orientada a lo que quieres resolver de tu rostro, eso que te da fastidio, eso que no quieres ver en el espejo, eso que sientes que te quita belleza. Luego procederé con la corrección, realizaré el tratamiento que más se adapte a lo que tu quieres corregir, armonizando tu belleza y donandote más luz.\n<br></br>\nA los 15 días programaremos una visita de control donde evaluaré los resultados y realizaré un plan de tratamiento específico para tus necesidades, de manera que le puedas regalar un toque de aire fresco a tu rostro.<br><br> \nEn el caso que no te puedas realizar tratamientos conmigo, te ofrezco efectuar el programa <b><span class="blu-text">" La tua valutazione online".</b></span> Puedes encontrarlo en el elenco de programas y conocer los beneficios para ti.\n', 'e5a19-1950_500_aria_fresca.png', 'c72f5-580_700_aria_fresca.png', 3),
(8, 6, 'Sé consciente de tu luz y de tu sombra y empieza a brillar', 'Brilla intensamente', 'Una de las cosas más maravillosas que tienes como ser humano son tus sombras, porque cuando te haces consciente de ellas logras entenderte mejor y al abrazarlas y respetarlas se convierten en tu motor, en tu impulso, para hacerte brillar desde tu luz.<br><br><b>¿Cómo lo haces, cómo creas consciencia?</b><br>A través de la Numerología Cuántica se descodifica toda la información contenida en tus números, haciendote consciente de: quien eres: desde tu esencia, tu luz y tus sombras, lo que vienes a trabajar en esta vida, que obtáculos deberías trascender, que instrumentos traes de tu vida pasada para emplear en esta vida, cual es el regalo o Don que te ha sido dado para estar aquí, éste es tu herramienta principal para salir de cualquier circunstacia que te toque vivir.<br><br>La Numerología te ayuda a tener consciencia plena de quien eres y así brillar intensamente y cuando lo haces te sientes ùnica, verdadera, genuina y entiendes lo importante que eres como pieza de engranaje en este mundo maravilloso brillando desde tu propia luz.<br><br>\nCuando reconoces tus sombras, no quiere decir que las resuelves y ya nunca más estarán en tu vida. Tus sombras son tuyas, te pertenecen, lo que superas son las circunstancias que vives desde ellas y a medida que elevas tus niveles de conciencia, será más fácil para ti reconocerlas y hacer paces con ellas, empoderandote así de tu propia luz y no dejando que sean tus sombras las que se empoderen de ti.<br><br>Te recuerdo que la numerología cuántica te ayuda a hacerte consciente y a vibrar en los diferentes niveles de consciencia y así como tú evolocionas, tus números también lo hacen para ayudarte a vibrar con más intensidad.<br><br>\nLa manera ideal de ser cocreadora es saber desde donde estás creando tu vida, si desde tu luz o desde tus sombras y ciertamente para vivir la vida que quieres será necesario que tomes consciencia para empezar a crear en positivo y con magnetismo.<br><br> \n<strong>Inspirate e inicia a tener consciencia de tu luz y de tu sombra y empieza a brillar.</strong>\n\n', '9396f-1950_500_brilla.png', '7ed32-580_700_brilla.png', 3),
(9, 0, 'Il corpo è l’involucro della tua anima', 'Innamorati di te 2', 'Questo programma mi permetterà di conoscere cosa vorresti migliorare di te.  Attraverso tecniche di valorizzazione personale e trattamenti di medicina estetica ti aiuterò a raggiungere il tuo obiettivo, armonizzando ed equilibrando l’aspetto che non ti fa sentire bene dentro la tua pelle, ridandoti quella sensazione di benessere interiore.\n\nSvilupperò un piano specifico per te e per i tuoi bisogni. Contemporaneamente, ponendo a frutto la mia esperienza personale, ti guiderò e sosterrò con alcuni strumenti, che ho applicato sulla mia persona, che ti aiuteranno a sviluppare un legame più forte con te stessa e trovare quella pace interna di cui hai bisogno.\n\nIl programma dura 4 mesi, tempo sufficiente per iniziare a conoscere il tuo valore personale.\n', '', '', 1),
(10, 0, '"El cuerpo es el hogar de tu alma."', 'Innamorate di te 2', 'En este programa te ofrezco una evaluación que me permitirá conocer que es lo que quieres mejorar en tí y a través de técnicas de valor personal y tratamientos de medicina estética, te ayudaré a lograr tu objetivo, armonizando y equilibrando ese aspecto con el que no te sientes a gusto, ridandote esa sensación de bienestar interno.\nRealizaré un plan a ejecutar específico para tí y tus necesidades. Contemporaneamente, desde mi experiencia personal te guiaré y te apoyaré con algunas herramientas que he aplicado en mi para que te conectes contigo misma y te encamines a esa paz interna que estas buscando, tomando en cuenta siempre que somos seres bioindividuales.\nEl programa tiene una duracion de 4 meses, período suficiente para que empieces a conocer tu valor personal.\nEn el caso que no te puedas realizar tratamientos conmigo, te ofrezco <u>tu evaluación online</u> que puedes encontrar en la sección de programas y así saber cuales son los beneficions para tí. \nAunque no puedas realizar los tratamientos de medicina estética, podemos realizar online la otra parte de este programa que se enfoca en el valor personal.  \n', '', '', 3),
(11, 0, '“Si cambias el modo de ver las cosas, cambiarás la manera de vivir tu vida”. ', 'Innamorati di te 2', 'En este programa te ofrezco una evaluación que me permitirá conocer que es lo que quieres mejorar en tí y a través de técnicas de valor personal y tratamientos de medicina estética, te ayudaré a lograr tu objetivo, armonizando y equilibrando ese aspecto con el que no te sientes a gusto, ridandote esa sensación de bienestar interno.\nRealizaré un plan a ejecutar específico para tí y tus necesidades. Contemporaneamente, desde mi experiencia personal te guiaré y te apoyaré con algunas herramientas que he aplicado en mi para que te conectes contigo misma y te encamines a esa paz interna que estas buscando, tomando en cuenta siempre que somos seres bioindividuales.\nEl programa tiene una duracion de 4 meses, período suficiente para que empieces a conocer tu valor personal.\nEn el caso que no te puedas realizar tratamientos conmigo, te ofrezco <u>tu evaluación online</u> que puedes encontrar en la sección de programas y así saber cuales son los beneficions para tí. \nAunque no puedas realizar los tratamientos de medicina estética, podemos realizar online la otra parte de este programa que se enfoca en el valor personal.\n', '', '', 3),
(12, 2, '“Si cambias el modo de ver las cosas, cambiarás la manera de vivir tu vida”. ', 'Innamorati di te 2', 'Este programa <b>online</b> te ayudará a hacer cambios en tu existencia, a través de técnicas de valor personal, armonizando y equilibrando esas áreas de tu vida que te hacen sentir insatisfecha, logrando así bienestar interno.\n<br></br>\nTe guiaré en el reconocimiento de tus creencias limitantes y cómo podrías transformar esas creencias de tal manera que puedas empoderarte de tí y dejar ir todo lo que pueda estar bloaqeuando tu energía y felicidad.\n<br></br>\nTe facilitaré herramientas que te ayudarán a desarrollar un vínculo más fuerte contigo misma, reconocer quién eres, asi como  también sobre qué deberías trabajar para que empieces a brillar con tu propia luz.\n<br></br>\nSerán 5 semanas de trabajo, guía y busqueda interna, porque todo comienza dentro de tí. No hay varita mágica, los resultados los verás en la misma medida en la que estés comprometida contigo misma. <strong>Te recuerdo que la belleza externa es un reflejo de tu estado interior, sea a nivel celular (materia) que a nivel emocional (espiritu).</strong>\n<br></br>\n<b><big><span class="orange-text">¿Quieres saber qué te espera en estas 5 semanas? </b></big></span>\n<br></br>\n<big><strong>Semana 1:</big><strong>\n<big><br><i>En qué área de tu vida te encuentras insatisfecha, sé honesta contigo misma porque este es el punto de partida.</big></br></i>\n<big><strong>Semana 2:</big></strong>\n<big><br><i>¡Sé consciente de tus reales límites! ¿Cuánto estás dispuesto a hacer para reconocerlos?</big></br></i>\n<big><strong>Semana 3:</big></strong>\n<big><br><i>Deja ir el pasado ... aquello que te bloquea y no te permite vibrar alto.</big></br></i>\n<big><strong>Semana 4:</big></strong>\n<big><br><i>Crea y organiza un plan de acción. ¡Empoderate de tí!</big></br></i>\n<big><strong>Semana 5:</big></strong>\n<big><br><i>Sé consciente de tu luz, pero también de tus sombras, te ayudará a dar el salto.</big></br></i>\n<br><br>\n<span class="blu-text"><big>Las fechas de inicio de este programa serán anunciadas proximamente.</span></big>\n<br></br>\n', '83906-1950_500_innamorati.png', '43643-580_700_innamorati.png', 3),
(13, 3, '“El cuerpo es el hogar de tu alma”.', 'Il corpo è il tuo tempio 2', 'Este programa inicia con una evaluación orientada al aspecto que quieres resolver de tu cuerpo, preguntas sobre tu alimentación y algunos aspectos de tu vida que me ayudara a entender como te ves en ella y cual es tu relación con la comida. Luego proseguiré a realizar el programa médico estético específicamente para tí.\n<br></br> \nTe ayudaré a reconocer el lenguaje de tu cuerpo y poco a poco experimentarás cosas que no conocías de tí logrando aplicar un nuevo estilo de vida, que te permitirá sentirte con más energía, activa y ligera y como un plus mejorará tu imagen corporal.\n<br></br>\n<b>El tiempo de aplicación de este programa es de 4 meses.</b>\n<br></br>\nRecuerda que, el programa <strong><span class="blu-text">"Il tuo corpo è il tuo tempio"</strong></span> de <strong><span class="orange-text"><big>Sei Essenzialmente Bella</strong></span></big>, es un viaje que se realiza paso a paso, disfrutando el camino para así llegar firme al objetivo.\n<br></br>\nEste programa se realiza junto al programa <b><span class="blu-text">"Nutre corpo e anima 3"</b></span> que se lleva a cabo <span class="blu-text"><big>ONLINE</span></big>\n<br></br>\nEn el caso de que no puedas realizar los tratamientos de medicina estética conmigo,  te propongo efectuar el programa <strong><span class="blu-text">"La tua valutazione online".</strong></span> Puedes encontrarlo en el elenco de programas y conocer los beneficios para ti.\n\n', 'd6f5b-1950_500_corpo_tempio.png', 'c157d-580_700_corpo_tempio.png', 3),
(14, 4, '“Mejora tu relación con los alimentos y vive tu experiencia”.', 'Nutre corpo e anima 3', 'En este programa seré tu guía, te ayudaré a conocer qué alimentos le dan energía y la sensación de bienestar a tu cuerpo y  qué alimentos pueden crearte hinchazón, pesadez o hacerte sentir mal. El objetivo no es tanto acerca de la comida, si no el de reconocer las señales de tu cuerpo, de esta manera desarrollarás la capacidad de ser tu propia guía en tu alimentación sin la necesidad de tener a alguien que lo haga por tí (eceptuando casos particulares).\n<br></br>\nLa comida es lo que te debería dar energía, no restartela, así que si después de comer te sientes cansada y con ganas de dormir hay algo que no esta andando como debería, aunque si esto te podría parecer normal, porque es el común denominador en la sociedad.\n<br></br>\nEres un ser individual y es mejor aprender las señales de tu cuerpo, saber lo que te està ocurriendo y qué lo causó. De esta manera tu intestino funcionará mejor, sin desencadenar procesos inflamatorios que son el origen del sobrepeso y la obesidad e incluso el envejecimiento de la piel.\n<br></br>\nTe propongo iniciar un viaje de 4 meses que va a <b>cambiar tu relación con la comida y contigo misma</b> y absolutamente <strong>mejorará tu imagen corporal.</strong>\n<br></br>\n<strong>Este programa se desarrolla totalmente</strong> <big>ONLINE.</big>\n<br></br>', '94241-1950_500_nutre.png', '17461-580_700_nutre.png', 3),
(15, 5, '“La distancia no es un obstáculo quando quieres saber más”.', 'La tua valutazione online', '<br></br>\n<span class="orange-text"><strong><big>Si no estas cerca de mi y no tienes la posibilidad de encontrarme en persona, te ofrezco:</span></strong></big>\n<br></br>\n+ indicaciones sobre como armonizar tu cuerpo y rostro con un enfoque holístico;\n<br></br> \n+ ayudarte a obtener el conocimiento de cuales son los tratamientos de medicina estética adaptos a tus necesidades;\n<br></br>\n+ un plan de trabajo personalizado.\n<br></br>\n<strong>De este modo obtendrás los conocimientos para lograr la mejor versión de tí y la tranquilidad de saber cuales son los tratamientos y productos de origen certificada adecuados para tí.</strong>\n', 'f2f37-1950_500_valutazione.png', '4aeda-580_700_valutazione.png', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `programmi_trattamenti`
--

CREATE TABLE IF NOT EXISTS `programmi_trattamenti` (
  `id_programmi_trattamenti` int(11) NOT NULL,
  `id_programma` int(11) NOT NULL,
  `id_trattamento` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `programmi_trattamenti`
--

INSERT INTO `programmi_trattamenti` (`id_programmi_trattamenti`, `id_programma`, `id_trattamento`) VALUES
(1, 1, 1),
(11, 1, 2),
(12, 1, 3),
(13, 1, 12),
(14, 1, 5),
(15, 1, 8),
(16, 1, 7),
(17, 1, 9),
(18, 1, 11),
(19, 1, 13),
(21, 3, 12),
(22, 3, 6),
(23, 3, 5),
(24, 3, 4),
(25, 3, 10),
(26, 3, 7);

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_descrizione`
--

CREATE TABLE IF NOT EXISTS `stato_descrizione` (
  `stato_descrizione_id` int(11) NOT NULL,
  `stato_descrizione_text` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_descrizione`
--

INSERT INTO `stato_descrizione` (`stato_descrizione_id`, `stato_descrizione_text`) VALUES
(1, 'ATTIVO'),
(2, 'CANCELLATO'),
(3, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `studi_medici`
--

CREATE TABLE IF NOT EXISTS `studi_medici` (
  `id_studio_medico` int(11) NOT NULL,
  `citta` varchar(200) NOT NULL,
  `indirizzo` varchar(250) NOT NULL,
  `lat` float NOT NULL,
  `long` float NOT NULL,
  `stato` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `studi_medici`
--

INSERT INTO `studi_medici` (`id_studio_medico`, `citta`, `indirizzo`, `lat`, `long`, `stato`) VALUES
(1, 'Macomer', 'Via Tigellio, 4-12 - 08015 (NU)', 40.2658, 8.78028, 1),
(2, 'Nuoro', 'Via Angioj, 12 - 08100 (NU)', 40.3219, 9.33272, 1),
(3, 'Cagliari', 'Viale Marconi, 26 - 09131 (CA)', 39.2265, 9.12979, 3),
(4, 'Tortolì', 'Via Bertulottu - 08048 (OG)', 39.9263, 9.6779, 1),
(5, 'Oristano', 'Via Giovanni XXIII, 10 - 09170 (OR)', 39.9039, 8.58714, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_template_default`
--

CREATE TABLE IF NOT EXISTS `tipo_template_default` (
  `id_tipo_template_default` int(11) NOT NULL,
  `desc_tipo_template_default` varchar(255) NOT NULL,
  `nome_tipo_template_default` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_template_default`
--

INSERT INTO `tipo_template_default` (`id_tipo_template_default`, `desc_tipo_template_default`, `nome_tipo_template_default`) VALUES
(1, 'Template di email benvenuto per chi si iscrive alla newsletter', 'NEWSLETTER_BENVENUTO'),
(2, 'Email il risposta alla richiesta di contatto dal modulo del sito', 'CONTATTO_RICVUTO');

-- --------------------------------------------------------

--
-- Struttura della tabella `traduzioni_file_lang`
--

CREATE TABLE IF NOT EXISTS `traduzioni_file_lang` (
  `id_tfl` int(11) NOT NULL,
  `LANGUAGE_NAME` varchar(250) NOT NULL,
  `LANGUAGE_ABBR` varchar(250) NOT NULL,
  `LANGUAGE_ID` varchar(250) NOT NULL,
  `LANGUAGE_LOCALE_PAYPAL` varchar(250) NOT NULL,
  `LANGUAGE_CI_CODE` varchar(250) NOT NULL,
  `MENU_HOME` varchar(250) NOT NULL,
  `MENU_ABOUTME` varchar(250) NOT NULL,
  `MENU_BIO` varchar(250) NOT NULL,
  `MENU_MYWORK` varchar(250) NOT NULL,
  `MENU_PROGRAMS` varchar(250) NOT NULL,
  `MENU_TREATMENTS` varchar(250) NOT NULL,
  `MENU_STUDIES` varchar(250) NOT NULL,
  `MENU_CONTACTME` varchar(250) NOT NULL,
  `MENU_LANGUAGE` varchar(250) NOT NULL,
  `MENU_PRIVACY` varchar(250) NOT NULL,
  `SUBSCRIBE_BTN` varchar(250) NOT NULL,
  `SEND_MESSAGE_BTN` varchar(250) NOT NULL,
  `SEND_AREYOUSURE_BTN` varchar(250) NOT NULL,
  `SUBSCRIBE_NEWSLETTER_H1` varchar(250) NOT NULL,
  `SUBSCRIBE_NEWSLETTER_DESC` varchar(250) NOT NULL,
  `SUBSCRIBE_NEWSLETTER_NAME` varchar(250) NOT NULL,
  `SUBSCRIBE_NEWSLETTER_EMAIL` varchar(250) NOT NULL,
  `PROGRAM_TREATMENTS` varchar(250) NOT NULL,
  `PROGRAM_TREATMENTS_DESC` varchar(250) NOT NULL,
  `BENEFITS_QUESTION` varchar(250) NOT NULL,
  `CONTACTME_DESC` varchar(250) NOT NULL,
  `LABEL_NAME` varchar(250) NOT NULL,
  `LABEL_EMAIL` varchar(250) NOT NULL,
  `LABEL_PHONE` varchar(250) NOT NULL,
  `LABEL_LOCATION` varchar(250) NOT NULL,
  `LABEL_MESSAGE` varchar(250) NOT NULL,
  `LABEL_CHOOSE` varchar(250) NOT NULL,
  `LABEL_FAQ` varchar(250) NOT NULL,
  `LABEL_UNSUBSCRIBE` varchar(250) NOT NULL,
  `MSG_REQUIRED_NAME` varchar(250) NOT NULL,
  `MSG_REQUIRED_EMAIL` varchar(250) NOT NULL,
  `MSG_VALID_EMAIL` varchar(250) NOT NULL,
  `MSG_REQUIRED_PHONE` varchar(250) NOT NULL,
  `MSG_VALID_PHONE` varchar(250) NOT NULL,
  `MSG_REQUIRED_OFFICE` varchar(250) NOT NULL,
  `MSG_REQUIRED_MESSAGE` varchar(250) NOT NULL,
  `MSG_SUCCESS_CONTACT` varchar(250) NOT NULL,
  `MSG_FAILURE_CONTACT` varchar(250) NOT NULL,
  `MSG_SUCCESS_NEWSLETTER` varchar(250) NOT NULL,
  `MSG_UNIQUE_NEWSLETTER` varchar(250) NOT NULL,
  `MSG_UNSUBSCRIBE_DONE` varchar(250) NOT NULL,
  `MSG_UNSUBSCRIBE_NOTFOUND` varchar(250) NOT NULL,
  `HOME_SERVICIOS_PATH` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `traduzioni_file_lang`
--

INSERT INTO `traduzioni_file_lang` (`id_tfl`, `LANGUAGE_NAME`, `LANGUAGE_ABBR`, `LANGUAGE_ID`, `LANGUAGE_LOCALE_PAYPAL`, `LANGUAGE_CI_CODE`, `MENU_HOME`, `MENU_ABOUTME`, `MENU_BIO`, `MENU_MYWORK`, `MENU_PROGRAMS`, `MENU_TREATMENTS`, `MENU_STUDIES`, `MENU_CONTACTME`, `MENU_LANGUAGE`, `MENU_PRIVACY`, `SUBSCRIBE_BTN`, `SEND_MESSAGE_BTN`, `SEND_AREYOUSURE_BTN`, `SUBSCRIBE_NEWSLETTER_H1`, `SUBSCRIBE_NEWSLETTER_DESC`, `SUBSCRIBE_NEWSLETTER_NAME`, `SUBSCRIBE_NEWSLETTER_EMAIL`, `PROGRAM_TREATMENTS`, `PROGRAM_TREATMENTS_DESC`, `BENEFITS_QUESTION`, `CONTACTME_DESC`, `LABEL_NAME`, `LABEL_EMAIL`, `LABEL_PHONE`, `LABEL_LOCATION`, `LABEL_MESSAGE`, `LABEL_CHOOSE`, `LABEL_FAQ`, `LABEL_UNSUBSCRIBE`, `MSG_REQUIRED_NAME`, `MSG_REQUIRED_EMAIL`, `MSG_VALID_EMAIL`, `MSG_REQUIRED_PHONE`, `MSG_VALID_PHONE`, `MSG_REQUIRED_OFFICE`, `MSG_REQUIRED_MESSAGE`, `MSG_SUCCESS_CONTACT`, `MSG_FAILURE_CONTACT`, `MSG_SUCCESS_NEWSLETTER`, `MSG_UNIQUE_NEWSLETTER`, `MSG_UNSUBSCRIBE_DONE`, `MSG_UNSUBSCRIBE_NOTFOUND`, `HOME_SERVICIOS_PATH`, `id_lingua`) VALUES
(1, 'ITALIANO', 'IT', '1', 'it_IT', 'italian', 'Home', 'Su di me', 'Mini biografia', 'Il mio lavoro', 'Programmi', 'Trattamenti', 'Studi medici', 'Contattami', 'Lingua', 'Privacy', 'ISCRIVITI', 'INVIA MESSAGGIO', 'SEI SICURO ?', 'ISCRIVITI ALLA NEWSLETTER', 'Inizia a ricevere consigli, ispirazioni e informazioni che ti potranno aiutare a raggiungere una vita con maggiori soddisfazioni.', 'Inserisci il tuo nome', 'Inserisci la tua email', 'Trattamenti del programma', 'Clicca sui link per vedere i dettagli dei singoli trattamenti', 'Quali sono i benefici per te ?', 'Se hai qualche domanda, se vuoi prenotare il trattamento o chiedere qualche informazione.', 'Nome', 'Email', 'Telefono', 'Studio di zona', 'Messaggio', 'Scegli...', 'Domande frequenti', 'Cancellati', 'Inserisci il tuo nome', 'Inserisci la tua email', 'Inserisci una email valida', 'Inserisci il tuo numero di telefono', 'Inserisci un numero valido', 'Seleziona uno studio medico', 'Inserisci il messaggio', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'Abbiamo riscontrato un problema nell''invio del mesaggio. Riprova!', 'Adesso sei iscritto alla newsletter. Grazie !', 'L''indirizzo email è già iscritto alla newsletter', 'La tua email/iscrizione è stata rimossa. Grazie!', 'Il contatto richiesto non è attualmente registrato', 'servicios.png', 1),
(2, 'ENGLISH', 'EN', '2', 'en_GB', 'english', 'Home', 'About me', 'Mini biography', 'My work', 'Programs', 'Treatments', 'Medical offices', 'Contact me', 'Language', 'Privacy', 'SUBSCRIBE', 'SEND MESSAGE', 'ARE YOU SURE ?', 'SUBSCRIBE TO NEWSLETTER', 'Get started with tips, inspirations and information that will help you live a life with greater satisfaction.', 'Insert your name', 'Insert your email', 'Treatments of the program', 'Click on the links to see the details of individual treatments', 'What are the benefits for you?', 'If you have any questions, whether you want to book the treatment or ask for some information.', 'Name', 'Email', 'Phone', 'Medical office', 'Message', 'Choose...', 'Frequent questions', 'Unsubscribe', 'Insert your name', 'Insert your email', 'Insert a valid email address', 'Insert your phone number', 'Inserit a valid number', 'Choose a office', 'Insert a message', 'Your message has been sent correctly. Thank you !', 'Sorry it seems that our mail server is not responding. Please try again later!', 'Now you are subscribed to the newsletter. Thank you !', 'The email address is already subscribed to the newsletter.', 'Your email/subscription was removed. Thank you!', 'The required contact is not currently registered', 'servicios.png', 2),
(3, 'ESPANOL', 'ES', '3', 'es_ES', 'spanish', 'Inicio', 'Sobre mi', 'Mini biografía', 'Mi trabajo', 'Programas', 'Tratamientos', 'Consultorio Médico', 'Contáctame', 'Idiomas', 'Politica de privacidad', 'Sí, quiero unirme', 'Envía mensaje', '¿Estás seguro?', 'Inscríbete a los boletines', 'Comienza a recibir consejos, inspiración e información que te pueden ayudar a tener una vida llena de satisfacción.', 'Tu nombre', 'Tu email', 'Tratamientos del programa', 'Haz click sobre el link para ver los detalles de cada tratamiento', '&#191;Qué beneficios tiene para tí ?', 'Si tienes preguntas, quieres una cita o quieres pedir información.', 'Nombre', 'Email', 'Teléfono', 'Consultorio Médico Zona', 'Mensaje', 'Elige...', 'Preguntas Frecuentes', 'Darse de baja', 'Escribe tu nombre', 'Escribe tu email', 'Escribe un email válido', 'Escribe tu teléfono', 'Escribe un número válido', 'Selecciona un consultorio médico', 'Escribe el mensaje', 'Tu mensaje ha sido enviado correctamente. ¡ Gracias !', 'Hemos encontrado un problema en el envío del mensaje. ¡Intenta otra vez!', 'Ahora estás suscrito al boletín. Gracias!', 'Su dirección de correo electrónico ya está incluida en el boletín.', 'Su correo electrónico / registro ha sido eliminado. Gracias!', 'El contacto requerido no está registrado actualmente', 'servicios.png', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `trattamenti`
--

CREATE TABLE IF NOT EXISTS `trattamenti` (
  `id_trattamento` int(11) NOT NULL,
  `titolo_trattamento` varchar(250) NOT NULL,
  `stato_trattamento` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `trattamenti`
--

INSERT INTO `trattamenti` (`id_trattamento`, `titolo_trattamento`, `stato_trattamento`) VALUES
(1, 'Biostimolazione', 1),
(2, 'Biorivitalizzazione', 1),
(3, 'Botox', 1),
(4, 'Iperidrosi con Botox', 1),
(5, 'Fili PDO', 1),
(6, 'Epilazione laser', 3),
(7, 'Needling medico', 1),
(8, 'Filler', 1),
(9, 'Peeling', 1),
(10, 'Mesoterapia', 1),
(11, 'Rinofiller', 1),
(12, 'Carbossiterapia', 1),
(13, 'Sublimazione dermica', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `trattamenti_benefici`
--

CREATE TABLE IF NOT EXISTS `trattamenti_benefici` (
  `id_trattamenti_benefici` int(11) NOT NULL,
  `id_trattamento` int(11) NOT NULL,
  `id_benefici` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `trattamenti_benefici`
--

INSERT INTO `trattamenti_benefici` (`id_trattamenti_benefici`, `id_trattamento`, `id_benefici`) VALUES
(1, 1, 8),
(3, 1, 10),
(4, 1, 11),
(5, 1, 12),
(7, 2, 28),
(8, 2, 31),
(9, 2, 29),
(10, 2, 30),
(11, 3, 51),
(12, 3, 52),
(13, 3, 50),
(14, 3, 49),
(15, 4, 53),
(16, 4, 54),
(17, 4, 55),
(18, 4, 56),
(19, 5, 57),
(20, 5, 61),
(22, 5, 58),
(23, 5, 60),
(24, 5, 66),
(25, 7, 64),
(26, 7, 66),
(27, 7, 67),
(28, 7, 63),
(29, 7, 62),
(30, 7, 65),
(31, 8, 72),
(32, 8, 70),
(33, 8, 51),
(34, 8, 68),
(35, 8, 71),
(36, 9, 75),
(37, 9, 76),
(38, 9, 73),
(39, 9, 74),
(40, 10, 80),
(41, 10, 78),
(42, 10, 79),
(43, 10, 77),
(44, 10, 81),
(45, 11, 85),
(46, 11, 86),
(47, 11, 87),
(48, 11, 83),
(49, 11, 84),
(50, 12, 97),
(51, 12, 94),
(52, 12, 96),
(53, 12, 91),
(54, 12, 92),
(55, 12, 52),
(56, 12, 95),
(57, 12, 90),
(58, 12, 93),
(59, 12, 88),
(60, 13, 98),
(61, 13, 100),
(62, 13, 102),
(63, 13, 99),
(64, 13, 101);

-- --------------------------------------------------------

--
-- Struttura della tabella `trattamenti_faq`
--

CREATE TABLE IF NOT EXISTS `trattamenti_faq` (
  `id_trattamenti_faq` int(11) NOT NULL,
  `id_trattamento` int(11) NOT NULL,
  `id_faq` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `trattamenti_faq`
--

INSERT INTO `trattamenti_faq` (`id_trattamenti_faq`, `id_trattamento`, `id_faq`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 13, 64),
(4, 13, 65),
(5, 13, 66),
(6, 13, 67),
(7, 12, 60),
(8, 12, 63),
(9, 12, 62),
(10, 12, 57),
(11, 12, 56),
(12, 12, 61),
(13, 12, 59),
(14, 12, 58),
(15, 12, 5),
(16, 11, 52),
(17, 11, 55),
(18, 11, 51),
(19, 11, 54),
(20, 11, 53),
(21, 10, 46),
(22, 10, 47),
(23, 10, 44),
(24, 10, 50),
(25, 10, 45),
(26, 10, 49),
(27, 10, 48),
(28, 9, 42),
(29, 9, 40),
(30, 9, 38),
(31, 9, 43),
(32, 9, 41),
(33, 9, 39),
(34, 8, 32),
(35, 8, 33),
(36, 8, 29),
(37, 8, 28),
(38, 8, 34),
(39, 8, 31),
(40, 8, 30),
(46, 5, 18),
(47, 5, 22),
(48, 5, 19),
(49, 5, 20),
(50, 5, 21),
(51, 4, 37),
(52, 4, 35),
(53, 4, 36),
(54, 3, 16),
(55, 3, 17),
(56, 3, 13),
(57, 3, 12),
(58, 3, 14),
(59, 3, 5),
(60, 3, 15),
(61, 2, 7),
(62, 2, 1),
(63, 2, 9),
(64, 2, 8),
(65, 2, 5),
(66, 2, 11),
(67, 1, 7),
(68, 1, 8),
(69, 1, 6),
(70, 1, 4),
(71, 1, 5),
(72, 7, 25),
(73, 7, 27),
(74, 7, 24),
(75, 7, 26),
(76, 7, 23);

-- --------------------------------------------------------

--
-- Struttura della tabella `trattamenti_traduzioni`
--

CREATE TABLE IF NOT EXISTS `trattamenti_traduzioni` (
  `id_trattamenti_traduzioni` int(11) NOT NULL,
  `id_trattamento` int(11) NOT NULL,
  `nome_trattamento` varchar(250) NOT NULL,
  `descrizione_trattamento` text NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `trattamenti_traduzioni`
--

INSERT INTO `trattamenti_traduzioni` (`id_trattamenti_traduzioni`, `id_trattamento`, `nome_trattamento`, `descrizione_trattamento`, `lingua_traduzione_id`) VALUES
(1, 1, 'Biostimolazione', 'Si tratta di un trattamento iniettivo che si utilizza per stimolare il derma agendo sulla produzione di sostanze come il collagene e l’acido ialuronico che garantiscono l''elasticità e la tonicità della pelle.', 1),
(2, 2, 'Biorivitalizzazione', 'Consiste in un mix di ialuronici, ad alto e basso peso molecolare a lunga durata, associati ad una tecnica iniettiva innovativa che donano piacevoli e naturali effetti estetici.', 1),
(3, 3, 'Botox', 'Consiste in una tecnica iniettiva che agisce riducendo in modo selettivo la forza contrattile dei muscoli coinvolti nella mimica.', 1),
(4, 4, 'Iperidrosi con Botox', 'In parole più semplici <i>sudorazione eccessiva.</i> È un evento fisiologico che colpisce generalmente gli uomini, ma non risparmia neanche le donne. Si verifica in maggior misura nelle ascelle ma si può presentare anche in fronte e nella regione palmo-plantare.', 1),
(5, 5, 'Fili PDO', 'Sono fili riassorbibili che vengono inseriti nel derma tramite aghi sottili. La sua azione è il riposizionamento parziale del tessuto cadente. Si utilizza nelle lassità cutanee del viso e di aree specifiche del corpo (interno braccia, interno cosce, addome, ginocchia).', 1),
(6, 6, 'Epilazione laser', 'L’energia del laser diodo danneggia i follicoli dei peli ed altera la relativa capacità di svilupparne altri. Col tempo i peli cadranno e la crescita sarà rallentata fino alla progressiva scomparsa.', 1),
(7, 7, 'Needling medico', 'La cute è sottoposta a microperforazioni multiple, effettuate tramite una pennetta dotata di piccoli aghi (1,5 mm), previa applicazione di anestesia topica. L’obbiettivo è quello di attivare la produzione di collagene ed elastina attraverso la stimolazione del tuo tessuto.', 1),
(8, 8, 'Filler', 'Questo trattamento permette di armonizzare e rallentare l’invecchiamento fisiologico del corpo. Il prodotto più utilizzato è l’acido ialuronico e viene applicato, con tecnica iniettiva, in maggior misura sul volto.\n<br></br>\nIl <strong>beneficio</strong> più importante è un volto più <strong>fresco, equilibrato e riposato</strong> attraverso diverse procedure che permettono di:', 1),
(9, 9, 'Peeling', 'Consiste nell’applicazione di uno o più acidi (miscelati) come l’acido mandelico, piruvico, salicilico, cogico, ecc. in grado di attivare la produzione di collagene ed elastina per rivitalizzare e ringiovanire la pelle.', 1),
(10, 10, 'Mesoterapia', 'È una tecnica iniettiva per somministrare un prodotto a livello intradermico in un’area anatomica ben definita sul volto e/o sul corpo.', 1),
(11, 11, 'Rinofiller', 'È una tecnica che permette di migliorare in maniera concreta e rapida il profilo del naso, senza ricorrere ad interventi chirurgici. Il trattamento prevede l’uso di filler (acido ialuronico) che riempiono le aree irregolari, alzano la radice del naso o sollevano la punta.  ', 1),
(12, 12, 'Carbossiterapia', 'Somministrazione sottocutanea, tramite piccole iniezioni, di anidride carbonica. Questo provoca la liberazione di ossigeno nell’organismo ottenendo diversi benefici.', 1),
(13, 13, 'Sublimazione dermica (Blefaroplastica non chirurgica)', 'Questa è una tecnica che permette di accorciare la cute in eccesso delle palpebre senza dover incidere per asportarla. Si effettua senza anestesia (se non locale con pomate a base di lidocaina), non necessita di sutura.\n<br></br>\nSi usa anche per asportare tutto quello che potrebbe sporgere dalla pelle, come fibromi, verruche, xantelasmi, ecc.\n', 1),
(14, 1, 'Biostimulation', 'It is an injectable treatment that is used to stimulate the dermis by acting on the production of substances such as collagen and hyaluronic acid that guarantee the elasticity and tonicity of the skin.', 2),
(15, 1, 'Bioestimulación', 'Se trata de un trattamiento inyectivo que estimula la dermis favoreciendo la producción de sustancias como colágeno y ácido hialurónico que garantizan la elasticidad y tonicidad de la piel.', 3),
(16, 2, 'Biorevitalización', 'Consiste en un mix de hialurónicos a bajo y alto peso molecular y de larga duración que asociado a una técnica inyectiva novedosa donan un natural y agradable efecto estético.', 3),
(17, 3, 'Botox', 'Es un tratamiento inyectivo. Su principal acción es reducir de manera selectiva la fuerza de cantracción de los músculos que intervienen en la mímica facial.', 3),
(18, 4, 'Hiperidrosis con Botox', 'En palabras simples, <i>sudoración eccesiva.</i> Es un evento fisiológico que se presenta mayormente en los hombres, aunque si muchas mujeres no escapan de ello. Aunque si se verifica en mayor medida en las axilas, también se puede presentar en la frente y región palmo-plantar.', 3),
(19, 5, 'Hilos PDO', 'Se trata de hilos absorbibles que se insertan en la dermis a través de agujas muy delgadas, su acción se basa en la reposición parcial de los tejidos con poca tonicidad. Se utiliza en la relajación cutánea del rostro y de áreas específicas del cuerpo (interno de los brazos, interno de las piernas, abdome, rodillas).', 3),
(20, 6, 'Depilación Laser', 'La energía del laser de diodo daña el folículo de los vellos y altera la relativa capacidad para desarrollar otros. Con el tiempo los vellos caerán y su crecimiento se enlentecerá hasta su progresiva desaparición.', 3),
(21, 7, 'Needling Médico', 'La piel es expuesta a microperforaciones múltiples, efectuadas a través de una especie de lápiz con agujas ultra finas y aplicación previa de anestesía tópica. El objetivo es el de activar la producción de cológeno y elastina para obtener una serie de resultados beneficiosos.', 3),
(22, 8, 'Filler ', 'Este tratamiento ayuda a arminizar y desacelerar el envejecimiento fisiológico del cuerpo. El producto màs utilizado es el àcido hialurónico y se aplica a través de inyecciones.\n<br></br>\nEl <strong>beneficio</strong> más importante es el resultado de un rostro más <strong>fresco, equilibrado y reposado,</strong> esto se obtiene a través de la estimulación que produce el ácido hialurónico en los tejidos.', 3),
(23, 9, 'Peeling', 'Consiste en la aplicación de uno o más ácidos (mezclados) como el ácido mandelico, pirúvico, glicólico, salicílico, etc, en grado de activar la producción de colágeno y elastina para revitalizar y rejuvenecer la piel.', 3),
(24, 10, 'Mesoterapia', 'Es una técnica inyectiva a través de la cual se suministras farmacos o productos homeopáticos a nivel intradérmico en una zona anatómica definida en el rostro y/o el cuerpo.', 3),
(25, 11, 'Rinofiller', 'Es una técnica que permite mejorar de manera concreta y rápida el perfil de la nariz, sin recurrir a la intervención quirúrgica. El tratamiento prevee el uso de ácido hialurónico para rellenar el área irregular y/o alzar la punta de la nariz.', 3),
(26, 12, 'Carboxiterapia', 'Se trata de la suministración subcutánea de dióxido de carbono a través de pequeñas infiltraciones en los tejidos afectos. Esto provoca la liberación de oxígeno en el organismo obteniendo diferentes beneficios.', 3),
(27, 13, 'Sublimación Dérmica (Blefaroplástica sin cirugía)', 'Se trata de una técnica que permite acortar la piel, de los parpados superiores, en ecceso, sin tener que realizar un intervento quirúrgico. Se efectúa aplicando una pomada anestética y luego se procede a la sublimación de la piel. No se realizan suturas. \n<br></br>\nTambién se usa para eliminar todo lo que pueda sobre salir de la piel, como pequeños fibromas, verrugas, xantelasma, etc.\n', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'roberto.rossi77@gmail.com', '$2y$08$thYAVQ43pc.dHmdNDhQz/evLOaAKHbthgR/dkfc7MIjpEwCcDYdMy', '', 'roberto.rossi77@gmail.com', '', NULL, NULL, NULL, 1268889823, 1519902938, 1, 'Roberto', 'Rossi', 'ADMIN', '4234234'),
(3, '127.0.0.1', 'sei@seiessenzialmentebella.com', '$2y$08$FY9Hvlbhhg8tIQVWagvKDegFz0xIICm8uqOiduDibtEvTsEaNtve6', NULL, 'sei@seiessenzialmentebella.com', NULL, 'CjsboKf1IOZHPmrbpLT9tub695a8165e4e961a7a', 1509700421, NULL, 1508231534, 1520264460, 1, 'Orlena', 'Zotti', NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(4, 3, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `benefici`
--
ALTER TABLE `benefici`
  ADD PRIMARY KEY (`id_benefici`);

--
-- Indexes for table `benefici_traduzioni`
--
ALTER TABLE `benefici_traduzioni`
  ADD PRIMARY KEY (`id_benefici_traduzioni`);

--
-- Indexes for table `commenti`
--
ALTER TABLE `commenti`
  ADD PRIMARY KEY (`id_commento`);

--
-- Indexes for table `constants_framework`
--
ALTER TABLE `constants_framework`
  ADD PRIMARY KEY (`id_cf`);

--
-- Indexes for table `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  ADD PRIMARY KEY (`id_contatto`);

--
-- Indexes for table `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  ADD PRIMARY KEY (`id_contatto_newsletter`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id_template`);

--
-- Indexes for table `email_templates_traduzioni`
--
ALTER TABLE `email_templates_traduzioni`
  ADD PRIMARY KEY (`id_template_trad`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indexes for table `faq_traduzioni`
--
ALTER TABLE `faq_traduzioni`
  ADD PRIMARY KEY (`id_faq_traduzioni`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lingue`
--
ALTER TABLE `lingue`
  ADD PRIMARY KEY (`id_lingue`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagine`
--
ALTER TABLE `pagine`
  ADD PRIMARY KEY (`id_pagina`);

--
-- Indexes for table `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  ADD PRIMARY KEY (`id_pc`);

--
-- Indexes for table `programmi`
--
ALTER TABLE `programmi`
  ADD PRIMARY KEY (`id_programma`);

--
-- Indexes for table `programmi_benefici`
--
ALTER TABLE `programmi_benefici`
  ADD PRIMARY KEY (`id_programmi_benefici`);

--
-- Indexes for table `programmi_gallery`
--
ALTER TABLE `programmi_gallery`
  ADD PRIMARY KEY (`id_programmi_gallery`);

--
-- Indexes for table `programmi_traduzioni`
--
ALTER TABLE `programmi_traduzioni`
  ADD PRIMARY KEY (`id_programmi_traduzioni`);

--
-- Indexes for table `programmi_trattamenti`
--
ALTER TABLE `programmi_trattamenti`
  ADD PRIMARY KEY (`id_programmi_trattamenti`);

--
-- Indexes for table `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  ADD PRIMARY KEY (`stato_descrizione_id`);

--
-- Indexes for table `studi_medici`
--
ALTER TABLE `studi_medici`
  ADD PRIMARY KEY (`id_studio_medico`);

--
-- Indexes for table `tipo_template_default`
--
ALTER TABLE `tipo_template_default`
  ADD PRIMARY KEY (`id_tipo_template_default`);

--
-- Indexes for table `traduzioni_file_lang`
--
ALTER TABLE `traduzioni_file_lang`
  ADD PRIMARY KEY (`id_tfl`);

--
-- Indexes for table `trattamenti`
--
ALTER TABLE `trattamenti`
  ADD PRIMARY KEY (`id_trattamento`);

--
-- Indexes for table `trattamenti_benefici`
--
ALTER TABLE `trattamenti_benefici`
  ADD PRIMARY KEY (`id_trattamenti_benefici`);

--
-- Indexes for table `trattamenti_faq`
--
ALTER TABLE `trattamenti_faq`
  ADD PRIMARY KEY (`id_trattamenti_faq`);

--
-- Indexes for table `trattamenti_traduzioni`
--
ALTER TABLE `trattamenti_traduzioni`
  ADD PRIMARY KEY (`id_trattamenti_traduzioni`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`), ADD KEY `fk_users_groups_users1_idx` (`user_id`), ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `benefici`
--
ALTER TABLE `benefici`
  MODIFY `id_benefici` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `benefici_traduzioni`
--
ALTER TABLE `benefici_traduzioni`
  MODIFY `id_benefici_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=199;
--
-- AUTO_INCREMENT for table `commenti`
--
ALTER TABLE `commenti`
  MODIFY `id_commento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `constants_framework`
--
ALTER TABLE `constants_framework`
  MODIFY `id_cf` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  MODIFY `id_contatto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  MODIFY `id_contatto_newsletter` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `email_templates_traduzioni`
--
ALTER TABLE `email_templates_traduzioni`
  MODIFY `id_template_trad` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `faq_traduzioni`
--
ALTER TABLE `faq_traduzioni`
  MODIFY `id_faq_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lingue`
--
ALTER TABLE `lingue`
  MODIFY `id_lingue` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pagine`
--
ALTER TABLE `pagine`
  MODIFY `id_pagina` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  MODIFY `id_pc` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `programmi`
--
ALTER TABLE `programmi`
  MODIFY `id_programma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `programmi_benefici`
--
ALTER TABLE `programmi_benefici`
  MODIFY `id_programmi_benefici` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `programmi_gallery`
--
ALTER TABLE `programmi_gallery`
  MODIFY `id_programmi_gallery` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `programmi_traduzioni`
--
ALTER TABLE `programmi_traduzioni`
  MODIFY `id_programmi_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `programmi_trattamenti`
--
ALTER TABLE `programmi_trattamenti`
  MODIFY `id_programmi_trattamenti` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `studi_medici`
--
ALTER TABLE `studi_medici`
  MODIFY `id_studio_medico` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tipo_template_default`
--
ALTER TABLE `tipo_template_default`
  MODIFY `id_tipo_template_default` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `traduzioni_file_lang`
--
ALTER TABLE `traduzioni_file_lang`
  MODIFY `id_tfl` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `trattamenti`
--
ALTER TABLE `trattamenti`
  MODIFY `id_trattamento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `trattamenti_benefici`
--
ALTER TABLE `trattamenti_benefici`
  MODIFY `id_trattamenti_benefici` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `trattamenti_faq`
--
ALTER TABLE `trattamenti_faq`
  MODIFY `id_trattamenti_faq` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `trattamenti_traduzioni`
--
ALTER TABLE `trattamenti_traduzioni`
  MODIFY `id_trattamenti_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `users_groups`
--
ALTER TABLE `users_groups`
ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
