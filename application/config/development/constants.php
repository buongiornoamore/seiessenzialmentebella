<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
/* Global frmework DEVELOPMENT */
define('SITE_URL_PATH',		               		'http://localhost/seiessenzialmentebella');
define('SITE_TITLE_NAME',		               	'Sei essenzialmente bella');
/* Email smtp global vars */
define('SMTP_HOST_CUSTOM',		                'secureit18.sgcpanel.com');
define('SMTP_USER_CUSTOM',		                'sei@seiessenzialmentebella.com');
define('SMTP_PASS_CUSTOM',		                'oz17@Sei');
/* Database MySql global vars */
define('DB_HOSTNAME',	    	                'localhost');
define('DB_USERNAME',	    	                'root');
define('DB_PASSWORD',	    	                '');
define('DB_DATABASE',	    	                'seiessen_framework');
/* Company global vars */
define('COMPANY_NAME',		                    'Seiessenzialmentebella');
define('COMPANY_EMAIL',		                    'sei@seiessenzialmentebella.com');
define('COMPANY_COPYRIGHT',		                '&copy; 2017 Seiessenzialmentebella');
define('COMPANY_PHONE',		                    ' +39 3923576272');
define('COMPANY_ADDRESS',		                'Sardegna');
define('GPLUS_LINK',		                    '');
define('YOUTUBE_LINK',		                    '');
define('PINTEREST_LINK',		                '');
define('TWITTER_LINK',		                    '');
define('FACEBOOK_LINK',		                    'https://www.facebook.com/dottoressaorlenazotti/');
define('INSTAGRAM_LINK',		                'https://www.instagram.com/dottoressaorlenazotti/');
define('GOOGLE_ANALITYCS_ID',		            'UA-108374307-1');
/* Coming soon OPTIONS */
define('COMNINGSOON_LOGO',		                'seibella.png');
define('COMNINGSOON_BACK_IMAGE',		        'yoga.jpg');
define('COMNINGSOON_BTN_COLOR',		            '23bdc3');
/* Payments info */
define('PAYPAL_EMAIL',		                    '');
define('PAYPAL_ENV',		                    '');
define('PAYPAL_SANDBOX_CLIENT_ID',		        '');
define('PAYPAL_LIVE_CLIENT_ID',		            '');
define('STRIPE_PK',	    	                    '');
define('STRIPE_SK',		                        '');
/* Frontend assets global vars */
define('ASSETS_ROOT_FOLDER_FRONTEND',		    SITE_URL_PATH . '/assets/assets-frontend');
define('ASSETS_ROOT_FOLDER_FRONTEND_CSS',		SITE_URL_PATH . '/assets/assets-frontend/css');
define('ASSETS_ROOT_FOLDER_FRONTEND_JS',		SITE_URL_PATH . '/assets/assets-frontend/js');
define('ASSETS_ROOT_FOLDER_FRONTEND_IMG',		SITE_URL_PATH . '/assets/assets-frontend/img');
define('ASSETS_ROOT_FOLDER_FRONTEND_FONTS',		SITE_URL_PATH . '/assets/assets-frontend/fonts');
define('ASSETS_ROOT_FOLDER_FRONTEND_SASS',		SITE_URL_PATH . '/assets/assets-frontend/sass');/* ADMIN */
/* ADMIN */
define('ASSETS_ROOT_FOLDER_ADMIN',		        SITE_URL_PATH . '/assets/assets-admin');
define('ASSETS_ROOT_FOLDER_ADMIN_CSS',		    SITE_URL_PATH . '/assets/assets-admin/css');
define('ASSETS_ROOT_FOLDER_ADMIN_JS',		    SITE_URL_PATH . '/assets/assets-admin/js');
define('ASSETS_ROOT_FOLDER_ADMIN_IMG',		    SITE_URL_PATH . '/assets/assets-admin/img');
define('ASSETS_ROOT_FOLDER_ADMIN_FONTS',		SITE_URL_PATH . '/assets/assets-admin/fonts');
define('ASSETS_ROOT_FOLDER_ADMIN_SASS',		    SITE_URL_PATH . '/assets/assets-admin/sass');
/* SITE_URL_ROOT è definita nella ROOT index.php
define('SITE_URL_ROOT', 'machlo.com');
*/
/* End of file constants.php */
/* Location: ./application/config/constants.php */
