<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'defaultcontroller';
$route['404_override'] = 'defaultcontroller/redirect_404';

/* Base url */
$route['comingsoon'] = 'comingsoon';
$route['mail'] = 'Mail';
$route['mail/newsletter'] = 'Mail/newsletter';
$route['mail/send_save'] = 'Mail/send_save';
$route['admin/send_email_template/(:any)'] = 'Mail/send_email_template/$1';
$route['admin/html_preview_email_template/(:any)'] = 'Mail/html_preview_email_template/$1';
$route['admin/send_email_libera'] = 'Mail/send_email_libera';
$route['html_online_viewer/(:any)'] = 'Mail/html_online_viewer/$1';
$route['unsubscribe_confirm/(:any)/(:any)'] = 'admin/Users/unsubscribe_confirm/$1/$2';
$route['unsubscribe/(:any)/(:any)/(:any)'] = 'admin/Users/unsubscribe/$1/$2/$3';
$route['unsubscribe_execute/(:any)/(:any)/(:any)'] = 'admin/Users/unsubscribe_execute/$1/$2/$3';
$route['admin/updateSiteStatus'] = 'admin/AdminConfigurazioni/updateSiteStatus';
$route['sitemap\.xml'] = "Sitemap";
$route['(:any)/sitemap_pages\.xml'] = "Sitemap/pages/$1";
$route['(:any)/sitemap_programmi\.xml'] = "Sitemap/programmi/$1";
$route['(:any)/sitemap_trattamenti\.xml'] = "Sitemap/trattamenti/$1";
$route['(:any)/sitemap_images\.xml'] = "Sitemap/images/$1";
/* Frontend url from tables */
// PAGES
require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();
$query = $db->get('pagine');
$result = $query->result();
foreach( $result as $row )
{
	if($row->url_pagina != '' && $row->controller != '' && $row->url_pagina != 'default_controller') {
    	$route[ $row->url_pagina ]           = $row->controller;
    	//$route[ $row->url_pagina.'/:any' ]   = $row->controller;
    	$route[ $row->controller ]           = 'error404';
    	$route[ $row->controller.'/:any' ]   = 'error404';
	}
}
/*************************/

$route['account'] = 'frontend/Account';
$route['logout'] = 'frontend/Account/logout';
$route['login'] = 'frontend/Account/login';
$route['register'] = 'frontend/Account/register';
$route['salvaAccount'] = 'frontend/Account/salvaDatiProfilo';

/* Admin url */
$route['admin'] = 'admin/Users';
$route['admin/login'] = 'admin/Users/login';
$route['admin/logout'] = 'admin/Users/logout';
$route['admin/forgot'] = 'admin/Users/forgot_password';
$route['admin/reset_password/(:any)'] = 'admin/Users/reset_password/$1';
$route['admin/dashboard'] = 'admin/Dashboard';
$route['admin/register'] = 'admin/Users/register';

$admin_crud_routes = array(
	'programmi' => 'AdminProgrammi',
	'trattamenti' => 'AdminTrattamenti',	
	'benefici' => 'AdminBenefici',
	'faq' => 'AdminFaq',
	'commenti' => 'AdminCommenti',
	'contatti' => 'AdminContatti',
	'studimedici' => 'AdminStudimedici',
	'newsletter' => 'AdminNewsletter',
	'emailtemplates' => 'AdminEmailTemplates',
	'emailtemplatestraduzioni' => 'AdminEmailTemplatesTraduzioni',
	'pagine' => 'AdminPagine',
	'configurazioni' => 'AdminConfigurazioni',
    'retreats' => 'AdminRetreats'
 );

foreach($admin_crud_routes as $key => $value){
	$route['admin/'.$key] = 'admin/'.$value.'/crud';
	$route['admin/'.$key.'/print'] = 'admin/'.$value.'/crud/print';
	$route['admin/'.$key.'/export'] = 'admin/'.$value.'/crud/export';
	$route['admin/'.$key.'/delete_multiple'] = 'admin/'.$value.'/crud/delete_multiple';
	$route['admin/'.$key.'/upload_file/(:any)'] = 'admin/'.$value.'/crud/upload_file/$1';
	$route['admin/'.$key.'/delete_file/(:any)'] = 'admin/'.$value.'/crud/delete_file/$1';
	$route['admin/'.$key.'/add'] = 'admin/'.$value.'/crud/add';
	$route['admin/'.$key.'/insert'] = 'admin/'.$value.'/crud/insert';
	$route['admin/'.$key.'/insert_validation'] = 'admin/'.$value.'/crud/insert_validation';
	$route['admin/'.$key.'/success/:num'] = 'admin/'.$value.'/crud/success';
	$route['admin/'.$key.'/delete/:num'] = 'admin/'.$value.'/crud/delete';
	$route['admin/'.$key.'/edit/:num'] = 'admin/'.$value.'/crud/edit';
	$route['admin/'.$key.'/update_validation/:num'] = 'admin/'.$value.'/crud/update_validation';
	$route['admin/'.$key.'/update/:num'] = 'admin/'.$value.'/crud/update';
	$route['admin/'.$key.'/ajax_list_info'] = 'admin/'.$value.'/crud/ajax_list_info';
	$route['admin/'.$key.'/ajax_list'] = 'admin/'.$value.'/crud/ajax_list';
	$route['admin/'.$key.'/read/:num'] = 'admin/'.$value.'/crud/read';
}
							
/* Admin CUSTOM routes */
// Programmi
$route['admin/programmi/trad/(:any)'] = 'admin/AdminProgrammi/traductions/$1';
$route['admin/programmi/trad/ajax_list_info'] = 'admin/AdminProgrammi/traductions/ajax_list_info';
$route['admin/programmi/trad/ajax_list'] = 'admin/AdminProgrammi/traductions/ajax_list';
$route['admin/programmi/gallery/(:any)'] = 'admin/AdminProgrammi/gallery/$1'; 
// Trattamenti CRUD
$route['admin/trattamenti/trad/(:any)'] = 'admin/AdminTrattamenti/traductions/$1'; 
$route['admin/trattamenti/trad/ajax_list_info'] = 'admin/AdminTrattamenti/traductions/ajax_list_info';
$route['admin/trattamenti/trad/ajax_list'] = 'admin/AdminTrattamenti/traductions/ajax_list';
// Benefici CRUD
$route['admin/benefici/trad/(:any)'] = 'admin/AdminBenefici/traductions/$1'; 
$route['admin/benefici/trad/ajax_list_info'] = 'admin/AdminBenefici/traductions/ajax_list_info';
$route['admin/benefici/trad/ajax_list'] = 'admin/AdminBenefici/traductions/ajax_list';
// FAQ CRUD
$route['admin/faq/trad/(:any)'] = 'admin/AdminFaq/traductions/$1'; 
// Commenti CRUD
// Contatti CRUD
$route['admin/contatti/email/(:any)'] = 'admin/AdminContatti/email/$1'; 
$route['admin/contatti/email/ajax_list_info'] = 'admin/AdminContatti/email/ajax_list_info';
$route['admin/contatti/email/ajax_list'] = 'admin/AdminContatti/email/ajax_list';
// Newsletter CRUD
$route['admin/newsletter/email'] = 'admin/AdminNewsletter/email'; 
$route['admin/newsletter/email/ajax_list_info'] = 'admin/AdminNewsletter/email/ajax_list_info';
$route['admin/newsletter/email/ajax_list'] = 'admin/AdminNewsletter/email/ajax_list';
// Email templates CRUD
/* End of file routes.php */
/* Location: ./application/config/routes.php */
