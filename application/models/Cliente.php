<?
class Cliente extends CI_Model {

	public $id_cliente;
	public $cognome = '';
	public $nome = '';
	public $data_nascita = '';
	public $partita_iva = '';
	public $codice_fiscale = '';
	public $email = '';
	public $telefono_fisso = '';
	public $cellulare = '';
	public $user_id;
	public $fax = '';


	public function get_Cliente_by_User_id($user_id)
	{

		$this->db->select('*');
		$this->db->from('clienti');
		$this->db->join('indirizzo_fatturazione', 'indirizzo_fatturazione.id_cliente = clienti.id_cliente');
		$this->db->join('indirizzo', 'indirizzo_fatturazione.id_indirizzo = indirizzo.id_indirizzo');
		$this->db->where('user_id', $user_id);

		$query = $this->db->get();
		$cliente_info = $query->row(); // ORIGINALE : $user_info = $query->row();
		//log_message('info', '>>>>>>>>>> Cliente  >> get_Cliente_by_User_id >  ' . var_dump($cliente_info) );
		return $cliente_info;
		//return (isset($user_info) ? $user_info : $this->get_blank_array_user_info() ) ;

		// log_message( 'info', '>>>>>>>>>>>>>>>>> .$user_id ' .$user_id );
		// $query = $this->db->query('select address from users_info where user_id = ' .$user_id );
		// $user_info = $query->result();
		//
		// if (isset($user_info)){
		// 	  log_message( 'info', '>>>>>>>>>>>>>>>>> 1' );
		// 	foreach ($query->result() as $row)
		// 	{
		// 		log_message( 'info', '>>>>>>>>>>>>>>>>> 2' );
    //     log_message( 'info', '>>>>>>>>>>>>>>>>> ' . $row->address );
		// 	}
		// }



		// ** Prendo gli indirizzi
		/*
		$this->db->select('*');
		$this->db->from('indirizzo');
		$this->db->where('id_indirizzo', $user_id);

		$query = $this->db->get();
		$user_info = $query->row();
		*/
		// **

	}

}
?>
