<?
class User_info extends CI_Model {

	public $id;
	public $user_id;
	public $country;
	public $city;
	public $address;
	public $postal_code;
	public $address_notes;
	public $newsletter;
	public $points;

	public function get_user_info($user_id)
	{
		$this->db->select('*');
		$this->db->from('users_info');
		$this->db->where('user_id', $user_id);
		
		$query = $this->db->get();
		$user_info = $query->row();
		return $user_info;
	}
	
}
?>