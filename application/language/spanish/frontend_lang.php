<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Frontend language - spanish
*/
/* LANG CONTENTS */
$lang['id_tfl'] = "3";
$lang['LANGUAGE_NAME'] = "ESPANOL";
$lang['LABEL_SURNAME'] = "Apellido";
$lang['LABEL_CITY'] = "País";
$lang['LANGUAGE_ABBR'] = "ES";
$lang['LANGUAGE_ID'] = "3";
$lang['LANGUAGE_LOCALE_PAYPAL'] = "es_ES";
$lang['LANGUAGE_CI_CODE'] = "spanish";
$lang['MENU_HOME'] = "Inicio";
$lang['MENU_ABOUTME'] = "Sobre mi";
$lang['MENU_BIO'] = "Mini biografía";
$lang['MENU_MYWORK'] = "Mi trabajo";
$lang['MENU_PROGRAMS'] = "Programas";
$lang['MENU_TREATMENTS'] = "Tratamientos";
$lang['MENU_STUDIES'] = "Consultorio Médico";
$lang['MENU_CONTACTME'] = "Contáctame";
$lang['MENU_LANGUAGE'] = "Idiomas";
$lang['MENU_PRIVACY'] = "Politica de privacidad";
$lang['MENU_GRATIS'] = "Gratis";
$lang['MENU_RETREATS'] = "Escapadas";
$lang['SUBSCRIBE_BTN'] = "Sí, quiero unirme";
$lang['SEND_MESSAGE_BTN'] = "Envía mensaje";
$lang['SEND_AREYOUSURE_BTN'] = "¿Estás seguro?";
$lang['SUBSCRIBE_NEWSLETTER_H1'] = "Inscríbete a los boletines";
$lang['SUBSCRIBE_NEWSLETTER_DESC'] = "Comienza a recibir consejos, inspiración e información que te pueden ayudar a tener una vida llena de satisfacción.";
$lang['SUBSCRIBE_NEWSLETTER_NAME'] = "Tu nombre";
$lang['SUBSCRIBE_NEWSLETTER_EMAIL'] = "Tu email";
$lang['SUBSCRIBE_NEWSLETTER_PRIVACY'] = "Doy mi consentimiento para el procesamiento de datos y confirmo la política de privacidad*";
$lang['PROGRAM_TREATMENTS'] = "Tratamientos del programa";
$lang['PROGRAM_TREATMENTS_DESC'] = "Haz click sobre el link para ver los detalles de cada tratamiento";
$lang['BENEFITS_QUESTION'] = "&#191;Qué beneficios tiene para tí ?";
$lang['CONTACTME_DESC'] = "Si tienes preguntas, quieres una cita o quieres pedir información.";
$lang['LABEL_NAME'] = "Nombre";
$lang['LABEL_EMAIL'] = "Email";
$lang['LABEL_PHONE'] = "Teléfono";
$lang['LABEL_LOCATION'] = "Consultorio Médico Zona";
$lang['LABEL_MESSAGE'] = "Mensaje";
$lang['LABEL_CHOOSE'] = "Elige...";
$lang['LABEL_FAQ'] = "Preguntas Frecuentes";
$lang['LABEL_UNSUBSCRIBE'] = "Darse de baja";
$lang['MSG_REQUIRED_NAME'] = "Escribe tu nombre";
$lang['MSG_REQUIRED_SURNAME'] = "Escribe tu Apellido";
$lang['MSG_REQUIRED_CITY'] = "Escribe tu pàis";
$lang['MSG_REQUIRED_EMAIL'] = "Escribe tu email";
$lang['MSG_VALID_EMAIL'] = "Escribe un email válido";
$lang['MSG_REQUIRED_PHONE'] = "Escribe tu teléfono";
$lang['MSG_VALID_PHONE'] = "Escribe un número válido";
$lang['MSG_REQUIRED_OFFICE'] = "Selecciona un consultorio médico";
$lang['MSG_REQUIRED_MESSAGE'] = "Escribe el mensaje";
$lang['MSG_REQUIRED_CHECK'] = "
Debes dar tu consentimiento al tratamiento y datos personales";
$lang['MSG_SUCCESS_CONTACT'] = "Tu mensaje ha sido enviado correctamente. ¡ Gracias !";
$lang['MSG_FAILURE_CONTACT'] = "Hemos encontrado un problema en el envío del mensaje. ¡Intenta otra vez!";
$lang['MSG_SUCCESS_NEWSLETTER'] = "Ahora estás suscrito al boletín. Gracias!";
$lang['MSG_UNIQUE_NEWSLETTER'] = "Su dirección de correo electrónico ya está incluida en el boletín.";
$lang['MSG_UNSUBSCRIBE_DONE'] = "Su correo electrónico / registro ha sido eliminado. Gracias!";
$lang['MSG_UNSUBSCRIBE_NOTFOUND'] = "El contacto requerido no está registrado actualmente";
$lang['HOME_SERVICIOS_PATH'] = "servicios.png";
$lang['id_lingua'] = "3";
/* PAGES CONTENTS */
$lang['HOME_ID_PC'] = "3";
$lang['HOME_PC_PAGE_CODE'] = "HOME";
$lang['HOME_PC_TITOLO_1'] = "Home";
$lang['HOME_PC_IMMAGINE_1'] = "tiracconto3.jpg";
$lang['HOME_PC_TESTO_BODY_1'] = "Te cuento...<br><br>Una gran parte de tus miedos a la medicina estética se alimenta de la sociedad en la que vives, que te hace ver los resultados más inapropiados provocados por quien no tiene la capacidad y/o preparación en esta disciplina o quien te hace creer que la belleza física no cuenta.<br><br>
    Para mí cuenta (y seamos sinceras, vives en un mundo que es materia), pero cuenta cuando se tiene en <span class=\"blu-text\"><b>equilibrio la belleza externa y la belleza interna.</b></span><br>
    Quizás no conoces la Medicina Estética y si la “conoces” (esto no significa que sabes verdaderamente de que se trata), sientes miedo porque piensas que te inflamarás, te transformarás o te convertirás en otra.<br><br>
    Yo <span class=\"orange-text\"><b>te ofrezco armonía y equilibrio</b></span>, serás quien eres fisicamente, pero te verás y te verán <span class=\"blu-text\"><b>mejor, reposada, fresca y fantástica </b></span> y si me lo permites te guiaré a tu renacer interior.
    <br><span class=\"orange-text\"><b>Rompe las creencias limitantes</b></span>, sobre la bellezza externa,<span class=\"blu-text\"><b>no te avergüences </b></span> de sentirte bien dentro de tu propia piel y de buscar ayuda para sentirte más bonita.<br><br>
    <span class=\"orange-text\"><b>Haz aquello que sientes</b></span> que es mejor para tí, <span class=\"blu-text\"><b>pero siéntelo con el corazón</b></span> y vive con placer la <span class=\"orange-text\"><b>armonía externa y equilibrio interno</b></span> que podrías lograr si así lo quieres.<br><br><span class=\"blu-text text-24\"><b>¡Todo depende de tí!</b></span>";
$lang['HOME_PC_TITOLO_2'] = "";
$lang['HOME_PC_IMMAGINE_2'] = "";
$lang['HOME_PC_TESTO_BODY_2'] = "";
$lang['HOME_PC_TESTO_BOLD_1'] = "Hola, soy Orlena Zotti, medico estético, mi negocio se llama <span class=\"orange-text\"><b>\"Eres Esencialmente Bella\"</b></span>.<br>Yo ayudo a las mujeres a sentirse bien dentro de su piel a través del amor propio y el cuidado propio. Amo ofrecer herramientas a las mujeres que quieren un cambio y buscan conectar con la esencia de la belleza.";
$lang['HOME_PC_TESTO_BOLD_2'] = "<span class=\"text-24\"><b>¿Qué es la medicina estética para mi?</b></span><br>
La definiría como un conjunto de tratamientos médicos, realizados con procedimientos poco o para nada invasivos, que son alternativos a la cirugía plástica, logrando así armonizar la belleza física.";
$lang['HOME_PC_TESTO_BOLD_3'] = "“La Medicina Estética es un viaje, un recorrido, que se realiza paso a paso.
El objetivo es hacerte sentir bien dentro de tu propia piel en un modo natural y equilibrado.”";
$lang['HOME_PC_TOP_IMAGE'] = "6dc2c-ina-11.png";
$lang['HOME_PC_TOP_IMAGE_MOBILE'] = "7250b-ima-11.png";
$lang['HOME_ID_LINGUA'] = "3";
$lang['SUDIME_ID_PC'] = "6";
$lang['SUDIME_PC_PAGE_CODE'] = "SUDIME";
$lang['SUDIME_PC_TITOLO_1'] = "Sobre mi";
$lang['SUDIME_PC_IMMAGINE_1'] = "sudime.jpg";
$lang['SUDIME_PC_TESTO_BODY_1'] = "<p align=\"justify\"><br>A este país (Italia) me trajo la soledad, el sentirme un pez fuera del agua, el sentir que no pertenecía al lugar donde me encontraba, ni a la manera de hacer la medicina estética que hacía, pensé que cambiando de ambiente podría mejorar el como me sentía, pero esto no sucedió.
                        </p>
                        <p align=\"justify\">
                            Entonces comencé a buscar lo que fuera que me ayudara para comprender que quería de verdad, que me hacía feliz y sobre todo que vine a hacer en este mundo.
                        </p>
                        <p align=\"justify\">
                        	Inicié perfeccionando mis conocimientos como médico estético, leí muchos libros sobre evolución espiritual, conocí el mundo de la meditación, hice muchos cursos de crecimiento personal y de como nutrir mi cuerpo y todo esto se sumó a mi pasión: la medicina estética.
                        </p>
                        <p align=\"justify\">
                        	Así me invadieron las ganas de aplicar a la medicina estética este recorrido interno y fue cuando empecé a unificar en programas todas las herramientas que he experimentado.<br><br>Esto me ha permitido crear una forma de ofrecer ayuda a las mujeres, armonizando la belleza externa y equilibrando la belleza interna, acomplando dos mundos que la sociedad a vendido como diferentes pero que para mí son inseparables porque los dos se nutren equilibradamente.
                        </p>";
$lang['SUDIME_PC_TITOLO_2'] = "Mini biografía";
$lang['SUDIME_PC_IMMAGINE_2'] = "";
$lang['SUDIME_PC_TESTO_BODY_2'] = "<p>
                      Me gradué de Médico Cirujano en Venezuela (mi país de nacimiento) en el 2005, casi inmediatamente después, conocí el mundo de la medicina estética y me enamoré de ella, tanto que la trasformé en mi única profesión.
                    </p>
                    <p>
                        Legué a Italia en el 2010 y realicé todo el procedimiento para validar mi título en este país, que llevo en mi sangre gracias a mi abuelo.
                    </p>
                    <p>
                        Para perfeccionar mis conocimientos en este campo seguí un curso de cuatro años opteniendo un Diploma de Medicina Estética de la Fondazione Internazionale Fatebenefratelli en Roma.
                    </p>
                    <p>
                      Ahora me encuentro en Cerdeña, donde, gracias a mi patrimonio de conocimientos, desarrollo mi trabajo ofreciendo un concepto de medicina estetica único y equilibrado.
                    </p>
                    <p>
                        ";
$lang['SUDIME_PC_TESTO_BOLD_1'] = "";
$lang['SUDIME_PC_TESTO_BOLD_2'] = "";
$lang['SUDIME_PC_TESTO_BOLD_3'] = "";
$lang['SUDIME_PC_TOP_IMAGE'] = "";
$lang['SUDIME_PC_TOP_IMAGE_MOBILE'] = "";
$lang['SUDIME_ID_LINGUA'] = "3";
$lang['MIOLAVORO_ID_PC'] = "9";
$lang['MIOLAVORO_PC_PAGE_CODE'] = "MIOLAVORO";
$lang['MIOLAVORO_PC_TITOLO_1'] = "Mi trabajo";
$lang['MIOLAVORO_PC_IMMAGINE_1'] = "ilmiolavoro.jpg";
$lang['MIOLAVORO_PC_TESTO_BODY_1'] = "<p align=\"justify\">
                            Es una verdadera misión  integrar belleza física y crecimiento interno,  porque en este despertar de consciencia que estamos teniendo,  \"nuestro cuerpo es nuestro templo\". Pero actualmente en este despertar este frase sirve solo si tú cuidas tu cuerpo desde la alimentación y la actividad física, de esta manera te mantendrás en equilibrio contigo misma.
                        </p>
                        <p align=\"justify\"> 
¿Pero cómo uno puede incluir la bellezza en este tiempo de tomar consciencia?  
Por muchos años la belleza física ha sido atacada y ha sido tomada como superficial, porque no importa el como te veas (fea o bonita) o te sientas,  si no quién eres y cómo eres. Por eso lo que yo propongo, por ejemplo, es armonizar tu belleza,  que las arrugas sean menos evidentes y que tu rostro se vea más reposado y fresco. No te avergüences por querer verte mejor y sentirte bien dentro de tu propia piel.
                        </p>
                        <p align=\"justify\">
                        	Regala a tu cuerpo y a tu rostro ese aire fresco que ha perdido con los años y solo es eso,  aire fresco,  no es una reconstrucción,  no es magia,  no es convertirte en otra persona. No es que dejes de ser quien eres. Yo te ayudo a encontrar esa belleza que no ves o esa belleza que piensas haber perdido. Te ayudo a que te empieces a ver con otros ojos,  con ojos de amor por ti misma.
                        </p>
                        <p align=\"justify\">
                        	Lo que a muchos le funciona de adentro hacia fuera a otros les funciona de manera increíble de afuera hacia adentro; porque muchas veces viendo al espejo lo que se puede obtener eso hace que la luz interna brille  con más intensidad y que empieces a ver lo que estaba escondido,  por el desgaste de los años,  por la falta de amor a tí misma,  por falta de tiempo.
                        </p>";
$lang['MIOLAVORO_PC_TITOLO_2'] = "";
$lang['MIOLAVORO_PC_IMMAGINE_2'] = "";
$lang['MIOLAVORO_PC_TESTO_BODY_2'] = "";
$lang['MIOLAVORO_PC_TESTO_BOLD_1'] = "Recuerda que tu cuerpo es tu templo y que muchas veces la actividad física y la alimentación más saludable no basta,  también es importante para tu templo el aire fresco.";
$lang['MIOLAVORO_PC_TESTO_BOLD_2'] = "";
$lang['MIOLAVORO_PC_TESTO_BOLD_3'] = "";
$lang['MIOLAVORO_PC_TOP_IMAGE'] = "";
$lang['MIOLAVORO_PC_TOP_IMAGE_MOBILE'] = "";
$lang['MIOLAVORO_ID_LINGUA'] = "3";
$lang['PRIVACY_ID_PC'] = "12";
$lang['PRIVACY_PC_PAGE_CODE'] = "PRIVACY";
$lang['PRIVACY_PC_TITOLO_1'] = "Politica de privacidad";
$lang['PRIVACY_PC_IMMAGINE_1'] = "";
$lang['PRIVACY_PC_TESTO_BODY_1'] = "<p align=\"justify\">De conformidad con el Decreto Legislativo no. 196, Sei essenzialmente Bella,  es autorizado por el usuario al uso de la información personal que proporcione, dentro de la actividad. Fuente de datos personales. Los datos personales a los que nos referimos se recogen directamente en este sitio web. Todos estos datos serán procesados en cumplimiento de dicha ley y los requisitos de confidencialidad que siempre se han inspirado en la actividad del estudio.</p>
                    <p align=\"justify\">
                       <b>Se recogerán los siguientes datos:</b>
                       <ul class=\"privacy-ul\">
                        <li>dirección IP del usuario;</li>
                        <li>dirección de correo electrónico personal;</li>
                        <li>URL de origen;</li>
                        <li>número de teléfono</li>
                        <li>nombre</li>
                        <li>pais></li>
                        <li>apellido</li>
                       </ul>
                    </p>
                    <p align=\"left\">
                       <b>Finalidad del tratamiento al que se destinan los datos personales que usted proporciona:</b>
                       <ul class=\"privacy-ul\">
                       	<li>apoyo y complemento del acceso;</li>
                        <li>para hacer cumplir las obligaciones legales;</li>
                        <li>para las necesidades operacionales y de gestión;</li>
                        <li>para promociones y comunicaciones promocionales reservadas a los miembros del sitio;</li>
					  </ul>
                    </p>
                    <p align=\"justify\">MMétodos de procesamiento de datos en relación con las finalidades indicadas, el tratamiento de datos personales son tratados con herramientas manuales, equipo y datos con lógica estrictamente relacionados con los mismos fines y, de todos modos, a fin de garantizar la seguridad y confidencialidad de los datos. Los datos se procesan utilizando medios adecuados para garantizar la seguridad y la confidencialidad y se pueden realizar usando herramientas automatizadas para almacenar, gestionar y transmitir los datos. Los datos proporcionados no serán cedidos y / o revendidos a terceros.<br><br>
Los derechos a que se refiere el artículo 7. Le informamos, que el art. 7 del Decreto Legislativo 196/2003 otorga a los interesados el ejercicio de derechos específicos. En particular, el interesado podrá obtener del registrador la confirmación de la existencia o no de sus datos personales y que dichos datos se pondrán a su disposición de forma inteligible. El interesado también puede solicitar conocer el origen de los datos personales, la finalidad y la forma de tratamiento; la lógica aplicada en el caso de la transformación realizada con ayuda de instrumentos electrónicos; los datos de identificación del titular y del administrador; para obtener la cancelación, la transformación en forma anónima o el bloqueo de los datos tratados en violación de la ley y la actualización, corrección o, si está interesado, la integración de los datos; para oponerse, por motivos legítimos, al tratamiento de los datos que le conciernen también con el fin de enviar material publicitario o ventas directas.<br><br>Los derechos en cuestión también podrán ejercerse, incluso a través de una persona encargada, mediante una solicitud dirigida a la persona designada por carta. En el ejercicio de los derechos, la persona interesada podrá conferir por escrito, delegar o procurar a personas físicas, organismos, asociaciones u organismos. Sede legal: Via sumirago,31,00188. Roma. Italia
                    </p>
					<p align=\"justify\">
					<b>Acerca de las cookies</b><br/>
					Las cookies son cadenas de texto (pequeñas porciones de información) almacenadas en computadoras, tabletas, smartphones, portátiles, reutilizables durante la misma visita (cookies de sesión) o retransmitidas en los mismos sitios en una visita posterior.<br/><br/>Según el art. 13 del Decreto Legislativo núm. 196/2003 este sitio usa solamente cookies tecnicos o relacionadas a estos que no requieren consentimiento previo; son necesarios para el buen funcionamiento del web, sirven para hacer la navegación.
					</p>";
$lang['PRIVACY_PC_TITOLO_2'] = "";
$lang['PRIVACY_PC_IMMAGINE_2'] = "";
$lang['PRIVACY_PC_TESTO_BODY_2'] = "";
$lang['PRIVACY_PC_TESTO_BOLD_1'] = "";
$lang['PRIVACY_PC_TESTO_BOLD_2'] = "";
$lang['PRIVACY_PC_TESTO_BOLD_3'] = "";
$lang['PRIVACY_PC_TOP_IMAGE'] = "";
$lang['PRIVACY_PC_TOP_IMAGE_MOBILE'] = "";
$lang['PRIVACY_ID_LINGUA'] = "3";
$lang['GRATIS_ID_PC'] = "14";
$lang['GRATIS_PC_PAGE_CODE'] = "GRATIS";
$lang['GRATIS_PC_TITOLO_1'] = "Gratis";
$lang['GRATIS_PC_IMMAGINE_1'] = "";
$lang['GRATIS_PC_TESTO_BODY_1'] = "<p align=\"justify\">Gratis es</p>";
$lang['GRATIS_PC_TITOLO_2'] = "";
$lang['GRATIS_PC_IMMAGINE_2'] = "";
$lang['GRATIS_PC_TESTO_BODY_2'] = "";
$lang['GRATIS_PC_TESTO_BOLD_1'] = "";
$lang['GRATIS_PC_TESTO_BOLD_2'] = "";
$lang['GRATIS_PC_TESTO_BOLD_3'] = "";
$lang['GRATIS_PC_TOP_IMAGE'] = "";
$lang['GRATIS_PC_TOP_IMAGE_MOBILE'] = "";
$lang['GRATIS_ID_LINGUA'] = "3";
