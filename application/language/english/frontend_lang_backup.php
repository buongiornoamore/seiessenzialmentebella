<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Frontend language - English
*/
// LANG from Lingue table
$lang['LANGUAGE_NAME'] = 'ENGLISH';
$lang['LANGUAGE_ABBR'] = 'EN';
$lang['LANGUAGE_ID'] = '2';
$lang['LANGUAGE_LOCALE_PAYPAL'] = 'en_GB';
$lang['LANGUAGE_CI_CODE'] = 'english';
// MENU
$lang['MENU_HOME'] = 'Home';
$lang['MENU_ABOUTME'] = 'About me';
$lang['MENU_BIO'] = 'Mini biography';
$lang['MENU_MYWORK'] = 'My work';
$lang['MENU_PROGRAMS'] = 'Programs';
$lang['MENU_TREATMENTS'] = 'Treatments';
$lang['MENU_STUDIES'] = 'Medical offices';
$lang['MENU_CONTACTME'] = 'Contact me';
$lang['MENU_LANGUAGE'] = 'Language';
$lang['MENU_PRIVACY'] = 'Privacy';
// BUTTONS
$lang['SUBSCRIBE_BTN'] = 'SUBSCRIBE';
$lang['SEND_MESSAGE_BTN'] = 'SEND MESSAGE';
$lang['SEND_AREYOUSURE_BTN'] = 'ARE YOU SURE ?';
// LABELS
$lang['SUBSCRIBE_NEWSLETTER_H1'] = 'SUBSCRIBE TO NEWSLETTER';
$lang['SUBSCRIBE_NEWSLETTER_DESC'] = 'Get started with tips, inspirations and information that will help you live a life with greater satisfaction.';
$lang['SUBSCRIBE_NEWSLETTER_NAME'] = 'Insert your name';
$lang['SUBSCRIBE_NEWSLETTER_EMAIL'] = 'Insert your email';
$lang['PROGRAM_TREATMENTS'] = 'Treatments of the program';
$lang['PROGRAM_TREATMENTS_DESC'] = 'Click on the links to see the details of individual treatments';
$lang['BENEFITS_QUESTION'] = 'What are the benefits for you?';
$lang['CONTACTME_DESC'] = 'If you have any questions, whether you want to book the treatment or ask for some information.';
$lang['LABEL_NAME'] = 'Name';
$lang['LABEL_EMAIL'] = 'Email';
$lang['LABEL_PHONE'] = 'Phone';
$lang['LABEL_LOCATION'] = 'Medical office';
$lang['LABEL_MESSAGE'] = 'Message';
$lang['LABEL_CHOOSE'] = 'Choose...';
$lang['LABEL_FAQ'] = 'Frequent questions';
$lang['LABEL_UNSUBSCRIBE'] = 'Unsubscribe';
// MSG
$lang['MSG_REQUIRED_NAME'] = 'Insert your name';
$lang['MSG_REQUIRED_EMAIL'] = 'Insert your email';
$lang['MSG_VALID_EMAIL'] = 'Insert a valid email address';
$lang['MSG_REQUIRED_PHONE'] = 'Insert your phone number';
$lang['MSG_VALID_PHONE'] = 'Inserit a valid number';
$lang['MSG_REQUIRED_OFFICE'] = 'Choose a office';
$lang['MSG_REQUIRED_MESSAGE'] = 'Insert a message';
$lang['MSG_SUCCESS_CONTACT'] = 'Your message has been sent correctly. Thank you !';
$lang['MSG_FAILURE_CONTACT'] = 'Sorry it seems that our mail server is not responding. Please try again later!';
$lang['MSG_SUCCESS_NEWSLETTER'] = 'Now you are subscribed to the newsletter. Thank you !';
$lang['MSG_UNIQUE_NEWSLETTER'] = 'The email address is already subscribed to the newsletter.';
$lang['MSG_UNSUBSCRIBE_DONE'] = 'Your email/subscription was removed. Thank you!';
$lang['MSG_UNSUBSCRIBE_NOTFOUND'] = 'The required contact is not currently registered';
/* PAGES CONTENTS */
// HOME
$lang['HOME_SERVICIOS_PATH'] = 'servicios.png';
$lang['HOME_TOP_IMG_PATH'] = '1950_500.png';
$lang['HOME_TOP_IMG_MOBILE_PATH'] = '580_700.png';
$lang['HOME_TOP_TEXT'] = 'Hello, I\'m Orlena Zotti, aesthetic, creator of <span class="orange-text"><b>"Sei essenzialmente bella"</b></span>.<br/>Helping women find stability and strengthen love through physical harmony. <br> I love to provide tools to women who want a change and seek to connect with the essence of beauty.';
$lang['HOME_TOP_TEXT_2'] = '<span class="text-24"><b>What is aesthetic medicine?</b></span><br>
I would define it as all that is alternative to plastic surgery to harmonize physical beauty, through treatments carried out with little or no invasive procedures.';
$lang['HOME_TOP_TEXT_3'] = '“Aesthetic Medicine is a journey, a journey that takes place step by step.<br/>
The goal is to make you feel more beautiful by believing others that you simply rested.”';
$lang['HOME_SIDE_TEXT'] = 'I tell you ...<br><br>A lot of your fears about aesthetic medicine are fueled by the company itself, where they show you horrors or make you believe that physical beauty does not matter.<br/><br/>
    For me it counts (and we are honest, because you live in a world that is matter), but it matters when you are in the <span class="blu-text"><b>balance between outer and inner beauty.</b></span><br>
    Maybe you do not know the Aesthetic Medicine and if you "know" (and this does not mean you really know what it is), you\'re afraid because you think it will swell, turn you or you will become another.<br><br>
    <span class="orange-text"><b>I offer you harmony and balance</b></span>, you will be who you are physically but you will see and will see you <span class="blu-text"> better, rested and fantastic</b></span> and if you let me, I will guide you to your inner renewal.
    <br><b> Break the Limiting Beliefs </b> </ span>, as far as the outside beauty is concerned, <span class = "orange-text"> <b> do not have shame </b></span> to feel good inside your own skin and try to feel more beautiful. <br> <br>
    <span class="orange-text"><b>Do what you feel</b></span> do <span class="blu-text"><b> but feel it with your heart </b></span> and you will enjoy <span class="orange-text"><b>external harmony and interior renewal</b></span> that you could achieve if you really want it.<br><br><span class="blu-text text-24"><b>Everything depends on you!</b></span>';
// PRIVACY
$lang['PRIVACY_BODY_TEXT'] = '<p align="justify">Pursuant to Legislative Decree no. 196, You are essentially beautiful, as a sole proprietor of the treatment, you are required to provide some information regarding the use of the personal data you provide, or otherwise acquired in the course of your business. Source of personal data.
The personal data we refer to is collected directly from this website. All of these data will be processed in compliance with the said law and the confidentiality requirements that have always been inspired by the activity of the study.</p>
					<p align="justify">
                       <b>The following data will be collected:</b>
                       <ul class="privacy-ul">
                        <li>user IP address</li>
                        <li>ersonal email address</li>
                        <li>source URL</li>
                        <li>phone number</li>
                        <li>name</li>
                        <li>surname</li>
                       </ul>
                    </p>
					
					<p align="left">
                       <b>Purpose of the treatment to which the data is intended The personal data you provide:</b>
                       <ul class="privacy-ul">
                       	<li>completion and Access Support;</li>
                        <li>to enforce legal obligations;</li>
                        <li>for operational and management needs</li>
                        <li>for promotions and promotional communications reserved for members of the site</li>
					  </ul>
                    </p>
                    <p align="justify">How data is processed in relation to the aforementioned purposes, the processing of personal data is done through manual, computer and telematic tools with logic strictly related to the purposes themselves and, in any case, in order to guarantee the security and confidentiality of the data themselves. Data processing will be carried out by means of appropriate tools to ensure its security and confidentiality, and it can also be carried out by means of automated tools for storing, managing and transmitting the data itself. The provided data will not be transferred and / or resold to third parties
The rights referred to in art. 7 We inform you, finally, that art. 7 of Legislative Decree 196/2003 grants the persons concerned the exercise of specific rights. In particular, the data subject may obtain from the Registrar the confirmation of the existence or not of his / her personal data and that such data shall be made available to him in an intelligible form. The person concerned may also ask to know the origin of personal data, the purpose and the manner of processing; the logic applied in the case of processing carried out with the aid of electronic instruments; the identification details of the holder and the manager; to obtain the cancellation, transformation into anonymous form or the blocking of the data processed in violation of the law as well as the updating, correction or, if there is interest, the integration of the data; to oppose, for legitimate reasons, the processing of data concerning him also for the purpose of sending advertising material or direct sale. <br> <br> The rights in question may also be exercised, through an agent as well request addressed to the person named by letter. In the exercise of rights, the person concerned may confer in writing, delegate or procure to natural persons, bodies, associations or bodies. The person concerned can also be assisted by a trusted person. Further information can be obtained, in writing, at the headquarters of the medical office, located in Rome, Via delle Belle Arti, 7 - 00196
                    </p>
					<p align="justify">
					<b>About cookies</b><br/>
					Cookies are text strings (small portions of information) that are stored on computers, tablets, smartphones, notebooks, reusable during the same visit (session cookies) or retransmitted to the same sites on a subsequent.<br/><br/>Visit. art. 13 of Legislative Decree no. 196/2003 this site uses only technical cookies or similar assimilates, which do not require prior consent; these are the necessary cookies, essential for the proper functioning of the site, they serve to make navigation.
					</p>';
// ABOUT ME
$lang['ABOUTME_BODY_TEXT'] = '<br/><p align="justify">Here loneliness has been brought to me, feeling like a fish out of water, feeling not to belong to the place where I was or how to do aesthetic medicine as I did. I thought that changing the environment could improve the situation, but that did not happen.
                        </p>
						<br/>
                        <p align="justify">
                            Then I started my search for everything that would help me to understand what I really wanted, what made me happy and, above all, what I came to do in this world.
                        </p>
						<br/>
                        <p align="justify">
                        	First of all, I decided to expand my skills as an aesthetic doctor and extend knowledge to other horizons of global well-being, so I started reading many books about spiritual evolution, I discovered the world of meditation, I attended several courses of personal growth and how to nourish my body and soul. All this has been added to my passion, Aesthetic Medicine.
                        </p>
						<br/>
                        <p align="justify">
                        	So I had the desire to apply this inner journey to aesthetic medicine and I began to unify all the tools I experienced in specific programs. This has allowed me to create a way of offering women\'s help, harmonizing the exterior beauty and balancing the inner beauty, by merging two worlds that the society has sold as different, but which for me are inseparable because they both nourish equilibrally.
                        </p>';	
$lang['BIO_BODY_TEXT'] = '<p>
                       I graduated from the Medical Surgeon in Venezuela (my birth country) in 2005, soon after I met the world of aesthetic medicine and I fell in love with him so much that I did my only job.
                    </p>
                    <p>
                        I arrived in Italy in 2010 and followed the procedure for the recognition of my degree in this country, which I bring in my blood thanks to my grandfather.
                    </p>
                    <p>
                        To refine my knowledge in this field I followed a four-year course of study by completing the Diploma of Aesthetic Medicine of the Fatebenefratelli International Foundation in Rome.
                    </p>
                    <p>
                        Now I am in Sardinia, where, thanks to my knowledge of wealth, I do my job by offering a concept of unique and balanced aesthetic medicine.
                    </p>';				
// MY WORK
$lang['MYWORK_BODY_TEXT'] = '<br/><p align="justify">
                           Integrating physical beauty and internal growth is a real mission, because in this awakening to the awareness that we are having, "our body is our temple," but in this awakening, this phrase only serves if you take care of your body through nutrition and physical activity, in this way you can keep it in balance.
                        </p>
                        <p align="justify">
                            But how can beauty be included in this awareness-raising moment?
For many years, physical beauty has been stigmatized because for common thinking it does not matter how you see (ugly or pretty) or you feel, if you are not who you are and how are you. For this, what I propose is to harmonize your beauty, for example to make your wrinkles less noticeable and make your face more rested and cool. Do not be ashamed of wanting to look better and feel good inside your skin.
                        </p>
                        <p align="justify">
                        	Give your body and your face fresh air that has lost over the years, and that\'s just this, fresh air, it\'s not a reconstruction, it\'s no magic, it\'s not becoming someone else. It does not mean to stop being who you are. I will help you find the beauty you do not see or think you\'ve lost. I will help you begin to see with different eyes, with eyes of love for yourself.
                        </p>
						<br/>
                        <p align="justify">
                        	The beauty that for many people emanates from the inside out is mostly reflected from the outside inward; because many times you look in the mirror what you can get brings your inner light shining with more intensity and helps you to see what was hidden from years of lack of love for yourself by the lack of time.
                        </p>';	
$lang['MYWORK_SLOGAN'] = 'Remember that your body is your temple and many times physical activity and healthy eating is not enough, it is also important for your temple to breathe fresh air.';						