<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Frontend language - Italian
*/
// LANG from Lingue table
$lang['LANGUAGE_NAME'] = 'ITALIANO';
$lang['LANGUAGE_ABBR'] = 'IT';
$lang['LANGUAGE_ID'] = '1';
$lang['LANGUAGE_LOCALE_PAYPAL'] = 'it_IT';
$lang['LANGUAGE_CI_CODE'] = 'italian';
// MENU
$lang['MENU_HOME'] = 'Home';
$lang['MENU_ABOUTME'] = 'Su di me';
$lang['MENU_BIO'] = 'Mini biografia';
$lang['MENU_MYWORK'] = 'Il mio lavoro';
$lang['MENU_PROGRAMS'] = 'Programmi';
$lang['MENU_TREATMENTS'] = 'Trattamenti';
$lang['MENU_STUDIES'] = 'Studi medici';
$lang['MENU_CONTACTME'] = 'Contattami';
$lang['MENU_LANGUAGE'] = 'Lingua';
$lang['MENU_PRIVACY'] = 'Privacy';
// BUTTONS
$lang['SUBSCRIBE_BTN'] = 'ISCRIVITI';
$lang['SEND_MESSAGE_BTN'] = 'INVIA MESSAGGIO';
$lang['SEND_AREYOUSURE_BTN'] = 'SEI SICURO ?';
// LABELS
$lang['SUBSCRIBE_NEWSLETTER_H1'] = 'ISCRIVITI ALLA NEWSLETTER';
$lang['SUBSCRIBE_NEWSLETTER_DESC'] = 'Inizia a ricevere consigli, ispirazioni e informazioni che ti potranno aiutare a raggiungere una vita con maggiori soddisfazioni.';
$lang['SUBSCRIBE_NEWSLETTER_NAME'] = 'Inserisci il tuo nome';
$lang['SUBSCRIBE_NEWSLETTER_EMAIL'] = 'Inserisci la tua email';
$lang['PROGRAM_TREATMENTS'] = 'Trattamenti del programma';
$lang['PROGRAM_TREATMENTS_DESC'] = 'Clicca sui link per vedere i dettagli dei singoli trattamenti';
$lang['BENEFITS_QUESTION'] = 'Quali sono i benefici per te ?';
$lang['CONTACTME_DESC'] = 'Se hai qualche domanda, se vuoi prenotare il trattamento o chiedere qualche informazione.';
$lang['LABEL_NAME'] = 'Nome';
$lang['LABEL_EMAIL'] = 'Email';
$lang['LABEL_PHONE'] = 'Telefono';
$lang['LABEL_LOCATION'] = 'Studio di zona';
$lang['LABEL_MESSAGE'] = 'Messaggio';
$lang['LABEL_CHOOSE'] = 'Scegli...';
$lang['LABEL_FAQ'] = 'Domande frequenti';
$lang['LABEL_UNSUBSCRIBE'] = 'Cancellati';
// MSG
$lang['MSG_REQUIRED_NAME'] = 'Inserisci il tuo nome';
$lang['MSG_REQUIRED_EMAIL'] = 'Inserisci la tua email';
$lang['MSG_VALID_EMAIL'] = 'Inserisci una email valida';
$lang['MSG_REQUIRED_PHONE'] = 'Inserisci il tuo numero di telefono';
$lang['MSG_VALID_PHONE'] = 'Inserisci un numero valido';
$lang['MSG_REQUIRED_OFFICE'] = 'Seleziona uno studio medico';
$lang['MSG_REQUIRED_MESSAGE'] = 'Inserisci il messaggio';
$lang['MSG_SUCCESS_CONTACT'] = 'Il tuo messaggio è stato inviato correttamente. Grazie !';
$lang['MSG_FAILURE_CONTACT'] = 'Abbiamo riscontrato un problema nell\'invio del mesaggio. Riprova!';
$lang['MSG_SUCCESS_NEWSLETTER'] = 'Adesso sei iscritto alla newsletter. Grazie !';
$lang['MSG_UNIQUE_NEWSLETTER'] = 'L\'indirizzo email è già iscritto alla newsletter';
$lang['MSG_UNSUBSCRIBE_DONE'] = 'La tua email/iscrizione è stata rimossa. Grazie!';
$lang['MSG_UNSUBSCRIBE_NOTFOUND'] = 'Il contatto richiesto non è attualmente registrato';
/* PAGES CONTENTS */
$lang['HOME_SERVICIOS_PATH'] = 'servicios.png';
// HOME
$lang['HOME_TOP_IMG_PATH'] = '1950_500.png';
$lang['HOME_TOP_IMG_MOBILE_PATH'] = '580_700.png';
$lang['HOME_TOP_TEXT'] = 'Ciao, sono Orlena Zotti, medico estetico, creatrice di <span class="orange-text"><b>"Sei Essenzialmente Bella"</b></span>.<br>Aiuto le donne a trovare la stabilità e rafforzare l\'amor proprio attraverso l\'armonia fisica.<br>Amo fornire strumenti alle donne che vogliono un cambiamento e cercano di connettersi con l\'essenza della bellezza.';
$lang['HOME_TOP_TEXT_2'] = '<span class="text-24"><b>Cosa è la medicina estetica?</b></span><br>
La definirei come tutto quello che è alternativo alla chirurgia plastica per armonizzare la bellezza fisica, tramite trattamenti realizzati con procedure poco o per niente invasive.';
$lang['HOME_TOP_TEXT_3'] = '“La Medicina Estetica è un viaggio, un percorso, che si realizza passo a passo.<br>
L’obbiettivo è farti sentire più bella facendo credere agli altri che ti sei semplicemente riposata.”';
$lang['HOME_SIDE_TEXT'] = 'Ti racconto...<br><br>Una gran parte delle tue paure riguardo la medicina estetica, vengono alimentate dalla società stessa, dove ti fanno vedere orrori o ti fanno credere che la bellezza fisica non conta.<br><br>
    Per me conta (e siamo sinceri, perché vivi in un mondo che è materia), ma conta quando hai in <span class="blu-text"><b>equilibrio la bellezza esterna e quella interiore.</b></span><br>
    Forse non conosci la Medicina Estetica e se la “conosci” (e questo non significa che sai veramente di cosa si tratta), hai paura perché pensi che ti gonfierà, ti trasformerà o diventerai un’altra.<br><br>
    Io <span class="orange-text"><b>ti offro armonia ed equilibrio</b></span>, rimarrai chi sei fisicamente ma ti vedrai e ti vedranno <span class="blu-text"><b>meglio, riposata e fantastica</b></span> e se me lo permetti ti guiderò verso la tua rinnovazione interna.
    <br><span class="orange-text"><b>Rompi le credenze limitanti</b></span>, per quello che riguarda la bellezza esteriore, <span class="blu-text"><b>non avere vergogna</b></span> di sentirti bene dentro della tua propria pelle e cercare di sentirti più bella.<br><br>
    <span class="orange-text"><b>Fai quello che senti</b></span> di fare, <span class="blu-text"><b>ma sentilo con il cuore</b></span> e vivi con piacere l\'<span class="orange-text"><b>armonia esterna e il rinnovamento interno</b></span> che potresti raggiungere se veramente lo vuoi.<br><br><span class="blu-text text-24"><b>Tutto dipende da te!</b></span>';
// PRIVACY
$lang['PRIVACY_BODY_TEXT'] = '<p align="justify">Ai sensi del decreto legislativo 30 giugno 2003 n. 196, Sei essenzialmente bella, in qualità di Titolare autonomo del trattamento, è tenuta a fornire alcune informazioni riguardanti l\'utilizzo dei dati personali da Lei forniti, ovvero altrimenti acquisiti nell\'ambito della rispettiva attività.
Fonte dei dati personali.
I dati personali cui ci si riferisce sono raccolti direttamente da questo sito web. Tutti questi dati verranno trattati nel rispetto della citata legge e degli obblighi di riservatezza cui si è sempre ispirata l\'attività dello studio.</p>
                    <p align="justify">
                       <b>Verranno raccolti i seguenti dati:</b>
                       <ul class="privacy-ul">
                        <li>indirizzo IP dell\'utente;</li>
                        <li>indirizzo e-mail personale;</li>
                        <li>indirizzo URL di provenienza;</li>
                        <li>numero di telefono</li>
                        <li>nome</li>
                        <li>cognome</li>
                       </ul>
                    </p>
                    <p align="left">
                       <b>Finalità del trattamento cui sono destinati i dati I dati personali da Lei forniti:</b>
                       <ul class="privacy-ul">
                       	<li>completamento e supporto dell\'accesso;</li>
                        <li>per eseguire obblighi di legge;</li>
                        <li>per esigenze di tipo operativo e gestionale</li>
                        <li>per inziative e comunicazioni promozionali riservate agli iscritti al sito</li>
					  </ul>
                    </p>
                    <p align="justify">Modalità di trattamento dei dati
In relazione alle indicate finalità,il trattamento dei dati personali avviene mediante strumenti manuali, informatici e telematici con logiche strettamente correlate alle finalità stesse e, comunque, in modo da garantire la sicurezza e la riservatezza dei dati stessi. Il trattamento dei dati avverrà mediante strumenti idonei a garantirne la sicurezza e la riservatezza e potrà essere effettuato anche attraverso strumenti automatizzati atti a memorizzare, gestire e trasmettere i dati stessi. I dati forniti non verranno ceduti e/o rivenduti a soggetti terzi.<br><br>
Diritti di cui all\'art.7 La informiamo, infine, che l\'art. 7 del decreto legislativo 196/2003 conferisce agli interessati l\'esercizio di specifici diritti. In particolare, l\'interessato può ottenere dal Titolare la conferma dell\'esistenza o no di propri dati personali e che tali dati vengano messi a sua disposizione in forma intelligibile. L\'interessato può altresì chiedere di conoscere l\'origine dei dati personali, la finalità e modalità del trattamento; la logica applicata in caso di trattamento effettuato con l\'ausilio di strumenti elettronici; gli estremi identificativi del titolare e del responsabile; di ottenere la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge nonché l\'aggiornamento, la rettificazione o, se vi è interesse, l\'integrazione dei dati; di opporsi, per motivi legittimi, al trattamento di dati che lo riguardano anche ai fini di invio di materiale pubblicitario o di vendita diretta.<br><br>I diritti in oggetto potranno essere esercitati, anche per il tramite di un incaricato, mediante richiesta rivolta al responsabile nominato con lettera. Nell\'esercizio dei diritti, l\'interessato può conferire per iscritto, delega o procura a persone fisiche, enti, associazioni od organismi. L\'interessato può, altresì, farsi assistere da una persona di fiducia. Ulteriori informazioni potranno essere rischieste, per iscritto, presso la sede dello studio medico, sita in Roma, Via delle Belle Arti, 7 - 00196
                    </p>
					<p align="justify">
					<b>Informazioni sui cookie</b><br/>
					I cookie sono stringhe di testo (piccole porzioni di informazioni) che vengono memorizzati su computer, tablet, smartphone, notebook, da riutilizzare nel corso della medesima visita (cookie di sessione) o per essere ritrasmessi agli stessi siti in una visita successiva.<br/><br/>Ai sensi dell\'art. 13 del D.l.vo n. 196/2003 questo sito utilizza unicamente cookie tecnici od a questi assimilati, che non richiedono un preventivo consenso; si tratta di cookie necessari, indispensabili per il corretto funzionamento del sito, servono per effettuare la navigazione.
					</p>';
// ABOUT ME
$lang['ABOUTME_BODY_TEXT'] = '<p align="justify">Qui mi ha portato la solitudine, il sentirmi come un pesce fuor d\'acqua, la sensazione di non appartenere al luogo in cui mi trovavo né alla maniera di fare la medicina estetica come la facevo. Ho pensato che cambiare ambiente poteva far migliorare la situazione, ma questo non è successo.
                        </p>
                        <p align="justify">
                            Allora ho iniziato la mia ricerca di tutto ciò che mi avrebbe aiutato a capire cosa volevo veramente, cosa mi rendeva felice e, soprattutto, cosa sono venuta a fare in questo mondo.
                        </p>
                        <p align="justify">
                        	Prima di tutto, ho deciso di ampliare le mie competenze come medico estetico ed estendere la conoscenza ad altri orizzonti del benessere globale, così ho intrapreso la lettura di molti libri sull’evoluzione spirituale, ho scoperto il mondo della meditazione, ho frequentato diversi corsi di crescita personale e su come nutrire il mio corpo e la mia anima. Tutto questo si è aggiunto alla mia passione, la Medicina Estetica.
                        </p>
                        <p align="justify">
                        	Così mi è sorto il desiderio di applicare alla medicina estetica questo viaggio interiore e ho cominciato ad unificare tutti gli strumenti che ho sperimentato in programmi specifici. Questo mi ha permesso di creare un modo per offrire aiuto alle donne, armonizzando la bellezza esteriore ed equilibrando la bellezza interiore, accorpando due mondi che la società ha venduto come diversi, ma che per me sono inseparabili perché entrambi si nutrono equilibratamente.
                        </p>';	
$lang['BIO_BODY_TEXT'] = '<p>
                       Ho conseguito la laurea di Medico Chirurgo in Venezuela (il mio paese di nascita) nel 2005, subito dopo ho conosciuto il mondo della medicina estetica e me ne sono innamorata, tanto che ne ho fatto la mia unica professione.
                    </p>
                    <p>
                        Sono arrivata in Italia nel 2010 e ho seguito la procedura per il riconoscimento della mia laurea in questo paese, che porto nel mio sangue grazie a mio nonno.
                    </p>
                    <p>
                        Per perfezionare le mie conoscenze in questo campo ho seguito un percorso quadriennale di studi conseguendo il Diploma di Medicina Estetica della Fondazione Internazionale Fatebenefratelli a Roma.
                    </p>
                    <p>
                        Ora mi trovo in Sardegna, dove, grazie al mio patrimonio di conoscenze, svolgo il mio lavoro offrendo un concetto di medicina estetica unico ed equilibrato. 
                    </p>';								
// MY WORK
$lang['MYWORK_BODY_TEXT'] = '<p align="justify">
                           Integrare la bellezza fisica e la crescita interna è una vera e propria missione, perché in questo risveglio alla consapevolezza che stiamo avendo, "il nostro corpo è il nostro tempio", ma attualmente, in questo risveglio, questa frase serve solo se ti prendi cura del tuo corpo attraverso l’alimentazione e l\'attività fisica, in questo modo ti puoi mantenere in equilibrio.
                        </p>
                        <p align="justify">
                            Ma come si può includere la bellezza in questo momento di presa di consapevolezza?
Per molti anni la bellezza fisica è stata stigmatizzata perché per il pensiero comune non importa come ti vedi (brutta o carina) o ti senti, se non chi sei e come sei. Per questo quello che io propongo è armonizzare la tua bellezza, ad esempio far diventare le tue rughe meno evidenti e far diventare il tuo volto più riposato e fresco. Non avere vergogna di volerti guardare meglio e di sentirti bene dentro la tua pelle.
                        </p>
                        <p align="justify">
                        	Regala al tuo corpo e al tuo volto l\'aria fresca che ha perso nel corso degli anni, ed è solo questo, aria fresca, non è una ricostruzione, non è magia, non è diventare qualcun altro. Non significa smettere di essere chi sei. Ti aiuterò a ritrovare la bellezza che non vedi o che pensi di aver perso. Ti aiuterò ad iniziare a vedere con occhi diversi, con occhi d’amore per te stessa.
                        </p>
                        <p align="justify">
                        	La bellezza che per molte persone si sprigiona da dentro verso fuori per la maggior parte si riflette dall\'esterno verso l\'interno; perché molte volte guardare allo specchio quello che si può ottenere fa risplendere la tua luce interiore con più intensità e ti aiuta a vedere ciò che era nascosto, dal passare degli anni, dalla mancanza d’amore per te stessa,  dalla mancanza di tempo.
                        </p>';	
$lang['MYWORK_SLOGAN'] = 'Ricordati che il tuo corpo è il tuo tempio e tante volte l\'attività fisica e il mangiare sano non è sufficiente, è anche importante per il tuo tempio un soffio d’aria fresca.';				