<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <link rel="apple-touch-icon" sizes="76x76" href="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/apple-icon.png" />
    <link rel="icon" type="image/png" href="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo lang('LABEL_UNSUBSCRIBE'); ?> | <? echo SITE_TITLE_NAME; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
</head>

<body>
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/login.jpg">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content">
                <div class="container">
                    <div class="row">
                    	<div class="col-md-12" align="center" style="margin-bottom: 50px;">
                            <a href="<? echo site_url();?>">
                                <img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/logo_medium.png" />
                            </a>    
                        </div>
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <form method="post" id="form" action="<? echo site_url();?>admin/login">
                                <div class="card card-login card-hidden">
                                    <div class="card-header text-center" data-background-color="rose">
                                        <h4 class="card-title"><? echo lang('LABEL_UNSUBSCRIBE'); ?></h4>
                                     <!--   <div class="social-line">
                                            <a href="#btn" class="btn btn-just-icon btn-simple">
                                                <i class="fa fa-facebook-square"></i>
                                            </a>
                                            <a href="#pablo" class="btn btn-just-icon btn-simple">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                            <a href="#eugen" class="btn btn-just-icon btn-simple">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </div>-->
                                    </div>
                                    <div id="infoMessage" class="text-danger text-center"><?php echo isset($message) ? $message : '' ;?></div>		
                                <?php if(isset($contact_mail) ? $contact_mail : '') { ?>
                                    <div class="card-content">            		
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label"><?php echo lang('login_identity_label');?></label>
                                                <input type="email" name="identity" id="identity" class="form-control" value="<?php echo urldecode($contact_mail); ?>" readonly>
                                            </div>
                                        </div>
                                     
                                    </div>
                                    <div class="footer text-center">
                                        <a href="<?php echo site_url('unsubscribe_confirm/'.$from_newsletter.'/'.$contact_mail);?>" class="btn btn-primary btn-lg"><?php echo lang('SEND_AREYOUSURE_BTN');?></a>
                                    </div>
                                <?php } ?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <!--div class="container">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                    </p>
                </div-->
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery-ui.min.js" type="text/javascript"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/bootstrap.min.js" type="text/javascript"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/material.min.js" type="text/javascript"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/bootstrap-notify.js"></script>
<!-- DateTimePicker Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/nouislider.min.js"></script>
<!-- Select Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src=".<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/demo.js"></script>
<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>

</html>
