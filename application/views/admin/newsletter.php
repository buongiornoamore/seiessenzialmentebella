<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php 
// import grocery and codigniter css
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<!-- END JS / CSS added to GROCERY CRUD template --> 
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>  
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">send</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title">Newsletter&nbsp;&nbsp;<a href="" id="guide-btn" title="Visualizza la guida"><i class="fa fa-info-circle"></i></a></h4>
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>   
                        	</div>          
                        </div>    
                    </div>
                 </div>
            </div>        
     		<? include('include/footer.php'); ?>
        </div>
    </div>
    <form name="newsletterForm" id="newsletterForm" action="<? echo base_url(); ?>admin/newsletter/email" method="post">
    	<input type="hidden" name="newsletter_list" value="" id="newsletter_list" />
    </form>
</body>
<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
		$('.header-tools > .floatL').append('<a id="send-news-btn" href="#" class="btn btn-default gc-list-active-button hidden"><i class="fa fa-envelope"></i> &nbsp;&nbsp;Invia newsletter</a>');
		
		$("#send-news-btn").on("click", function(e) {
			e.preventDefault();
			var delete_selected = [];
		//	var data_to_send = {};
			$('.select-row:checked').each(function () {
				delete_selected.push($(this).data('id'));
			});
			// call action
			console.log(delete_selected);
			$('#newsletter_list').val(JSON.stringify(delete_selected)); //store array
			$('#newsletterForm').submit();
		});
			
    	// sweet guide
		$("#guide-btn").on("click", function(e) {
			e.preventDefault();
			swal({
			  title: '<b>Guida all\'uso</b>',
			  width: 800,
			  type: 'question',
			  html: '<p align="justify">La funzionalità newsletter permette di gestire i contatti e le newsletter del sito.E\' possibile inserire nuovi contatti oltre che modificare, visualizzare e nascondere gli stessi.</p><p align="justify">Ogni contatto è composto da un insieme di campi, alcuni obbligatori e altri facoltativi.</p><p align="justify">Nelle tabelle di elenco sono presenti filtri di ricerca generali <i class="fa fa-search" style="color: #9c27b0"></i> oppure specifici sulle singole colonne. Le colonne possono essere ordinate (drill down) cliccando direttamente sul nome della colonna (es. Nome). Le liste tabellari possono essere stampate o esportate in excel per utilizzi esterni.</p><p align="justify">Se si intende escludere un contatto si può procedere modificandone lo stato o cancellandolo dal SITO.</p><p align="justify">La funzionalità di invio newsletter permette di inviare email libere o da template a tutti i contatti selezionati. Nel testo delle email (template) si può utilizzare la variabile VAR_NOME_CLIENTE per far inserire in automatico dal sistema il nome del cliente.</p>',
			  showCloseButton: true,
			  showCancelButton: false,
			  confirmButtonText:
				'<i class="fa fa-close"></i> Chiudi'
			});
		});
	});
</script>
</html>