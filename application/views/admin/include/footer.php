<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="<?php echo SITE_URL_PATH; ?>">
                        Home sito
                    </a>   
                </li>
          <!--      <li>
                    <a href="#">
                        Company
                    </a>
                </li>
                <li>
                    <a href="#">
                        Portfolio
                    </a>
                </li>
                <li>
                    <a href="#">
                        Blog
                    </a>
                </li>-->
            </ul>
        </nav>
        <p class="copyright pull-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
            <a href="<?php echo SITE_URL_PATH; ?>"><? echo COMPANY_NAME; ?></a>&nbsp;all rights reserved
        </p>
    </div>
</footer>