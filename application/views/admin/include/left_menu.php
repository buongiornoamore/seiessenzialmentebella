<div class="se-pre-con" style="display:none"></div>
<div class="sidebar" data-active-color="rose" data-background-color="white" data-image="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/sidebar-bg.jpg">
    <!--
Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
Tip 2: you can also add an image using data-image tag
Tip 3: you can change the color of the sidebar with data-background-color="white | black"
-->
    <div class="logo">
        <a href="<?php echo SITE_URL_PATH; ?>" class="simple-text" target="_blank">
            <img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/logo_small.png" />
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="<?php echo SITE_URL_PATH; ?>" class="simple-text" target="_blank">
            <img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/apple-icon.png" />
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/faces/avatar2.jpg" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    <? echo $this->ion_auth->user()->row()->first_name . ' ' . $this->ion_auth->user()->row()->last_name; ?>
                    <b class="caret"></b>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                    <!--    <li>
                            <a href="#">Dati profilo</a>
                        </li> -->
                        <li>
                            <a href="" onClick="return showConfirmDialog('Conferma logout', 'Sei sicuro di voler abbandonare la sessione?', '', '', '', '', '', '', '<?php echo site_url('admin/logout')?>', 'false'); ">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
           <li <? echo ($current_page == 'ADMIN-PROGRAMMI' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/programmi')?>">
                    <i class="material-icons">library_books</i>
                    <p>Programmi</p>
                </a>
            </li>
            <li <? echo ($current_page == 'ADMIN-TRATTAMENTI' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/trattamenti')?>">
                    <i class="material-icons">spa</i>
                    <p>Trattamenti</p>
                </a>
            </li>
            <li <? echo ($current_page == 'ADMIN-BENEFICI' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/benefici')?>">
                    <i class="material-icons">add_circle</i>
                    <p>Benefici</p>
                </a>
            </li>
            <li <? echo ($current_page == 'ADMIN-RETREATS' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/retreats')?>">
                    <i class="material-icons">card_travel</i>
                    <p>Retreats</p>
                </a>
            </li>
            <li <? echo ($current_page == 'ADMIN-FAQ' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/faq')?>">
                    <i class="material-icons">question_answer</i>
                    <p>FAQ</p>
                </a>
            </li>
            <li <? echo ($current_page == 'ADMIN-COMMENTI' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/commenti')?>">
                    <i class="material-icons">comment</i>
                    <p>Commenti</p>
                </a>
            </li>
           <!--  <li <//? echo ($current_page == 'ADMIN-STUDIMEDICI' ? 'class="active"' : ''); ?>>
                <a href="<//?php echo site_url('admin/studimedici')?>">
                    <i class="material-icons">local_hospital</i>
                    <p>Studi medici</p>
                </a>
            </li> -->
            <li>
                <a data-toggle="collapse" href="#email" class="collapsible-link" data-id="email">
                    <i class="material-icons">local_post_office</i>
                    <p>Email
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="email">
                    <ul class="nav">
                    	<li <? echo ($current_page == 'ADMIN-CONTATTI' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/contatti')?>">
                                <i class="material-icons">contact_mail</i>
                                <p>Contatti</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-NEWSLETTER' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/newsletter')?>">
                                <i class="material-icons">send</i>
                                <p>Newsletter</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-EMAILTEMPLATES' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/emailtemplates')?>">
                                <i class="material-icons">dashboard</i>
                                <p>Templates</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-EMAILTEMPLATESTRADUZIONI' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/emailtemplatestraduzioni')?>">
                                <i class="material-icons">flag</i>
                                <p>Traduzioni</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li <? echo ($current_page == 'ADMIN-PAGINE' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/pagine')?>">
                    <i class="material-icons">content_paste</i>
                    <p>Pagine</p>
                </a>
            </li>
            <li <? echo ($current_page == 'ADMIN-CONFIGURAZIONI' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/configurazioni')?>">
                    <i class="material-icons">settings</i>
                    <p>Configurazioni</p>
                </a>
            </li>  
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
		var parentMenuActive = '<?php echo $data['collapseParentMenu']; ?>';
		$('.sidebar').perfectScrollbar();
		if(parentMenuActive != '' && !$('#'+parentMenuActive).attr("aria-expanded")) {
			$('#'+parentMenuActive).collapse(); 	
			$('.sidebar').perfectScrollbar('update');
		}
		$('.collapsible-link').on('click', function(e){
		   e.preventDefault();
		   if(parentMenuActive != '' && $(this).data('id') != parentMenuActive) {
			 return true;
		   } else {
		   	return false;
		   }
		   $('.sidebar').perfectScrollbar('update');
		});
	});	
</script>
