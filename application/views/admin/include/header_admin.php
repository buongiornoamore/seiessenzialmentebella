<?
	$current_page = (isset($data['curr_page']) ? $data['curr_page'] : (isset($curr_page) ? $curr_page : ''));
	$current_page_title = (isset($data['curr_page_title']) ? $data['curr_page_title'] : (isset($curr_page_title) ? $curr_page_title : ''));
?>
    <link rel="apple-touch-icon" sizes="76x76" href="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/apple-icon.png" />
    <link rel="icon" type="image/png" href="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/favicon.png" />
    <!-- Bootstrap core CSS  -->  
    <link href="<? echo ASSETS_ROOT_FOLDER_ADMIN_CSS; ?>/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<? echo ASSETS_ROOT_FOLDER_ADMIN_CSS; ?>/material-dashboard.css" rel="stylesheet" />
    <? //if($data['resourcetype'] == 'NOCRUD') { ?>
    <? // } ?>
    <!--  CSS for Demo Purpose, don't include it in your project    
    <link href="<?// echo ASSETS_ROOT_FOLDER_ADMIN_CSS; ?>/demo.css" rel="stylesheet" /> -->
    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
        