<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php 
// import grocery and codigniter css
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<!-- END JS / CSS added to GROCERY CRUD template --> 
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>  
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">contact_mail</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title"><?php echo $data['curr_function_title'];?>&nbsp;<a href="<?php echo site_url('admin/contatti')?>" class="btn btn-danger btn-simple btn-little-icon" title="Ritorna ai contatti">
                                            <i class="material-icons">arrow_back</i>
                                        </a>
                                    </h4>    
                                    <!-- mettere una dropdown per cambiare prodotto -->
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>   
                        	</div>          
                        </div>    
                    </div>
                 </div>
            </div>        
     		<? include('include/footer.php'); ?>
        </div>
    </div>   
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" align="center" style="font-weight:800"></h4>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer text-center" align="center">
            <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
          </div>
        </div>
      </div>
    </div>
</body>

<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
		$('.header-tools > .floatL').append('<a id="freemail-btn" href="#" class="btn btn-default"><i class="fa fa-plus"></i> &nbsp;&nbsp;Email libera</a>');
		$(".header-tools > .floatL > .btn:first-child").hide();
		
		$('#freemail-btn').on('click', function(e){
		  e.preventDefault();
		  swal({
			  title: 'Email libera a ' + '<?php echo $data['curr_customer_email']; ?>',
			  width: 800,
			  customClass: 'html-modal-swal',
			  html:
				'<input id="swal-input1" class="swal2-input" placeholder="Inserisci il soggetto della email">' +
				'<textarea id="swal-input2" class="swal2-input html-textarea" placeholder="Inserisci il testo HTML della email"></textarea>',
			  focusConfirm: false,
			  showCancelButton: true,
			  showConfirmButton: true,
			  confirmButtonText: 'Invia',
			  cancelButtonText: 'Annulla',
			  preConfirm: function () {
				return new Promise(function (resolve, reject) {
					setTimeout(function() {
						if ($('#swal-input1').val() === '' || $('#swal-input2').val() === '') {
						  reject('Inserisci soggetto e testo HTML della email.')
						} else {
						  resolve()
						}
				  	}, 1000)
				})
			  },
			  allowOutsideClick: false
			}).then(function (result) {
			  var subject = $('#swal-input1').val();
			  var text = $('#swal-input2').val();
			  if(subject != '' && text) {
					swal({
					  title: 'Sei sicuro?',
					  text: "La tua email verrà inviata a <?php echo $data['curr_customer_email']; ?>",
					  type: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Si, inviala!'
					}).then(function () {
					    $('.se-pre-con').show();
						var postData = {
						  'subject_email': subject,	
						  'testo_email': text,
						  'contatti_list' : '<?php echo json_encode(array($data['curr_customer_email'])); ?>',
						  'nomi_list' : '<?php echo json_encode(array($data['curr_customer_name'])); ?>',
						  'lingua_id' : '<?php echo $data['curr_customer_lang'];?>'
						};
						$.ajax({
							url: '<? echo base_url();?>admin/send_email_libera',
							type: 'POST',
							dataType: "HTML",
							async: true,
							data: postData,
							success: function(response) {
								$(".se-pre-con").delay(200).fadeOut("slow"); 
								swal({
									title: 'Email inviata',
									text: 'La tua email è stata inviata con successo !',
									type: 'success',
									confirmButtonClass: "btn btn-success",
									buttonsStyling: false
								});
							},
							error: function () {
								$(".se-pre-con").delay(200).fadeOut("slow"); 
								swal({
									title: 'Errore!',
									text: 'Errore durante l\'invio della email.',
									type: 'error',
									confirmButtonClass: "btn btn-success",
									buttonsStyling: false
									});
							}
						});
					});
				}
			}).catch(swal.noop);
		});

		$('.grocery-crud-table .btn-success').on('click', function(e){
		  e.preventDefault();
		  $('.modal-title').html('Preview email HTML');
		  $('.modal-body').load( $(this).attr('href'), function() {
		  	$('#myModal').modal('show');
		  });
		});

		$('.grocery-crud-table .btn-danger').on('click', function(e){
		  e.preventDefault();
		  var href = $(this).attr('href');
		  swal({
				title: 'Conferma invio email',
				text: 'Sei sicuro di voler inviare questa email?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Si, procedi!',
				cancelButtonText: 'No, fermati',
				confirmButtonClass: "btn btn-success",
				cancelButtonClass: "btn btn-danger",
				buttonsStyling: false
			}).then(function() {
			 	$('.se-pre-con').show();
				var postData = {
				  'contatti_list' : '<?php echo json_encode(array($data['curr_customer_email'])); ?>',
				  'nomi_list' : '<?php echo json_encode(array($data['curr_customer_name'])); ?>',
				  'modulo' : '0'
				};
				$.ajax({
					url: href,
					type: 'POST',
					dataType: "HTML",
					async: true,
					data: postData,
					success: function(response) {
						$(".se-pre-con").delay(200).fadeOut("slow"); 
						swal({
							title: 'Email inviata',
							text: 'La tua email è stata inviata con successo !',
							type: 'success',
							confirmButtonClass: "btn btn-success",
							buttonsStyling: false
						});
					},
					error: function () {
						$(".se-pre-con").delay(200).fadeOut("slow"); 
						swal({
							title: 'Errore!',
							text: 'Errore durante l\'invio della email.',
							type: 'error',
							confirmButtonClass: "btn btn-success",
							buttonsStyling: false
							});
					}
				});
			 
			}, function(dismiss) {
			  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
			  if (dismiss === 'cancel') {
				swal({
				  title: 'Operazione ignorata',
				  text: 'Non è stata effettuata nessuna operazione.',
				  type: 'error',
				  confirmButtonClass: "btn btn-info",
				  buttonsStyling: false
				});
			  }
			});

			return false; // for good measure
		});
		
		$('.image-thumbnail').fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	600, 
			'speedOut'		:	200, 
			'overlayShow'	:	false,
			'type'          :   'image'
		});	
    });
</script>
</html>