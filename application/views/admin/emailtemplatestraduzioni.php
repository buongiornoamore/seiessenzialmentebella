<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php 
// import grocery and codigniter css
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<!-- END JS / CSS added to GROCERY CRUD template --> 
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>  
            <!-- GROCERY CRUD output 
    		<div class="content">
                <div class="container-fluid">
                	<?php// echo $output; ?>
                </div>
            </div> -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">flag</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title">Email templates traduzioni&nbsp;&nbsp;<a href="" id="guide-btn" title="Visualizza la guida"><i class="fa fa-info-circle"></i></a></h4>
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>   
                        	</div>          
                        </div>    
                    </div>
                 </div>
            </div>        
     		<? include('include/footer.php'); ?>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" align="center" style="font-weight:800"></h4>
          </div>
          <div class="modal-body" align="justify"></div>
          <div class="modal-footer text-center" align="center">
            <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
          </div>
        </div>
      </div>
    </div>
</body>
<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
		/*$('.image-thumbnail').fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	600, 
			'speedOut'		:	200, 
			'overlayShow'	:	false,
			'type'          :   'image'
		});	*/
		
    	// sweet guide
		$("#guide-btn").on("click", function(e) {
			e.preventDefault();
			swal({
			  title: '<b>Guida all\'uso</b>',
			  width: 800,
			  type: 'question',
			  html: '<p align="justify">La funzionalità templates traduzioni permette di gestire le traduzioni dei campi fissi deii template (modelli) per le email da inviare.E\' possibile modificare e visualizzare le traduzioni per le sole lingue installate nel sistema.</p><p align="justify">Ogni template è composto da un insieme di campi, alcuni obbligatori e altri facoltativi.</p><p align="justify">Nelle tabelle di elenco sono presenti filtri di ricerca generali <i class="fa fa-search" style="color: #9c27b0"></i> oppure specifici sulle singole colonne. Le colonne possono essere ordinate (drill down) cliccando direttamente sul nome della colonna (es. Nome). Le liste tabellari possono essere stampate o esportate in excel per utilizzi esterni. Nel testo footer unsubscribe si può utilizzare la variabile VAR_UNSUBSCRIBE_LINK per far inserire in automatico dal sistema la url del link di cancellazione dalla newsletter/contatti.</p>',
			  showCloseButton: true,
			  showCancelButton: false,
			  confirmButtonText:
				'<i class="fa fa-close"></i> Chiudi'
			});
		});
	});
</script>
</html>