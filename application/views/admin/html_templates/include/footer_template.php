<!-- === FOOTER SECTION === -->
<tr>
    <td align="center" valign="top"  bgcolor="#23bdc3" class="em_aside" style="background-image:url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/br_loto_repeat.png);background-repeat: repeat-x; color:white;">
      <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table" style="table-layout:fixed;">
        <tr>
          <td height="0" class="em_height">&nbsp;</td>
        </tr>
        <tr>
          <td valign="top" align="center">
            <table border="0" cellspacing="0" cellpadding="0" align="center">
              <tr>
                <? if(FACEBOOK_LINK != '') {?>
                <td valign="top"><a href="<? echo FACEBOOK_LINK; ?>" target="_blank" style="text-decoration:none;"><img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/emails/fb.png" width="24" height="24" style="display:block;font-family: Arial, sans-serif; font-size:10px; line-height:18px; color:#ffffff; " border="0" alt="Fb" /></a></td>
                <td width="7">&nbsp;</td>
                <? } ?>
                <? if(TWITTER_LINK != '') {?>
                <td valign="top"><a href="<? echo TWITTER_LINK; ?>" target="_blank" style="text-decoration:none;"><img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/emails/tw.png" width="24" height="24" style="display:block;font-family: Arial, sans-serif; font-size:10px; line-height:18px; color:#feae39; " border="0" alt="Tw" /></a></td>
                <td width="7">&nbsp;</td>
                <? } ?>
                <? if(PINTEREST_LINK != '') {?>
                <td valign="top"><a href="<? echo PINTEREST_LINK; ?>" target="_blank" style="text-decoration:none;"><img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/emails/pint.png" width="24" height="24" style="display:block;font-family: Arial, sans-serif; font-size:10px; line-height:18px; color:#feae39; " border="0" alt="pint" /></a></td>
                <td width="7">&nbsp;</td>
                <? } ?>
                <? if(GPLUS_LINK != '') {?>
                <td valign="top"><a href="<? echo GPLUS_LINK; ?>" target="_blank" style="text-decoration:none;"><img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/emails/google.png" width="24" height="24" style="display:block;font-family: Arial, sans-serif; font-size:10px; line-height:18px; color:#feae39; " border="0" alt="G+" /></a></td>
                <td width="7">&nbsp;</td>
                <? } ?>
                <? if(INSTAGRAM_LINK != '') {?>
                <td valign="top"><a href="<? echo INSTAGRAM_LINK; ?>" target="_blank" style="text-decoration:none;"><img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/emails/insta.png" width="24" height="24" style="display:block;font-family: Arial, sans-serif; font-size:10px; line-height:18px; color:#feae39; " border="0" alt="Insta" /></a></td>
                <td width="7">&nbsp;</td>
                <? } ?>
                <? if(YOUTUBE_LINK != '') {?>
                <td valign="top"><a href="<? echo YOUTUBE_LINK; ?>" target="_blank" style="text-decoration:none;"><img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/emails/yt.png" width="24" height="24" style="display:block;font-family: Arial, sans-serif; font-size:10px; line-height:18px; color:#feae39; " border="0" alt="Yt" /></a></td>
                <? } ?>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td height="0" class="em_height">&nbsp;</td>
        </tr>
       <!-- <tr>
          <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:12px; line-height:18px; color:#ffffff; text-transform:uppercase;">
           <span style="text-decoration:underline;"><a href="#" target="_blank" style="text-decoration:underline; color:#ffffff;">PRIVACY STATEMENT</a></span> &nbsp;&nbsp;|&nbsp;&nbsp; <span style="text-decoration:underline;"><a href="#" target="_blank" style="text-decoration:underline; color:#ffffff;">TERMS OF SERVICE</a></span><span class="em_hide"> &nbsp;&nbsp;|&nbsp;&nbsp; </span><span class="em_br"></span><span style="text-decoration:underline;"><a href="#" target="_blank" style="text-decoration:underline; color:#ffffff;">RETURNS</a></span>
          </td>
        </tr>-->
        <tr>
          <td height="0" style="font-size:1px; line-height:1px;">&nbsp;</td>
        </tr>
        <tr>
          <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:14px; line-height:18px; color:#ffffff;font-weight:500">
            &copy;<? echo date("Y") . ' ' . COMPANY_NAME; ?></a>&nbsp;<span class="mobile-show"><br></span><? echo $testo_footer_copyright; ?>
          </td>
        </tr>
        <tr>
          <td height="10" style="font-size:1px; line-height:1px;">&nbsp;</td>
        </tr>
        <tr>
          <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:12px; line-height:18px; color:#ffffff;font-weight:500">
          <?php echo str_replace('VAR_UNSUBSCRIBE_LINK', $unsubscribe_link, $testo_footer_unsubscribe); ?>	
          </td>
        </tr>
        <tr>
          <td height="0" class="em_height">&nbsp;</td>
        </tr>
      </table>
    </td>
</tr>
<!-- === //FOOTER SECTION === -->