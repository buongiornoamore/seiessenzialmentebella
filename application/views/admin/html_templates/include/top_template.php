  <!-- === LOGO SECTION === -->
<tr>
  <td height="40" class="em_height">&nbsp;</td>
</tr>
<tr>
  <td align="center"><a href="<?php echo SITE_URL_PATH; ?>" target="_blank" style="text-decoration:none;"><img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/emails/logo.png" width="150" height="150" style="display:block;font-family: Arial, sans-serif; font-size:15px; line-height:18px; color:#30373b;  font-weight:bold;" border="0" alt="<?php echo SITE_TITLE_NAME; ?>" /></a></td>
</tr>
<tr>
  <td height="30" class="em_height">&nbsp;</td>
</tr>
<!-- === //LOGO SECTION === -->
<?php if($show_navigation) { ?>
<!-- === NEVIGATION SECTION === -->
<tr>
  <td height="1" bgcolor="#fed69c" style="font-size:0px; line-height:0px;"><img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/emails/spacer.gif" width="1" height="1" style="display:block;" border="0" alt="" /></td>
</tr>
<tr>
  <td height="14" style="font-size:1px; line-height:1px;">&nbsp;</td>
</tr>
<tr>
  <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:15px; line-height:18px;text-transform:uppercase; font-weight:bold;" class="em_font blu-text">
    <a href="<?php echo SITE_URL_PATH; ?>" target="_blank" style="text-decoration:none;" class="blu-text">home</a> &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo SITE_URL_PATH; ?>/sudime" target="_blank" style="text-decoration:none;" class="blu-text">su di me</a><span class="em_hide"> &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="em_br"></span><a href="<?php echo SITE_URL_PATH; ?>/studimedici" target="_blank" style="text-decoration:none;" class="blu-text">studi medici</a> &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo SITE_URL_PATH; ?>/contattami" target="_blank" style="text-decoration:none;" class="blu-text">contattami</a>
  </td>
</tr>
<tr>
  <td height="14" style="font-size:1px; line-height:1px;">&nbsp;</td>
</tr>
<tr>
  <td height="1" bgcolor="#fed69c" style="font-size:0px; line-height:0px;"><img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/emails/spacer.gif" width="1" height="1" style="display:block;" border="0" alt="" /></td>
</tr>
<!-- === //NEVIGATION SECTION === -->
<?php } ?>