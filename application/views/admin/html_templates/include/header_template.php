<head>
    <title><?php echo $template_name . ' - ' . COMPANY_NAME; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <meta name="format-detection" content="telephone=no" />
    <link rel="apple-touch-icon" sizes="76x76" href="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/apple-icon.png" />
    <link rel="icon" type="image/png" href="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/favicon.png" />
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!--<![endif]-->
    <style type="text/css">
      body {
      -webkit-text-size-adjust: 100% !important;
      -ms-text-size-adjust: 100% !important;
      -webkit-font-smoothing: antialiased !important;
      }
      img {
      border: 0 !important;
      outline: none !important;
      }
      p {
      Margin: 0px !important;
      Padding: 0px !important;
      }
      table {
      border-collapse: collapse;
      mso-table-lspace: 0px;
      mso-table-rspace: 0px;
      }
      td, a, span {
      border-collapse: collapse;
      mso-line-height-rule: exactly;
      }
      .ExternalClass * {
      line-height: 100%;
      }
      span.MsoHyperlink {
      mso-style-priority:99;
      color:inherit;}
      span.MsoHyperlinkFollowed {
      mso-style-priority:99;
      color:inherit;}
	  .orange-text {
	  color: #fbad18 !important;}
	  .blu-text {
	  color: #23bdc3 !important;}
	  table.email_template_body ul {
      padding: 0;}
	  table.email_template_body ul li {
      background-image: url('<?php echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/loto.png');
      background-repeat: no-repeat;
      line-height: 25px;
      padding-left: 40px;
	  padding-bottom: 10px;
	  list-style-type:none;
	  text-decoration:none;
	  text-align:justify;}
	  .orange-text {
	  color: #fbad18 !important;}
      .blu-text {
	  color: #23bdc3 !important;}
	  .mobile-show {
      	display:none !important;
	  }
	  .desktop-show {
      	display:block !important;
	  }
	  a {
      	text-decoration: none !important;
	  }
      </style>
      <style media="only screen and (min-width:481px) and (max-width:599px)" type="text/css">
      @media only screen and (min-width:481px) and (max-width:599px) {
      table[class=em_main_table] {
      width: 100% !important;
      }
      table[class=em_wrapper] {
      width: 100% !important;
      }
      td[class=em_hide], br[class=em_hide] {
      display: none !important;
      }
      img[class=em_full_img] {
      width: 100% !important;
      height: auto !important;
      }
      td[class=em_align_cent] {
      text-align: center !important;
      }
      td[class=em_aside]{
      padding-left:10px !important;
      padding-right:10px !important;
      }
      td[class=em_height]{
      height: 20px !important;
      }
      td[class=em_font]{
      font-size:14px !important;	
      }
      td[class=em_align_cent1] {
      text-align: center !important;
      padding-bottom: 10px !important;
      }
	  .mobile-show {
      	display:block !important;
	  }
	  .desktop-show {
      	display:none !important;
	  }
      }
      </style>
      <style media="only screen and (max-width:480px)" type="text/css">
      @media only screen and (max-width:480px) {
      table[class=em_main_table] {
      width: 100% !important;
      }
      table[class=em_wrapper] {
      width: 100% !important;
      }
      td[class=em_hide], br[class=em_hide], span[class=em_hide] {
      display: none !important;
      }
      img[class=em_full_img] {
      width: 100% !important;
      height: auto !important;
      }
      td[class=em_align_cent] {
      text-align: center !important;
      }
      td[class=em_align_cent1] {
      text-align: center !important;
      padding-bottom: 10px !important;
      }
	  .mobile-show {
      	display:block !important;
	  }
	  .desktop-show {
      	display:none !important;
	  }
      td[class=em_height]{
      height: 20px !important;
      }
      td[class=em_aside]{
      padding-left:10px !important;
      padding-right:10px !important;
      } 
      td[class=em_font]{
      font-size:14px !important;
      line-height:28px !important;
      }
      span[class=em_br]{
      display:block !important;
      }
      }
    </style>
  </head>