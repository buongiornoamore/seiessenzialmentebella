<?php
	$show_navigation = false;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <? include('include/header_template.php'); ?>
  <body style="margin:0px; padding:0px;" bgcolor="#ffffff">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="email_template_body">
      <? include('include/preheader_template.php'); ?>
      <!-- === BODY SECTION=== --> 
      <tr>
        <td align="center" valign="top"  bgcolor="#ffffff">
          <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table" style="table-layout:fixed;">
            <? include('include/top_template.php'); ?>
            <!-- === IMG WITH TEXT AND COUPEN CODE SECTION === -->
            <tr>
              <td valign="top" class="em_aside">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <?php if($image_banner != '') {?>
                  <tr>
                    <td height="36" class="em_height">&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="middle" align="center"><img src="<?php echo $image_banner; ?>" width="333" height="303" alt="WELCOME" style="display:block; font-family:Arial, sans-serif; font-size:25px; line-height:303px; color:#c27cbb;max-width:333px;" class="em_full_img" border="0" /></td>
                  </tr>
                  <tr>
                    <td height="41" class="em_height">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="1" bgcolor="#d8e4f0" style="font-size:0px;line-height:0px;"><img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/emails/spacer.gif" width="1" height="1" style="display:block;" border="0" alt="" /></td>
                  </tr>
                  <? } ?>
                  <tr>
                    <td height="35" class="em_height">&nbsp;</td>
                  </tr> 
                  <tr>
                    <td align="left" style="font-family:'Open Sans', Arial, sans-serif; font-size:15px; line-height:22px; color:#999999;"><?php echo str_replace('VAR_NOME_CLIENTE', $customer_name, $email_body); ?>
                    </td>
                  </tr>
                 <!-- <tr>
                    <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:15px; font-weight:bold; line-height:18px; color:#30373b;">Ciao &lt;Firstname&gt;</td>
                  </tr>
                  <tr>
                    <td height="22" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:18px; font-weight:bold; line-height:20px; color:#feae39;">Free 20% OFF coupon code for singing&#45;up!</td>
                  </tr>
                  <tr>
                    <td height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:18px; line-height:20px; color:#feae39;">Use Coupon Code</td>
                  </tr>
                  <tr>
                    <td height="12" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="top" align="center">
                      <table width="210" border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr>
                          <td valign="middle" align="center" height="45" bgcolor="#feae39" style="font-family:'Open Sans', Arial, sans-serif; font-size:17px; font-weight:bold; color:#ffffff; text-transform:uppercase;">off20code</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td height="34" class="em_height">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center" style="font-family:'Open Sans', Arial, sans-serif; font-size:15px; line-height:22px; color:#999999;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium<br class="em_hide"/>
                      doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo<br class="em_hide" />
                      inventore veritatis et quasi architecto beatae vitae<br class="em_hide" />
                      dicta sunt explicabo.
                    </td>
                  </tr>-->
                  <tr>
                    <td height="31" class="em_height">&nbsp;</td>
                  </tr>
                </table>
              </td>
            </tr>
            <!-- === //IMG WITH TEXT AND COUPEN CODE SECTION === -->
          </table>
        </td>
      </tr>
      <!-- === //BODY SECTION=== -->
      <? include('include/footer_template.php'); ?>
    </table>
    <div style="display:none; white-space:nowrap; font:20px courier; color:#ffffff; background-color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
  </body>
</html>