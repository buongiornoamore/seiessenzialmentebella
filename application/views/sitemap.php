<?
	header('Content-type: application/xml');	
				
	echo '<?xml version="1.0" encoding="UTF-8"?>';
	
	if($sitemap_urls_type == 'general') {
		echo '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		foreach($urls as $url) {
		?>	
		<sitemap>
			<loc><?= $url ?></loc>
			<lastmod><?= $max_date ?></lastmod>
		</sitemap>
		<? } 
		echo '</sitemapindex>';
	} else if($sitemap_urls_type == 'images') {
		echo'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
                xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
          foreach($urls as $url) {
			echo $url;
		} 
        echo '</urlset>'; 
	} else {
		echo '<urlset
				  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
				  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				  xmlns:xhtml="http://www.w3.org/1999/xhtml"
				  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
				  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
	
		foreach($urls as $url) {
			echo $url;
		} 
																
		echo '</urlset>';		
	}
?>