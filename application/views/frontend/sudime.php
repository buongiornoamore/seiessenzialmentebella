<!DOCTYPE html>
<html lang="en">

<head>
    <title>Orlena Zotti | <?php echo lang('MENU_ABOUTME'); ?></title>
	<? require_once("include/header_common.php"); ?>
</head>

<body id="page-top" class="index">
    <? require_once("include/header.php"); ?>
	<div class="topNavSpacerBig"></div> 
    <br>
    <!-- su di me section -->
	<section class="section-quarter section-loto" id="sudime">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p style="margin:0;"><?php echo lang('MENU_ABOUTME'); ?></p>
    			</div>
            </div>
        </div>
    </section>
	<section class="section-half">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                    <div class="col-lg-6" align="center">
                    	<img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/'.lang('SUDIME_PC_IMMAGINE_1'); ?>" style="width: 100%;" alt="Seiessenzialmentebella.com - Dott. Orlena zotti" title="Seiessenzialmentebella.com - Dott. Orlena zotti" />
                    </div>
                    <div class="col-lg-6" align="center">
                        <br>
                        <?php echo stripslashes(lang('SUDIME_PC_TESTO_BODY_1')); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br><br>
    <!-- mini biografia -->
   <section class="section-quarter section-loto" id="minibiografia">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p style="margin:0;" class="text-big-camille"><?php echo lang('MENU_BIO'); ?></p>
    			</div>
            </div>
        </div>
    </section>
    <section class="section-half">
        <div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                    <?php echo stripslashes(lang('SUDIME_PC_TESTO_BODY_2')); ?>
				</div>
            </div>
        </div>
    </section>	
    <div align="center">
    	<p><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flor_orange.png" class="flower-divider"/></p>
    </div> 
    <br><br>
    
    <? require_once("include/footer.php"); ?>
	<script type="text/javascript">
		$(document).ready(function($){
			// anchor load
			var ancorid = '<? echo $ancorid; ?>';
			//console.log('ancorid ' + ancorid);
			if(ancorid != '')
				scrollToAnchor(ancorid, 0, null);
		});
        $(window).load(function() {	
        });
		function scrollToAnchor(aid, marginTop, event){//* Ancoraggio del menu al click *//
			if(event != null)
				event.preventDefault();
			console.log('scrollToAnchor ' + aid);
			var aTag = $("#"+ aid);
			$('html,body').animate({scrollTop: aTag.offset().top - marginTop}, 500);
		}
    </script>		
</body>

</html>
