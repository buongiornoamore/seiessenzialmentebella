<!DOCTYPE html>
<html lang="en">

<head>
    <title>Orlena Zotti | <?php echo lang('MENU_MYWORK'); ?></title>
	<? require_once("include/header_common.php"); ?>
</head>

<body id="page-top" class="index">
    <? require_once("include/header.php"); ?>
	<div class="topNavSpacerBig"></div> 
    <br>
    <!-- su di me section -->
	<section class="section-quarter section-loto" id="sudime">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p style="margin:0;"><?php echo lang('MENU_MYWORK'); ?></p>
    			</div>
            </div>
        </div>
    </section>
	<section class="section-half">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                    <div class="col-lg-6" align="center" style="padding-top:50px;">
                    	<img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/'.lang('MIOLAVORO_PC_IMMAGINE_1'); ?>" style="width: 100%;" alt="Seiessenzialmentebella.com - Dott. Orlena zotti" title="Seiessenzialmentebella.com - Dott. Orlena zotti" />
                    </div>
                    <div class="col-lg-6" align="center">
                        <br>
                        <?php echo stripslashes(lang('MIOLAVORO_PC_TESTO_BODY_1')); ?>
                    </div>
                    <div class="col-lg-12" align="center" style="margin-top:35px;">
                    	<p class="orange-text text-24"><b><?php echo stripslashes(lang('MIOLAVORO_PC_TESTO_BOLD_1')); ?></b></p>
					</div>
                </div>
            </div>
        </div>
    </section>
    <div align="center">
    	<p><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flor_verde.png" class="flower-divider" /></p>
    </div> 
    <br><br>
    
    <? require_once("include/footer.php"); ?>
    
	<script type="text/javascript">
		$(document).ready(function($){
		});
        $(window).load(function() {	
        });
    </script>		
</body>

</html>
