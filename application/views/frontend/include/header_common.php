<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Orlena Zotti, medicina estetica, valorizzazione personale e alimentazione."/>
<meta property="og:locale" content="it_IT" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Orlena Zotti | <?php echo SITE_TITLE_CURRENT;?>" />
<meta property="og:description" content="Orlena Zotti, medicina estetica, valorizzazione personale e alimentazione." />
<meta property="og:url" content="<?php echo SITE_URL_CURRENT; ?>" />
<meta property="og:image" content="https://www.seiessenzialmentebella.com/assets/assets-frontend/img/seibella.png" />
<meta property="og:site_name" content="<?php echo SITE_TITLE_CURRENT;?>" />

<link rel="icon" href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/favicon.ico" type="image/x-icon">

<!-- Bootstrap Core CSS -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/bootstrap.min.css" rel="stylesheet">

<!-- Theme CSS -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/freelancer.css" rel="stylesheet">
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/custom.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet" type="text/css" />-->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- GoogleAnalytics -->
<?php include_once("analyticstracking.php") ?>