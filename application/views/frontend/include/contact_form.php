<!-- Contact Section -->
<section id="contact" class="<? echo (isset($contactclass) && $contactclass != '' ? $contactclass : 'section' ); ?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            	<?
					if($contacttitle)
                		echo '<h3 class="blu-text">'.lang('MENU_CONTACTME').'</h3>';
				?>
              <!--  <hr class="star-primary">
                <div class="orange-text" align="center" style="font-size:20px"><b><//? echo $contacttext; ?></b>
</div>-->
              <br/>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                <form name="sentMessage" id="contactForm" novalidate>
                	<div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="surname"><?php echo lang('LABEL_SURNAME'); ?></label>
                            <input type="text" class="form-control" placeholder="<?php echo lang('LABEL_SURNAME'); ?> *" id="surname" required data-validation-required-message="<?php echo lang('MSG_REQUIRED_SURNAME'); ?>">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="name"><?php echo lang('LABEL_NAME'); ?></label>
                            <input type="text" class="form-control" placeholder="<?php echo lang('LABEL_NAME'); ?> *" id="name" required data-validation-required-message="<?php echo lang('MSG_REQUIRED_NAME'); ?>">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="city"><?php echo lang('LABEL_CITY'); ?></label>
                            <input type="text" class="form-control" placeholder="<?php echo lang('LABEL_CITY'); ?> *" id="city" required data-validation-required-message="<?php echo lang('MSG_REQUIRED_CITY'); ?>">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="email"><?php echo lang('LABEL_EMAIL'); ?></label>
                            <input type="email" class="form-control" placeholder="<?php echo lang('LABEL_EMAIL'); ?> *" id="email" required data-validation-required-message="<?php echo lang('MSG_REQUIRED_EMAIL'); ?>" data-validation-email-message="<?php echo lang('MSG_VALID_EMAIL'); ?>">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="phone"><?php echo lang('LABEL_PHONE'); ?></label>
                            <input type="tel" class="form-control" placeholder="<?php echo lang('LABEL_PHONE'); ?> *" id="phone" name="phone" required data-validation-required-message="<?php echo lang('MSG_REQUIRED_PHONE'); ?>" data-validation-number-message="<?php echo lang('MSG_VALID_PHONE'); ?>">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                 <!--    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <div class="form-group col-xs-6" style="padding-left:0px;">
                                <label for="studio" style="opacity:100 !important;font-size: 20px;font-weight: 400;color: #999;top: 1.5em;"><//?php echo lang('LABEL_LOCATION'); ?></label>
                            </div>
                            <div class="form-group col-xs-6">
                            <select class="form-control" id="studio" data-validation-required-message="<//?php echo lang('MSG_REQUIRED_OFFICE'); ?>" style="margin-top:25px;font-size: 1.5em;height: 50px !important;">
                                <option value=""><//?php echo lang('LABEL_CHOOSE'); ?> </option>
                           <//php
							foreach ($studiList as &$studio) {
							?>
                            	<option value="<//?php echo $studio->id_studio_medico; ?>"><//?php echo $studio->citta. ' - ' . $studio->indirizzo; ?></option>
						<//?php
				 		}
						?>     
                            </select>
                            </div>
                       		<div class="form-group col-xs-12" style="padding-left:0px;">
                            	<p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div> -->
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="message"><?php echo lang('LABEL_MESSAGE'); ?></label>
                            <textarea rows="5" class="form-control" placeholder="<?php echo lang('LABEL_MESSAGE'); ?> *" id="message" required data-validation-required-message="<?php echo lang('MSG_REQUIRED_MESSAGE'); ?>"></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12" style="padding-top:15px;font-size:20px !important;font-weight:bold;" align="center">
                            <input type="checkbox" name="checkPrivacy" name="checkPrivacy" value="check" required data-validation-required-message="<?php echo lang('MSG_REQUIRED_CHECK'); ?>">
                            <?php echo lang('SUBSCRIBE_NEWSLETTER_PRIVACY'); ?>
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <br>
                    <div id="success" align="center"></div>
                    <div class="row" align="center">
                        <div class="form-group col-xs-12">
                            <button type="submit" id="submit-contact-btn" class="btn btn-success btn-lg"><?php echo lang('SEND_MESSAGE_BTN'); ?></button>
                        </div>
                    </div>
                    <input type="hidden" id="provenienza" name="provenienza" value="<? echo (isset($provenienza) ? $provenienza : ''); ?>"/>
                </form>
            </div>
        </div>
    </div>
</section>