<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
    <div class="container">
        <!--<div class="col-md-12 hide-onmobile" align="center">
            <img src="img/seibella_orizz.png" height="150">
            <img src="img/servicios.png" width="800" style="margin-top:30px;margin-bottom:10px;">
        </div>-->
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span><i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="<?php echo SITE_URL_PATH; ?>"><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/seibella.png" class="logo-nav-img"></a>
          <!--  <a class="navbar-brand show-onmobile " href="<?//php echo SITE_URL_PATH; ?>"><img src="<?// echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/seibella.png" class="logo-nav-img"></a>-->
            <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/<?php echo lang('HOME_SERVICIOS_PATH');?>" class="servicios-img" />
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li class="page-scroll">
                    <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_HOME')); ?>"><?php echo lang('MENU_HOME'); ?></a>
                </li>
                <li class="page-scroll dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <?php echo lang('MENU_ABOUTME'); ?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_ABOUTME')); ?>">&bull; <?php echo lang('MENU_ABOUTME'); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_BIO')); ?>">&bull; <?php echo lang('MENU_BIO'); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_MYWORK')); ?>">&bull; <?php echo lang('MENU_MYWORK'); ?></a>
                        </li>
                    </ul>
                </li>
             <!--     <li class="page-scroll">
                    <a href="<//?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_MYWORK')); ?>"><//?php echo lang('MENU_MYWORK'); ?></a>
                </li>-->
                <li class="page-scroll dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <?php echo lang('MENU_PROGRAMS'); ?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                    	 <?php
                         if(!empty($programmiList)) {
	  						foreach ($programmiList as &$progamma) {
								?>
                                <li>
                                    <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_PROGRAMS')); ?>/<?php echo $progamma->id_programma; ?>/<? echo cleanString($progamma->nome_programma, true); ?>">&bull; <?php echo $progamma->nome_programma; ?></a>
                                </li>
                                <?php
							}
						 } ?>
                    </ul>
                </li>
                <li class="page-scroll dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <?php echo lang('MENU_TREATMENTS'); ?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                    	 <?php
                         if(!empty($trattamentiList)) {
	  						foreach ($trattamentiList as &$trattamento) {
								?>
                                <li>
                                    <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_TREATMENTS')); ?>/<?php echo $trattamento->id_trattamento; ?>/<? echo cleanString($trattamento->nome_trattamento, true); ?>">&bull; <?php echo $trattamento->nome_trattamento; ?></a>
                                </li>
                                <?php
							}
						 } ?>
                    </ul>
                </li>
                <li class="page-scroll dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <?php echo lang('MENU_RETREATS'); ?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                    	 <?php
                    	 if(!empty($retreatsList)) {
                    	     foreach ($retreatsList as &$retreat) {
								?>
                                <li>
                                    <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_RETREATS')); ?>/<?php echo $retreat->retreats_id; ?>">&bull; <?php echo $retreat->retreats_name; ?></a>
                                </li>
                                <?php
							}
						 } ?>
                    </ul>
                </li>
                <li class="page-scroll">
                    <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_GRATIS')); ?>"><?php echo lang('MENU_GRATIS'); ?></a>
                </li>
                <?php 
                $studi_medici_enabled = false; 
                if($studi_medici_enabled) {
                ?>
                <li class="page-scroll dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_STUDIES')); ?>" role="button" aria-haspopup="true" aria-expanded="false">
                      <?php echo lang('MENU_STUDIES'); ?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu"> 
                         <?php
                         if(!empty($studiList)) {
	  						foreach ($studiList as &$studio) {
								?>
                                <li>
                            <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_STUDIES')); ?>">&bull; <?php echo $studio->citta; ?> <br class="rwd-break">- <?php echo $studio->indirizzo; ?></a>
                        </li>
                                <?php
							}
						 } ?>
                    </ul>
                </li>
                <?php } ?>
                <li class="page-scroll">
                    <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_CONTACTME')); ?>"><?php echo lang('MENU_CONTACTME'); ?></a>
                </li>
                <li class="page-scroll dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?php echo lang('MENU_LANGUAGE'); ?><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                    	<?php 
							// load installed langs 
							foreach ($installedLangs as $lingua) {
						?>
						<li>
                            <a href="<?php echo base_url(); ?>LangSwitch/switchLanguage/<? echo $lingua->codice_ci; ?>/<? echo $this->uri->segment(1).'/'.$this->uri->segment(2).($this->uri->segment(3) != '' ? '/'.$this->uri->segment(3) : '').($this->uri->segment(4) != '' ? '/'.$this->uri->segment(4) : ''); ?>"><img src="<?php echo ASSETS_ROOT_FOLDER_FRONTEND_IMG . '/flags/'.strtoupper(substr($lingua->codice_ci, 0, 2)).'.png'; ?>" alt="<? echo $lingua->codice_ci; ?>">&nbsp;<? echo strtoupper(substr($lingua->codice_ci, 0, 2)); ?>
                            </a>
                        </li>
						<? } ?>   
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>