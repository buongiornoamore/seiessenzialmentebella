<!-- Footer -->
<footer class="text-center">
    <div class="footer-below white-text" style="background-color:#23bdc3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                    	<? if(FACEBOOK_LINK != '') {?>
                        <li>
                            <a href="<? echo FACEBOOK_LINK; ?>" class="btn-social btn-outline" target="_blank"><span class="sr-only">Facebook</span><i class="fa fa-fw fa-facebook"></i></a>
                        </li>
                        <? } ?>
                        <? if(INSTAGRAM_LINK != '') {?>
                        <li>
                            <a href="<? echo INSTAGRAM_LINK; ?>" class="btn-social btn-outline" target="_blank"><span class="sr-only">Instagram</span><i class="fa fa-fw fa-instagram"></i></a>
                        </li>
                        <? } ?>
                        <? if(YOUTUBE_LINK != '') {?>
                        <li>
                            <a href="<? echo YOUTUBE_LINK; ?>" class="btn-social btn-outline" target="_blank"><span class="sr-only">Youtube</span><i class="fa fa-fw fa-youtube"></i></a>
                        </li>
                        <? } ?>
                        <li>
                            <a href="mailto:sei@seiessenzialmentebella.com" class="btn-social btn-outline" target="_blank"><span class="sr-only">Email</span><i class="fa fa-fw fa-envelope"></i></a>
                        </li>
                    </ul>
                    <b><a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_MYWORK')); ?>" class="white-text"><?php echo lang('MENU_MYWORK'); ?></a> | <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_PRIVACY')); ?>" class="white-text"><?php echo lang('MENU_PRIVACY'); ?></a> | <a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_CONTACTME')); ?>" class="white-text"><?php echo lang('MENU_CONTACTME'); ?></a> <br>© Seiessenzialmentebella 2017</b>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
    <a class="btn btn-primary" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>

<!-- jQuery -->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/jqBootstrapValidation.js"></script>
<script type="text/javascript">
	// Message vars global for contact me
//	window.success_message_sendmail = "<?//php echo lang('MSG_SUCCESS_CONTACT'); ?>";
	window.failure_message_sendmail = "<?php echo lang('MSG_FAILURE_CONTACT'); ?>";
	window.mail_sendsave_url = "<? echo base_url();?>mail/send_save";
	window.mail_newsletter_url = "<? echo base_url();?>mail/newsletter";
</script>	
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/contact_me.js"></script>

<!-- Theme JavaScript -->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/freelancer.js"></script>