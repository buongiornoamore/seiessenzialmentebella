<!DOCTYPE html>
<html lang="en">

<head>
    <title>Orlena Zotti | <?php echo lang('MENU_PROGRAMS'); ?></title>
	<? require_once("include/header_common.php"); ?>
</head>

<body id="page-top" class="index">
    <? require_once("include/header.php"); ?>
	<div class="topNavSpacerBig"></div> 
    <br>
	<section class="section-quarter" style="background-image:url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/br_rombi_repeat.png);background-repeat: repeat-x; color:white;" id="ilmiolavoro">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p style="margin:0;line-height:1;" class="camille text-big-camille"><?php echo lang('MENU_PROGRAMS'); ?></p>
    			</div>
            </div>
        </div>
    </section>
    <header class="header-image-blank" style="background: rgba(0, 0, 0, 0) url(<?php echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/programmi/<?php echo $prog->immagine_programma; ?>) no-repeat scroll center center / cover;">
        <div class="headline-blank">
            <div class="container">
            </div>
        </div>
    </header>
    <header class="header-image-blank-mobile" style="background: rgba(0, 0, 0, 0) url(<?php echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/programmi/<?php echo $prog->immagine_programma_mobile; ?>) no-repeat scroll center center / cover;">
        <div class="headline-blank">
            <div class="container">
            </div>
        </div>
    </header>
    <section class="section-quarter" style="background-image:url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/br_loto_repeat.png);background-repeat: repeat-x; color:white;" id="ilmiolavoro">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p style="margin:0;line-height:1;" class="text-30"><?php echo $prog->frase_programma; ?><br><br><span style="font-style:italic">Orlena Zotti</span></p>
    			</div>
            </div>
        </div>
    </section>
	<section class="section-half">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="blu-text text-30" align="center"><b><?php echo $prog->nome_programma; ?></b></p>
                    <p align="justify"><?php echo $prog->descrizione_programma; ?></p>
                    <?php
                    if(!empty($benefici)) {
					?>
                    <br>
                    <br>
                    <p align="center">
                       <span class="orange-text text-30"><b><?php echo lang('BENEFITS_QUESTION'); ?></b></span>
                    </p>
                    <?php } ?>
                </div>
                <div class="col-lg-9 col-lg-offset-3">
                	<p>   
                      <ul class="benefici-ul">
                      <?php
                      	 if(!empty($benefici)) {
	  						foreach ($benefici as &$ben) {
								?>
                                <li><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/loto.png"><?php echo $ben->testo_benefici;?></li>
                                <?php
							}
						 }
					  ?>	 
                      </ul>
                    </p>
                    <br>
                    <br>
                </div>  
            </div>
        </div>
    </section>
    <?php
    if(!empty($trattamenti)) {
    ?>
	<section class="section-quarter" style="background-image:url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/br_rombi_repeat.png);background-repeat: repeat-x;color: white;">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p>
                	<span class="text-big"><b><?php echo lang('PROGRAM_TREATMENTS'); ?></b></span><br>
<span style="font-size:22px;"><b><?php echo lang('PROGRAM_TREATMENTS_DESC'); ?></b></span>
					</p>
    			</div>
            </div>
        </div>
    </section>
    <section class="section-quarter">
    	<div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-4">
                     <ul class="trattamenti-ul">
                     	<?php
						foreach ($trattamenti as &$tratt) {
							?>
							 <li><a href="<?php echo createUrlMenu(lang('LANGUAGE_ABBR').'/'.lang('MENU_TREATMENTS')); ?>/<?php echo $tratt->id_trattamento;?>/<?php echo cleanString($tratt->nome_trattamento, true);?>"><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/loto.png"><?php echo $tratt->nome_trattamento;?></a></li>
							<?php
						}
					    ?>	 
                     </ul>
            	</div>         
    		</div>
        </div>
    </section>
	<? } ?>
    <?php
    if(!empty($gallery)) {
    ?>   
    <br>  
    <section class="section-quarter" style="background-color:#23bdc3; color:white;">
    	<div class="container">            
            <div class="row">
                <!-- The carousel -->
                <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel"  style="text-align:center">
                    <!-- Indicators -->
                    <ol class="carousel-indicators" style="bottom:-35px;">
                    	<?php
						$i_gal = 0;
						foreach ($gallery as &$gal) {
						?>
                        	<li data-target="#transition-timer-carousel" data-slide-to="<? echo $i_gal; ?>" class="<?php echo ($i_gal == 0 ? 'active' : ''); ?>"></li>
                        <?php
							$c_gal++;
						}
					    ?>	 
                    <!--    <li data-target="#transition-timer-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#transition-timer-carousel" data-slide-to="1"></li>
                        <li data-target="#transition-timer-carousel" data-slide-to="2"></li>
                        <li data-target="#transition-timer-carousel" data-slide-to="3"></li>-->
                    </ol>
        
                    <!-- Wrapper for slides -->		 
                    <div class="carousel-inner">
                    	<?php
						$c_gal = 0;
						foreach ($gallery as &$gal) {
							?>
                             <div class="item <?php echo ($c_gal == 0 ? 'active' : ''); ?>">
                                <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/gallery/<?php echo $gal->img_gallery;?>" alt="<?php echo $prog->nome_programma; ?>">
                            </div>
							<?php
							$c_gal++;
						}
					    ?>	 
                    </div>
        			
                    <!-- Controls -->
                    <a class="left carousel-control" href="#transition-timer-carousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#transition-timer-carousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                    
                    <!-- Timer "progress bar" 
                    <hr class="transition-timer-carousel-progress-bar animate" />-->
                </div>
            </div>
        </div> 
    </section>    
    <?php } ?> 
    <br> 
    <br>          
    <div align="center">
    	<p><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flor_orange.png" class="flower-divider" /></p>
    </div> 
    <br>
    <? 
		$contactclass = 'section-quarter';
		$contacttitle = true;	
		$contacttext = lang('CONTACTME_DESC');	
		$provenienza = $prog->nome_programma;
		require_once("include/contact_form.php");
	?>
    <? require_once("include/footer.php"); ?>
	<script type="text/javascript">
		$(document).ready(function($){
		});
        $(window).load(function() {	
        });
    </script>		
</body>

</html>
