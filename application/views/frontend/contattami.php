<!DOCTYPE html>
<html lang="en">

<head>
    <title>Orlena Zotti | <?php echo lang('MENU_CONTACTME'); ?></title>
	<? require_once("include/header_common.php"); ?>
</head>

<body id="page-top" class="index">
    <? require_once("include/header.php"); ?>
	<div class="topNavSpacerBig"></div>
    <br>
    <!-- su di me section -->
	<section class="section-quarter section-loto" id="sudime">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p style="margin:0;"><?php echo lang('MENU_CONTACTME'); ?></p>
    			</div>
            </div>
        </div>
    </section>
    <!-- contact module -->
    <? 
		$contactclass = 'section-quarter';
		$contacttitle = false;		
		$contacttext = lang('CONTACTME_DESC');	
		//$contacttext = 'Se hai qualche domanda, se vuoi prenotare un appuntamento o chiedere qualche informazione.';
		$provenienza = 'contattami';
		require_once("include/contact_form.php");
	?>
    <div align="center">
    	<p><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flor_orange.png" class="flower-divider" /></p>
    </div> 
    <br><br>
    <? require_once("include/footer.php"); ?>
	<script type="text/javascript">
		$(document).ready(function($){
		});
    </script>	
</body>

</html>
