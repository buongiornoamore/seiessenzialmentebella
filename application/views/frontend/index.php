<!DOCTYPE html>
<html lang="en">
<head>
    <title>Orlena Zotti | <?php echo SITE_TITLE_CURRENT;?></title>
	<? require_once("include/header_common.php"); ?>
</head>
<body id="page-top" class="index">  
	<? require_once("include/header.php"); ?>
     <header class="header-image hide-on-mobile" style="background: rgba(0, 0, 0, 0) url(<?php echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/<?php echo lang('HOME_PC_TOP_IMAGE'); ?>) no-repeat scroll center center / cover;">
        <div class="headline">
            <div class="container">
            </div>
        </div>
    </header>
    <header class="header-image show-on-mobile" style="background: rgba(0, 0, 0, 0) url(<?php echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/<?php echo lang('HOME_PC_TOP_IMAGE_MOBILE'); ?>) no-repeat scroll center center / cover;">
        <div class="headline">
            <div class="container">
            </div>
        </div>
    </header>
    <!--
    <header class="header-image">
        <div class="headline">
            <div class="container">
            </div>
        </div>
    </header>-->

    <!-- About Section -->
    <section class="section-half">
        <div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                    <p class="text-24"><?php echo stripslashes(lang('HOME_PC_TESTO_BOLD_1')); ?></p>
				</div>
            </div>
        </div>
    </section>	

	<section class="section-quarter" style="background-image:url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/br_loto_repeat.png); color:white;">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p class="text-24"><?php echo stripslashes(lang('HOME_PC_TESTO_BOLD_2')); ?></p>
    			</div>
            </div>
        </div>
    </section>
	<section class="section-half">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
					<p class="orange-text text-24"><b><?php echo stripslashes(lang('HOME_PC_TESTO_BOLD_3')); ?></b></p>
					<br><br>
                    <div class="col-lg-6 col-lg-push-6" align="center">
                    	<img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/'.lang('HOME_PC_IMMAGINE_1'); ?>" style="width: 100%;" alt="Seiessenzialmentebella.com - Dott. Orlena zotti" title="Seiessenzialmentebella.com - Dott. Orlena zotti" />
                    </div>
                    <div class="col-lg-6 col-lg-pull-6" align="center">
                        <p><?php echo stripslashes(lang('HOME_PC_TESTO_BODY_1')); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
                
	<?php
    if(!empty($commenti)) {
    ?>
    <section class="section-zero" style="background-color:#fbad18; color:white;">
    	<div class="container">            
            <div class="row">
                <!-- The carousel -->
                <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                    	<?php
						$i_ind = 0;
						foreach ($commenti as &$commento) {
							?>
                            <li data-target="#transition-timer-carousel" data-slide-to="<?php echo $i_ind; ?>" class="<?php echo ($i_ind == 0 ? 'active' : ''); ?>"></li>
							<?php
							$i_ind++;
						}
						?>	
                    </ol>
        		
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                    	<?php
						$i_commm = 0;
						foreach ($commenti as &$commento) {
							?>
							<div class="item <?php echo ($i_commm == 0 ? 'active' : ''); ?>" style="min-height:250px;">
                                <div class="carousel-caption">
                                    <h1 class="carousel-caption-header"><?php echo $commento->nome_commento; ?></h1>
                                    <p class="carousel-caption-text"><?php echo $commento->testo_commento; ?></p>
                                </div>
                            </div>
							<?php
							$i_commm++;
						}
						?>	
                    </div>
        
                    <!-- Controls -->
                    <a class="left carousel-control" href="#transition-timer-carousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#transition-timer-carousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                    
                    <!-- Timer "progress bar" 
                    <hr class="transition-timer-carousel-progress-bar animate" />-->
                </div>
            </div>
        </div> 
     </section>    
    <?php            
    }
    ?>	 				
  <!--  <div align="center">
    	<p><img src="img/flor_verde.png" /></p>
    </div>-->
	<!-- Newsletter Section -->
    <section id="newsletter" class="section-half">
        <div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<form name="subscribe" id="newsletterForm" novalidate>
                        <div class="row control-group">
                        	<h3 class="blu-text"><?php echo lang('SUBSCRIBE_NEWSLETTER_H1'); ?></h3>
                            <div class="orange-text" align="center" style="padding-bottom:15px;"><p style="font-size:20px !important;"><b><?php echo lang('SUBSCRIBE_NEWSLETTER_DESC'); ?></b></p>
</div>
							<div class="form-group controls col-xs-12 col-lg-4 col-lg-offset-4">
                                <input type="text" class="form-control" placeholder="<?php echo lang('SUBSCRIBE_NEWSLETTER_NAME'); ?>*" id="name" required data-validation-required-message="<?php echo lang('SUBSCRIBE_NEWSLETTER_NAME'); ?>" style="font-size: 1.5em;height: 50px !important;">    
                            </div>
                            <div class="form-group controls col-xs-12 col-lg-4 col-lg-offset-4">
                                <input type="email" class="form-control" placeholder="<?php echo lang('SUBSCRIBE_NEWSLETTER_EMAIL'); ?>*" id="email" required data-validation-required-message="<?php echo lang('SUBSCRIBE_NEWSLETTER_EMAIL'); ?>" style="font-size: 1.5em;height: 50px !important;" data-validation-email-message="<?php echo lang('MSG_VALID_EMAIL'); ?>"> 
                            </div>
                            <div class="form-group controls col-xs-12 col-lg-4 col-lg-offset-4" style="font-size:16px !important;font-weight:bold;">
								<?php echo lang('SUBSCRIBE_NEWSLETTER_PRIVACY'); ?>
                            </div>
                            <div class="col-xs-12 col-lg-4 col-lg-offset-4" align="center">
                            	<p class="help-block text-danger"></p>
                            </div>
                        </div>
                  		<div id="success" align="center"></div>
                        <div class="row control-group" align="center">
                            <div class="form-group col-xs-12">
                                <button type="submit" id="submit-news-btn" class="btn btn-success btn-lg"><?php echo lang('SUBSCRIBE_BTN'); ?></button>
                            </div>
                        </div>
                    </form>    
    			</div>
            </div>
        </div>
    </section>
    <!-- footer and js import -->
 	<? require_once("include/footer.php"); ?>
    
	<script>
	$(document).ready(function () {
	
		$(".navbar-nav li a").click(function (event) {
		// check if window is small enough so dropdown is created
		//console.log('clicked');
	   // var toggle = $(".navbar-toggle").is(":visible");
	  //  if (toggle) {
	   //   $(".navbar-collapse").collapse('hide');
	   // }
	  });
	
	});
	</script>
</body>

</html>
