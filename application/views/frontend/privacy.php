<!DOCTYPE html>
<html lang="en">

<head>
    <title>Orlena Zotti | <?php echo lang('MENU_PRIVACY'); ?></title>
	<? require_once("include/header_common.php"); ?>
</head>

<body id="page-top" class="index">
    <? require_once("include/header.php"); ?>
	<div class="topNavSpacerBig"></div> 
    <br>
	<section class="section-quarter section-loto" id="privacy">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p style="margin:0;"><?php echo lang('MENU_PRIVACY'); ?></p>
    			</div>
            </div>
        </div>
    </section>
	<section class="section-half">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center"> 
                 	<?php echo stripslashes(lang('PRIVACY_PC_TESTO_BODY_1')); ?>
                </div>
            </div>
        </div>
    </section>
    <div align="center">
    	<p><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flor_verde.png" class="flower-divider" /></p>
    </div> 
    <br><br>
    
    <? require_once("include/footer.php"); ?>
	<script type="text/javascript">
		$(document).ready(function($){
		});
        $(window).load(function() {	
        });
    </script>		
</body>

</html>
