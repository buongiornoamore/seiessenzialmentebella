<!DOCTYPE html>
<html lang="en">

<head>
    <title>Orlena Zotti | <?php echo lang('MENU_TREATMENTS'); ?></title>
    <? require_once("include/header_common.php"); ?>
</head>

<body id="page-top" class="index">
    <? require_once("include/header.php"); ?>
	<div class="topNavSpacerBig"></div> 
    <br>
    <section class="section-quarter" style="background-image:url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/br_rombi_repeat.png);background-repeat: repeat-x; color:white;" id="ilmiolavoro">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p style="margin:0;line-height:1;" class="camille text-big-camille"><?php echo lang('MENU_TREATMENTS'); ?></p>
    			</div>
            </div>
        </div>
    </section>
	<section class="section-quarter">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                    <p class="blu-text text-30"><?php echo $tratt->nome_trattamento;?></p>
                    <p align="justify" class="text-24"><?php echo $tratt->descrizione_trattamento;?></p>
					<?php
				//	var_dump($benefici);
                     if(!empty($benefici)) {
                     ?>
                    <br>
                    <br>
                    <p align="center">
                       <span class="orange-text text-24"><b><?php echo lang('BENEFITS_QUESTION'); ?></b></span>
                    </p>
                    <? } ?>
                </div>
                <div class="col-lg-8 col-lg-offset-4 col-xs-12">
                	<p>   
                       <ul class="benefici-ul">
                       	<?php
                      	 if(!empty($benefici)) {
	  						foreach ($benefici as &$ben) {
								?>
                                <li><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/loto.png"><?php echo $ben->testo_benefici;?></li>
                                <?php
							}
						 }
					    ?>	 
                       </ul>
                    </p>
                    <br>
                    <br>
                </div>  
            </div>
        </div>
    </section>
    <?php  if(!empty($faq)) { ?>
    <section class="section-quarter" style="background-image:url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/br_loto_repeat.png);background-repeat: repeat-x; color:white;">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p>
                	<span class="camille text-big-camille" style="line-height:1"><?php echo lang('LABEL_FAQ'); ?></span>
					</p>
    			</div>
            </div>
        </div>
    </section>
    <section class="section-half">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<?php
						foreach ($faq as &$f) {
							?>
							<span class="orange-text text-24"><b><?php echo $f->testo_faq; ?></b></span>
                            <? if($f->allineamento_faq != 'list') { ?>
								<p align="<?php echo $f->allineamento_faq; ?>" style="padding-bottom:15px;">
							   		<?php echo $f->risposta_faq; ?>
								</p>
                            <? } else { ?>
								<div align="justify" class="listaFaq" style="padding-bottom:15px;">
							   		<?php echo $f->risposta_faq; ?>
								</div>
                            <?
							}
						}
					 ?>	 
     			</div>
            </div>
        </div>
    </section>       
    <?php } ?>         
    <div align="center">
    	<p><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flor_verde.png" class="flower-divider" /></p>
    </div> 
    <br>
    <? 
		$contactclass = 'section-quarter';
		$contacttitle = true;	
		$contacttext = lang('CONTACTME_DESC');	
		$provenienza = $tratt->nome_trattamento;
		require_once("include/contact_form.php");
	?>
    <? require_once("include/footer.php"); ?>

	<script type="text/javascript">
		$(document).ready(function($){
		});
        $(window).load(function() {	
        });
    </script>		
</body>

</html>
