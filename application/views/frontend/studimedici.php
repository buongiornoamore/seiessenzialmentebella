<!DOCTYPE html>
<html lang="en">

<head>
    <title>Orlena Zotti | <?php echo lang('MENU_STUDIES'); ?></title>
	<? require_once("include/header_common.php"); ?>
    <style>
		 .map-class {
			width: 100%; height: 400px; margin-top: 40px; margin-bottom: 40px;
		}
	</style>
</head>

<body id="page-top" class="index">
    <? require_once("include/header.php"); ?>
	<div class="topNavSpacerBig"></div> 
    <br>
    <!-- su di me section -->
	<section class="section-quarter section-loto" id="sudime">
    	<div class="container">
            <div class="row">
                <div class="col-lg-12" align="center">
                	<p style="margin:0;"><?php echo lang('MENU_STUDIES'); ?></p>
    			</div>
            </div>
        </div>
    </section>
   
	<section class="section-half">
    	<div class="container">
            <div class="row">  
            	 <?php
				 if(!empty($studiList)) {
					$map_i = 1;
					foreach ($studiList as &$studio) {
					?>
                        <div class="col-lg-12" align="center" id="map_<?php echo $map_i; ?>">
                            <div id="map<?php echo $map_i; ?>" class="map-class" data-citta="<?php echo $studio->citta; ?>" data-indirizzo="<?php echo $studio->indirizzo; ?>" data-lat="<?php echo $studio->lat; ?>" data-long="<?php echo $studio->long; ?>"></div>  
                        </div>     
					<?php
						$map_i++;
					} 
				 }
					?>       
            </div>
        </div>
    </section>
   
    <div align="center">
    	<p><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flor_orange.png" class="flower-divider" /></p>
    </div> 
    <br><br>
    
    <? require_once("include/footer.php"); ?>    
    <!--Google Maps API-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyaEReZsP8ZV6_I_CXDLZ4eRIPoEsKtJo"></script>
     
	<script type="text/javascript">
		$(document).ready(function($){
			// anchor load
			//var ancorid = '<?// echo $ancorid; ?>';
			//console.log('ancorid ' + ancorid);
			//if(ancorid != '')
			//	scrollToAnchor(ancorid, 0, null);
		});
        $(window).load(function() {	
        });
		function scrollToAnchor(aid, marginTop, event){//* Ancoraggio del menu al click *//
			if(event != null)
				event.preventDefault();
			console.log('scrollToAnchor ' + aid);
			var aTag = $("#"+ aid);
			$('html,body').animate({scrollTop: aTag.offset().top - marginTop}, 500);
		}
		$(function () {
			function initMaps() {
				
			/*	data-citta
				data-indirizzo
				data-lat
				data-long*/
				var c_map = 1;
				$(".map-class").each(function() {
    				console.log($(this).data('citta'));
					initMap('map'+c_map, $(this).data('lat'), $(this).data('long'), $(this).data('citta'), $(this).data('indirizzo'));
					c_map++;
				});

			/*	initMap('map', 39.9039268, 8.5871362, 'Oristano', 'Via Giovanni XXIII, 10 - 09170 (OR)');
				initMap('map2', 40.325406, 9.318223, 'Nuoro', 'Via Einaudi, 48 - 08100 (NU)');
				initMap('map3', 39.226461, 9.129793, 'Cagliari', 'Viale Marconi, 26 - 09131 (CA)');
				initMap('map4', 39.9262792, 9.6779038, 'Tortolì', 'Via Bertulottu - 08048 (OG)');*/
			}
			
			function initMap(id, lat, lng, name, address) {
		
				var location = new google.maps.LatLng(lat, lng);
		
				var mapCanvas = document.getElementById(id);
				var mapOptions = {
					center: location,
					zoom: 14,
					panControl: false,
					scrollwheel: false,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				var map = new google.maps.Map(mapCanvas, mapOptions);
		
				var markerImage = '<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/marker_sb.png';
		
				var marker = new google.maps.Marker({
					position: location,
					map: map,
					icon: markerImage
				});
		
				var contentString = '<div class="info-window">' +
						'<h3>'+name+'</h3>' +
						'<div class="info-content">' +
						'<p>'+address+'</p>' +
						'</div>' +
						'</div>';
		
				var infowindow = new google.maps.InfoWindow({
					content: contentString,
					maxWidth: 400
				});
		
				marker.addListener('click', function () {
					infowindow.open(map, marker);
				});
				infowindow.open(map,marker);
		
			/*	var styles = [{"featureType": "landscape", "stylers": [{"saturation": -100}, {"lightness": 65}, {"visibility": "on"}]}, {"featureType": "poi", "stylers": [{"saturation": -100}, {"lightness": 51}, {"visibility": "simplified"}]}, {"featureType": "road.highway", "stylers": [{"saturation": -100}, {"visibility": "simplified"}]}, {"featureType": "road.arterial", "stylers": [{"saturation": -100}, {"lightness": 30}, {"visibility": "on"}]}, {"featureType": "road.local", "stylers": [{"saturation": -100}, {"lightness": 40}, {"visibility": "on"}]}, {"featureType": "transit", "stylers": [{"saturation": -100}, {"visibility": "simplified"}]}, {"featureType": "administrative.province", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "elementType": "labels", "stylers": [{"visibility": "on"}, {"lightness": -25}, {"saturation": -100}]}, {"featureType": "water", "elementType": "geometry", "stylers": [{"hue": "#ffff00"}, {"lightness": -25}, {"saturation": -97}]}];
		
				map.set('styles', styles);*/
	
				map.panToWithOffset(location, 0, -100);
			}
			
			google.maps.Map.prototype.panToWithOffset = function(latlng, offsetX, offsetY) {
				var map = this;
				var ov = new google.maps.OverlayView();
				ov.onAdd = function() {
					var proj = this.getProjection();
					var aPoint = proj.fromLatLngToContainerPixel(latlng);
					aPoint.x = aPoint.x+offsetX;
					aPoint.y = aPoint.y+offsetY;
					map.panTo(proj.fromContainerPixelToLatLng(aPoint));
				}; 
				ov.draw = function() {}; 
				ov.setMap(this); 
			};
		
			google.maps.event.addDomListener(window, 'load', initMaps);
		});
    </script>
</body>

</html>
