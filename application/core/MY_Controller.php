<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('framework_helper');
		$this->load->library('form_validation');
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
		$this->lang->load('auth', 'italian');
		$this->lang->load('ion_auth');
	}
	
}

class Frontend_Controller extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	function show_view_with_menu($view_name, $data) {
		$this->default_controller_mode();
		$this->checkLanguagePermalink();
		
	    // programmi
		$this->db->select('*');
		$this->db->from('programmi');
		$this->db->join('programmi_traduzioni', 'programmi_traduzioni.id_programma = programmi.id_programma');
		$this->db->where('programmi.stato_programma', 1);
		$this->db->where('programmi_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$query_prog = $this->db->get();
		$data['programmiList'] = $query_prog->result();
	    // trattamenti
	    $this->db->select('*');
		$this->db->from('trattamenti');
		$this->db->join('trattamenti_traduzioni', 'trattamenti_traduzioni.id_trattamento = trattamenti.id_trattamento');
		$this->db->where('trattamenti.stato_trattamento', 1);
		$this->db->where('trattamenti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$query_tratt = $this->db->get();
		$data['trattamentiList'] = $query_tratt->result();
		// retreats
		$this->db->select('*');
		$this->db->from('retreats');
		$this->db->where('retreats.retreats_lang_id', lang('LANGUAGE_ID'));
		$query_ret = $this->db->get();
		$data['retreatsList'] = $query_ret->result();
	    // studi medici
		$this->db->select('*');
		$this->db->from('studi_medici');
		$this->db->where('stato', 1);
		$query_sm = $this->db->get();
		$data['studiList'] = $query_sm->result();
		// lingue
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('stato_lingua >', 0);
		$query_lang = $this->db->get();
		$data['installedLangs'] = $query_lang->result();
		
		// pagine attive o disattive dal menu
	    $this->load->view($view_name, $data); // the actual view you wanna load
	}
	
	function default_controller_mode() {
		// se DEFAULT_CONTROLLER è COMINGSOON redireziona sempre li tranne che per utente admin loggato
		if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
		{
			if($this->router->routes['default_page'] == 'Comingsoon')
				redirect('comingsoon');	
		}
	}
	
	function checkLanguagePermalink() {
		if($this->uri->segment(1) != strtolower(lang('LANGUAGE_ABBR'))) {
			$this->db->select('codice_ci');
			$this->db->from('lingue');
			$this->db->where('abbr_lingue', strtoupper($this->uri->segment(1)));
			$query_lang = $this->db->get();
			$curr_lang = $query_lang->row();
			$this->session->set_userdata('site_lang', $curr_lang->codice_ci);
			$this->config->set_item('language', $curr_lang->codice_ci);
			redirect(base_url().$this->uri->uri_string());
		}
	}
	
}

class Admin_Controller extends MY_Controller {
	
	public $site_status = 'offline';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('framework_helper');
		$this->load->helper('file_helper');
		$this->load->library('grocery_CRUD');
		$this->load->library('form_validation');
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
		$this->lang->load('auth', 'italian');
		$this->lang->load('ion_auth');
	}
	
	function checkUserPermissions() 
	{
		if (!$this->ion_auth->logged_in())
		{
			log_message('info','********************* LOGGED IN ** index() - NOT LOGGED ');
			// redirect them to the login page
			 redirect('admin/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			log_message('info','********************* LOGGED IN ** index() index() - NOT ADMIN ');
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		log_message('info','********************* LOGGED IN ** index() index() - ADMIN ');
	}
	
}