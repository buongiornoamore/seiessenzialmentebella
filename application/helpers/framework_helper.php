<?php defined('BASEPATH') OR exit('No direct script access allowed.');

if (!function_exists('stampaValutaHtml')) {

    /* stampa valuta da duoble con . a stringa con , in carratteri html */
    function stampaValutaHtml($val, $ico, $zero) {
		$val = number_format($val, 2);
		
		if($val > 0 || $val < 0) {
			$printVal = str_replace(".", ",", $val);
			if($ico)
				return "&euro;" . $printVal;
			else
				return $printVal;
		} else {
			if($zero)
				if($ico)
					return "&euro;0,00";
				else
					return "0,00";	 
			else 
				return "";
		}	
	}

}

if (!function_exists('stampaValuta')) {

    /* stampa valuta da duoble con . a stringa con , */
	function stampaValuta($val, $ico, $zero) {
		$val = number_format($val, 2);
		
		if($val > 0 || $val < 0) {
			$printVal = str_replace(".", ",", $val);
			if($ico)
				return "€" . $printVal;
			else
				return $printVal;
		} else {
			if($zero)
				if($ico)
					return "€0,00";
				else
					return "0,00";	 
			else 
				return "";
		}	
	}

}

if (!function_exists('cleanString')) {
	/* Elimina caratteri non ammessi nei permalink */
	function cleanString($string, $to_lower){
		$strResult = str_ireplace("à", "a", $string);
		$strResult  = str_ireplace("á", "a", $strResult);
		$strResult =  str_ireplace("è", "e", $strResult);
		$strResult =  str_ireplace("é", "e", $strResult);
		$strResult =  str_ireplace("ì", "i", $strResult);
		$strResult =  str_ireplace("í", "i", $strResult);
		$strResult =  str_ireplace("ò", "o", $strResult);
		$strResult =  str_ireplace("ó", "o", $strResult);
		$strResult =  str_ireplace("ù", "u", $strResult);
		$strResult =  str_ireplace("ú", "u", $strResult);
		$strResult =  str_ireplace("ç", "c", $strResult);
		$strResult =  str_ireplace("ö", "o", $strResult);
		$strResult =  str_ireplace("û", "u", $strResult);
		$strResult =  str_ireplace("ê", "e", $strResult);
		$strResult =  str_ireplace("ü", "u", $strResult);
		$strResult =  str_ireplace("ë", "e", $strResult);
		$strResult =  str_ireplace("ä", "a", $strResult);
		$strResult =  str_ireplace("'", " ", $strResult);
	 
		$strResult = preg_replace('/[^A-Za-z0-9 ]/', "", $strResult);
		$strResult = trim($strResult);
		$strResult =  preg_replace('/[ ]{2,}/', " ", $strResult);
	 
		$strResult = str_replace(" ", "-", $strResult);
	 
		return ($to_lower ? strtolower($strResult) : $strResult);
	}
}

// generate language file on the fly 
if (!function_exists('updatelangfile')) {
	function updatelangfile($pagine, $trad_lang){
		
$langstr = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Frontend language - ".$trad_lang->LANGUAGE_CI_CODE."
*/\n";

$langstr.= "/* LANG CONTENTS */\n";
foreach($trad_lang as $key=>$val)
{
	$langstr.= "\$lang['$key'] = \"".addslashes($val)."\";"."\n";
}

$langstr.= "/* PAGES CONTENTS */\n";
foreach($pagine as $pagina)
{
	foreach($pagina as $key=>$val)
	{
		//$string.="$key:$val,";
		$langstr.= "\$lang['".$pagina->pc_page_code."_".strtoupper($key)."'] = \"".addslashes($val)."\";"."\n";
	}
}
		write_file('./application/language/'.$trad_lang->LANGUAGE_CI_CODE.'/frontend_lang.php', $langstr);

    }
}

// generate config file on the fly
if (!function_exists('updateconfigfile')) {
	function updateconfigfile($result){

$confstr = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');		
/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
/* Coming soon OPTIONS */
define('COMNINGSOON_LOGO',		                'seibella.png');
define('COMNINGSOON_BACK_IMAGE',		        'yoga.jpg');
define('COMNINGSOON_BTN_COLOR',		            '23bdc3');\n\n";
	  
foreach ($result as $row){
	$confstr .= "define('".$row->cf_name."', '".$row->cf_value."');\n";
}

$confstr .= "\n/* Frontend assets global vars */
define('ASSETS_ROOT_FOLDER_FRONTEND',		    SITE_URL_PATH . '/assets/assets-frontend');
define('ASSETS_ROOT_FOLDER_FRONTEND_CSS',		SITE_URL_PATH . '/assets/assets-frontend/css');
define('ASSETS_ROOT_FOLDER_FRONTEND_JS',		SITE_URL_PATH . '/assets/assets-frontend/js');
define('ASSETS_ROOT_FOLDER_FRONTEND_IMG',		SITE_URL_PATH . '/assets/assets-frontend/img');
define('ASSETS_ROOT_FOLDER_FRONTEND_FONTS',		SITE_URL_PATH . '/assets/assets-frontend/fonts');
define('ASSETS_ROOT_FOLDER_FRONTEND_SASS',		SITE_URL_PATH . '/assets/assets-frontend/sass');
/* ADMIN */
define('ASSETS_ROOT_FOLDER_ADMIN',		        SITE_URL_PATH . '/assets/assets-admin');
define('ASSETS_ROOT_FOLDER_ADMIN_CSS',		    SITE_URL_PATH . '/assets/assets-admin/css');
define('ASSETS_ROOT_FOLDER_ADMIN_JS',		    SITE_URL_PATH . '/assets/assets-admin/js');
define('ASSETS_ROOT_FOLDER_ADMIN_IMG',		    SITE_URL_PATH . '/assets/assets-admin/img');
define('ASSETS_ROOT_FOLDER_ADMIN_FONTS',		SITE_URL_PATH . '/assets/assets-admin/fonts');
define('ASSETS_ROOT_FOLDER_ADMIN_SASS',		    SITE_URL_PATH . '/assets/assets-admin/sass');";

        write_file('./application/config/production/constants.php', $confstr);

    }
}

// crete url from Menu option name
if (!function_exists('createUrlMenu')) {
	function createUrlMenu($my_string){
        return base_url() . str_replace(' ', '', strtolower(transliterateString($my_string)));
    }
}

if (!function_exists('transliterateString')) {	
	function transliterateString($txt) {
		$transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE', 'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');
		return str_replace(array_keys($transliterationTable), array_values($transliterationTable), $txt);
	}
}