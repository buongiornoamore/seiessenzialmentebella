<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class defaultcontroller extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();   
	}
	/**
	 * Il nome del file deve essere in minuscolo comingsoon.php
	 */
	public function index()
	{
	    if($this->router->routes['default_page'] == 'Comingsoon') {
			redirect('comingsoon');
	    } else {
	        if((strpos(base_url(), 'eresesencialmentebella') !== false))
	            redirect('es/inicio');
	        else
	           redirect('it/home');		
		}
	}
	
	public function redirect_404() 
	{
		$this->load->view('404');
	}
	
}

/* End of file Comingsoon.php */
/* Location: ./application/controllers/Comingsoon.php */