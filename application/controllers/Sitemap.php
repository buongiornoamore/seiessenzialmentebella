<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('framework_helper');
	}
	
	public function index()
	{
	    $site_url = SITE_URL_CURRENT;
		$max_date = date("Y-m-d\TH:i:sP");
		$blank_date = "0000-00-00";
		$change_freq = 'daily';
		$priority = '1';
		$priority_child = '0.8';
		
		$urls = array();
		
		// load languages
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->order_by("id_lingue", "asc");
		$query_lingue = $this->db->get();
		
		foreach ($query_lingue->result() as $lingua)
		{
			array_push($urls, $site_url.strtolower($lingua->abbr_lingue).'/sitemap_pages.xml', $site_url.strtolower($lingua->abbr_lingue).'/sitemap_programmi.xml', $site_url.strtolower($lingua->abbr_lingue).'/sitemap_trattamenti.xml' , $site_url.strtolower($lingua->abbr_lingue).'/sitemap_images.xml');
		}
		
		$data = array(
					  'max_date' => $max_date, 
					  'blank_date' => $blank_date,
					  'change_freq' => $change_freq,
					  'priority' => $priority,
					  'priority_child' => $priority_child,
					  'sitemap_urls_type' => 'general',
					  'urls' => $urls
					  );		
					  
		// load general sitemap info			
		$this->load->view('sitemap', $data);
	}
	
	public function pages($lang_code)
	{
		// load lingua		
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('lingue.abbr_lingue', $lang_code);
		$query_lang = $this->db->get();
		$curr_lang = $query_lang->row();
		
		// load pages		
	//	$site_url = base_url();
		$site_url = SITE_URL_CURRENT;
		$max_date = date("Y-m-d\TH:i:sP");
		$blank_date = "0000-00-00";
		$change_freq = 'daily';
		$priority = '1';
		$priority_child = '0.8';
		
		$urls = array();
		
		// load pagine		
		$this->db->select('*');
		$this->db->from('pagine');
		$this->db->where('id_lingua', $curr_lang->id_lingue);
		$this->db->where('tipo_pagina', 'statica');
		
		$query_pages = $this->db->get();

		foreach ($query_pages->result() as $page)
		{
			$tmp_url = $site_url . $page->url_pagina;
			array_push($urls, $this->createUrl($tmp_url, $max_date, $change_freq, $priority));		
		}
		
		$data = array(
					  'max_date' => $max_date, 
					  'blank_date' => $blank_date,
					  'change_freq' => $change_freq,
					  'priority' => $priority,
					  'priority_child' => $priority_child,
					  'sitemap_urls_type' => 'detail',
					  'urls' => $urls
					  );		
					  
		$this->load->view('sitemap', $data);
	}
	
	public function programmi($lang_code)
	{	
	    $site_url = SITE_URL_CURRENT;
		$max_date = date("Y-m-d\TH:i:sP");
		$blank_date = "0000-00-00";
		$change_freq = 'daily';
		$priority = '1';
		$priority_child = '0.8';
		
		$urls = array();
		
		// load lingua		
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('lingue.abbr_lingue', $lang_code);
		$query_lang = $this->db->get();
		$curr_lang = $query_lang->row();
		
		$this->db->select('*');
		$this->db->from('programmi');
		$this->db->join('programmi_traduzioni', 'programmi.id_programma = programmi_traduzioni.id_programma', "LEFT");
		$this->db->where('programmi_traduzioni.lingua_traduzione_id', $curr_lang->id_lingue);
		$this->db->order_by("nome_programma", "asc");
		
		$query = $this->db->get();
		foreach ($query->result() as $prog)
		{
			// load lingua	 trad url	
			$this->db->select('url_pagina');
			$this->db->from('pagine');
			$this->db->where('id_lingua', $curr_lang->id_lingue);
			$this->db->where('trad_code', 'programmi');
			$query_path = $this->db->get();
			$curr_path = $query_path->row();
			
			$exploded = explode("/", $curr_path->url_pagina);
			
			$tmp_url = $site_url . $exploded[0] . '/' . $exploded[1] . '/' . $prog->id_programma . '/' . cleanString($prog->nome_programma, true);
			array_push($urls, $this->createUrl($tmp_url, $max_date, $change_freq, $priority));	
			
		}
		
		$data = array(
					  'max_date' => $max_date, 
					  'blank_date' => $blank_date,
					  'change_freq' => $change_freq,
					  'priority' => $priority,
					  'priority_child' => $priority_child,
					  'sitemap_urls_type' => 'detail',
					  'urls' => $urls
					  );		
					  
		$this->load->view('sitemap', $data);
	}
	
	public function trattamenti($lang_code)
	{	
	    $site_url = SITE_URL_CURRENT;
		$max_date = date("Y-m-d\TH:i:sP");
		$blank_date = "0000-00-00";
		$change_freq = 'daily';
		$priority = '1';
		$priority_child = '0.8';
		
		$urls = array();
		
		// load lingua		
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('lingue.abbr_lingue', $lang_code);
		$query_lang = $this->db->get();
		$curr_lang = $query_lang->row();
		
		$this->db->select('*');
		$this->db->from('trattamenti');
		$this->db->join('trattamenti_traduzioni', 'trattamenti.id_trattamento = trattamenti_traduzioni.id_trattamento', "LEFT");
		$this->db->where('trattamenti_traduzioni.lingua_traduzione_id', $curr_lang->id_lingue);
		$this->db->order_by("nome_trattamento", "asc");
		
		$query = $this->db->get();
		foreach ($query->result() as $prog)
		{
			// load lingua	 trad url	
			$this->db->select('url_pagina');
			$this->db->from('pagine');
			$this->db->where('id_lingua', $curr_lang->id_lingue);
			$this->db->where('trad_code', 'trattamenti');
			$query_path = $this->db->get();
			$curr_path = $query_path->row();
			
			$exploded = explode("/", $curr_path->url_pagina);
			
			$tmp_url = $site_url . $exploded[0] . '/' . $exploded[1] . '/' . $prog->id_trattamento . '/' . cleanString($prog->nome_trattamento, true);
			array_push($urls, $this->createUrl($tmp_url, $max_date, $change_freq, $priority));	
			
		}
		
		$data = array(
					  'max_date' => $max_date, 
					  'blank_date' => $blank_date,
					  'change_freq' => $change_freq,
					  'priority' => $priority,
					  'priority_child' => $priority_child,
					  'sitemap_urls_type' => 'detail',
					  'urls' => $urls
					  );		
					  
		$this->load->view('sitemap', $data);
	}
	
	public function images($lang_code)
	{	
	    $site_url = SITE_URL_CURRENT;
		$urls = array();
		// load lingua		
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('lingue.abbr_lingue', $lang_code);
		$query_lang = $this->db->get();
		$curr_lang = $query_lang->row();
		
		// load programmi			
		$this->db->select('*');
		$this->db->from('programmi');
		$this->db->join('programmi_traduzioni', 'programmi.id_programma = programmi_traduzioni.id_programma', "LEFT");
		$this->db->where('programmi_traduzioni.lingua_traduzione_id', $curr_lang->id_lingue);
		$this->db->order_by("nome_programma", "asc");		
		$query = $this->db->get();
		
		foreach ($query->result() as $prog)
		{
			// load lingua	 trad url	
			$this->db->select('url_pagina');
			$this->db->from('pagine');
			$this->db->where('id_lingua', $curr_lang->id_lingue);
			$this->db->where('trad_code', 'programmi');
			$query_path = $this->db->get();
			$curr_path = $query_path->row();
			
			$exploded = explode("/", $curr_path->url_pagina);
			
			$image_list = array();
			
			$tmp_url = $site_url . $exploded[0] . '/' . $exploded[1] . '/' . $prog->id_programma . '/' . cleanString($prog->nome_programma, true);
			
			if($prog->immagine_programma != '') {
				$url_immagine =  ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/programmi/'.$prog->immagine_programma;
				array_push($image_list, $this->createImageUrl($url_immagine, $prog->frase_programma, SITE_TITLE_CURRENT.' | '.$prog->nome_programma));
			}
			
			if($prog->immagine_programma_mobile != '') {
				$url_immagine_mobile = ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/programmi/'.$prog->immagine_programma_mobile;
				array_push($image_list, $this->createImageUrl($url_immagine_mobile, $prog->frase_programma, SITE_TITLE_CURRENT.' | '.$prog->nome_programma . ' - Mobile'));
			}
			
			// aggiungi immagine della gallery se presenti
			$this->db->select('img_gallery');
			$this->db->from('programmi_gallery');
			$this->db->where('id_programma', $prog->id_programma);
			$query_img = $this->db->get();
			foreach ($query_img->result() as $gall_img)
			{
				$url_gallery = ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/gallery/'.$gall_img->img_gallery;
				array_push($image_list, $this->createImageUrl($url_gallery, $prog->nome_programma . ' - ' . $gall_img->img_gallery, SITE_TITLE_CURRENT.' | '.$gall_img->img_gallery . ' - GALLERY ' . $curr_lang->abbr_lingue));
			}
			
			// add all
			array_push($urls, $this->createImageUrlLoc($tmp_url, $image_list));
			
		}
		
		$data = array(
					  'sitemap_urls_type' => 'images',
					  'urls' => $urls
					  );		
					  
		$this->load->view('sitemap', $data);
	}
	
	/* stampa url per sitemap */
	public function createUrl($url, $lastmod, $change_freq, $priority) {
	
		return '<url>
				<loc>' . $url . '</loc>
				' . ($lastmod != "0000-00-00" ? '<lastmod>' . date_format(date_create($lastmod), "Y-m-d\TH:i:sP") . '</lastmod>' : '') .  ($change_freq != '' ? '<changefreq>' . $change_freq . '</changefreq>' : '') . ($priority != '' ? '<priority>' . $priority . '</priority>' : '') . '
			  </url>';
		  
	}
	
	public function createImageUrlLoc($loc, $image_list) {
	
		$return_str = '<url>
							<loc>' . $loc . '</loc>';
		
		foreach ($image_list as $image_url)
		{
			$return_str .= $image_url;
		}
					
		$return_str .= '</url>';
	
		return $return_str;
		  
	}
	
	public function createImageUrl($loc_image, $caption, $title) {
	
		return '<image:image>
					<image:loc>' . $loc_image . '</image:loc>
					<image:title>' . $this->string_sanitize($title) . '</image:title>
					<image:caption>' . $this->string_sanitize($caption) . '</image:caption>
				</image:image>';
		  
	}
	
	public function string_sanitize($s) {
		$result = preg_replace("/[^\s\p{L} 0-9|-]/u", "", html_entity_decode($s, ENT_QUOTES));
		return $result;
	}
}