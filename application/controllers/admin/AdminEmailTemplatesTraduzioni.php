<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminEmailTemplatesTraduzioni extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('email_templates_traduzioni');
			// nome in tabella
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			// colonne da mostrare
			$crud->columns('testo_preheader', 'testo_footer_copyright', 'testo_footer_unsubscribe', 'lingua_traduzione_id');
			$crud->required_fields('testo_preheader', 'testo_footer_copyright', 'testo_footer_unsubscribe');
			//$crud->unset_texteditor('testo_preheader');
			//$crud->unset_texteditor('testo_footer_copyright');
			//$crud->unset_texteditor('testo_footer_unsubscribe');
			$crud->unset_add();
			$crud->unset_delete();
			// set update only after update 
			if ($crud->getState() == 'edit') {
				$crud->change_field_type('lingua_traduzione_id', 'readonly');
			} 
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-EMAILTEMPLATESTRADUZIONI';
			$data['curr_page_title'] = 'Email';
			$data['collapseParentMenu'] = 'email';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/emailtemplatestraduzioni',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
		
}
