<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminStudimedici extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('studi_medici');
			// nome in tabella
			//$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
		//	$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			$crud->set_relation('stato', 'stato_descrizione', 'stato_descrizione_text');
			// colonne da mostrare
		//	$crud->columns('nome_commento', 'testo_commento', 'lingua_traduzione_id');
		//	$crud->unset_texteditor('testo_commento');
			$crud->required_fields('citta', 'indirizzo', 'lat', 'long');
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-STUDIMEDICI';
			$data['curr_page_title'] = 'Studi medici';
			$data['collapseParentMenu'] = 'studimedici';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/studimedici',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
