<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminTrattamenti extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('trattamenti');
			// nome in tabella
			$crud->display_as('stato_trattamento', 'Stato');
			// realazioni join
			$crud->set_relation('stato_trattamento', 'stato_descrizione', 'stato_descrizione_text');
			$crud->set_relation_n_n('benefici', 'trattamenti_benefici', 'benefici', 'id_trattamento', 'id_benefici', 'titolo_benefici');
			$crud->set_relation_n_n('faq', 'trattamenti_faq', 'faq', 'id_trattamento', 'id_faq', 'titolo_faq');
			// colonne da mostrare
			$crud->required_fields('titolo_trattamento', 'stato_trattamento');
			$crud->columns('titolo_trattamento', 'stato_trattamento');
			// delete action
			$crud->callback_before_delete(array($this, 'delete_relation_trattamenti'));
			
			// custom action
			$crud->add_action('Traduzioni trattamento', '', '', 'fa-flag', array($this, 'load_traduzioni'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-TRATTAMENTI';
			$data['curr_page_title'] = 'Trattamenti';
			$data['collapseParentMenu'] = 'trattamenti';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/trattamenti',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function delete_relation_trattamenti($primary_key)
	{
		$this->db->delete('trattamenti_benefici', array('id_trattamento' => $primary_key)); 	
		$this->db->delete('programmi_trattamenti', array('id_trattamento' => $primary_key)); 
		$this->db->delete('trattamenti_traduzioni', array('id_trattamento' => $primary_key)); 
		$this->db->delete('trattamenti_faq', array('id_trattamento' => $primary_key)); 
		return true;
	}
	
	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/trattamenti/trad/'.$row->id_trattamento);
	}
	
	public function traductions($tratt_id) {
		$this->checkUserPermissions();
		//CRUD traduzioni
		try {
			// load trattamento
			$this->db->select('titolo_trattamento');
			$this->db->from('trattamenti');
			$this->db->where('id_trattamento', $tratt_id);
			$query = $this->db->get();
			$prog = $query->row();
			
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('trattamenti_traduzioni');
			$crud->where('trattamenti_traduzioni.id_trattamento', $tratt_id);
			// nome in tabella
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			//$crud->set_relation('id_trattamento', 'trattamenti', 'titolo_trattamento');
			// campi obbligatori
			$crud->required_fields('nome_trattamento', 'descrizione_trattamento', 'lingua_traduzione_id');
			$crud->edit_fields('nome_trattamento', 'descrizione_trattamento', 'lingua_traduzione_id');
			$crud->add_fields('id_trattamento', 'nome_trattamento', 'descrizione_trattamento', 'lingua_traduzione_id');
			// colonne da mostrare
			$crud->columns('nome_trattamento', 'descrizione_trattamento', 'lingua_traduzione_id');
			// unset delete action
			if ($crud->getState() == 'add') {
				$crud->field_type('id_trattamento', 'hidden', $tratt_id);
			}
		//	$crud->unset_texteditor('descrizione_trattamento');

			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-TRATTAMENTI';
			$data['curr_page_title'] = 'Trattamenti';
			$data['collapseParentMenu'] = 'trattamenti';
			$data['curr_function_title'] = 'Traduzioni per <b>' . $prog->titolo_trattamento . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/trattamenti_trad',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}
