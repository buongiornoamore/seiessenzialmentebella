<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminEmailTemplates extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('email_templates');
			// nome in tabella
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			$crud->display_as('id_tipo_template_default', 'Default');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			$crud->set_relation('id_tipo_template_default', 'tipo_template_default', 'nome_tipo_template_default');
			// colonne da mostrare
			$crud->columns('nome_template', 'testo_template', 'lingua_traduzione_id', 'id_tipo_template_default');
			$crud->required_fields('nome_template', 'testo_template', 'lingua_traduzione_id');
		//	$crud->unset_texteditor('testo_template');
			$crud->add_action('Html preview', '', '', 'fa-html5', array($this, 'load_preview'));
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-EMAILTEMPLATES';
			$data['curr_page_title'] = 'Email';
			$data['collapseParentMenu'] = 'email';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/emailtemplates',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function load_preview($primary_key , $row)
	{
		return site_url('admin/html_preview_email_template/'.$row->id_template);
	}
		
}
