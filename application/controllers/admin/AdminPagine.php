<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminPagine extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('pagine_contenuti');
			$crud->order_by('id_pc', 'asc');
			// file upload
			$crud->set_field_upload('pc_immagine_1', 'assets/assets-frontend/img');
			$crud->set_field_upload('pc_immagine_2', 'assets/assets-frontend/img');
			$crud->set_field_upload('pc_top_image', 'assets/assets-frontend/img');
			$crud->set_field_upload('pc_top_image_mobile', 'assets/assets-frontend/img');
			// nome in tabella
			$crud->display_as('pc_immagine_1', 'Immagine');
			$crud->display_as('pc_page_code', 'Codice pagina');
			$crud->display_as('pc_titolo_1', 'Titolo');
			$crud->display_as('id_lingua', 'Lingua');
			$crud->display_as('pc_testo_body_1', 'Testo body');
			$crud->display_as('pc_testo_body_2', 'Testo body 2');
			$crud->display_as('pc_titolo_2', 'Titolo 2');
			$crud->display_as('pc_immagine_2', 'Immagine 2');
			$crud->display_as('pc_testo_bold_1', 'Testo evidenziato 1');
			$crud->display_as('pc_testo_bold_2', 'Testo evidenziato 2');
			$crud->display_as('pc_testo_bold_3', 'Testo evidenziato 3');
			$crud->display_as('pc_top_image', 'Immagine TOP');
			$crud->display_as('pc_top_image_mobile', 'Immagine TOP mobile');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			// colonne da mostrare
			$crud->columns('pc_immagine_1', 'pc_titolo_1', 'id_lingua');
			// unset delete action
			$crud->unset_delete();
			$crud->unset_add();
		/*	$crud->unset_texteditor('pc_testo_body_1');
			$crud->unset_texteditor('pc_testo_body_2');
			$crud->unset_texteditor('pc_testo_bold_1');
			$crud->unset_texteditor('pc_testo_bold_2');
			$crud->unset_texteditor('pc_testo_bold_3');*/
		//	$crud->required_fields('cf_value');
			// custom action
			//callbacks
			$crud->callback_after_update(array($this, 'update_lang_callback'));
			
			// set update only after update 
			if ($crud->getState() == 'edit') {
				$crud->change_field_type('pc_titolo_1', 'readonly');
				$crud->change_field_type('pc_page_code', 'readonly');
			} 
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PAGINE';
			$data['curr_page_title'] = 'Pagine';
			$data['collapseParentMenu'] = 'Pagine';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/pagine',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function update_lang_callback($post_array, $primary_key)
	{
		// @TODO caricare tutte le pagine di quella lingua
		// pagine_contenuti
		$this->db->where('id_lingua', $post_array['id_lingua']);
		//$this->db->join('lingue', 'lingue.id_lingue = pagine_contenuti.id_lingua');
		$this->db->from('pagine_contenuti');
		$query = $this->db->get();
		$pagine = $query->result();
		//print_r($this->db->last_query());	
		// traduzioni_file_lang
		$this->db->where('id_lingua', $post_array['id_lingua']);
		$this->db->from('traduzioni_file_lang');
		$query_lang = $this->db->get();
		$trad_lang = $query_lang->row();
		
		updatelangfile($pagine, $trad_lang);
		return true;
	}
	
}
