<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminCommenti extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('commenti');
			// nome in tabella
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			// colonne da mostrare
			$crud->columns('nome_commento', 'testo_commento', 'lingua_traduzione_id');
			$crud->unset_texteditor('testo_commento');
			$crud->required_fields('nome_commento', 'testo_commento', 'lingua_traduzione_id');
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-COMMENTI';
			$data['curr_page_title'] = 'Commenti';
			$data['collapseParentMenu'] = 'commenti';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/commenti',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
