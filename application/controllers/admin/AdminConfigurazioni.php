<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminConfigurazioni extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('constants_framework');
			// nome in tabella
			$crud->display_as('cf_name', 'Proprietà');
			$crud->display_as('cf_value', 'Valore');
			$crud->display_as('cf_description', 'Descrizione');
			// realazioni join
			// colonne da mostrare
			$crud->columns('cf_name', 'cf_value', 'cf_description');
			// unset delete action
			$crud->unset_delete();
			$crud->unset_add();
			$crud->unset_texteditor('cf_description');
		//	$crud->required_fields('cf_value');
			// custom action
			//callbacks
			$crud->callback_after_update(array($this, 'update_config_callback'));
			
			// set update only after update 
			if ($crud->getState() == 'edit') {
				$crud->change_field_type('cf_name', 'readonly');
				$crud->change_field_type('cf_description', 'readonly');
			} 
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CONFIGURAZIONI';
			$data['curr_page_title'] = 'Configurazioni';
			$data['collapseParentMenu'] = 'configurazioni';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/configurazioni',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function update_config_callback($primary_key)
	{
		// solo se update
		$query = $this->db->get('constants_framework');
		updateconfigfile($query->result());
		return true;
	}
	
	public function updateSiteStatus()
	{
		$value = $this->input->post('value');
		$data = array(
		   'controller' => $value
		);	
		$this->db->where('url_pagina', 'default_page');
		$this->db->update('pagine', $data); 
	}
}
