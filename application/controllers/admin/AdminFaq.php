<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminFaq extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('faq');
			// nome in tabella
			// realazioni join
			// colonne da mostrare
			$crud->columns('titolo_faq', 'allineamento_faq');
			$crud->field_type('allineamento_faq','dropdown', array('center' => 'center', 'justify' => 'justify', 'list' => 'list'));
			// unset delete action
			//$crud->unset_delete();
			$crud->unset_texteditor('titolo_faq');
			$crud->required_fields('titolo_faq', 'allineamento_faq');
			// custom action
			$crud->add_action('Traduzioni faq', '', '', 'fa-flag', array($this, 'load_traduzioni'));
			$crud->callback_before_delete(array($this, 'delete_relation_faq'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-FAQ';
			$data['curr_page_title'] = 'Faq';
			$data['collapseParentMenu'] = 'faq';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/faq',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function delete_relation_faq($primary_key)
	{
		$this->db->delete('trattamenti_faq', array('id_faq' => $primary_key)); 
		$this->db->delete('faq_traduzioni', array('id_faq' => $primary_key)); 
		return true;
	}

	function load_traduzioni($primary_key, $row)
	{
		return site_url('admin/faq/trad/'.$row->id_faq);
	}
	
	public function traductions($faq_id) {
		$this->checkUserPermissions();
		//CRUD traduzioni
		try {
			// load trattamento
			$this->db->select('titolo_faq');
			$this->db->from('faq');
			$this->db->where('id_faq', $faq_id);
			$query = $this->db->get();
			$prog = $query->row();
			
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('faq_traduzioni');
			$crud->where('id_faq', $faq_id);
			// nome in tabella
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			// campi obbligatori
			$crud->required_fields('testo_faq', 'risposta_faq', 'lingua_traduzione_id');
			$crud->edit_fields('testo_faq', 'risposta_faq', 'lingua_traduzione_id');
			$crud->add_fields('testo_faq', 'risposta_faq', 'lingua_traduzione_id', 'id_faq');
			if ($crud->getState() == 'add') {
				$crud->field_type('id_faq', 'hidden', $faq_id);
			}
			
			// colonne da mostrare
			$crud->columns('testo_faq', 'risposta_faq', 'lingua_traduzione_id');
			//$crud->unset_texteditor('testo_faq', 'risposta_faq');
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-FAQ';
			$data['curr_page_title'] = 'Faq';
			$data['collapseParentMenu'] = 'faq';
			$data['curr_function_title'] = 'Traduzioni per <b>' . substr($prog->titolo_faq, 0, 30) . ' ...</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/faq_trad',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}
