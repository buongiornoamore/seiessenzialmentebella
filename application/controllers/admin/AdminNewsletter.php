<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminNewsletter extends Admin_Controller {

	private $curr_emails = array();
	private $curr_names = array();
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('contatti_newsletter');
			// nome in tabella
			$crud->display_as('stato_contatto', 'Stato');
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			$crud->set_relation('stato_contatto', 'stato_descrizione', 'stato_descrizione_text');
			// colonne da mostrare
			$crud->columns('nome_contatto', 'email_contatto', 'data_contatto', 'lingua_traduzione_id', 'stato_contatto');
			$crud->required_fields('nome_contatto', 'email_contatto', 'data_contatto', 'lingua_traduzione_id', 'stato_contatto');
			$crud->change_field_type('data_unsubscribe', 'date');
			$crud->change_field_type('data_contatto', 'date');
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-NEWSLETTER';
			$data['curr_page_title'] = 'Email';
			$data['collapseParentMenu'] = 'email';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/newsletter',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function email() {

		//CRUD contatti email
		try {
			$this->checkUserPermissions();
			
			// TODO METTERE QUESTE INFO IN UNA VARIABILE GENERALE DI CLASSE E VERIFICARE SE ESISTE GIA'
			if(!empty($this->input->post('newsletter_list', true))) {
				$newsletter_list = json_decode($this->input->post('newsletter_list', true));	
				$newsletter_list_int = [];
				for( $i =0; $i < count( $newsletter_list ); $i++ ){    
					$newsletter_list_int[$i] = (int) $newsletter_list[$i];
				}
				// load coontatti
				$this->db->from('contatti_newsletter');
				$this->db->where_in('id_contatto_newsletter', $newsletter_list_int);
				
				$query_address = $this->db->get();
				$this->curr_emails = array();
				$this->curr_names = array();
				foreach ($query_address->result() as $address)
				{
					// metti nella lista di invio solo i contatti non cancellati dalla newsletter
					if($address->stato_contatto == 1) {
						array_push($this->curr_emails, $address->email_contatto);
						array_push($this->curr_names, $address->nome_contatto);
					}
				}
			}
				
			//	print_r($this->db->last_query());			
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('email_templates');
			//$crud->where('lingua_traduzione_id', $cont->id_lingua);
			// nome in tabella
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			//$crud->set_relation('id_benefici', 'benefici', 'titolo_benefici');
			// campi obbligatori
			//$crud->required_fields('testo_benefici');
			//$crud->edit_fields('testo_benefici');
			//$crud->add_fields('testo_benefici');
			// colonne da mostrare
			$crud->columns('nome_template', 'lingua_traduzione_id');
			// unset delete action
			$crud->unset_delete();
			$crud->unset_edit();
			//$crud->unset_add();
			$crud->unset_read();
			$crud->add_action('Invia email', '', '', 'fa-envelope', array($this, 'send_email_templates'));
			$crud->add_action('Preview email', '', '', 'fa-html5', array($this, 'preview_email_templates'));
			//$crud->unset_texteditor('testo_benefici');
			//$crud->field_type('id_contatto', 'hidden', $cont_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-NEWSLETTER';
			$data['curr_page_title'] = 'Email';
			$data['collapseParentMenu'] = 'email';
			$data['curr_function_title'] = 'Invia newsletter';
			$data['curr_emails'] = $this->curr_emails;
			$data['curr_names'] = $this->curr_names;
	
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/newsletter_email',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function send_email_templates($primary_key, $row)
	{
		return site_url('admin/send_email_template/'.$row->id_template);
	}
	
	function preview_email_templates($primary_key, $row)
	{
		return site_url('admin/html_preview_email_template/'.$row->id_template);
	}
	
}
