<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminBenefici extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		// CRUD 
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('benefici');
			// nome in tabella
			// realazioni join
			// colonne da mostrare
			$crud->columns('titolo_benefici');
			// unset delete action
			//$crud->unset_delete();
			$crud->unset_texteditor('titolo_benefici');
			$crud->required_fields('titolo_benefici');
			// custom action
			$crud->add_action('Traduzioni benefici', '', '', 'fa-flag', array($this, 'load_traduzioni'));
			$crud->callback_before_delete(array($this, 'delete_relation_benefici'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-BENEFICI';
			$data['curr_page_title'] = 'Benefici';
			$data['collapseParentMenu'] = 'benefici';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/benefici',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function delete_relation_benefici($primary_key)
	{
		$this->db->delete('trattamenti_benefici', array('id_benefici' => $primary_key)); 
		$this->db->delete('programmi_benefici', array('id_benefici' => $primary_key)); 
		$this->db->delete('benefici_traduzioni', array('id_benefici' => $primary_key)); 
		return true;
	}

	function load_traduzioni($primary_key, $row)
	{
		return site_url('admin/benefici/trad/'.$row->id_benefici);
	}
	
	public function traductions($ben_id) {
		$this->checkUserPermissions();
		//CRUD traduzioni
		try {
			// load trattamento
			$this->db->select('titolo_benefici');
			$this->db->from('benefici');
			$this->db->where('id_benefici', $ben_id);
			$query = $this->db->get();
			$prog = $query->row();
			
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('benefici_traduzioni');
			$crud->where('id_benefici', $ben_id);
			// nome in tabella
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			//$crud->set_relation('id_benefici', 'benefici', 'titolo_benefici');
			// campi obbligatori
			$crud->required_fields('testo_benefici', 'lingua_traduzione_id');
			$crud->edit_fields('testo_benefici', 'lingua_traduzione_id');
			$crud->add_fields('testo_benefici', 'lingua_traduzione_id', 'id_benefici');
			if ($crud->getState() == 'add') {
				$crud->field_type('id_benefici', 'hidden', $ben_id);
			}
			
			// colonne da mostrare
			$crud->columns('testo_benefici', 'lingua_traduzione_id');
			// unset delete action
			//$crud->callback_before_delete(array($this, 'delete_relation_traduzioni'));
			//$crud->unset_delete();
			$crud->unset_texteditor('testo_benefici');
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-BENEFICI';
			$data['curr_page_title'] = 'Benefici';
			$data['collapseParentMenu'] = 'benefici';
			$data['curr_function_title'] = 'Traduzioni per <b>' . substr($prog->titolo_benefici, 0, 30) . ' ...</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/benefici_trad',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}
