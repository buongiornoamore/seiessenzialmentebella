<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminRetreats extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('retreats');
			// nome in tabella
			$crud->display_as('retreats_name', 'Nome');
			$crud->display_as('retreats_lang_id', 'Lingua');
			// realazioni join
			$crud->set_relation('retreats_lang_id', 'lingue', 'nome_lingue');
			// colonne da mostrare
			$crud->columns('retreats_name', 'retreats_lang_id');
			$crud->required_fields('retreats_name', 'retreats_text', 'retreats_lang_id');
			// delete action
			//$crud->callback_before_delete(array($this, 'delete_relation_programmi'));
			// custom action
		//	$crud->add_action('Traduzioni programma', '', '', 'fa-flag', array($this, 'load_traduzioni'));
		//	$crud->add_action('Gallery', '', '', 'fa-picture-o', array($this, 'load_gallery'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-RETREATS';
			$data['curr_page_title'] = 'Retreats';
			$data['collapseParentMenu'] = 'retreats';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/retreats',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
