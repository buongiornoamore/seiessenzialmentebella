<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminProgrammi extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('programmi');
			// nome in tabella
			$crud->display_as('stato_programma', 'Stato');
			// realazioni join
			$crud->set_relation('stato_programma', 'stato_descrizione', 'stato_descrizione_text');
			$crud->set_relation_n_n('benefici', 'programmi_benefici', 'benefici', 'id_programma', 'id_benefici', 'titolo_benefici');
			$crud->set_relation_n_n('trattamenti', 'programmi_trattamenti', 'trattamenti', 'id_programma', 'id_trattamento', 'titolo_trattamento');
			// colonne da mostrare
			$crud->columns('titolo_programma', 'stato_programma');
			$crud->required_fields('titolo_programma', 'stato_programma');
			// delete action
			$crud->callback_before_delete(array($this, 'delete_relation_programmi'));
			// custom action
			$crud->add_action('Traduzioni programma', '', '', 'fa-flag', array($this, 'load_traduzioni'));
			$crud->add_action('Gallery', '', '', 'fa-picture-o', array($this, 'load_gallery'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PROGRAMMI';
			$data['curr_page_title'] = 'Programmi';
			$data['collapseParentMenu'] = 'programmi';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/programmi',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function delete_relation_programmi($primary_key)
	{
		// remove also gallery files from filesystem
		$query_gallery = $this->db->where('id_programma', $primary_key)->get('programmi_gallery');
		foreach ($query_gallery->result() as $row)
		{
			$url = 'assets/assets-frontend/img/gallery/'.$row->img_gallery;
			unlink($url);
		}		
		$this->db->delete('programmi_gallery', array('id_programma' => $primary_key)); 	
		
		// remove also programmi files from filesystem
		$query_trad = $this->db->where('id_programma', $primary_key)->get('programmi_traduzioni');
		foreach ($query_trad->result() as $traduzione)
		{
			$urlImgProg = 'assets/assets-frontend/img/programmi/'.$traduzione->immagine_programma;
			unlink($urlImgProg);
			$urlImgProgMobile = 'assets/assets-frontend/img/programmi/'.$traduzione->immagine_programma_mobile;
			unlink($urlImgProgMobile);
		}
		$this->db->delete('programmi_traduzioni', array('id_programma' => $primary_key)); 
		
		$this->db->delete('programmi_trattamenti', array('id_programma' => $primary_key)); 
		$this->db->delete('programmi_benefici', array('id_programma' => $primary_key)); 
		return true;
	}
	
	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/programmi/trad/'.$row->id_programma);
	}
	
	function load_gallery($primary_key , $row)
	{
		return site_url('admin/programmi/gallery/'.$row->id_programma);
	}
	
	public function gallery($prog_id)
	{
		$this->checkUserPermissions();
		//CRUD gallery
		try {
			$this->prog_id_gallery = $prog_id;
			// load programma
			$this->db->select('titolo_programma');
			$this->db->from('programmi');
			$this->db->where('id_programma', $prog_id);
			$query = $this->db->get();
			$prog = $query->row();
			
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('programmi_gallery');
			$crud->where('programmi_gallery.id_programma', $prog_id);
			// nome in tabella
			$crud->display_as('img_gallery', 'Immagine');
			// file upload
			$crud->set_field_upload('img_gallery', 'assets/assets-frontend/img/gallery');
			// campi obbligatori
			$crud->required_fields('img_gallery');
			$crud->add_fields('id_programma','img_gallery');
			// colonne da mostrare
			$crud->columns('img_gallery');
			// unset actions
			$crud->unset_edit();
			// delete action
			$crud->callback_before_delete(array($this, 'delete_from_gallery'));
			if ($crud->getState() == 'add') {
				$crud->field_type('id_programma', 'hidden', $prog_id);
			}
			// upload action 
			//$crud->callback_after_upload(array($this,'save_file_callback_after_upload'));
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PROGRAMMI';
			$data['curr_page_title'] = 'Programmi';
			$data['collapseParentMenu'] = 'programmi';
			$data['curr_function_title'] = 'Gallery per <b>' . $prog->titolo_programma . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/programmi_gallery',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function save_file_callback_after_upload($uploader_response, $field_info, $files_to_upload)
	{
	   $data = array(
		   'id_programmi_gallery' => NULL,
		   'id_programma' => $this->prog_id_gallery,
		   'img_gallery' => $uploader_response[0]->name
		);

	   $this->db->insert('programmi_gallery', $data);
	  // header('Location: '.base_url().'controller');
	   //redirect('admin/programmi/gallery/'.$this->prog_id_gallery, 'refresh');
	  // redirect('auth/login', 'refresh');
	  // return true;
	}

	public function delete_from_gallery($primary_key)
	{
		$row = $this->db->where('id_programmi_gallery', $primary_key)->get('programmi_gallery')->row();
		$url = 'assets/assets-frontend/img/gallery/'.$row->img_gallery;
		unlink($url);
		return true;
	}
	
	public function traductions($prog_id) {
		$this->checkUserPermissions();
		//CRUD traduzioni
		try {
			// load programma
			$this->db->select('titolo_programma');
			$this->db->from('programmi');
			$this->db->where('id_programma', $prog_id);
			$query = $this->db->get();
			$prog = $query->row();
			
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('programmi_traduzioni');
			$crud->where('id_programma', $prog_id);
			// nome in tabella
			$crud->display_as('immagine_programma_mobile', 'Immagine');
			$crud->display_as('immagine_programma', 'Immagine grande');
			//$crud->display_as('descrizione_breve', 'Descrizione breve');
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// file upload
			$crud->set_field_upload('immagine_programma', 'assets/assets-frontend/img/programmi');
			$crud->set_field_upload('immagine_programma_mobile', 'assets/assets-frontend/img/programmi');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			//$crud->set_relation('id_programma', 'programmi', 'titolo_programma');
			// campi obbligatori
			$crud->required_fields('frase_programma', 'nome_programma', 'descrizione_programma', 'immagine_programma', 'immagine_programma_mobile', 'lingua_traduzione_id');
			$crud->edit_fields('frase_programma', 'nome_programma', 'descrizione_programma', 'immagine_programma', 'immagine_programma_mobile', 'lingua_traduzione_id');
			$crud->add_fields('id_programma', 'frase_programma', 'nome_programma', 'descrizione_programma', 'lingua_traduzione_id');
			// delete action
			$crud->callback_before_delete(array($this, 'delete_from_trad'));
			
			// colonne da mostrare
			$crud->columns('immagine_programma_mobile', 'nome_programma', 'frase_programma', 'lingua_traduzione_id');
			// unset delete action
			if ($crud->getState() == 'add') {
				$crud->field_type('id_programma', 'hidden', $prog_id);
			}
		//	$crud->unset_texteditor('frase_programma');
		//	$crud->unset_texteditor('descrizione_programma');
		
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PROGRAMMI';
			$data['curr_page_title'] = 'Programmi';
			$data['collapseParentMenu'] = 'programmi';
			$data['curr_function_title'] = 'Traduzioni per <b>' . $prog->titolo_programma . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/programmi_trad',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function delete_from_trad($primary_key)
	{
		// remove also programmi files from filesystem
		$traduzione = $this->db->where('id_programmi_traduzioni', $primary_key)->get('programmi_traduzioni')->row();
		$urlImgProg = 'assets/assets-frontend/img/programmi/'.$traduzione->immagine_programma;
		unlink($urlImgProg);
		$urlImgProgMobile = 'assets/assets-frontend/img/programmi/'.$traduzione->immagine_programma_mobile;
		unlink($urlImgProgMobile);
		return true;
	}
}
