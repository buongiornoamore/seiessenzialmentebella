<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trattamenti extends Frontend_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
	}
	
	public function detail($trattId = null, $seoText = null)
	{
		$this->db->where('trattamenti.id_trattamento', $trattId);
		$this->db->join('trattamenti_traduzioni', 'trattamenti_traduzioni.id_trattamento = trattamenti.id_trattamento');
		$this->db->where('trattamenti.stato_trattamento >', 0);
		$this->db->where('trattamenti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));	
		$this->db->from('trattamenti');
		$query = $this->db->get();
		
		$tratt = $query->row();
		
		if (isset($tratt) && count($tratt) > 0)
		{
			// carica benefici
			$this->db->select('*');
			$this->db->from('trattamenti_benefici');
			//$this->db->join('benefici', 'benefici.id_benefici = trattamenti_benefici.id_benefici');
			$this->db->join('benefici_traduzioni', 'benefici_traduzioni.id_benefici = trattamenti_benefici.id_benefici');
			$this->db->where('benefici_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
			$this->db->where('trattamenti_benefici.id_trattamento', $trattId);
			$query_ben = $this->db->get();
			$data['benefici'] = $query_ben->result();
		//	print_r($this->db->last_query());
			// carica domande
			$this->db->select('*');
			$this->db->from('trattamenti_faq');
			$this->db->join('faq', 'faq.id_faq = trattamenti_faq.id_faq');
			$this->db->join('faq_traduzioni', 'faq_traduzioni.id_faq = faq.id_faq');
			$this->db->where('trattamenti_faq.id_trattamento', $trattId);
			$this->db->where('faq_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
			$query_faq = $this->db->get();
			$data['faq'] = $query_faq->result();
			
			$data['tratt'] = $tratt;
			$this->show_view_with_menu('frontend/trattamenti', $data);
		} else {
			redirect('home');
		}	
		
	}
	
}