<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Retreats extends Frontend_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
	}
	
	public function detail($retreatId = null)
	{

	    $this->db->where('retreats_id', $retreatId);
	//	$this->db->where('programmi_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->from('retreats');
		$query = $this->db->get();
		
		$retreat = $query->row();
		
		if (isset($retreat))
		{	
		    $data['retreat'] = $retreat;			
			$this->show_view_with_menu('frontend/retreats', $data);
			//$this->load->view('frontend/programmi', $data);
		} else {
			redirect('home');
		}	
		
	}
	
}