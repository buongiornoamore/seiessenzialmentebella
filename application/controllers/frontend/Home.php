<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Frontend_Controller {
	
	public function __construct() 
	{
        parent::__construct();   
	}
	
	public function index()
	{
		$data = array();	
		// carica commenti
		$this->db->select('*');
		$this->db->from('commenti');
		$this->db->where('lingua_traduzione_id', lang('LANGUAGE_ID'));
		$query_com = $this->db->get();
		$data['commenti'] = $query_com->result();
		$this->show_view_with_menu('frontend/index', $data);
	}
	
	public function sudime()
	{
		$data = array('ancorid' => '');	
		$this->show_view_with_menu('frontend/sudime', $data);
	}
	
	public function minibiografia()
	{
		$data = array('ancorid' => 'minibiografia');	
		$this->show_view_with_menu('frontend/sudime', $data);
	}
	
	public function ilmiolavoro()
	{
		$data = array();
		$this->show_view_with_menu('frontend/ilmiolavoro', $data);
	}
	
	public function studimedici()
	{
		$data = array();
		$this->show_view_with_menu('frontend/studimedici', $data);
	}
	
	public function contattami()
	{	
		$data = array();	
		$this->show_view_with_menu('frontend/contattami', $data);
	}
	
	public function privacy()
	{
		$data = array();		
		$this->show_view_with_menu('frontend/privacy', $data);
	}
	
	public function gratis()
	{
	    $data = array();
	    $this->show_view_with_menu('frontend/gratis', $data);
	}
}