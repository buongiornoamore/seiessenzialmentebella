<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Programmi extends Frontend_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
	}
	
	public function detail($progId = null, $seoText = null)
	{

		$this->db->where('programmi.id_programma', $progId);
		$this->db->join('programmi_traduzioni', 'programmi_traduzioni.id_programma = programmi.id_programma');
		$this->db->where('programmi.stato_programma', 1);
		$this->db->where('programmi_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->from('programmi');
		$query = $this->db->get();
		
		$prog = $query->row();
		
		if (isset($prog) && count($prog) > 0)
		{	
			$data['prog'] = $prog;
			// carica benefici
			$this->db->select('*');
			$this->db->from('programmi_benefici');
			$this->db->join('benefici', 'benefici.id_benefici = programmi_benefici.id_benefici');
			$this->db->join('benefici_traduzioni', 'benefici_traduzioni.id_benefici = benefici.id_benefici');
			$this->db->where('programmi_benefici.id_programma', $progId);
			$this->db->where('benefici_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
			$query_ben = $this->db->get();
			$data['benefici'] = $query_ben->result();
			// carica trattamenti
			$this->db->select('*');
			$this->db->from('programmi_trattamenti');
			$this->db->join('trattamenti', 'trattamenti.id_trattamento = programmi_trattamenti.id_trattamento');
			$this->db->join('trattamenti_traduzioni', 'trattamenti_traduzioni.id_trattamento = trattamenti.id_trattamento');
			$this->db->where('trattamenti.stato_trattamento', 1);
			$this->db->where('trattamenti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
			$this->db->where('programmi_trattamenti.id_programma', $progId);
			$query_tratt = $this->db->get();
			$data['trattamenti'] = $query_tratt->result();
			// carica gallery
			$this->db->select('*');
			$this->db->from('programmi_gallery');
			$this->db->where('id_programma', $progId);
			$query_img = $this->db->get();
			$data['gallery'] = $query_img->result();
			
			$this->show_view_with_menu('frontend/programmi', $data);
			//$this->load->view('frontend/programmi', $data);
		} else {
			redirect('home');
		}	
		
	}
	
}