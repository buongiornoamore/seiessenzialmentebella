<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class comingsoon extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();   
	//	$this->load->database();
	//	$this->load->helper('framework_helper');
	//	$this->load->helper('cookie');     
	//	$this->load->library('form_validation');
	//	$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
	//	$this->load->library('ion_auth');	 
	}
	/**
	 * Il nome del file deve essere in minuscolo comingsoon.php
	 */
	public function index()
	{
		$this->load->view('comingsoon');
	}
	
}

/* End of file Comingsoon.php */
/* Location: ./application/controllers/Comingsoon.php */