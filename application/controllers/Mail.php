<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
		$this->load->library('email');
		$this->lang->load('auth', $this->session->userdata('site_lang'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	public function index() {
	}
	
	public function send() {
		$to_mail = $this->input->post('to_mail', true);
		$mail_type = $this->input->post('mail_type', true);
		$mail_text = $this->input->post('mail_text', true);
		$subject = $this->input->post('subject', true);
		$mail_reply = $this->input->post('mail_reply', true);
		echo $to_mail . ' ' . $mail_type;
	
		$message = '<p>'.$mail_text.'</p>';
		
		// Get full html:
		$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
			<title>' . html_escape($subject) . '</title>
			<style type="text/css">
				body {
					font-family: Arial, Verdana, Helvetica, sans-serif;
					font-size: 16px;
				}
			</style>
		</head>
		<body>
		' . $message . '
		</body>
		</html>';
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);
		
		$result = $this->email
				->from($to_mail, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($to_mail)
				->subject($subject)
				->message($body)
				->send();
		
		var_dump($result);
		echo '<br />';
		echo $this->email->print_debugger();
	}
	
	public function send_save() {

		$name = $this->input->post('name', true);	
		$surname = $this->input->post('surname', true);	
		$city = $this->input->post('city', true);	
		$phone = $this->input->post('phone', true);		
		$email = $this->input->post('email', true);		
	//	$studio = $this->input->post('studio', true);		
		$message_post = $this->input->post('message', true);	
		$provenienza = $this->input->post('provenienza', true);		
	
		// salva sul DB la richiesta contatto
		$data = array(
		   'id_contatto' => NULL,
		   'nome' => $name,
		   'cognome' => $surname,
		   'citta' => $city,
		   'email' => $email,
		   'telefono' => $phone,
		   'messaggio' => $message_post,
		   'provenienza' => $provenienza,
		   'data' => date('Y-m-d H:i:s'),
		   'stato_contatto' => 1,
		   'id_lingua' => lang('LANGUAGE_ID')
		);
		$this->db->insert('contatti_moduli', $data);
		
		// send email CONTATTO_RICVUTO
		$this->send_email_template_to_email("CONTATTO_RICVUTO", $email, 1, $name, true, false);
		
		$this->data['message'] = lang('MSG_SUCCESS_CONTACT');
		$this->data['type'] = 'success';
		echo json_encode($this->data);
	}
	
	/* Registrazione email newsletter frontend */
	public function newsletter() {

		$name = $this->input->post('name', true);			
		$email = $this->input->post('email', true);		
		
		// @TODO controlla se esiste già in newsletter
		$this->db->where('email_contatto', $email);
		$query_exist = $this->db->get('contatti_newsletter');
		if ($query_exist->num_rows() > 0)
		{
			if($query_exist->row()->stato_contatto == 1) {
				// error
				$this->data['message'] = lang('MSG_UNIQUE_NEWSLETTER');
				$this->data['type'] = 'danger';
			} else {
				$data = array(
				   'stato_contatto' => 1,
				   'nome_contatto' => $name,
				   'data_contatto' => date('Y-m-d H:i:s'),
				   'lingua_traduzione_id' => lang('LANGUAGE_ID')
				);	
				$this->db->where('email_contatto', $email);
				$this->db->update('contatti_newsletter', $data); 	
				
				// invio email template NEWSLETTER_BENVENUTO
				$this->send_email_template_to_email("NEWSLETTER_BENVENUTO", $email, 1, $name, true, false);
				
				$this->data['message'] = lang('MSG_SUCCESS_NEWSLETTER');
				$this->data['type'] = 'success';
			}
		}
		else
		{
			// salva sul DB la richiesta contatto
			$data_insert = array(
			   'id_contatto_newsletter' => NULL,
			   'nome_contatto' => $name,
			   'email_contatto' => $email,
			   'data_contatto' => date('Y-m-d H:i:s'),
			   'stato_contatto' => 1,
			   'data_unsubscribe' => NULL,
		  	   'lingua_traduzione_id' => lang('LANGUAGE_ID')
			);
			$this->db->insert('contatti_newsletter', $data_insert);
			
			// send email NEWSLETTER_BENVENUTO
			$this->send_email_template_to_email("NEWSLETTER_BENVENUTO", $email, 1, $name, true, false);
			
			$this->data['message'] = lang('MSG_SUCCESS_NEWSLETTER');
			$this->data['type'] = 'success';
		}
		
		echo json_encode($this->data);
		
	}
	
	/* Invia template email a un utente  */
	public function send_email_template_to_email($template_default_name, $email_to, $modulo, $customer_name, $send_to_company, $show_online_link) {
				
		//$template_id = $this->input->post('template_id', true);				
		//$modulo = $this->input->post('modulo', true); // 0 contatti - 1 newsletter
		
		// template
		$this->db->from('email_templates');
		$this->db->join('tipo_template_default', 'tipo_template_default.id_tipo_template_default = email_templates.id_tipo_template_default');
		$this->db->where('tipo_template_default.nome_tipo_template_default', $template_default_name);
		$this->db->where('email_templates.lingua_traduzione_id', lang('LANGUAGE_ID'));		
		$query = $this->db->get();
		$template = $query->row();
		
		// template trad
		$this->db->from('email_templates_traduzioni');
		$this->db->join('lingue', 'lingue.id_lingue = email_templates_traduzioni.lingua_traduzione_id');
		$this->db->where('email_templates_traduzioni.lingua_traduzione_id', $template->lingua_traduzione_id);
		$query_trad = $this->db->get();
		$template_trad = $query_trad->row();
		
		$mail_reply = COMPANY_EMAIL;
		$subject = $template->nome_template;
		$email_body = $template->testo_template;
		

		// $data sarà il corpo della mail da mettere nel template con le info necessarie
		$data = array(
			'template_name' => $template->nome_template,
			'image_banner' => '',
			'unsubscribe_link' => site_url('unsubscribe/'.$template_trad->codice_ci.'/'.$modulo.'/'.urlencode($email_to)),
			'testo_preheader' => $template_trad->testo_preheader,
			'testo_footer_copyright' => $template_trad->testo_footer_copyright,
			'testo_footer_unsubscribe' => $template_trad->testo_footer_unsubscribe,
			'online_link' => ($show_online_link ? site_url('+/'.$template->id_template) : ''),
			'email_body' => $email_body,
			'customer_name' => $customer_name
		);			
		
		$body = $this->load->view('admin/html_templates/template.php', $data, TRUE);
	
		$this->email->clear(TRUE);
		 
		$this->email
			->from($mail_reply, COMPANY_NAME)
			->reply_to($mail_reply)    // Optional, an account where a human being reads.
			->to($email_to)
			->subject($subject)
			->message($body)
			->send();
		
		// invia anche all'indirizzo COMPANY_EMAIL	
		if($send_to_company) {
			$this->email
			->from($mail_reply, COMPANY_NAME)
			->reply_to($mail_reply)    // Optional, an account where a human being reads.
			->to($mail_reply)
			->subject($subject. ' | ' . $email_to)
			->message($body)
			->send();
		}	
	}
	
	/* Invia template email a una lsita di contatti */
	public function send_email_template($template_id) {
				
		//$template_id = $this->input->post('template_id', true);				
		$contatti_list = json_decode($this->input->post('contatti_list', true));	
		$nomi_list = json_decode($this->input->post('nomi_list', true));
		$modulo = $this->input->post('modulo', true); // 0 contatti - 1 newsletter
		
		// template
		$this->db->from('email_templates');
		$this->db->where('id_template', $template_id);
		$query = $this->db->get();
		$template = $query->row();
		
		// template trad
		$this->db->from('email_templates_traduzioni');
		$this->db->join('lingue', 'lingue.id_lingue = email_templates_traduzioni.lingua_traduzione_id');
		$this->db->where('email_templates_traduzioni.lingua_traduzione_id', $template->lingua_traduzione_id);
		$query_trad = $this->db->get();
		$template_trad = $query_trad->row();
		
		$mail_reply = COMPANY_EMAIL;
		$subject = $template->nome_template;
		$email_body = $template->testo_template;
		
		// bulk newsletter
		for($i = 0; $i < count($contatti_list); ++$i) {		
			// $data sarà il corpo della mail da mettere nel template con le info necessarie
			$data = array(
				'template_name' => $template->nome_template,
				'image_banner' => '',
				'unsubscribe_link' => site_url('unsubscribe/'.$template_trad->codice_ci.'/'.$modulo.'/'.urlencode($contatti_list[$i])),
				'testo_preheader' => $template_trad->testo_preheader,
				'testo_footer_copyright' => $template_trad->testo_footer_copyright,
				'testo_footer_unsubscribe' => $template_trad->testo_footer_unsubscribe,
				'online_link' => site_url('html_online_viewer/'.$template_id),
				'email_body' => $email_body,
			    'customer_name' => $nomi_list[$i]
			);			
			
			$body = $this->load->view('admin/html_templates/template.php', $data, TRUE);
		
			$this->email->clear(TRUE);
			 
			$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($contatti_list[$i])
				->subject($subject)
				->message($body)
				->send();
		}
		
	}
	
	public function send_email_libera() {
		
		$subject = $this->input->post('subject_email', true);		
		$testo_email = $this->input->post('testo_email', true);				
		$contatti_list = json_decode($this->input->post('contatti_list', true));		
		$nomi_list = json_decode($this->input->post('nomi_list', true));
		$lingua_id = json_decode($this->input->post('lingua_id', true));
		
		$mail_reply = COMPANY_EMAIL;
		
		// template trad
		$this->db->from('email_templates_traduzioni');
		$this->db->join('lingue', 'lingue.id_lingue = email_templates_traduzioni.lingua_traduzione_id');
		$this->db->where('email_templates_traduzioni.lingua_traduzione_id', $lingua_id);
		$query_trad = $this->db->get();
		$template_trad = $query_trad->row();
		
		// bulk newsletter
		for($i = 0; $i < count($contatti_list); ++$i) {				
			$data = array(
				'template_name' => 'Email - ',
				'image_banner' => '',
				'unsubscribe_link' => site_url('unsubscribe/'.$template_trad->codice_ci.'/0/'.urlencode($contatti_list[$i])),
				'testo_preheader' => $template_trad->testo_preheader,
				'testo_footer_copyright' => $template_trad->testo_footer_copyright,
				'testo_footer_unsubscribe' => $template_trad->testo_footer_unsubscribe,
				'online_link' => '',
				'email_body' => $testo_email,
				'customer_name' => $nomi_list[$i]
			);			
			
			$body = $this->load->view('admin/html_templates/template.php', $data, TRUE);
					
			$this->email->clear(TRUE);
			 
			$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($contatti_list[$i])
				->subject($subject)
				->message($body)
				->send();
		}		
		//add the header here
    //	header('Content-Type: application/json');
    //	echo json_encode($to_mail);
			
	}
	
	public function html_preview_email_template($template_id)
	{	
		$this->db->from('email_templates');
		$this->db->where('id_template', $template_id);
		$query = $this->db->get();
		$temp = $query->row();
		
		// template trad
		$this->db->from('email_templates_traduzioni');
		$this->db->where('lingua_traduzione_id', $temp->lingua_traduzione_id);
		$query_trad = $this->db->get();
		$template_trad = $query_trad->row();
		
		$data = array(
			'template_name' => $temp->nome_template,
			'image_banner' => '',
			'unsubscribe_link' => 'UNSUBSCRIBE',
			'testo_preheader' => $template_trad->testo_preheader,
			'testo_footer_copyright' => $template_trad->testo_footer_copyright,
			'testo_footer_unsubscribe' => $template_trad->testo_footer_unsubscribe,
			'online_link' => '',
			'email_body' => $temp->testo_template,
			'customer_name' => 'NOME'
		);	
		$this->load->view('admin/html_templates/template', $data);
	}
	
	public function html_online_viewer($template_id) {
		if($template_id > 0)
			$this->html_preview_email_template($template_id);
	}
	
}