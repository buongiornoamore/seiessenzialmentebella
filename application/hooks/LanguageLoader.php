<?
class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');
		
		// set default language from config-php $config['language']
		if(!$ci->session->userdata('site_lang')) {
        	$ci->session->set_userdata('site_lang', $ci->config->item('language'));
		} 
		
		$site_lang = $ci->session->userdata('site_lang');
		
		if ($site_lang) {
            $ci->lang->load('frontend',$ci->session->userdata('site_lang'));
			$ci->config->set_item('language', $ci->session->userdata('site_lang')); 
        } else {
            $ci->lang->load('frontend','english');
        }
    }
}
?>